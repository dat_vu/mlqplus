package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.ModuleFreeText;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class FreetextQuestionAdapter extends ArrayAdapter<ModuleFreeText> {
    private Activity mContext = null;
    private int mLayoutID;
    private int questionIndexStart;
    public static int FREETEXT_IN_MODULE = -1;
    private ViewHolder mViewHolder = null;

    public FreetextQuestionAdapter(Activity mContext, int mLayoutID, List<ModuleFreeText> mFreetexts, int questionIndexStart) {
        super(mContext, mLayoutID, mFreetexts);
        this.mContext = mContext;
        this.mLayoutID = mLayoutID;

        // questionIndexStart=-1 -> freetext in module; else -> in survey
        this.questionIndexStart = questionIndexStart;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();

        if (convertView == null) {
            convertView = lInflater.inflate(mLayoutID, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tvQuestion = (TextView) convertView
                    .findViewById(R.id.tvQuestion);
            mViewHolder.edtAnswer = (EditText) convertView
                    .findViewById(R.id.edtAnswer);
            mViewHolder.llBackground = (LinearLayout) convertView.findViewById(R.id.lnBackground);

			/*
            mViewHolder.edtAnswer.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					//freetext.setAnswer(s.toString());
					getItem(mViewHolder.pos).setAnswer(s.toString());
				}
			});*/

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        if (mViewHolder.textWatcher != null)
            mViewHolder.edtAnswer.removeTextChangedListener(mViewHolder.textWatcher);


        //mViewHolder.edtAnswer.setId(position);


        // Set text size
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tvQuestion, mContext,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.edtAnswer, mContext,
                R.dimen.textMedium);

        final ModuleFreeText freetext = getItem(position);
        if (freetext.getQuestion() != null)
            if (questionIndexStart == FREETEXT_IN_MODULE)
                mViewHolder.tvQuestion.setText(freetext.getQuestion());
            else
                mViewHolder.tvQuestion.setText((questionIndexStart + position) + ". " + freetext.getQuestion());
        else
            mViewHolder.tvQuestion.setVisibility(View.GONE);

        if (freetext.getSuggestionText() != null)
            mViewHolder.edtAnswer.setHint(freetext.getSuggestionText());

        if (freetext.getAnswer() != null)
            mViewHolder.edtAnswer.setText(freetext.getAnswer());
        else
            mViewHolder.edtAnswer.setText("");

        mViewHolder.textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                freetext.setAnswer(s.toString());
            }
        };

        mViewHolder.edtAnswer.addTextChangedListener(mViewHolder.textWatcher);
		
/*		edtAnswer.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus)
				{
					freetext.setAnswer(edtAnswer.getText().toString());
				}
			}
		});*/


        mViewHolder.edtAnswer.clearFocus();

        mViewHolder.llBackground.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                MLQPlusFunctions.hideKeyboard(mContext);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        TextView tvQuestion;
        EditText edtAnswer;
        TextWatcher textWatcher;
        LinearLayout llBackground;

    }

}
