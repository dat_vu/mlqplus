/*
package com.example.datvu.mlqplus.activities;

import android.annotation.SuppressLint;
import android.widget.LinearLayout;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.ModuleQuizResultscreenActivity.ModuleQuizResultListenner;
import com.mlqplus.trustedleader.generator.ModuleGenerator;
import com.mlqplus.trustedleader.model.Module;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_module_content2)
public class ModuleContentscreenActivity2 extends BaseActivity {
    private Module module;
    public static String MODULE = "module";


    @ViewById
    LinearLayout ln_parent;

    @SuppressLint("NewApi")
    @AfterViews
    void init() {
        getScreenSize();

        // Fake Data
        getData();

        // Show data
        showData();
    }

    private void showData() {
        if (module != null)
            ln_parent.addView(new ModuleGenerator().generateView(module, ModuleContentscreenActivity2.this));
        ModuleQuizResultscreenActivity.listenner = moduleQuizResultListenner;
    }

    private void getData() {
        try {
            module = (Module) getIntent().getBundleExtra(MLQPlusConstant.BUNDLE).getSerializable(MODULE);
        } catch (Exception e) {
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    ModuleQuizResultListenner moduleQuizResultListenner = new ModuleQuizResultListenner() {

        @Override
        public void onFinish() {
            // TODO Auto-generated method stub
            ModuleContentscreenActivity2.this.finish();
        }
    };
}
*/
