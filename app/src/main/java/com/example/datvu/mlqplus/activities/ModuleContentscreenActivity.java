/*
package com.example.datvu.mlqplus.activities;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.fragment.FragmentSession_;
import com.mlqplus.trustedleader.model.Module;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.TabIndicatorView.OnClickListenner;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_module_content)
public class ModuleContentscreenActivity extends BaseActivity {

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    TextView tv_title;

    @ViewById
    ImageView img_back;

    @ViewById
    com.mlqplus.trustedleader.view.TabIndicatorView_ tab_indicator;

    @ViewById
    ViewPager viewPager;

    private Module module;

    @SuppressLint("NewApi")
    @AfterViews
    void init() {
        getScreenSize();

        // Creat zoomable layout
        MLQPlusFunctions.makeZoomableView(ModuleContentscreenActivity.this, ln_root, ln_child);

        // Set text size for all view
        setTextSize();

        // Fake Data
        fakeData();

        // Show Data
        showData();
    }

    private void fakeData() {
        String moduleJson = "{\"id\":1,\"moduleName\":\"Foundations\",\"moduleSubname\":\"Foundations\",\"moduleIcon\":\"\",\"description\":\"Description description description description description description description\",\"price\":0.99,\"isUnlocked\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"progress\":75,\"sections\":[{\"id\":1,\"sectionName\":\"What\",\"sectionTitle\":\"What is Leadership?\",\"topBackground\":\"http://i.imgur.com/zSIc5Jy.png?1\",\"activities\":[{\"id\":5,\"activityType\":16,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":{\"title\":\"Select all that apply\",\"questions\":[\"Question 1\",\"Question 2\",\"Question 3\",\"Question 4\"],\"answer\":8},\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":3,\"activityType\":4,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":null,\"separator\":{\"separatorColor\":\"#0000ff\"},\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":1,\"activityType\":1,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":{\"content\":\"Text content content content content content content content content content content content content content.<br/>Text content content content content content content content content content.\",\"textAlignment\":1},\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":4,\"activityType\":8,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":null,\"separator\":null,\"freeText\":{\"question\":\"Question question question question question question question question question question question?\",\"suggestionText\":\"Effective leadership is...\",\"answer\":\"Answer answer\"},\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null}]},{\"id\":2,\"sectionName\":\"Why\",\"sectionTitle\":\"Why is Leadership?\",\"topBackground\":\"http://i.imgur.com/zSIc5Jy.png?1\",\"activities\":[{\"id\":2,\"activityType\":2,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":{\"imageId\":\"ID OF IMAGE\",\"imageUrl\":\"http://i.imgur.com/zSIc5Jy.png?1\"},\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":6,\"activityType\":32,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":{\"title\":\"Think of a great leader, what traits do you believe make them great?\",\"questions\":[\"Honest\",\"Confident\",\"Trustworthy\",\"Flexible\",\"Charismatic\"],\"answers\":[0,1,0,3,4]},\"tapCards\":null,\"quizQuestion\":null},{\"id\":8,\"activityType\":128,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":{\"title\":\"Foundation Quiz\",\"questions\":[{\"question\":\"Question 1\",\"answers\":[\"answer A\",\"answer B\",\"answer C\",\"answer D\"],\"correctAnswer\":1,\"answer\":2},{\"question\":\"Question 2\",\"answers\":[\"answer A\",\"answer B\",\"answer C\",\"answer D\"],\"correctAnswer\":1,\"answer\":2},{\"question\":\"Question 3\",\"answers\":[\"answer A\",\"answer B\",\"answer C\",\"answer D\"],\"correctAnswer\":1,\"answer\":2},{\"question\":\"Question 4\",\"answers\":[\"answer A\",\"answer B\",\"answer C\",\"answer D\"],\"correctAnswer\":1,\"answer\":2},{\"question\":\"Question 5\",\"answers\":[\"answer A\",\"answer B\",\"answer C\",\"answer D\"],\"correctAnswer\":1,\"answer\":2}]}},{\"id\":7,\"activityType\":64,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":[{\"title\":\"Charismatic\",\"backgroundColor\":\"#555555\",\"textColor\":\"#ff0000\",\"tabs\":[{\"activities\":[]},{\"activities\":[]},{\"activities\":[]},{\"activities\":[]}]},{\"title\":\"Servant\",\"backgroundColor\":\"#555555\",\"textColor\":\"#ff0000\",\"tabs\":[{\"activities\":[]},{\"activities\":[]},{\"activities\":[]},{\"activities\":[]}]},{\"title\":\"Bureau\",\"backgroundColor\":\"#555555\",\"textColor\":\"#ff0000\",\"tabs\":[{\"activities\":[]},{\"activities\":[]},{\"activities\":[]},{\"activities\":[]}]},{\"title\":\"Laize-Faire\",\"backgroundColor\":\"#555555\",\"textColor\":\"#ff0000\",\"tabs\":[{\"activities\":[]},{\"activities\":[]},{\"activities\":[]},{\"activities\":[]}]},{\"title\":\"Transactional\",\"backgroundColor\":\"#555555\",\"textColor\":\"#ff0000\",\"tabs\":[{\"activities\":[]},{\"activities\":[]},{\"activities\":[]},{\"activities\":[]}]},{\"title\":\"Transformation\",\"backgroundColor\":\"#555555\",\"textColor\":\"#ff0000\",\"tabs\":[{\"activities\":[]},{\"activities\":[]},{\"activities\":[]},{\"activities\":[]}]}],\"quizQuestion\":null}]},{\"id\":3,\"sectionName\":\"Types\",\"sectionTitle\":\"Leadership Types\",\"topBackground\":\"http://i.imgur.com/zSIc5Jy.png?1\",\"activities\":[{\"id\":7,\"activityType\":64,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":[{\"title\":\"Charismatic\",\"backgroundColor\":\"#555555\",\"textColor\":\"#ff0000\",\"tabs\":[{\"activities\":[{\"id\":1,\"activityType\":1,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":{\"content\":\"<b><font color=\\\"#69b5e4\\\">The Charismatic Leader</font></b>\",\"textAlignment\":0},\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":1,\"activityType\":1,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":{\"content\":\"Text content content content content content content content content content content content content content content content content content content content content content content content content content content content content\",\"textAlignment\":0},\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":2,\"activityType\":2,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":{\"imageId\":\"ID OF IMAGE\",\"imageUrl\":\"http://i.imgur.com/zSIc5Jy.png?1\"},\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null}]},{\"activities\":[{\"id\":1,\"activityType\":1,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":{\"content\":\"<b><font color=\\\"#69b5e4\\\">The Charismatic Leader</font></b>\",\"textAlignment\":0},\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":2,\"activityType\":2,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":{\"imageId\":\"ID OF IMAGE\",\"imageUrl\":\"http://i.imgur.com/zSIc5Jy.png?1\"},\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":1,\"activityType\":1,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":{\"content\":\"Text content content content content content content content content content content content content content content content content content content content content content content content content content content content content\",\"textAlignment\":0},\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null}]},{\"activities\":[{\"id\":1,\"activityType\":1,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":{\"content\":\"<b><font color=\\\"#69b5e4\\\">The Charismatic Leader</font></b>\",\"textAlignment\":0},\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":1,\"activityType\":1,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":{\"content\":\"Text content content content content content content content content content content content content content content content content content content content content content content content content content content content content\",\"textAlignment\":0},\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":2,\"activityType\":2,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":{\"imageId\":\"ID OF IMAGE\",\"imageUrl\":\"http://i.imgur.com/zSIc5Jy.png?1\"},\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null}]},{\"activities\":[{\"id\":1,\"activityType\":1,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":{\"content\":\"<b><font color=\\\"#69b5e4\\\">The Charismatic Leader</font></b>\",\"textAlignment\":0},\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":2,\"activityType\":2,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":null,\"image\":{\"imageId\":\"ID OF IMAGE\",\"imageUrl\":\"http://i.imgur.com/zSIc5Jy.png?1\"},\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null},{\"id\":1,\"activityType\":1,\"isAnswered\":false,\"createdAt\":\"2015-08-04T14:00:00.000Z\",\"updatedAt\":\"2015-08-04T14:00:00.000Z\",\"text\":{\"content\":\"Text content content content content content content content content content content content content content content content content content content content content content content content content content content content content\",\"textAlignment\":0},\"image\":null,\"separator\":null,\"freeText\":null,\"tickBox\":null,\"ratingQuestion\":null,\"tapCards\":null,\"quizQuestion\":null}]}]},{\"title\":\"Servant\",\"backgroundColor\":\"#555555\",\"textColor\":\"#ff0000\",\"tabs\":[{\"activities\":[]},{\"activities\":[]},{\"activities\":[]},{\"activities\":[]}]},{\"title\":\"Bureau\",\"backgroundColor\":\"#555555\",\"textColor\":\"#ff0000\",\"tabs\":[{\"activities\":[]},{\"activities\":[]},{\"activities\":[]},{\"activities\":[]}]}],\"quizQuestion\":null}]},{\"id\":4,\"sectionName\":\"Model\",\"sectionTitle\":\"Leadership Models\",\"topBackground\":\"http://i.imgur.com/zSIc5Jy.png?1\",\"activities\":[]},{\"id\":5,\"sectionName\":\"Quiz\",\"sectionTitle\":\"Quizzes\",\"topBackground\":\"http://i.imgur.com/zSIc5Jy.png?1\",\"activities\":[]}]}";
        module = new Gson().fromJson(moduleJson, Module.class);
    }

    private void showData() {
        tab_indicator.setTabTitle(module);
        tab_indicator.setOnClickListenner(onTabClickListenner);
        tv_title.setText(module.getName());

        FragmentManager fragmentManager = getFragmentManager();
        FragmentPagerAdapter sessionAdapter = new FragmentPagerAdapter(fragmentManager) {

            @Override
            public int getCount() {
                // TODO Auto-generated method stub
                return module.getSections().size();
            }

            @Override
            public Fragment getItem(int pos) {
                // TODO Auto-generated method stub
                FragmentSession_ fragmentSession_ = new FragmentSession_();
                //fragmentSession_.setData(module.getSections().get(pos));
                return fragmentSession_;
            }
        };

        viewPager.setAdapter(sessionAdapter);
        viewPager.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int pos) {
                // TODO Auto-generated method stub
                tab_indicator.selectedTab(pos);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_title, ModuleContentscreenActivity.this, R.dimen.textLarge);
    }

    @Click
    void img_back() {
        ModuleContentscreenActivity.this.onBackPressed();
    }

    OnClickListenner onTabClickListenner = new OnClickListenner() {

        @Override
        public void onClick(int pos) {
            // TODO Auto-generated method stub
            viewPager.setCurrentItem(pos);
        }
    };
}
*/
