package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.ModuleQuestion;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.view_question_style_quiz)
public class QuizQuestionStyleView extends LinearLayout {

	private Context mContext;

	@ViewById
	TextView tv_question, tv_answer0, tv_answer1, tv_answer2, tv_answer3;
	
	@ViewById
	FrameLayout fl_answer0, fl_answer1, fl_answer2, fl_answer3;

	private Integer selectedAnswer;
	private ModuleQuestion moduleQuestion;

	public QuizQuestionStyleView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public QuizQuestionStyleView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public QuizQuestionStyleView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public QuizQuestionStyleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public void setQuestion(ModuleQuestion question, int pos) {
		setTextSize();
		
		this.moduleQuestion=question;
		tv_question.setText((pos+1)+". "+question.getQuestion());
		
		tv_answer0.setText(question.getAnswers().get(0).getAnswer());
		tv_answer1.setText(question.getAnswers().get(1).getAnswer());
		tv_answer2.setText(question.getAnswers().get(2).getAnswer());
		tv_answer3.setText(question.getAnswers().get(3).getAnswer());
		
		selectAnAnswer(question.getAnswer());
	}

	public void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_question, mContext,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_answer0, mContext, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_answer1, mContext, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_answer2, mContext, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_answer3, mContext, R.dimen.textMedium);
	}
	
	@Click
	void fl_answer0()
	{
		selectAnAnswer(moduleQuestion.getAnswers().get(0).getValue());
	}
	
	@Click
	void fl_answer1()
	{
		selectAnAnswer(moduleQuestion.getAnswers().get(1).getValue());
	}
	
	@Click
	void fl_answer2()
	{
		selectAnAnswer(moduleQuestion.getAnswers().get(2).getValue());
	}
	
	@Click
	void fl_answer3()
	{
		selectAnAnswer(moduleQuestion.getAnswers().get(3).getValue());
	}

	public void selectAnAnswer(Integer pos) {
		selectedAnswer = pos;

		// reset all
		fl_answer0.setBackground(getResources().getDrawable(
				R.drawable.tv_dark_bg));
		fl_answer1.setBackground(getResources().getDrawable(
				R.drawable.tv_dark_bg));
		fl_answer2.setBackground(getResources().getDrawable(
				R.drawable.tv_dark_bg));
		fl_answer3.setBackground(getResources().getDrawable(
				R.drawable.tv_dark_bg));
		
		if (selectedAnswer!=null)// else module quiz
		{
			switch (getPosOfAnswer(selectedAnswer)) {
			case 0:
				fl_answer0.setBackground(getResources().getDrawable(
						R.drawable.blue_btn_bg)); break;
			case 1:
				fl_answer1.setBackground(getResources().getDrawable(
						R.drawable.blue_btn_bg)); break;
			case 2:
				fl_answer2.setBackground(getResources().getDrawable(
						R.drawable.blue_btn_bg)); break;
			case 3:
				fl_answer3.setBackground(getResources().getDrawable(
						R.drawable.blue_btn_bg)); break;
			}
			if (moduleQuestion!=null)
				moduleQuestion.setAnswer(selectedAnswer);
		}
	}

	public int getAnswers() {
		return selectedAnswer;
	}
	
	private int getPosOfAnswer(int value)
	{
		for (int i=0; i<moduleQuestion.getAnswers().size(); i++)
			if (value==moduleQuestion.getAnswers().get(i).getValue())
				return i;
		return 0;
	}
}
