/*
package com.example.datvu.mlqplus.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.fragment.FragmentLearn_;
import com.mlqplus.trustedleader.generator.ModuleGenerator;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.Module;
import com.mlqplus.trustedleader.model.ModuleActivities;
import com.mlqplus.trustedleader.model.ModuleActivity;
import com.mlqplus.trustedleader.model.ModuleActivityError;
import com.mlqplus.trustedleader.model.ModuleSection;
import com.mlqplus.trustedleader.model.ModuleTapCard;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

@EActivity(R.layout.activity_downloadmodulescreen)
public class DownloadModuleActivity extends BaseActivity {

    @ViewById
    LinearLayout ln_root;

    @ViewById
    RelativeLayout rl_child;

    @ViewById
    TextView tv_percent, tv_loading;

    @ViewById
    ImageView img_logo;

    private int countLoadedImages;
    private int countAllImages;
    private List<String> imageURLs = new ArrayList<String>();
    private Module module;
    public static String MODULE_ID = "moduleID";
    private static String ConnectionLost = "ConnectionLost";
    public static String TOTAL_ACTIVITIES = "totalModuleActivities";
    private boolean isLoadingImages = true;

    @AfterViews
    void init() {
        // Creat zoomable layout
        MLQPlusFunctions.makeZoomableView(DownloadModuleActivity.this, ln_root,
                rl_child);

        // Set textSize
        setTextSize();

        //startAnimation
        startAnimation();

        loadModule();
        isLoadingImages = false;
    }

    private void startAnimation() {
        final Animation zoomin = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
        final Animation zoomout = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
        zoomin.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                img_logo.startAnimation(zoomout);
            }
        });
        zoomout.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                img_logo.startAnimation(zoomin);
            }
        });

        img_logo.startAnimation(zoomin);
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_percent,
                DownloadModuleActivity.this, R.dimen.textLarge);
        MLQPlusFunctions.SetViewTextSize(tv_loading,
                DownloadModuleActivity.this, R.dimen.textMedium);
    }

    private void loadModule() {
        // Reset activities_error_list
        MLQPlusConstant.ACTIVITIES_ERRORS_LIST = new ArrayList<ModuleActivityError>();

        String moduleID = null;
        try {
            moduleID = getIntent().getBundleExtra(MLQPlusConstant.BUNDLE).getString(MODULE_ID);
        } catch (Exception e) {
        }
        if (moduleID != null) {
            if (!isInternetAvailable())
                back2MainActivityWhenLoadingfailed(ConnectionLost);
            else
                MLQPlusService.getBSSServices().getModulesDetail(MLQPlusFunctions.getAccessToken(), moduleID, new Callback<ApiResponse<Module>>() {
                    //MLQPlusTestService.getBSSServices().getModulesFreetextMultiQuestions(new Callback<ApiResponse<Module>>() {

                    @Override
                    public void success(ApiResponse<Module> mModule, Response arg1) {
                        // TODO Auto-generated method stub
                        DownloadModuleActivity.this.module = mModule.getData();

                        // Report Activities errors
                        if (MLQPlusConstant.ACTIVITIES_ERRORS_LIST != null && MLQPlusConstant.ACTIVITIES_ERRORS_LIST.size() > 0)
                            reportActivityError(0);

                        ModuleGenerator.module = mModule.getData();
                        ModuleGenerator.totalActivities = countPointActivities(mModule.getData());
                        countLoadedImages = 0;
                        countAllImages = 0;

                        // count all image in activity
                        for (int i = 0; i < module.getSections().size(); i++) {
                            ModuleSection section = module.getSections().get(i);
                            countAllImages(section.getActivities());
                        }

                        // load all image in activity
                        for (int i = 0; i < module.getSections().size(); i++) {
                            ModuleSection section = module.getSections().get(i);
                            loadImageActivity(section.getActivities());
                        }

                        // Check if there is no Image
                        if (countAllImages == 0)
                            navigateToModuleContent();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        // TODO Auto-generated method stub
                        if (error.getCause() instanceof SocketTimeoutException)
                            back2MainActivityWhenLoadingfailed(MLQPlusConstant.TIMEOUT);
                        else
                            back2MainActivityWhenLoadingfailed(error.getMessage());

                    }
                });
                */
/*MLQPlusTestService.getBSSServices().getModulesDetail(new Callback<ApiResponse<Module>>() {
					
					@Override
					public void success(ApiResponse<Module> mModule, Response arg1) {
						// TODO Auto-generated method stub
						DownloadModuleActivity.this.module=mModule.getData();
						
						// Report Activities errors
						if (MLQPlusConstant.ACTIVITIES_ERRORS_LIST!=null && MLQPlusConstant.ACTIVITIES_ERRORS_LIST.size()>0)
							reportActivityError(0);
							
						ModuleGenerator.module=mModule.getData();
						ModuleGenerator.totalActivities=countPointActivities(mModule.getData());
						countLoadedImages=0;
						countAllImages=0;
						
						// count all image in activity
						for (int i=0; i<module.getSections().size(); i++)
						{
							ModuleSection section=module.getSections().get(i);
							countAllImages(section.getActivities());
						}
						
						// load all image in activity
						for (int i=0; i<module.getSections().size(); i++)
						{
							ModuleSection section=module.getSections().get(i);
							loadImageActivity(section.getActivities());
						}
						
						// Check if there is no Image
						if (countAllImages==0)
							navigateToModuleContent();
					}
					
					@Override
					public void failure(RetrofitError error) {
						// TODO Auto-generated method stub
						back2MainActivityWhenLoadingfailed(error.getMessage());
					}
				});*//*

        }

    }

    private void reportActivityError(final int pos) {
        if (MLQPlusConstant.ACTIVITIES_ERRORS_LIST != null && MLQPlusConstant.ACTIVITIES_ERRORS_LIST.size() > pos) {
            final String errorJson = new Gson().toJson(MLQPlusConstant.ACTIVITIES_ERRORS_LIST.get(pos));

            MLQPlusService.getBSSServices().reportActivityError(MLQPlusFunctions.getAccessToken(), new TypedString(errorJson), new Callback<ApiResponse<ModuleActivityError>>() {

                @Override
                public void success(ApiResponse<ModuleActivityError> arg0, Response arg1) {
                    // TODO Auto-generated method stub
                    reportActivityError(pos + 1);
                }

                @Override
                public void failure(RetrofitError arg0) {
                    // TODO Auto-generated method stub
                    reportActivityError(pos + 1);
                }
            });
        }
    }

    private void back2MainActivityWhenLoadingfailed(String error) {
        if (error != null && error.contains(ConnectionLost))
            showToast("Please check your internet connection and try again");
        else
            showToast("Loading module failed");

        Bundle bundle = new Bundle();
        bundle.putString(MainActivity_.DATA, FragmentLearn_.class.getName());
        Intent iLearn = new Intent(DownloadModuleActivity.this, MainActivity_.class);
        iLearn.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        iLearn.putExtra(MLQPlusConstant.BUNDLE, bundle);
        startActivity(iLearn);
    }

    */
/*
     * Load all counted images
     *//*

    private void loadImageActivity(ArrayList<ModuleActivity> activities) {
        for (String imgURL : imageURLs)
            loadImage(imgURL);
    }

    */
/*
     * Count all images in ModuleContainscreenActivity2
     *//*

    private void countAllImages(ArrayList<ModuleActivity> activities) {
        if (activities != null) {
            for (int j = 0; j < activities.size(); j++) {
                ModuleActivity activity = activities.get(j);
                if (activity != null)
                    // is module image
                    if (activity.getImage() != null) {
                        if (!isContain(imageURLs, activity.getImage().getImageUrl())) {
                            countAllImages++;
                            imageURLs.add(activity.getImage().getImageUrl());
                        }
                    }
                    // is module tab card
                    else if (activity.getTapCards() != null && activity.getTapCards().size() > 0) {
                        List<ModuleTapCard> tapcards = activity.getTapCards();
                        for (int k = 0; k < tapcards.size(); k++) {
                            ArrayList<ModuleActivities> listModuleActivities = tapcards.get(k).getTabs();
                            for (int l = 0; l < listModuleActivities.size(); l++)
                                countAllImages(listModuleActivities.get(l).getActivities());
                        }

                    }
            }
        }
    }

    private boolean isContain(List<String> list, String text) {
        if (list != null)
            for (String value : list)
                if (value.equals(text))
                    return true;
        return false;
    }

    private void loadImage(String imageUrl) {
        MLQPlusFunctions.loadImage(DownloadModuleActivity.this, imageUrl, new ImageLoadingListener() {

            @Override
            public void onLoadingFailed(String imageUri, View view,
                                        FailReason failReason) {
                // TODO Auto-generated method stub
                if (!isInternetAvailable())
                    back2MainActivityWhenLoadingfailed("");
                else
                    checkDismissLoadingDialog();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // TODO Auto-generated method stub
                checkDismissLoadingDialog();
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                // TODO Auto-generated method stub
                checkDismissLoadingDialog();
            }

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                // TODO Auto-generated method stub

            }
        });
    }


    private void checkDismissLoadingDialog() {
        countLoadedImages++;
        int percent = (int) (countLoadedImages * 100f / countAllImages);
        tv_percent.setText(percent + "%");

        if (!isLoadingImages && countLoadedImages == countAllImages)
            navigateToModuleContent();

    }


    private void navigateToModuleContent() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(ModuleContentscreenActivity2.MODULE, module);
        startNewActivityNoAnimation(DownloadModuleActivity.this, ModuleContentscreenActivity2_.class, true, bundle);
    }

    private int countPointActivities(Module module) {
        int total = 0;
        if (module != null && module.getSections() != null)
            for (ModuleSection section : module.getSections())
                if (section.getActivities() != null)
                    for (ModuleActivity activity : section.getActivities())
                        // Only check complete with Freetext ("8"); TickBox ("16"); RatingQuestion ("32"); QuizQuestion ("128")
                        if (activity != null)
                            if (activity.getActivityType().equals("8") ||
                                    activity.getActivityType().equals("16") ||
                                    activity.getActivityType().equals("32") ||
                                    activity.getActivityType().equals("128"))
                                total++;
        return total;
    }

}
*/
