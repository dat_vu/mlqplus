/*
package com.example.datvu.mlqplus.activities;


import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.adapters.TermAdapter;
import com.example.datvu.mlqplus.models.Term;
import com.example.datvu.mlqplus.models.TermAndPolicy;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.example.datvu.mlqplus.views.WrapcontentListview;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_aboutscreen)
public class AboutActivity2 extends BaseActivity {

    private TermAdapter mTermAdapter = null;
    private TermAdapter mPolicyAdapter = null;

    private TermAndPolicy mTermAndPolicy = null;

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    ImageView img_back;

    @ViewById
    TextView tv_title, tv_terms_of_service, tv_privacy_policy;

    @ViewById
    WrapcontentListview lv_term, lv_policy;

    @AfterViews
    void init() {
        // Set text size for all view
        setTextSize();

        // fake data
        fakeData();

        // remove border of listview
        lv_term.setDivider(null);
        lv_policy.setDivider(null);

        // set adapter for lv_term
        mTermAdapter = new TermAdapter(AboutActivity2.this,
                R.layout.item_term_and_policy, mTermAndPolicy.getTerm());
        lv_term.setAdapter(mTermAdapter);

        // set adapter for lv_policy
        mPolicyAdapter = new TermAdapter(AboutActivity2.this,
                R.layout.item_term_and_policy, mTermAndPolicy.getPolicy());
        lv_policy.setAdapter(mPolicyAdapter);
    }

    */
/*
     * create fake data to test two list view
     *//*

    private void fakeData() {
        List<Term> terms = new ArrayList<Term>();
        List<Term> policies = new ArrayList<Term>();

        for (int i = 0; i < 3; i++)
            terms.add(new Term(
                    "Name " + i,
                    "content content content content content content content content content content content content content content content content content "));

        for (int i = 0; i < 4; i++)
            policies.add(new Term(
                    "Name " + i,
                    "content content content content content content content content content content content content content content content content content "));

        mTermAndPolicy = new TermAndPolicy(terms, policies);
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_title, AboutActivity2.this,
                R.dimen.textLarge);
        MLQPlusFunctions.SetViewTextSize(tv_terms_of_service,
                AboutActivity2.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_privacy_policy, AboutActivity2.this,
                R.dimen.textMediumLarge);
    }

    @Click
    void ln_back() {
        onBackPressed();
    }

}
*/
