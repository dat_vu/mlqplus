package com.example.datvu.mlqplus.services.commoninfo;

import com.example.datvu.mlqplus.models.Country;
import com.example.datvu.mlqplus.models.RestResponse;

import java.util.List;

import bolts.Task;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by solit_000 on 4/14/2016.
 */
public class CommonInfoService implements CommonInfoServiceInterface {

    CommonInfoRestServiceInterface restServiceInterface;

    public CommonInfoService(CommonInfoRestServiceInterface restServiceInterface) {
        this.restServiceInterface = restServiceInterface;
    }

    @Override
    public Task<List<Country>> getCountryList() {

        Task<List<Country>> result = null;

        try {
            Call<RestResponse<List<Country>>> call = restServiceInterface.getCountryList();
            Response<RestResponse<List<Country>>> response = call.execute();

            if (response.isSuccessful()) {
                RestResponse<List<Country>> responseObject = response.body();
                result = Task.forResult(responseObject.getData());
            } else {
                result = Task.forError(new Exception("Http error"));
            }
        } catch (Exception e) {
            result = Task.forError(e);
        }


        return result;

    }
}
