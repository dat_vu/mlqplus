package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class QuizAnswer implements Serializable{
	@SerializedName("answer")
	private String answer;
	
	@SerializedName("value")
	private Integer value;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
}
