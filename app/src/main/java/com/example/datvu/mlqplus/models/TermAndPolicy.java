package com.example.datvu.mlqplus.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TermAndPolicy {
	@SerializedName("term")
	private List<Term> term;

	@SerializedName("policy")
	private List<Term> policy;

	public List<Term> getTerm() {
		return term;
	}

	public void setTerm(List<Term> term) {
		this.term = term;
	}

	public List<Term> getPolicy() {
		return policy;
	}

	public void setPolicy(List<Term> policy) {
		this.policy = policy;
	}

	public TermAndPolicy(List<Term> term, List<Term> policy) {
		super();
		this.term = term;
		this.policy = policy;
	}
	
}
