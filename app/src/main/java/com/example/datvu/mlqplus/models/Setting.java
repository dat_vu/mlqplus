package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class Setting extends ObjectBase{
	@SerializedName("title")
	private String name;

	@SerializedName("text")
	private String description;

	@SerializedName("icon")
	private String icon;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Setting(String name, String description, String icon) {
		super();
		this.name = name;
		this.description = description;
		this.icon = icon;
	}

	public Setting() {
		super();

	}

}
