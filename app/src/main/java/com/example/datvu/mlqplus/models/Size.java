package com.example.datvu.mlqplus.models;


import com.example.datvu.mlqplus.R;

public enum Size {
    XXSMALL(R.dimen.textXXSmall),
    XSMALL(R.dimen.textXSmall),
    SMALL(R.dimen.textSmall),
    MEDIUM(R.dimen.textMedium),
    MEDIUMLARGE(R.dimen.textMediumLarge),
    LARGE(R.dimen.textLarge),
    XLARGE(R.dimen.textXLarge),
    XXLARGE(R.dimen.textXXLarge),;

    private final int size;

    private Size(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
