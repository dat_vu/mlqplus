package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class SocialSignupRequest extends LoginRequestBase implements Serializable {
	@SerializedName("access_token")
	private String access_token;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public SocialSignupRequest(String access_token) {
		super();
		this.access_token = access_token;
	}
	
}
