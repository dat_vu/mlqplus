package com.example.datvu.mlqplus.activities;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.fragments.FragmentHome_;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_homescreen)
public class HomescreenActivity extends BaseActivity {
    private ActionBarDrawerToggle mDrawerToggle;

    @ViewById
    at.grabner.circleprogress.CircleProgressView ccv_progress;

    @ViewById
    TextView tv_percent, tv_progress, tv_user, tv_setting_name, tv_rate_name,
            tv_about_name, tv_entercode_name, tv_logout_name;

    @ViewById
    LinearLayout ln_setting_bg, ln_rate_bg, ln_about_bg, ln_entercode_bg,
            ln_logout_bg, ln_left_drawer;

    @ViewById(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @ViewById(R.id.content_frame)
    FrameLayout content_frame;

    @ViewById
    ImageView img_logo_bottom;

    private int retryCount;

    @AfterViews
    void init() {
        //loadSettingData();
        showMainFragment();
        initDrawerMenu();

        // Check and complete purchasing Module (user completed purchasing on
        // Google Play Store but app cannot update that infor to our server ->
        // maybe because of internet connection
        //completePurchasingModule();

        // Init circle progress
        MLQPlusFunctions.showCircleProgress(
                getResources().getColor(R.color.blue_light_circle_progress),
                this, ccv_progress, 0,
                getResources().getInteger(R.integer.card_circle_progress_size));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTextSize();
    }

    ;

    /*private void completePurchasingModule() {
        String info = getSharedPreferencesService().purchasingModule().get();
        if (info.length() > 0) {
            String[] infos = info.split("@@@");
            if (infos.length == 2 && infos[0] != null && infos[1] != null) {
                retryCount = 0;
                if (infos[1].equals(MLQPlusConstant.FULL_MODULES))
                    purchaseFullModulesOnOurServer(infos[0], infos[1]);
                else
                    purchaseModuleOnOurServer(infos[0], infos[1]);
            }
        }
    }*/

    /*private void purchaseFullModulesOnOurServer(final String json,
                                                final String moduleID) {
        MLQPlusService.getBSSServices().purchaseFullModule(
                MLQPlusFunctions.getAccessToken(), new TypedString(json),
                new Callback<ApiResponse<Response>>() {

                    @Override
                    public void success(ApiResponse<Response> arg0,
                                        Response arg1) {
                        // TODO Auto-generated method stub
                        dismissWaitingDialog();

                        // Remove purchasingModule in sharePreferencesService
                        getSharedPreferencesService().purchasingModule()
                                .put("");

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        // TODO Auto-generated method stub
                        retryCount++;
                        if (retryCount <= 3)
                            purchaseFullModulesOnOurServer(json, moduleID);

                    }
                });
    }*/

   /* private void purchaseModuleOnOurServer(final String json,
                                           final String moduleID) {
        MLQPlusService.getBSSServices().purchaseAModule(
                MLQPlusFunctions.getAccessToken(), new TypedString(json),
                new Callback<ApiResponse<Response>>() {

                    @Override
                    public void success(ApiResponse<Response> arg0,
                                        Response arg1) {
                        dismissWaitingDialog();

                        // Remove purchasingModule in sharePreferencesService
                        getSharedPreferencesService().purchasingModule()
                                .put("");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        retryCount++;
                        if (retryCount <= 3)
                            purchaseModuleOnOurServer(json, moduleID);

                    }
                });
    }*/

    /*private void loadSettingData() {
        if (!MLQPlusFunctions.isInternetAvailable())
            showToast(MLQPlusConstant.CONNECTION_LOST);
        else {
            MLQPlusService.getBSSServices().getSettings(
                    MLQPlusFunctions.getAccessToken(),
                    new Callback<ApiResponse<SettingData>>() {

                        @Override
                        public void success(
                                ApiResponse<SettingData> settingData,
                                Response arg1) {
                            // TODO Auto-generated method stub
                            if (settingData != null
                                    && settingData.getData() != null) {
                                MLQPlusConstant.SETTING_DATA = settingData
                                        .getData();
                                MLQPlusConstant.TEXTSIZE = MLQPlusFunctions
                                        .getSizefromText(settingData.getData()
                                                .getFont());
                                getSharedPreferencesService().textSize().put(
                                        MLQPlusConstant.TEXTSIZE
                                                .getSizeInString());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            // TODO Auto-generated method stub
                        }
                    });
        }
    }*/

    private void initDrawerMenu() {
        mDrawerLayout.setDrawerShadow(null, GravityCompat.START);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_launcher, R.string.app_name, R.string.hello_world) {

            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (ln_left_drawer.getWidth() * slideOffset);
                content_frame.setTranslationX(moveFactor);
            }

            public void onDrawerClosed(View view) {

            }

            public void onDrawerOpened(View drawerView) {

            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        setTextSize();
    }

    private void setTextSize() {
        try {
            MLQPlusFunctions.SetViewTextSize(tv_setting_name,
                    HomescreenActivity.this, R.dimen.textMedium);
            MLQPlusFunctions.SetViewTextSize(tv_rate_name,
                    HomescreenActivity.this, R.dimen.textMedium);
            MLQPlusFunctions.SetViewTextSize(tv_about_name,
                    HomescreenActivity.this, R.dimen.textMedium);
            MLQPlusFunctions.SetViewTextSize(tv_entercode_name,
                    HomescreenActivity.this, R.dimen.textMedium);
            MLQPlusFunctions.SetViewTextSize(tv_logout_name,
                    HomescreenActivity.this, R.dimen.textMedium);
            MLQPlusFunctions.SetViewTextSize(tv_user, HomescreenActivity.this,
                    R.dimen.textMediumLarge);
        } catch (Exception e) {
        }

    }

    public void showMenu() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    private void showMainFragment() {
        Fragment fragment = new FragmentHome_();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment).commit();

    }

    public void setMenuData(int progress) {
        if (MLQPlusConstant.USERINFO != null
                && MLQPlusConstant.USERINFO.getUser() != null
                && MLQPlusConstant.USERINFO.getUser().getFirst_name() != null
                && tv_user != null)
            tv_user.setText(MLQPlusConstant.USERINFO.getUser().getFirst_name());
        tv_percent.setText(progress + "%");

        ccv_progress.setValue(progress);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (mDrawerToggle == null)

            mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /*@Click
    void ln_setting_bg() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(MainActivity.DATA,
                FragmentSettings_.class.getName());
        startNewActivityNoAnimation(HomescreenActivity.this,
                MainActivity_.class, false, bundle);

        // Hide menu
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }, 500);

    }*/

    /*@Click
    void fl_progress() {
        // Prevent fragment Progress from moving to Quiz progress
       // FragmentQuizzes.isStartFromFunScreen = true;

        Bundle bundle = new Bundle();
        bundle.putSerializable(MainActivity.DATA,
                FragmentProgress_.class.getName());
        startNewActivityNoAnimation(HomescreenActivity.this,
                MainActivity_.class, false, bundle);

        // Hide menu
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }, 500);

    }*/

    /*@Click
    void ln_rate_bg() {
        startNewZoomActivity(HomescreenActivity.this,
                RatescreenActivity_.class, false);

        // Hide menu
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }, 200);
    }*/

    /*@Click
    void ln_about_bg() {
        startNewActivityNoAnimation(HomescreenActivity.this,
                AboutActivity_.class, false);

        // Hide menu
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }, 500);

    }*/

    /*@Click
    void ln_entercode_bg() {
        startNewZoomActivity(HomescreenActivity.this,
                EnterCodescreenActivity_.class, false);

        // Hide menu
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }, 200);
    }*/

    @Click
    void ln_logout_bg() {
        //logoutClicked();

        // Hide menu
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }, 200);
    }

    @Click
    void img_logo_bottom() {
        String url = "http://mlqplus.com.au";
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);

        // Hide menu
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }, 500);
    }

    /*private void logoutClicked() {
        // Show confirmation dialog
        AlertDialog.Builder dlgLogoutConfirmation = new AlertDialog.Builder(
                HomescreenActivity.this);
        dlgLogoutConfirmation.setTitle("Confirmation");
        dlgLogoutConfirmation.setMessage("Are you sure you want to log out?");
        dlgLogoutConfirmation.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        if (!MLQPlusFunctions.isInternetAvailable())
                            showToast(MLQPlusConstant.CONNECTION_LOST);
                        else {
                            showWaitingDialog(HomescreenActivity.this, "",
                                    "Please wait...");
                            // Call API logout
                            String json = "{\"device_token\": \""
                                    + getSharedPreferencesService().deviceID()
                                    .get() + "\"}";
                            MLQPlusService
                                    .getBSSServices()
                                    .logout(MLQPlusFunctions.getAccessToken(),
                                            new TypedString(json),
                                            new Callback<ApiResponse<SignupResponse>>() {

                                                @Override
                                                public void failure(
                                                        RetrofitError arg0) {
                                                    // TODO Auto-generated
                                                    // method stub

                                                }

                                                @Override
                                                public void success(
                                                        ApiResponse<SignupResponse> arg0,
                                                        Response arg1) {
                                                    getSharedPreferencesService()
                                                            .lastSectionOpenModules()
                                                            .put("");
                                                }
                                            });

                            // Update data and load Welcomescreen
                            MLQPlusConstant.USERINFO = null;
                            // Reset Module info
                            MLQPlusConstant.CORE_MODULES = null;
                            MLQPlusConstant.CONFIG_MENU = null;
                            MLQPlusConstant.sFullModule = null;

                            getSharedPreferencesService().accessToken().put("");
                            getSharedPreferencesService().isNormalAccount()
                                    .put(false);
                            loadWelcomeScreenData();
                        }

                    }
                });
        dlgLogoutConfirmation.setNegativeButton("No", null);

        dlgLogoutConfirmation.create().show();

    }*/

    // ---------- load welcome screen ---------------
    /*private void loadWelcomeScreenData() {
        if (!isInternetAvailable())
            showToast("Please check your internet connection and try again");
        else {
            MLQPlusService.getBSSServices().getWelcomeScreenInfo(
                    new Callback<ApiResponse<List<WelcomeScreenInfo>>>() {

                        @Override
                        public void success(
                                ApiResponse<List<WelcomeScreenInfo>> mLWelcomeScreen,
                                Response response) {
                            dismissWaitingDialog();
                            if (mLWelcomeScreen != null
                                    && mLWelcomeScreen.getData() != null) {
                                ArrayList<WelcomeScreenInfo> mWelcomeScreenInfos = new ArrayList<WelcomeScreenInfo>();
                                mWelcomeScreenInfos.addAll(mLWelcomeScreen
                                        .getData());

                                Bundle bundle = new Bundle();
                                bundle.putSerializable(
                                        WelcomescreenActivity_.WELCOME_SCREEN_INFO,
                                        mWelcomeScreenInfos);

                                // Start welcome screen
                                startNewActivityNoAnimation(
                                        HomescreenActivity.this,
                                        WelcomescreenActivity_.class, true,
                                        bundle);
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            // TODO Auto-generated
                            dismissWaitingDialog();
                            if (error.getCause() instanceof SocketTimeoutException)
                                showToast(MLQPlusConstant.TIMEOUT);
                            else if (!MLQPlusFunctions.isInternetAvailable()) {
                                showToast(MLQPlusConstant.CONNECTION_LOST);
                                finish();
                            } else
                                showToast(error.getMessage());
                        }
                    });
        }
    }*/

    // ---------- load welcome screen ----------------
}
