package com.example.datvu.mlqplus.views;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.example.datvu.mlqplus.R;

import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

/**
* We use @SuppressWarning here because our java code
* generator doesn't know that there is no need
* to import OnXXXListeners from View as we already
* are in a View.
* 
*/
@SuppressWarnings("unused")
public final class MyTabPostionView
 extends TabPostionView
 implements HasViews, OnViewChangedListener
{

 private boolean alreadyInflated_ = false;
 private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

 public MyTabPostionView(Context context) {
     super(context);
     init_();
 }

 public MyTabPostionView(Context context, AttributeSet attrs) {
     super(context, attrs);
     init_();
 }

 public MyTabPostionView(Context context, AttributeSet attrs, int defStyleAttr) {
     super(context, attrs, defStyleAttr);
     init_();
 }

 public MyTabPostionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
     super(context, attrs, defStyleAttr, defStyleRes);
     init_();
 }

 public static TabPostionView build(Context context) {
     TabPostionView_ instance = new TabPostionView_(context);
     instance.onFinishInflate();
     return instance;
 }

 /**
  * The mAlreadyInflated_ hack is needed because of an Android bug
  * which leads to infinite calls of onFinishInflate()
  * when inflating a layout with a parent and using
  * the <merge /> tag.
  * 
  */
 @Override
 public void onFinishInflate() {
     if (!alreadyInflated_) {
         alreadyInflated_ = true;
         inflate(getContext(), R.layout.view_tab_position, this);
         onViewChangedNotifier_.notifyViewChanged(this);
     }
     super.onFinishInflate();
 }

 private void init_() {
     OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(onViewChangedNotifier_);
     OnViewChangedNotifier.registerOnViewChangedListener(this);
     OnViewChangedNotifier.replaceNotifier(previousNotifier);
 }

 public static TabPostionView build(Context context, AttributeSet attrs) {
     TabPostionView_ instance = new TabPostionView_(context, attrs);
     instance.onFinishInflate();
     return instance;
 }

 public static TabPostionView build(Context context, AttributeSet attrs, int defStyleAttr) {
     TabPostionView_ instance = new TabPostionView_(context, attrs, defStyleAttr);
     instance.onFinishInflate();
     return instance;
 }

 public static TabPostionView build(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
     TabPostionView_ instance = new TabPostionView_(context, attrs, defStyleAttr, defStyleRes);
     instance.onFinishInflate();
     return instance;
 }

 @Override
 public void onViewChanged(HasViews hasViews) {
     ln_parent = ((LinearLayout) hasViews.findViewById(R.id.ln_parent));
 }

}

