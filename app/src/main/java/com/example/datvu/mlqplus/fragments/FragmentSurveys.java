/*
package com.example.datvu.mlqplus.fragments;

import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.activity.SurveyActivity;
import com.mlqplus.trustedleader.adapter.SurveyAdapter;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.Size;
import com.mlqplus.trustedleader.model.Survey;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;
import com.mlqplus.trustedleader.activity.SurveyActivity_;
import com.mlqplus.trustedleader.fragment.FragmentSurveys_;

@EFragment(R.layout.fragment_surveys)
public class FragmentSurveys extends FragmentBase {
	public static FragmentSurveys newInstance() {
		return new FragmentSurveys_();
	} 
    
	@ViewById
	LinearLayout ln_child; 
 
	@ViewById
	TextView tv_title;  

	@ViewById
	ListView lv_surveys;
	
	@ViewById
	WaitingView vWaiting;

	private static List<Survey> mLSurveys = null;
	private SurveyAdapter mSurveyAdapter = null;
	private boolean isLoading = false;
	private int getDataFailedCount;
	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();
		getDataFailedCount=0;

		// Set text size for all view
		setTextSize();

		// Get Data
		getData();

	}

	private void getData() {
		if (!isLoading) {
			if (mLSurveys!=null)
				showData();
			
			if (!MLQPlusFunctions.isInternetAvailable())
				((BaseActivity)mActivity).showToast(MLQPlusConstant.CONNECTION_LOST);
			else
			{
				if (mLSurveys==null && getDataFailedCount==0)
					vWaiting.showWaitingView();
				isLoading=true;
				MLQPlusService.getBSSServices().getMySurvey(
						MLQPlusFunctions.getAccessToken(),
						new Callback<ApiResponse<List<Survey>>>() {
	
							@Override
							public void success(ApiResponse<List<Survey>> surveys,
									Response arg1) {
								// TODO Auto-generated method stub
								isLoading=false;
								
								if (surveys!=null && surveys.getData()!=null && surveys.getData().size()>0)
								{
									vWaiting.hideWaitingView();
									mLSurveys=surveys.getData();
									showData();
								}
								else
								{
									// Retry getting data
									getDataFailedCount++;
									if (getDataFailedCount<3)
										getData();
									else
										vWaiting.hideWaitingView();
								}
							}
	
							@Override
							public void failure(RetrofitError error) {
								// TODO Auto-generated method stub
								isLoading=false;
								
								// Retry getting data
								getDataFailedCount++;
								if (getDataFailedCount<3)
									getData();
								else
								{
									vWaiting.hideWaitingView();
									((BaseActivity)mActivity).showToast("Cannot load surveys. Error: "+error.getMessage());
								}
							}
						});
			}
		}
	}

	private void showData() {
		mSurveyAdapter = new SurveyAdapter(mActivity, R.layout.item_survey,
				mLSurveys);
		lv_surveys.setAdapter(mSurveyAdapter);
		lv_surveys.setSelector(R.drawable.btn_first_quiz);
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}

	@ItemClick
	void lv_surveys(int position) {
		Survey survey = (Survey) lv_surveys.getItemAtPosition(position);
		Bundle bundle=new Bundle();
		bundle.putString(SurveyActivity.SURVEY_ID, survey.getId());
		((BaseActivity)mActivity).startNewActivityNoAnimation(mActivity, SurveyActivity_.class, false, bundle);
	}
}
*/
