/*
package com.example.datvu.mlqplus.activities;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.adapter.ModuleStoredapter;
import com.mlqplus.trustedleader.billing.utils.Purchase;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.Module;
import com.mlqplus.trustedleader.model.PurchaseModuleEvent;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

@EActivity(R.layout.activity_modulestorescreen)
public class ModuleStorescreenActivity extends InAppBillingBaseActivity {

    @ViewById
    LinearLayout ln_root;

    @ViewById
    LinearLayout ln_child;

    @ViewById
    TextView tv_trusted_program, tv_access_single_module, tv_or, tv_name,
            tv_description, tv_description2, tv_price;

    @ViewById
    ListView lv_single_module;

    @ViewById
    ImageView img_purchased, img_icon;

    private ArrayList<Module> modules;
    private Module fullProgram;
    public static String MODULES = "modules";
    private EventBus eventBus;
    private boolean isPurchaseFull;
    private int retryCount = 0;

    @AfterViews
    void init() {
        // Register event Bus
        try {
            eventBus = EventBus.getDefault();
            eventBus.register(this);
        } catch (Exception e) {
        }

        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Make dialog not dim
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        // Get Data
        getData();

        // Show Data
        showData();
    }

    public void onEvent(PurchaseModuleEvent event) {
        if (modules != null) {
            for (Module module : modules)
                if (module.getId().equals(event.getModuleID()))
                    module.setUnlocked(true);

            isPurchaseFull = isPurchasedFull();
            fullProgram.setUnlocked(isPurchaseFull);

            showData();
        }
    }

    private void getData() {
        isPurchaseFull = true;
        try {
            modules = (ArrayList<Module>) getIntent().getBundleExtra(
                    MLQPlusConstant.BUNDLE).get(MODULES);

            // Check if user purchased full module
            isPurchaseFull = isPurchasedFull();

            fullProgram = new Module("Full Program", "", isPurchaseFull, false,
                    2.99, "");
        } catch (Exception e) {
        }
    }

    private boolean isPurchasedFull() {
        if (modules != null)
            for (int i = 0; i < 5 && i < modules.size(); i++)
                if (!modules.get(i).isUnlocked())
                    return false;
        return true;
    }

    private void showData() {
        ModuleStoredapter adapter = new ModuleStoredapter(
                ModuleStorescreenActivity.this, 1, modules);
        lv_single_module.setAdapter(adapter);
        lv_single_module.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (modules.get(position).getPublished().equals("1")) {
                    if (!modules.get(position).isUnlocked()) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(
                                ModuleInformationscreenActivity.LIST_MODULES,
                                modules);
                        bundle.putSerializable(
                                ModuleInformationscreenActivity.FULL_PROGRAM,
                                fullProgram);
                        bundle.putInt(
                                ModuleInformationscreenActivity.PAGE_START,
                                position);
                        startNewZoomActivity(ModuleStorescreenActivity.this,
                                ModuleInformationscreenActivity_.class, false,
                                bundle);
                    }
                } else {
                    showToast("This module hasn't been published");
                }

            }
        });

        // show Full Program
        showFullProgram();
    }

    private void showFullProgram() {
        tv_name.setText(fullProgram.getName());
        tv_price.setText("$" + fullProgram.getPrice());

        img_icon.setColorFilter(getResources().getColor(
                R.color.module_icon_store));

        if (fullProgram.isUnlocked()) {
            tv_price.setVisibility(View.GONE);
            img_purchased.setVisibility(View.VISIBLE);
        } else {
            tv_price.setVisibility(View.VISIBLE);
            img_purchased.setVisibility(View.GONE);
        }

        tv_description.setVisibility(View.VISIBLE);
        tv_description.setText(getResources().getString(
                R.string.access_all_5_modules));
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_trusted_program,
                ModuleStorescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_access_single_module,
                ModuleStorescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_or, ModuleStorescreenActivity.this,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_name,
                ModuleStorescreenActivity.this, R.dimen.module_name);
        MLQPlusFunctions.SetViewTextSize(tv_description,
                ModuleStorescreenActivity.this, R.dimen.module_des);
        MLQPlusFunctions.SetViewTextSize(tv_description2,
                ModuleStorescreenActivity.this, R.dimen.module_des);
        MLQPlusFunctions.SetViewTextSize(tv_price,
                ModuleStorescreenActivity.this, R.dimen.fragment_module_information_module_price);

    }

    @Click
    void ln_close() {
        this.finish();
    }

    @Click
    void tv_price() {
        if (!isPurchaseFull) {
            if (!MLQPlusFunctions.isInternetAvailable())
                ModuleStorescreenActivity.this
                        .showToast("Connection's lost. Please check your connection and try again");
            else {
                purchaseAnItem(MLQPlusConstant.FULL_MODULES, ModuleStorescreenActivity.this,
                        new InAppPurchaseListenner() {

                            @Override
                            public void onPurchasedSuccessfully(String moduleID) {
                                // TODO Auto-generated method stub
                                // Update purchase full
                                isPurchaseFull = true;
                                for (int i = 0; i < 5 && i < modules.size(); i++)
                                    modules.get(i).setUnlocked(true);

                                // Update UI
                                showData();

                                // update view
                                tv_price.setVisibility(View.GONE);
                                img_purchased.setVisibility(View.VISIBLE);
                            }
                        });

            }
        } else
            showToast("You already purchased full modules");
    }

    @Override
    public void onPurchasedSuccessfully(Purchase purchase, final String moduleID) {
        showWaitingDialog(ModuleStorescreenActivity.this, "Updating data",
                "Please wait...");
        retryCount = 0;

        String json = "{\"platform\": \"0\",\"transaction\": \"test\",\"package_name\": \""
                + purchase.getPackageName()
                + "\",\"product_id\": \""
                + purchase.getSku()
                + "\",\"purchase_token\": \""
                + purchase.getToken() + "\"}";

        // Backup data to sharePreferencesService
        getSharedPreferencesService().purchasingModule().put(
                json + "@@@" + moduleID);

        purchaseFullModulesOnOurServer(json, moduleID);

    }

    private void purchaseFullModulesOnOurServer(final String json,
                                                final String moduleID) {
        MLQPlusService.getBSSServices().purchaseFullModule(
                MLQPlusFunctions.getAccessToken(), new TypedString(json),
                new Callback<ApiResponse<Response>>() {

                    @Override
                    public void success(ApiResponse<Response> arg0,
                                        Response arg1) {
                        // TODO Auto-generated method stub
                        dismissWaitingDialog();

                        // Remove purchasingModule in sharePreferencesService
                        getSharedPreferencesService().purchasingModule()
                                .put("");

                        updateUI(moduleID);

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        // TODO Auto-generated method stub
                        retryCount++;
                        if (retryCount > 3) {
                            dismissWaitingDialog();
                            if (error.getCause() instanceof SocketTimeoutException)
                                showToast(MLQPlusConstant.TIMEOUT);
                            else if (!MLQPlusFunctions.isInternetAvailable())
                                showToast(MLQPlusConstant.CONNECTION_LOST);
                            else
                                showToast("Purchase full modules error: "
                                        + error.getMessage());
                        } else
                            purchaseFullModulesOnOurServer(json, moduleID);
                    }
                });
    }
}
*/
