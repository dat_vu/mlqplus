package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class About extends ObjectBase {
	
	@SerializedName("name")
	private String name;
	
	@SerializedName("content")
	private String content;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
