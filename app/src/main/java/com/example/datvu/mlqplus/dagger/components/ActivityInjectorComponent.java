package com.example.datvu.mlqplus.dagger.components;


import com.example.datvu.mlqplus.dagger.modules.UserInfoModule;
import com.example.datvu.mlqplus.dagger.scopes.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = {BaseComponent.class}, modules = UserInfoModule.class)
public interface ActivityInjectorComponent {


}
