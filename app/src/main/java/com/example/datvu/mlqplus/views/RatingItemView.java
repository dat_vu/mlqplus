package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.view_rating_item)
public class RatingItemView extends LinearLayout {

	private Context mContext;

	@ViewById
	ImageView img_level_0, img_level_1, img_level_2, img_level_3, img_level_4;
	
	@ViewById 
	//TextView tvRatingItemQuestion;
	LinearLayout lnTitle;

	private OnRatingListenner onRatingListenner;
	private Integer selectedPosition;

	public RatingItemView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public RatingItemView(Context context, AttributeSet attrs, int defStyleAttr,
						  int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		mContext = context;
	}

	public RatingItemView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mContext = context;
	}

	public RatingItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}
	
	public void setData(String question, Integer selectedAnswer, OnRatingListenner mOnRatingListenner)
	{
		onRatingListenner=mOnRatingListenner;

		// Create tv to show title
		TextView tvTitle=new TextView(mContext);
		tvTitle.setSingleLine(false);
		tvTitle.setText(question);
		tvTitle.setTextColor(mContext.getResources().getColor(R.color.black_text));
		tvTitle.setTextIsSelectable(true);
		MLQPlusFunctions.SetViewTextSize(tvTitle, mContext, R.dimen.textMedium);
		
		lnTitle.addView(tvTitle);
		Log.d("MLQPlus", "Set text: "+question);
		setSelectedPosition(selectedAnswer);
	}

	@Click
	void img_level_0() {
		userRateClick(0);
	}

	@Click
	void img_level_1() {
		userRateClick(1);
	}

	@Click
	void img_level_2() {
		userRateClick(2);
	}

	@Click
	void img_level_3() {
		userRateClick(3);
	}

	@Click
	void img_level_4() {
		userRateClick(4);
	}
	
	private void userRateClick(int pos)
	{
		setSelectedPosition(pos);
		if (onRatingListenner!=null)
			onRatingListenner.onRating();
	}

	private void setSelectedPosition(Integer pos) {
		selectedPosition = pos;

		// reset views
		img_level_0.setImageResource(R.drawable.answer1_unselected);
		img_level_1.setImageResource(R.drawable.answer2_unselected);
		img_level_2.setImageResource(R.drawable.answer3_unselected);
		img_level_3.setImageResource(R.drawable.answer4_unselected);
		img_level_4.setImageResource(R.drawable.answer5_unselected);

		if (selectedPosition!=null)
		switch (pos) {
		case 0:
			img_level_0.setImageResource(R.drawable.answer1_selected);
			break;
		case 1:
			img_level_1.setImageResource(R.drawable.answer2_selected);
			break;
		case 2:
			img_level_2.setImageResource(R.drawable.answer3_selected);
			break;
		case 3:
			img_level_3.setImageResource(R.drawable.answer4_selected);
			break;
		case 4:
			img_level_4.setImageResource(R.drawable.answer5_selected);
			break;
		}
	}

	public Integer getSeletedAnswer() {
		return selectedPosition;
	}
	
	public interface OnRatingListenner{
		public void onRating();
	}
	
}
