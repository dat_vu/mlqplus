package com.example.datvu.mlqplus.services.commoninfo;

import com.example.datvu.mlqplus.models.Country;
import com.example.datvu.mlqplus.models.RestResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by solit_000 on 4/14/2016.
 */
public interface CommonInfoRestServiceInterface {
    @GET("/commonInfo/getCountryList")
    Call<RestResponse<List<Country>>> getCountryList();
}
