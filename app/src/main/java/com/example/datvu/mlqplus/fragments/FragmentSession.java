/*
package com.example.datvu.mlqplus.fragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.model.ModuleSection;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

@EFragment(R.layout.fragment_session)
public class FragmentSession extends FragmentBase {

	public static FragmentSession newInstance() {
		return new FragmentSession_();
	}

	private ModuleSection moduleSection;
	private Activity mActivity;

	@ViewById
	ImageView img_section_image;

	@ViewById
	TextView tv_section_title;

	@ViewById
	LinearLayout ln_section_content;

	@ViewById
	ScrollView scv_session_content;

	@ViewById
	FrameLayout flTitle;

	private boolean isScrollViewAtTop = false;
	private View view;
	private Boolean isInitialized = false;

	public void setData(ModuleSection moduleSection, View view, int pos) {
		this.moduleSection = moduleSection;
		this.view = view;
	}

	@AfterViews
	void init() {
		mActivity = getActivity();

		// Set text size
		MLQPlusFunctions.SetViewTextSize(tv_section_title, mActivity,
				R.dimen.textXXLarge);

		showData();
	}

	private void showData() {
		if (moduleSection != null) {
			// Show top image
			if (moduleSection.getTopBackground() != null
					&& moduleSection.getTopBackground().length() > 0)
				MLQPlusFunctions.showImage(mActivity,
						moduleSection.getTopBackground(), img_section_image);
			else
				img_section_image.setVisibility(View.GONE);

			// Show title
			if (moduleSection.getSectionTitle() != null
					&& moduleSection.getSectionTitle().length() > 0)
				tv_section_title.setText(moduleSection.getSectionTitle());
			else
				tv_section_title.setVisibility(View.GONE);

			if (!isInitialized) {
				isInitialized = true;
			} else {
				LinearLayout parent = (LinearLayout) view.getParent();
				parent.removeAllViews();
			}

			ln_section_content.addView(view);
			
			sendScroll();
			// MLQPlusFunctions.scrollToView(scv_session_content, flTitle);
		}
	}

	@Click
	void flTitle() {
		MLQPlusFunctions.hideKeyboard(mActivity);
	}

	private void sendScroll() {
		final Handler handler = new Handler();
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
				handler.post(new Runnable() {
					@Override
					public void run() {
						scv_session_content.fullScroll(View.FOCUS_UP);
					}
				});
			}
		}).start();
	}

}
*/
