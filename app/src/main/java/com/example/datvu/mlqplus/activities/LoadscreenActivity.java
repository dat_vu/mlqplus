package com.example.datvu.mlqplus.activities;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Country;
import com.example.datvu.mlqplus.models.WelcomeScreenInfo;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@EActivity(R.layout.activity_loadscreen)
public class LoadscreenActivity extends BaseActivity {

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    ImageView img_logo;

    @ViewById
    TextView tvText;

    private ArrayList<WelcomeScreenInfo> mWelcomeScreens = null;
    public static Map<String, Bitmap> mBitmaps;
    private List<Country> mCountries = null;
    private int currentImageCount = 0;
    private int animationCount = 0;


    @AfterViews
    void init() {
        // Get SETTING
        MLQPlusConstant.TEXTSIZE = MLQPlusFunctions
                .loadTextSize(LoadscreenActivity.this);

        // Get screen size
        getScreenSize();

        // Get country list
        MLQPlusFunctions.getCountryList(this);

        // Set TextSize
        //setTextSize();

        // Show Data
        showData();

        // Start animation
        //startAnimation();

    }

    private void setTextSize() {
        // TODO Auto-generated method stub
        MLQPlusFunctions.SetViewTextSize(tvText, LoadscreenActivity.this,
                R.dimen.textMedium);
    }

    private void showData() {
        // Get List texts
        List<String> loadingTexts = MLQPlusFunctions.getLoadingTextList();
        mBitmaps = new HashMap<String, Bitmap>();

        // Select random text to show
        Random random = new Random();
        tvText.setText(loadingTexts.get(random.nextInt(loadingTexts.size())));
    }


    private void loadWelcomeScreenData() {
        if (mWelcomeScreens == null) {
            if (!isInternetAvailable()) {
                showToast(MLQPlusConstant.CONNECTION_LOST);
                finish();
            } else
            {

            }
               /* MLQPlusService.getBSSServices().getWelcomeScreenInfo(
                        new Callback<ApiResponse<List<WelcomeScreenInfo>>>() {

                            @Override
                            public void success(
                                    ApiResponse<List<WelcomeScreenInfo>> mLWelcomeScreen,
                                    Response response) {
                                mWelcomeScreens = new ArrayList<WelcomeScreenInfo>();
                                if (mLWelcomeScreen != null) {
                                    mWelcomeScreens.addAll(mLWelcomeScreen
                                            .getData());

                                    for (int i = 0; i < mWelcomeScreens.size(); i++) {
                                        WelcomeScreenInfo welcomeScreenInfo = mWelcomeScreens
                                                .get(i);
                                        final int j = i;
                                        MLQPlusFunctions.loadImage(
                                                LoadscreenActivity.this,
                                                welcomeScreenInfo
                                                        .getBackground(),
                                                new ImageLoadingListener() {

                                                    @Override
                                                    public void onLoadingStarted(
                                                            String imageUri,
                                                            View view) {
                                                    }

                                                    @Override
                                                    public void onLoadingFailed(
                                                            String imageUri,
                                                            View view,
                                                            FailReason failReason) {
                                                        if (!MLQPlusFunctions
                                                                .isInternetAvailable()) {
                                                            showToast(MLQPlusConstant.CONNECTION_LOST);
                                                            finish();
                                                        } else
                                                            checkLoadCompleted();
                                                    }

                                                    @Override
                                                    public void onLoadingComplete(
                                                            String imageUri,
                                                            View view,
                                                            Bitmap loadedImage) {
                                                        mBitmaps.put(String.valueOf(j), loadedImage);
                                                        checkLoadCompleted();
                                                    }

                                                    @Override
                                                    public void onLoadingCancelled(
                                                            String imageUri,
                                                            View view) {
                                                        // TODO Auto-generated
                                                        // method stub

                                                    }
                                                });
                                    }
                                }

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                // TODO Auto-generated]
                                // Handle timeout
                                if (error.getCause() instanceof SocketTimeoutException) {
                                    showToast(MLQPlusConstant.TIMEOUT);
                                    finish();
                                } else {
                                    if (!MLQPlusFunctions.isInternetAvailable()) {
                                        showToast(MLQPlusConstant.CONNECTION_LOST);
                                        finish();
                                    } else
                                        showToast(error.getMessage());
                                }
                            }
                        });*/
        }
    }

    /*private void checkLoadCompleted() {
        currentImageCount++;
        if (currentImageCount == mWelcomeScreens.size()) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(WelcomescreenActivity_.WELCOME_SCREEN_INFO,
                    mWelcomeScreens);

            // Start welcome screen
            startNewActivityNoAnimation(LoadscreenActivity.this,
                    WelcomescreenActivity_.class, true, bundle);
        }
    }*/

    /*private void startAnimation() {

        final Animation zoomin = AnimationUtils.loadAnimation(this,
                R.anim.zoom_in);
        final Animation zoomout = AnimationUtils.loadAnimation(this,
                R.anim.zoom_out);
        zoomin.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub
                animationCount++;
                if (animationCount == 2) {
                    String accessToken = getSharedPreferencesService()
                            .accessToken().get();
                    if (accessToken.length() > 0)
                        loadUserProfile(accessToken);
                    else
                        loadWelcomeScreenData();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                img_logo.startAnimation(zoomout);
            }
        });
        zoomout.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                img_logo.startAnimation(zoomin);
            }
        });

        img_logo.startAnimation(zoomin);
    }*/

    /*private void loadUserProfile(String accessToken) {
        if (!isInternetAvailable()) {
            showToast(MLQPlusConstant.CONNECTION_LOST);
            finish();
        } else
            MLQPlusService.getBSSServices().getUserProfile(accessToken, new Callback<ApiResponse<SignupResponse>>() {

                @Override
                public void success(ApiResponse<SignupResponse> mSignupResponse, Response arg1) {
                    // TODO Auto-generated method stub
                    MLQPlusConstant.USERINFO = mSignupResponse.getData();

                    // Load config Data
                    MLQPlusFunctions.loadConfigMenu(LoadscreenActivity.this, loadConfigCallback);

                    // Start homeActivity if user completed first Quiz; else start first Quiz
                    if (MLQPlusConstant.USERINFO.getUser()
                            .getIs_has_leadership_style().equals("0") || !getSharedPreferencesService()
                            .userClickedGetStarted().get())
                        startNewActivityNoAnimation(LoadscreenActivity.this,
                                BackgroundActivity_.class, true);
                    else
                        startNewActivityNoAnimation(LoadscreenActivity.this,
                                HomescreenActivity_.class, true);


                }

                @Override
                public void failure(RetrofitError error) {
                    // If cannot get user Profile by accestoken -> it's expired or removed -> login again
                    loadWelcomeScreenData();
                }
            });
    }*/

    /*Callback<ApiResponse<ConfigMenu>> loadConfigCallback = new Callback<ApiResponse<ConfigMenu>>() {

        @Override
        public void success(ApiResponse<ConfigMenu> configs, Response arg1) {
            // TODO Auto-generated method stub
            if (configs != null && configs.getData() != null)
                MLQPlusConstant.CONFIG_MENU = configs.getData();
        }

        @Override
        public void failure(RetrofitError error) {
            // TODO Auto-generated method stub
            if (error.getCause() instanceof SocketTimeoutException)
                showToast("Load config settings: " + MLQPlusConstant.TIMEOUT);
        }
    };*/

}
