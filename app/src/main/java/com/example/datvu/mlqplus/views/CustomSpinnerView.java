package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Country;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EViewGroup(R.layout.view_customspinner)
public class CustomSpinnerView extends LinearLayout {
    @ViewById
    CustomSpinner sp;

    private Context mContext;
    private boolean isSelected = false; // check whether user select spinner item or not
    private List<Object> values;
    private String selectedValue;
    private boolean isInitial = true;
    private String hint;
    private int color; // color of spinner item
    private int textSize = R.dimen.textMedium; // size of spinner item
    private SpinnerClickListenner listenner;

    public CustomSpinnerView(Context context) {
        super(context);

        mContext = context;
    }

    @SuppressLint("NewApi")
    public CustomSpinnerView(Context context, AttributeSet attrs,
                             int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        // TODO Auto-generated constructor stub
        mContext = context;
    }

    public CustomSpinnerView(Context context, AttributeSet attrs,
                             int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // TODO Auto-generated constructor stub
        mContext = context;
    }

    public CustomSpinnerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
    }

    public void setData(List<Object> values1, String selectedValue1,
                        String hint, int color, int textSize, final SpinnerClickListenner listenner) {
        setTextSize();

        // set Value
        this.values = values1;
        this.selectedValue = selectedValue1;
        this.hint = hint;
        this.color = color;
        this.textSize = textSize;
        this.listenner = listenner;

        // init adapter
        if (values != null) {
            sp.setAdapter(spinnerAdapter);
            sp.setOnTouchListener(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    isSelected = true;
                    //((BaseAdapter) spinnerAdapter).notifyDataSetChanged();
                    if (listenner != null)
                        listenner.onClick();
                    return false;
                }
            });

            sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    // TODO Auto-generated method stub
                    if (!isInitial) {
                        // selected value will be country id if type of object
                        // is Country
                        if (values.get(0) instanceof Country)
                            selectedValue = ((Country) values.get(position))
                                    .getId();
                        else if (values.get(0) instanceof String)
                            selectedValue = (String) values.get(position);
                        ((BaseAdapter) spinnerAdapter).notifyDataSetChanged();

                    } else
                        isInitial = false;

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });
        }

        // Show data
        showData();
    }

    public void setSelection(int index) {
        sp.setSelection(index);
        isSelected = true;
        ((BaseAdapter) spinnerAdapter).notifyDataSetChanged();
    }

    // Custom spinner adapter
    private SpinnerAdapter spinnerAdapter = new BaseAdapter() {
        TextView tv_sp;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater lInflater = ((Activity) mContext)
                    .getLayoutInflater();
            if (convertView == null) {
                convertView = lInflater.inflate(R.layout.item_spinner, null);
            }
            tv_sp = (TextView) convertView.findViewById(R.id.tv_sp);

            if (!isSelected) {
                tv_sp.setText(hint);
            } else {
                // Displayed text of spinner will be country name if type of
                // object is Country
                if (values.get(0) instanceof Country)
                    tv_sp.setText(((Country) values.get(position)).getName());
                else if (values.get(0) instanceof String)
                    tv_sp.setText(((String) values.get(position)));
            }

            // set style for spinner item
            if (color != -1) {
                if (hint.equalsIgnoreCase(tv_sp.getText().toString()))
                    tv_sp.setTextColor(color);
                else
                    tv_sp.setTextColor(getResources().getColor(
                            R.color.white));
            }
            if (textSize != -1)
                MLQPlusFunctions.SetViewTextSize(tv_sp, mContext, textSize);

            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return values.get(position);
        }

        @Override
        public int getCount() {
            return values.size();
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            LayoutInflater lInflater = ((Activity) mContext)
                    .getLayoutInflater();
            if (convertView == null) {
                convertView = lInflater.inflate(
                        R.layout.item_custom_spinner_dropdown, null);
            }
            tv_sp = (TextView) convertView.findViewById(android.R.id.text1);
            // set style for spinner item
            /*
			 * if (color != -1) tv_sp.setTextColor(color); if (size != null)
			 * MLQPlusFunctions.SetViewTextSize(tv_sp, mContext, size);
			 */

            if (values.get(0) instanceof Country)
                tv_sp.setText(((Country) values.get(position)).getName());
            else if (values.get(0) instanceof String)
                tv_sp.setText(((String) values.get(position)));
            return convertView;
        }

        ;
    };

    private int getSelectedIndex() {
        if (values != null && selectedValue != null)
            for (int i = 0; i < values.size(); i++) {
                if (values.get(0) instanceof Country) {
                    if (((Country) values.get(i)).getId().equalsIgnoreCase(
                            selectedValue))
                        return i;
                } else if (values.get(0) instanceof String) {
                    if (((String) values.get(i))
                            .equalsIgnoreCase(selectedValue))
                        return i;
                }
            }
        return -1;
    }

    private void showData() {
        int selectedIndex = getSelectedIndex();

        if (selectedIndex != -1) {
            sp.setSelection(selectedIndex);
        }
    }

    public void setTextSize() {

    }

    @Click
    void img_dropdown() {
        if (listenner != null)
            listenner.onClick();
        sp.performClick();
        isSelected = true;
        //((BaseAdapter) spinnerAdapter).notifyDataSetChanged();
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    public interface SpinnerClickListenner {
        public void onClick();
    }
}