package com.example.datvu.mlqplus.models;

public class ModuleActivityEvent {
	private String activityID;
	private String activityType;
	private String answer;
	
	public String getActivityID() {
		return activityID;
	}
	public void setActivityID(String activityID) {
		this.activityID = activityID;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public ModuleActivityEvent(String activityID, String activityType, String answer) {
		super();
		this.activityID = activityID;
		this.answer = answer;
		this.activityType=activityType;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
}
