package com.example.datvu.mlqplus.models;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ModuleActivityError extends ObjectBase implements Serializable{	
	private String activity_id;
	private String platform="android";
	private String user_id;
	private String content;
	
	@SerializedName("activityType")
	private String activityType;

	public void setData(String content, String userID)
	{
		activity_id=getId();
		user_id=userID;
		this.content=content;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	
	
}
