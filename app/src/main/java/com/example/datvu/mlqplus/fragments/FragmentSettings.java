/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.adapter.SettingAdapter;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.Assessment;
import com.mlqplus.trustedleader.model.ConfigMenu;
import com.mlqplus.trustedleader.model.ConfigMenuBase;
import com.mlqplus.trustedleader.model.Setting;
import com.mlqplus.trustedleader.model.SettingData;
import com.mlqplus.trustedleader.model.Size;
import com.mlqplus.trustedleader.model.Tool;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;
import com.mlqplus.trustedleader.fragment.FragmentAccount_;
import com.mlqplus.trustedleader.fragment.FragmentFont_;
import com.mlqplus.trustedleader.fragment.FragmentNotifications_;
import com.mlqplus.trustedleader.fragment.FragmentPassword_;
import com.mlqplus.trustedleader.fragment.FragmentSettings_;
import com.simple.fb.actions.GetProfileAction;

@EFragment(R.layout.fragment_settings)
public class FragmentSettings extends FragmentBase {
	public static FragmentSettings newInstance() {
		return new FragmentSettings_();
	} 
  
	@ViewById
	LinearLayout ln_child;
 
	@ViewById
	TextView tv_title;  
 
	@ViewById   
	ListView lv_setting;

	@ViewById
	WaitingView vWaiting;

	private ArrayList<ConfigMenuBase> mLSetting = null;
	private SettingAdapter mSettingAdapter = null;
	public static String ACCOUNT = "account", PASSWORD = "password",
			NOTIFICATION = "notification", FONT = "font";
	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Set text size for all view
		setTextSize();

		// Get data
		getData();

		// Load detail setting data
		loadSettingData();
	}

	private void getData() {
		vWaiting.showWaitingView();
		if (MLQPlusConstant.CONFIG_MENU == null) {
			if (!MLQPlusFunctions.isInternetAvailable())
				((BaseActivity) mActivity)
						.showToast(MLQPlusConstant.CONNECTION_LOST);
			else {
				MLQPlusService.getBSSServices().getConfigMenu(
						MLQPlusFunctions.getAccessToken(),
						new Callback<ApiResponse<ConfigMenu>>() {

							@Override
							public void success(
									ApiResponse<ConfigMenu> configs,
									Response arg1) {
								if (configs != null
										&& configs.getData() != null) {
									MLQPlusConstant.CONFIG_MENU = configs
											.getData();
									getFragmentSettingsData();
								}
							}

							@Override
							public void failure(RetrofitError error) {
								vWaiting.hideWaitingView();
								
								if (error.getCause() instanceof SocketTimeoutException)
									((BaseActivity) mActivity).showToast("Load menu item failed: "+MLQPlusConstant.TIMEOUT);
								else
									((BaseActivity) mActivity)
										.showToast("Load menu item failed: "+error.getMessage());
							}
						});
			}
		} else
			getFragmentSettingsData();
	}

	private void loadSettingData() {
		if (!MLQPlusFunctions.isInternetAvailable())
			((BaseActivity) mActivity)
					.showToast(MLQPlusConstant.CONNECTION_LOST);
		else {
			MLQPlusService.getBSSServices().getSettings(
					MLQPlusFunctions.getAccessToken(),
					new Callback<ApiResponse<SettingData>>() {

						@Override
						public void success(
								ApiResponse<SettingData> settingData,
								Response arg1) {
							if (settingData != null
									&& settingData.getData() != null) {
								MLQPlusConstant.SETTING_DATA = settingData
										.getData();
								MLQPlusConstant.TEXTSIZE = MLQPlusFunctions
										.getSizefromText(settingData.getData()
												.getFont());
								((BaseActivity) mActivity)
										.getSharedPreferencesService()
										.textSize()
										.put(MLQPlusConstant.TEXTSIZE
												.getSizeInString());
							}
						}

						@Override
						public void failure(RetrofitError arg0) {
							((BaseActivity) mActivity).showToast("Get settings failed");
						}
					});
		}
	}

	private void getFragmentSettingsData() {
		mLSetting = new ArrayList<ConfigMenuBase>();
		addSuitableItem(ACCOUNT);
		addSuitableItem(PASSWORD);
		addSuitableItem(NOTIFICATION);
		addSuitableItem(FONT);

		// Load images before showing data
		List<String> images = new ArrayList<String>();
		for (ConfigMenuBase setting : mLSetting)
			images.add(setting.getIcon());

		showData();
		
		loadImages(images, mActivity);
	}

	private void addSuitableItem(String key) {
		for (ConfigMenuBase item : MLQPlusConstant.CONFIG_MENU.getSettings())
			if (item.getConfig_name().toUpperCase().equals(key.toUpperCase())) {
				mLSetting.add(item);
			}
	}

	private void showData() {
		mSettingAdapter = new SettingAdapter(mActivity, R.layout.item_setting,
				mLSetting);
		lv_setting.setAdapter(mSettingAdapter);
		lv_setting.setSelector(R.drawable.btn_first_quiz);
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}
	
	@ItemClick
	void lv_setting(int pos){
		FragmentBase fragment=null;
		ConfigMenuBase setting=mLSetting.get(pos);
		
		if (setting.getConfig_name().toUpperCase().equals(FragmentSettings.ACCOUNT.toUpperCase()))
			fragment = new FragmentAccount_();
		else
			if (setting.getConfig_name().toUpperCase().equals(FragmentSettings.PASSWORD.toUpperCase()))
			{
				if (((BaseActivity)mActivity).getSharedPreferencesService().isNormalAccount().get())
					fragment = new FragmentPassword_();
				else
					((BaseActivity)mActivity).showToast("Sorry! this function is disable with FaceBook and Linkedin Account");
			}
			else
				if (setting.getConfig_name().toUpperCase().equals(FragmentSettings.NOTIFICATION.toUpperCase()))
					fragment = new FragmentNotifications_();
				else
					if (setting.getConfig_name().toUpperCase().equals(FragmentSettings.FONT.toUpperCase()))
						fragment = new FragmentFont_();
		
		if (fragment!=null)
			MLQPlusFunctions.replaceFragment(mActivity,
					fragment, true);
	}
	
	@Override
	public void voidAfterLoadImages() {
		super.voidAfterLoadImages();
		vWaiting.hideWaitingView();
		
		showData();
	}
}
*/
