package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.view_custom_circle)
public class CustomCircleView extends FrameLayout {

	private Context mContext;
	private OnClickListenner onClickListenner;
	private String mFragName;

	@ViewById
	ImageView img_bg, img_border;

	public CustomCircleView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public CustomCircleView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public CustomCircleView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public CustomCircleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public void setData(OnClickListenner onClickListenner, String fragName) {
		this.onClickListenner = onClickListenner;
		mFragName = fragName;
		
		// Click effect
		img_border.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				float radius = img_border.getWidth() / 2;
				
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					// show clicked background 
					if (MLQPlusFunctions.distance(event.getX(), event.getY(), img_bg.getX() + radius, img_bg.getY() + radius) <= radius)
					{
						if (img_border.getParent() != null) {
							img_border.getParent().requestDisallowInterceptTouchEvent(true);
			            }
						img_bg.setVisibility(View.VISIBLE);
					}
					break;

				case MotionEvent.ACTION_MOVE:
					// Check and change to normal backgroud when mouse is out of
					// border
					if (MLQPlusFunctions.distance(event.getX(), event.getY(), img_bg.getX() + radius, img_bg.getY() + radius) > radius)
						img_bg.setVisibility(View.INVISIBLE);
					else
						img_bg.setVisibility(View.VISIBLE);
					break;

				case MotionEvent.ACTION_UP:
					// call back onClick event
					if (MLQPlusFunctions.distance(event.getX(), event.getY(), img_bg.getX() + radius, img_bg.getY() + radius) <= radius)
					{
						CustomCircleView.this.onClickListenner
								.onClick(CustomCircleView.this.mFragName);
						img_bg.setVisibility(View.INVISIBLE);
					}
					break;
				}
				return true;
			}
		});
	}

	public interface OnClickListenner {
		public void onClick(String fragName);
	}
}
