package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class ModuleOpenInfo extends ObjectBase implements Serializable{
	@SerializedName("sectionIndex")
	private int sectionIndex;

	public int getSectionIndex() {
		return sectionIndex;
	}

	public void setSectionIndex(int sectionIndex) {
		this.sectionIndex = sectionIndex;
	}

	public ModuleOpenInfo(int sectionIndex) {
		this.sectionIndex = sectionIndex;
	}
	
	public ModuleOpenInfo() {
	}
	
}
