package com.example.datvu.mlqplus.dagger.modules;


import com.example.datvu.mlqplus.dagger.scopes.ApplicationScope;
import com.example.datvu.mlqplus.models.SignupResponse;

import dagger.Module;
import dagger.Provides;

@Module
public class UserInfoModule {

    @ApplicationScope
    @Provides
    protected SignupResponse providesUserInfo() {
        return new SignupResponse();
    }
}
