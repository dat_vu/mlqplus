package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class Error {
	@SerializedName("errorCode")
	private String errorCode;
	
	@SerializedName("errorMessage")
	private String errorMessage;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
