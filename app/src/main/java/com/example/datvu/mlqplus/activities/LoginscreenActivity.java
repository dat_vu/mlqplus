package com.example.datvu.mlqplus.activities;


import android.os.AsyncTask;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.ApiResponse;
import com.example.datvu.mlqplus.models.ConfigMenu;
import com.example.datvu.mlqplus.models.LoginRequest;
import com.example.datvu.mlqplus.models.SignupResponse;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.Callback;

@EActivity(R.layout.activity_loginscreen)
public class LoginscreenActivity extends BaseActivity {

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    TextView tv_learn_more, tv_forgot_password, tv_not_a_member, tv_cancel;

    @ViewById
    EditText txt_email, txt_password;

    @ViewById
    Button btn_signin;

    @ViewById
    CheckBox chb_remember_me;

    private String mEmail, mPassword;
    private GoogleCloudMessaging mGoogleCloudMessaging;

    @AfterViews
    void init() {
        // Set text size for all view
        setTextSize();

        // Get and show saved login info
        String email = getSharedPreferencesService().email().get();
        String password = getSharedPreferencesService().password().get();
        if (email != null && password != null && email.length() > 0
                && password.length() > 0) {
            txt_email.setText(email);
            txt_password.setText(password);
        }
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_learn_more,
                LoginscreenActivity.this, R.dimen.textLarge);
        MLQPlusFunctions.SetViewTextSize(tv_forgot_password,
                LoginscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_not_a_member,
                LoginscreenActivity.this, R.dimen.textMedium);

        MLQPlusFunctions.SetViewTextSize(btn_signin, LoginscreenActivity.this,
                R.dimen.textMedium);

        MLQPlusFunctions.SetViewTextSize(txt_email, LoginscreenActivity.this,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(txt_password,
                LoginscreenActivity.this, R.dimen.textMedium);

        MLQPlusFunctions.SetViewTextSize(tv_cancel, LoginscreenActivity.this,
                R.dimen.textMedium);

        MLQPlusFunctions.SetViewTextSize(chb_remember_me,
                LoginscreenActivity.this, R.dimen.textMedium);
    }

    @Click
    void tv_forgot_password() {
        startNewActivityNoAnimation(LoginscreenActivity.this,
                ForgotPasswordscreenActivity_.class, true);
    }

    @Click
    void tv_not_a_member() {
        startNewActivityNoAnimation(LoginscreenActivity.this,
                CreateAccountscreenActivity_.class, true);
    }

    @Click
    void tv_cancel() {
        onBackPressed();
    }

    @Click
    void btn_signin() {
        if (!validateInputData())
            return;

        // Save login info
        if (chb_remember_me.isChecked()) {
            getSharedPreferencesService().email().put(
                    txt_email.getText().toString());
            getSharedPreferencesService().password().put(
                    txt_password.getText().toString());
        } else {
            getSharedPreferencesService().email().put("");
            getSharedPreferencesService().password().put("");
        }

        if (!isInternetAvailable())
            showToast("Please check your internet connection and try again");
        else {
            showWaitingDialog(LoginscreenActivity.this, "Login",
                    "Please wait...");
            LoginRequest loginRequest = new LoginRequest(mEmail, mPassword);

            registerGoogleCloudMessage(loginRequest);
        }
    }

    private void login(LoginRequest loginRequest) {
        /*MLQPlusService.getBSSServices().login(loginRequest,
                new Callback<ApiResponse<SignupResponse>>() {

                    @Override
                    public void success(
                            ApiResponse<SignupResponse> mSignupResponse,
                            Response arg1) {
                        dismissWaitingDialog();
                        MLQPlusConstant.USERINFO = mSignupResponse.getData();
                        getSharedPreferencesService().isNormalAccount().put(
                                true);
                        getSharedPreferencesService().accessToken().put(
                                MLQPlusConstant.USERINFO.getId());

                        if (MLQPlusConstant.USERINFO.getUser()
                                .getIsNeedChangePass().equals("1"))
                            showToast("Please change your password",
                                    Toast.LENGTH_LONG);

                        // Load config menu
                        MLQPlusFunctions.loadConfigMenu(
                                LoginscreenActivity.this, loadConfigCallback);

                        // Start homeActivity if user completed first Quiz; else
                        // start first Quiz
                        if (MLQPlusConstant.USERINFO.getUser()
                                .getIs_has_leadership_style().equals("0")
                                || !getSharedPreferencesService()
                                .userClickedGetStarted().get())
                            startNewActivityNoAnimation(
                                    LoginscreenActivity.this,
                                    BackgroundActivity_.class, true);
                        else
                            startNewActivityNoAnimation(
                                    LoginscreenActivity.this,
                                    HomescreenActivity_.class, true);

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        dismissWaitingDialog();
                        if (error.getCause() instanceof SocketTimeoutException)
                            showToast(MLQPlusConstant.TIMEOUT);
                        else
                            try {
                                String json = new String(((TypedByteArray) error
                                        .getResponse().getBody()).getBytes());
                                if (json.contains("Invalid Username\\/Password"))
                                    showToast("Invalid Username/Password");
                                else if (json.contains("Account was not found"))
                                    showToast("Account was not registered");
                                else
                                    showToast("Error: " + json);
                            } catch (Exception e) {
                            }

                    }
                });*/
    }

    /*Callback<ApiResponse<ConfigMenu>> loadConfigCallback = new Callback<ApiResponse<ConfigMenu>>() {

        @Override
        public void success(ApiResponse<ConfigMenu> configs, Response arg1) {
            // TODO Auto-generated method stub
            if (configs != null && configs.getData() != null)
                MLQPlusConstant.CONFIG_MENU = configs.getData();
        }

        @Override
        public void failure(RetrofitError error) {
            // TODO Auto-generated method stub
            if (error.getCause() instanceof SocketTimeoutException)
                showToast("Load config settings: " + MLQPlusConstant.TIMEOUT);
        }
    };*/

    /*
     * Validate input data from user
     */
    private boolean validateInputData() {
        try {
            mEmail = txt_email.getText().toString().trim();
            if (mEmail.length() == 0) {
                showToast("Email cannot be empty");
                return false;
            } else if (!MLQPlusFunctions.isValidEmail(mEmail)) {
                showToast("Invalid email address");
                return false;
            }

            mPassword = txt_password.getText().toString();
            if (mPassword.length() == 0) {
                showToast("Password cannot be empty");
                return false;
            } else if (mPassword.length() < 4 || mPassword.length() > 12) {
                showToast("Password length must be between 4 and 32 characters");
                return false;
            }
        } catch (Exception e) {
            showToast("Error " + e.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        startNewActivityNoAnimation(LoginscreenActivity.this,
                WelcomescreenActivity_.class, true);
    }

    private void registerGoogleCloudMessage(final LoginRequest loginRequest) {
        new AsyncTask() {

            @Override
            protected String doInBackground(Object... params) {
                // Push device ID
                String DeviceID = getSharedPreferencesService().deviceID()
                        .get();
                int count_time = 0;
                while ((DeviceID == null || DeviceID.equals(""))
                        && count_time < 10) {
                    count_time++;
                    if (mGoogleCloudMessaging == null) {
                        mGoogleCloudMessaging = GoogleCloudMessaging
                                .getInstance(LoginscreenActivity.this);
                        ;
                    }
                    try {
                        DeviceID = mGoogleCloudMessaging
                                .register(MLQPlusConstant.GCM_SENDER_ID);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return DeviceID;
            }

            @Override
            protected void onPostExecute(Object result) {
                try {
                    String DeviceID = String.valueOf(result);
                    if (DeviceID == null || DeviceID.length() == 0)
                        DeviceID = "123";
                    else
                        getSharedPreferencesService().deviceID().put(DeviceID);

                    loginRequest.setDevice_token(DeviceID);
                    login(loginRequest);

                } catch (Exception e) {
                }
            }

            ;

        }.execute(null, null, null);

    }

    @Click
    void ln_child() {
        MLQPlusFunctions.hideKeyboard(LoginscreenActivity.this);
    }
}
