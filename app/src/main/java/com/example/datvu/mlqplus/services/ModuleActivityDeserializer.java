/*
package com.example.datvu.mlqplus.services;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.mlqplus.trustedleader.model.ModuleActivity;
import com.mlqplus.trustedleader.model.ModuleActivityError;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;

public class ModuleActivityDeserializer implements JsonDeserializer<ModuleActivity> {

	@Override
	public ModuleActivity deserialize(JsonElement json, Type arg1,
			JsonDeserializationContext arg2) {
		// TODO Auto-generated method stub
		try
		{
			return new Gson().fromJson(json, ModuleActivity.class);
		}catch(Exception e){
			ModuleActivityError moduleActivityError=new Gson().fromJson(json, ModuleActivityError.class);
			
			String userID="1";
			if (MLQPlusConstant.USERINFO!=null && MLQPlusConstant.USERINFO.getUser_id()!=null)
				userID=MLQPlusConstant.USERINFO.getUser_id();
			
			moduleActivityError.setData(json.toString(), userID);
			
			MLQPlusConstant.ACTIVITIES_ERRORS_LIST.add(moduleActivityError);
			return null;
		}
	}
}
*/
