package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class LeadershipQuote extends ObjectBase{
	@SerializedName("category_name")
	private String name;

	@SerializedName("percent_unlock")
	private String percent_unlock;
	
	@SerializedName("isUnlock")
	private boolean isUnlocked;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		if (percent_unlock.equals("0"))
			return "";
		return "Unlock at "+percent_unlock+"% progress";
	}

	public boolean isUnlocked() {
		return isUnlocked;
	}

	public void setLocked(boolean isUnlocked) {
		this.isUnlocked = isUnlocked;
	}

	public LeadershipQuote(String name, String percent_unlock, boolean isUnlocked) {
		super();
		this.name = name;
		this.percent_unlock="10";
		this.isUnlocked = isUnlocked;
	}

	public String getPercent_unlock() {
		return percent_unlock;
	}

	public void setPercent_unlock(String percent_unlock) {
		this.percent_unlock = percent_unlock;
	}
	
	

}
