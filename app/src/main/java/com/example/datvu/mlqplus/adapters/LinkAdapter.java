package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Link;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class LinkAdapter extends ArrayAdapter<Link> {
    private Activity mContext = null;
    private ViewHolder mViewHolder = null;
    private int mLayoutID;

    public LinkAdapter(Activity mContext, int mLayoutID, List<Link> mALLink) {
        super(mContext, mLayoutID, mALLink);
        this.mContext = mContext;
        this.mLayoutID = mLayoutID;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(mLayoutID, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            mViewHolder.img_icon = (ImageView) convertView
                    .findViewById(R.id.img_icon);
            mViewHolder.vHeaderLine = convertView
                    .findViewById(R.id.vHeaderLine);

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        Link link = getItem(position);

        // Only show the header line in the first child
        if (position == 0)
            mViewHolder.vHeaderLine.setVisibility(View.VISIBLE);
        else
            mViewHolder.vHeaderLine.setVisibility(View.GONE);

        mViewHolder.tv_name.setText(link.getName());
        MLQPlusFunctions.showImage(mContext, link.getIcon(),
                mViewHolder.img_icon);

        // Set text size for widgets
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_name, mContext,
                R.dimen.itemTitle);

        return convertView;
    }

    private class ViewHolder {
        TextView tv_name;
        ImageView img_icon;
        View vHeaderLine;
    }

}
