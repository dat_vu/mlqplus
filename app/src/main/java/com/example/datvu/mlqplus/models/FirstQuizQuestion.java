package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class FirstQuizQuestion extends ObjectBase implements Serializable {
	@SerializedName("first_question_content")
	private String first_question_content;
	
	private int answer;

	public String getFirst_question_content() {
		return first_question_content;
	}

	public void setFirst_question_content(String first_question_content) {
		this.first_question_content = first_question_content;
	}

	public int getAnswer() {
		return answer;
	}

	public void setAnswer(int answer) {
		this.answer = answer;
	}
	
	
}
