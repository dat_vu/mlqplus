package com.example.datvu.mlqplus.dagger.modules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Provides;

/**
 * Created by solit_000 on 4/14/2016.
 */

public class ApplicationModule {
    private static Application sApplication;

    public ApplicationModule(Application application) {
        sApplication = application;
    }

    @Singleton
    @Provides
    Application providesApplication(){
        return sApplication;
    }

}
