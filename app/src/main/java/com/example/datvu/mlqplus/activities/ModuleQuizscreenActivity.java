/*
package com.example.datvu.mlqplus.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.fragment.FragmentModuleQuiz_;
import com.mlqplus.trustedleader.fragment.FragmentQuizzes_;
import com.mlqplus.trustedleader.generator.ModuleGenerator;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.ModuleActivity;
import com.mlqplus.trustedleader.model.ModuleActivityEvent;
import com.mlqplus.trustedleader.model.ModuleQuestion;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.net.SocketTimeoutException;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

@EActivity(R.layout.activity_module_quizscreen)
public class ModuleQuizscreenActivity extends BaseDialogActivity {

    @ViewById
    LinearLayout ln_root;

    @ViewById
    TextView tv_quiz_title, tv_current_question;

    @ViewById
    ViewPager viewPager;

    @ViewById
    LinearLayout ll_next, ll_back;

    private ModuleActivity moduleActivity;
    public static String MODULE_ACTIVITY = "moduleActivity";
    public static String CLASS_NAME = "className";
    private EventBus eventBus;
    private String mClassName = "";

    @AfterViews
    void init() {
        getScreenSize();

        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        // Get data
        getData();

        // Init EventBus
        eventBus = EventBus.getDefault();
    }

    public void getData() {
        try {
            this.mClassName = (String) getIntent().getBundleExtra(
                    MLQPlusConstant.BUNDLE).get(CLASS_NAME);
            moduleActivity = (ModuleActivity) getIntent().getBundleExtra(
                    MLQPlusConstant.BUNDLE).get(MODULE_ACTIVITY);

            // Show answer
            List<Integer> answers = MLQPlusFunctions.parseAnswerArray(
                    moduleActivity.getAnswer(), moduleActivity
                            .getQuizQuestion().getQuestions().size());
            for (int i = 0; i < moduleActivity.getQuizQuestion().getQuestions()
                    .size(); i++) {
                moduleActivity.getQuizQuestion().getQuestions().get(i)
                        .setAnswer(answers.get(i));

                // Fake value index for answers
                moduleActivity.getQuizQuestion().getQuestions().get(i).getAnswers().get(0).setValue(1);
                moduleActivity.getQuizQuestion().getQuestions().get(i).getAnswers().get(1).setValue(2);
                moduleActivity.getQuizQuestion().getQuestions().get(i).getAnswers().get(2).setValue(4);
                moduleActivity.getQuizQuestion().getQuestions().get(i).getAnswers().get(3).setValue(8);
            }


        } catch (Exception e) {
        }

        showData();
    }

    private void showData() {
        if (moduleActivity != null && moduleActivity.getQuizQuestion() != null) {
            if (this.mClassName != null && this.mClassName.equalsIgnoreCase(FragmentQuizzes_.class
                    .getName())) {
                tv_quiz_title.setText("Quiz");
            } else {

                tv_quiz_title.setText(ModuleGenerator.module.getName() + " Quiz");
            }

            tv_current_question.setText(getCurrentQuestionPosString(0));

            ll_back.setVisibility(View.INVISIBLE);

            // Init viewpager
            FragmentManager fragmentManager = getFragmentManager();
            FragmentPagerAdapter sessionAdapter = new FragmentPagerAdapter(
                    fragmentManager) {

                @Override
                public int getCount() {
                    return moduleActivity.getQuizQuestion().getQuestions()
                            .size();
                }

                @Override
                public Fragment getItem(int pos) {
                    FragmentModuleQuiz_ fragmentModuleQuiz_ = new FragmentModuleQuiz_();
                    fragmentModuleQuiz_.setData(moduleActivity
                            .getQuizQuestion().getQuestions().get(pos), pos, moduleActivity.getQuizQuestion().getQuestions().size());
                    return fragmentModuleQuiz_;
                }
            };

            viewPager.setAdapter(sessionAdapter);
            viewPager.setOnPageChangeListener(new OnPageChangeListener() {

                @Override
                public void onPageSelected(int pos) {
                    tv_current_question
                            .setText(getCurrentQuestionPosString(pos));
                    ll_back.setVisibility(View.VISIBLE);
                    ll_next.setVisibility(View.VISIBLE);

                    if (pos == 0) {
                        ll_back.setVisibility(View.INVISIBLE);
                    }
                    if (pos == (moduleActivity.getQuizQuestion().getQuestions()
                            .size() - 1)) {
                        ll_next.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                    // TODO Auto-generated method stub

                }
            });
        }
    }

    @Click
    void ll_back() {
        if (viewPager.getCurrentItem() > 0)
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
    }

    @Click
    void ll_next() {
        if (viewPager.getCurrentItem() < moduleActivity.getQuizQuestion()
                .getQuestions().size() - 1)
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
    }

    private String getCurrentQuestionPosString(int pos) {
        return (pos + 1) + " of "
                + moduleActivity.getQuizQuestion().getQuestions().size();
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_quiz_title,
                ModuleQuizscreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_current_question,
                ModuleQuizscreenActivity.this, R.dimen.textMedium);
    }

    public void btn_submit() {
        String answers = getAnswers();
        if (answers == null)
            showToast("Please answer all questions");
        else {
            if (!isInternetAvailable())
                showToast("Connection's lost. Please check your internet connection");
            else {
                // Update local
                moduleActivity.setAnswer(answers);

                // Call eventBus to check complete all activity
                eventBus.post(new ModuleActivityEvent(moduleActivity.getId(),
                        moduleActivity.getActivityType(), answers));

                showWaitingDialog(ModuleQuizscreenActivity.this,
                        "Submitting anwers", "Please wait...");
                String json_string = "{\"activity_id\": \""
                        + moduleActivity.getId() + "\", \"answer_content\": \""
                        + answers + "\", \"module_id\": \""
                        + ModuleGenerator.module.getId() + "\"}";
                MLQPlusService.getBSSServices().submitActivityAnswer(
                        MLQPlusFunctions.getAccessToken(),
                        new TypedString(json_string), new Callback<ApiResponse<Response>>() {

                            @Override
                            public void success(ApiResponse<Response> arg0, Response arg1) {
                                // TODO Auto-generated method stub
                                dismissWaitingDialog();
                                showQuizResult();
                                finish();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                // TODO Auto-generated method stub
                                dismissWaitingDialog();
                                if (error.getCause() instanceof SocketTimeoutException)
                                    showToast(MLQPlusConstant.TIMEOUT);
                                else
                                    showToast("Error: " + error.getMessage());
                            }
                        });
            }

        }
    }

    private String getAnswers() {
        String answers = "[";
        for (int i = 0; i < moduleActivity.getQuizQuestion().getQuestions()
                .size(); i++) {
            ModuleQuestion question = moduleActivity.getQuizQuestion()
                    .getQuestions().get(i);
            if (question.getAnswer() != null)
                answers += question.getAnswer() + "";
            else
                return null;

            if (i < moduleActivity.getQuizQuestion().getQuestions().size() - 1)
                answers += ", ";
            else
                answers += "]";
        }
        return answers;
    }

    @Click
    void lnClose() {
        ModuleQuizscreenActivity.this.finish();
    }

    private void showQuizResult() {
        int correctAnswers = 0;
        for (int i = 0; i < moduleActivity.getQuizQuestion().getQuestions()
                .size(); i++) {
            ModuleQuestion question = moduleActivity.getQuizQuestion()
                    .getQuestions().get(i);
            if (question.getAnswer() == question.getCorrectAnswer())
                correctAnswers++;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(MODULE_ACTIVITY, moduleActivity);
        bundle.putString(ModuleQuizResultscreenActivity.RESULT, correctAnswers
                + "/" + moduleActivity.getQuizQuestion().getQuestions().size());
        startNewZoomActivity(ModuleQuizscreenActivity.this,
                ModuleQuizResultscreenActivity_.class, true, bundle);
        finish();

    }
}
*/
