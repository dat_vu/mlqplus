package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class LoginRequestBase implements Serializable {

	@SerializedName("device_token")
	private String device_token;
	
	@SerializedName("device_type")
	private String device_type="android";

	public String getDevice_token() {
		return device_token;
	}

	public void setDevice_token(String device_token) {
		this.device_token = device_token;
	}
}
