/*
package com.example.datvu.mlqplus.fragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.model.Quote;
import com.mlqplus.trustedleader.model.Size;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.fragment.FragmentQuote_;

@EFragment(R.layout.fragment_quote)
public class FragmentQuote extends FragmentBase {
	public static FragmentQuote newInstance() {
		return new FragmentQuote_();
	} 
	 
	@ViewById 
	ScrollView scv;  
	
	private Activity mActivity;
	private Quote quote; 
  
	@SuppressLint("NewApi") 
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();  
 
		showData();
	}

	public void setData(Quote quote) {
		this.quote = quote;
	}

	public void showData() {
		LayoutInflater inflater = (LayoutInflater)mActivity.getApplicationContext().getSystemService
				  (Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.item_quote_content,null);
		
		TextView tv_content=(TextView) view.findViewById(R.id.tv_content);
		TextView tv_author=(TextView) view.findViewById(R.id.tv_author);

		// Settext size 
		MLQPlusFunctions.SetViewTextSize(tv_content, mActivity, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_author, mActivity, R.dimen.textMedium);
		
		tv_content.setText("\""+quote.getContent()+"\"");
		tv_author.setText("- "+quote.getAuthor());
		
		scv.addView(view);
	}


}
*/
