package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Module;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class ModuleStoredapter extends ArrayAdapter<Module> {
    private Activity mContext;
    private ViewHolder mViewHolder;

    public ModuleStoredapter(Activity context, int layoutID, List<Module> modules) {
        super(context, layoutID, modules);
        this.mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(R.layout.item_module_store, null);
            mViewHolder = new ViewHolder();

            // Get view
            mViewHolder.img_icon = (ImageView) convertView.findViewById(R.id.img_icon);
            mViewHolder.img_purchased = (ImageView) convertView.findViewById(R.id.img_purchased);
            mViewHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            mViewHolder.tv_price = (TextView) convertView.findViewById(R.id.tv_price);
            mViewHolder.tv_description = (TextView) convertView.findViewById(R.id.tv_description);
            mViewHolder.div_bot = (View) convertView.findViewById(R.id.div_bot);
            mViewHolder.ll_purchased = (LinearLayout) convertView.findViewById(R.id.ll_purchased);
            mViewHolder.ll_price = (LinearLayout) convertView.findViewById(R.id.ll_price);

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        // Get item
        Module module = getItem(position);

        mViewHolder.tv_name.setText(module.getName());
        mViewHolder.tv_price.setText("$" + String.format("%.2f", module.getPrice()));
        MLQPlusFunctions.showImage(mContext, module.getIcon(), mViewHolder.img_icon);
        mViewHolder.img_icon.setColorFilter(mContext.getResources().getColor(R.color.module_icon_store));

        mViewHolder.tv_name.setSelected(true);
        mViewHolder.tv_name.requestFocus();

        mViewHolder.tv_description.setSelected(true);
        mViewHolder.tv_description.requestFocus();

        // Set text size
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_name, mContext, R.dimen.module_name);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_price, mContext, R.dimen.fragment_module_information_module_price);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_description, mContext, R.dimen.module_des);


        if (module.isUnlocked()) {
            mViewHolder.ll_price.setVisibility(View.GONE);
            mViewHolder.ll_purchased.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.ll_price.setVisibility(View.VISIBLE);
            mViewHolder.ll_purchased.setVisibility(View.GONE);
        }

        if (position == getCount() - 1)
            mViewHolder.div_bot.setVisibility(View.GONE);
        else if (position == 0)
            mViewHolder.div_bot.setVisibility(View.VISIBLE);

        return convertView;
    }

    private class ViewHolder {
        ImageView img_icon;
        ImageView img_purchased;
        TextView tv_name;
        TextView tv_price;
        TextView tv_description;
        View div_bot;
        LinearLayout ll_purchased, ll_price;
    }
}
