/*
package com.example.datvu.mlqplus.dagger.interactors;


import android.database.Observable;

import com.example.datvu.mlqplus.dagger.interactors.base.BaseInteractor;
import com.example.datvu.mlqplus.utils.network.base.Retrofit;

import java.util.List;

import javax.inject.Inject;

public class NetworkInteractor extends BaseInteractor {


    private final Retrofit retrofit;

    @Inject
    public NetworkInteractor(Retrofit retrofit){
        this.retrofit = retrofit;
    }

    public Observable<UserProfile> attemptLogin(String username, String password) {
        return tweeterAPI.login(username, password);
    }

    public Observable<Object> logout(){
        return tweeterAPI.logout();
    }

    public Observable<String> fetchTweet() {
        return tweeterAPI.getTweet();
    }

    public Observable<List<String>> fetchTweets(int count) {
        return tweeterAPI.fetchXrecents(count);
    }

    public Observable<String> loadWebpage(String url){
        return retrofit.fetchSomePage(url);
    }
}
*/
