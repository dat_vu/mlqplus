package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class PurchasedModuleToken extends ObjectBase{
	@SerializedName("token")
	String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public PurchasedModuleToken(String id, String token) {
		setId(id);
		this.token = token;
	}
	
	public PurchasedModuleToken() {
	}
	
	
}
