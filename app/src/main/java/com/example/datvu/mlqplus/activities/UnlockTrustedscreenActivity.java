/*
package com.example.datvu.mlqplus.activities;

import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_unlock_trustedscreen)
public class UnlockTrustedscreenActivity extends BaseDialogActivity {

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    TextView tv_unlock_trusted, tv_access_to_premium, tv_full_access,
            tv_unlock, tv_test, tv_track, tv_monthly_access, tv_month,
            tv_annual_access, tv_year, tv_year_content;

    @ViewById
    Button btn_not_right_now;

    @AfterViews
    void init() {
        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Creat zoomable layout
        MLQPlusFunctions.makeZoomableView(UnlockTrustedscreenActivity.this,
                ln_root, ln_child);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        // Show Data
        showData();
    }

    private void showData() {
        tv_month.setText("$" + 1000 + " / " + getResources().getString(R.string.month));
        tv_year.setText("$" + 9999 + " / " + getResources().getString(R.string.year));
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_unlock_trusted,
                UnlockTrustedscreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_access_to_premium,
                UnlockTrustedscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_full_access,
                UnlockTrustedscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_unlock,
                UnlockTrustedscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_test,
                UnlockTrustedscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_track,
                UnlockTrustedscreenActivity.this, R.dimen.textMedium);

        MLQPlusFunctions.SetViewTextSize(tv_monthly_access,
                UnlockTrustedscreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_month,
                UnlockTrustedscreenActivity.this, R.dimen.textLarge);

        MLQPlusFunctions.SetViewTextSize(tv_annual_access,
                UnlockTrustedscreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_year,
                UnlockTrustedscreenActivity.this, R.dimen.textLarge);
        MLQPlusFunctions.SetViewTextSize(tv_year_content,
                UnlockTrustedscreenActivity.this, R.dimen.textMedium);

        MLQPlusFunctions.SetViewTextSize(btn_not_right_now,
                UnlockTrustedscreenActivity.this, R.dimen.textMedium);
    }

    @Click
    void btn_not_right_now() {
        onBackPressed();
    }
}
*/
