package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Tool;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class ToolAdapter extends ArrayAdapter<Tool> {
    private Activity mContext = null;
    private ViewHolder mViewHolder = null;
    private int mLayoutID;

    public ToolAdapter(Activity mContext, int mLayoutID,
                       List<Tool> mALTool) {
        super(mContext, mLayoutID, mALTool);
        this.mContext = mContext;
        this.mLayoutID = mLayoutID;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(mLayoutID, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            mViewHolder.tv_des = (TextView) convertView
                    .findViewById(R.id.tv_des);
            mViewHolder.img_icon = (ImageView) convertView
                    .findViewById(R.id.img_icon);
            mViewHolder.img_next_icon = (ImageView) convertView
                    .findViewById(R.id.img_unlocked_icon);
            mViewHolder.img_locked_icon = (ImageView) convertView
                    .findViewById(R.id.img_locked_icon);
            mViewHolder.lnContent = (LinearLayout) convertView.findViewById(R.id.lnContent);
            mViewHolder.firstLine = convertView.findViewById(R.id.firstLine);

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();


        Tool tool = getItem(position);

        // Show first line at the first item
        if (position == 0)
            mViewHolder.firstLine.setVisibility(View.VISIBLE);
        else
            mViewHolder.firstLine.setVisibility(View.GONE);

        mViewHolder.tv_name.setText(tool
                .getName());
        mViewHolder.tv_des.setText(tool
                .getDescription());

        mViewHolder.tv_name.setSelected(true);
        mViewHolder.tv_name.requestFocus();

        mViewHolder.tv_des.setSelected(true);
        mViewHolder.tv_des.requestFocus();

        MLQPlusFunctions.showImage(mContext, tool.getIcon(), mViewHolder.img_icon);
        LinearLayout.LayoutParams params = MLQPlusFunctions.getLayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);

        //tool.isHaveNext();
        boolean isHaveNext = true;
        if (isHaveNext) {
            mViewHolder.img_next_icon.setVisibility(View.VISIBLE);
            params.weight = 342;
        } else {
            mViewHolder.img_next_icon.setVisibility(View.GONE);
            params.weight = 358;
        }
        mViewHolder.img_locked_icon.setVisibility(View.GONE);

        mViewHolder.lnContent.setLayoutParams(params);

        // Set size for widgets
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_name, mContext,
                R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_des, mContext,
                R.dimen.textToolDes);

        return convertView;
    }

    private class ViewHolder {
        TextView tv_name;
        TextView tv_des;
        ImageView img_icon;
        ImageView img_next_icon, img_locked_icon;
        View firstLine;
        LinearLayout lnContent;
    }

}

