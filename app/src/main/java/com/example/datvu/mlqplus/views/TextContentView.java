package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.ModuleText;
import com.example.datvu.mlqplus.utils.CustomLinkMovementMethod;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.view_textcontent)
public class TextContentView extends LinearLayout {

	private Context mContext;

	@ViewById
	FixItalicLinearLayout lnTextContent;

	public TextContentView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public TextContentView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		mContext = context;
	}

	public TextContentView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mContext = context;
	}

	public TextContentView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}

	@SuppressLint("NewApi")
	public void setContent(ModuleText moduleText) {
		// Init Textview Content
		FixItalicTextView tvContent = new FixItalicTextView(mContext);
		tvContent.setSingleLine(false);
		tvContent.setTextColor(mContext.getResources().getColor(
				R.color.black_text));
		tvContent.setTextIsSelectable(true);
		MLQPlusFunctions.SetViewTextSize(tvContent, mContext,
				R.dimen.textMedium);
		if (moduleText.getContent() != null) {
			/*
			 * String test=
			 * "<p>http://yodiz.com/help/agile-user-stories-and-groomed-product-backlog/<br/><a href=\"http://yodiz.com/help/agile-user-stories-and-groomed-product-backlog/\">http://yodiz.com/help/agile-user-stories-and-groomed-product-backlog/</a></p>"
			 * ; test=test.replace("<p>", ""); test=test.replace("</p>", "");
			 * tvContent.setText(Html.fromHtml(test));
			 */
			// String
			// content="6634634634634634<br />\n6346346346346346346346<br />\n346346346<br />\n34634634634634";
			String content = moduleText.getContent();
			// Remove <p> and </p>
			content = content.replace("<p>", "");
			content = content.replace("</p>", "");
			content = content.replace("&nbsp;", " ");

			tvContent.setText(Html.fromHtml(content));

			tvContent.setClickable(true);
			tvContent.setMovementMethod(CustomLinkMovementMethod.getInstance());
		}

		tvContent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MLQPlusFunctions.hideKeyboard((Activity) mContext);
			}
		});

		// Add to parent view
		lnTextContent.addView(tvContent);

		// set align
		switch (moduleText.getTextAlignment()) {
		case 0:
			tvContent.setGravity(Gravity.CENTER);
			break;
		case 1:
			tvContent.setGravity(Gravity.LEFT);
			break;
		case 2:
			tvContent.setGravity(Gravity.RIGHT);
			tvContent.setLayoutParams(MLQPlusFunctions.getLayoutParams(
				    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			tvContent.setX((int) tvContent.getX() - 5);
			break;
		}
	}
	
	public String getCharPerLineString(String text){

	    String limitedCharPerLineString = "";
	    while (text.length() > 10) {

	        String buffer = text.substring(0, 10);
	        limitedCharPerLineString = limitedCharPerLineString + buffer + "/n";
	        text = text.substring(10);
	    }

	    limitedCharPerLineString = limitedCharPerLineString + text.substring(0);
	    return limitedCharPerLineString;
	}

	@Click
	void lnBackground() {
		MLQPlusFunctions.hideKeyboard((Activity) mContext);
	}

	@Click
	void lnTextContent() {
		MLQPlusFunctions.hideKeyboard((Activity) mContext);
	}
	
	private class FixItalicTextView extends TextView {

		@SuppressLint("NewApi")
		public FixItalicTextView(Context context, AttributeSet attrs,
				int defStyleAttr, int defStyleRes) {
			super(context, attrs, defStyleAttr, defStyleRes);
			// TODO Auto-generated constructor stub
		}

		public FixItalicTextView(Context context, AttributeSet attrs,
				int defStyleAttr) {
			super(context, attrs, defStyleAttr);
			// TODO Auto-generated constructor stub
		}

		public FixItalicTextView(Context context, AttributeSet attrs) {
			super(context, attrs);
			
			// TODO Auto-generated constructor stub
		}

		public FixItalicTextView(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		    super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		    final int measuredWidth = getMeasuredWidth();
		    final float tenPercentOfMeasuredWidth = measuredWidth * 0.01f;
		    final int newWidth = measuredWidth + (int) tenPercentOfMeasuredWidth;

		    setMeasuredDimension(newWidth, getMeasuredHeight());
		    
		}
	}
}
