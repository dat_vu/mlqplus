package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Quote implements Serializable {
	@SerializedName("content")
	private String content;

	@SerializedName("author")
	private String author;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Quote(String content, String author) {
		super();
		this.content = content;
		this.author = author;
	}

}
