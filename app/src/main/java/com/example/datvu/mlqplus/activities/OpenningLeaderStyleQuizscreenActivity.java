package com.example.datvu.mlqplus.activities;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_openning_leader_style_quiz)
public class OpenningLeaderStyleQuizscreenActivity extends BaseDialogActivity {

    public static final String IS_START_FROM_LEADER_QUIZ = "Is Start From Leader Quiz";
    private boolean isStartFromLeaderQuiz = false;

    @ViewById
    LinearLayout ln_root;

    @ViewById
    LinearLayout ln_child;

    @ViewById
    TextView tv_title, tv_welcome_to, tv_test_your_knowledge, tv_unlimited,
            tv_improve_your_score, tv_check_your_progress;

    @ViewById
    Button btn_start;

    @AfterViews
    void init() {
        // Get data
        getData();

        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Make dialog not dim
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        // Creat zoomable layout
        /*
         * MLQPlusFunctions.makeZoomableView(OpenningQuizscreenActivity.this,
		 * ln_root, ln_child);
		 */

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);
    }

    @SuppressLint("LongLogTag")
    private void getData() {
        try {
            Bundle bundle = getIntent().getBundleExtra(MLQPlusConstant.BUNDLE);
            isStartFromLeaderQuiz = bundle
                    .getBoolean(IS_START_FROM_LEADER_QUIZ);
        } catch (Exception e) {
            Log.e("OpenningLeaderStyleQuizscreenActivity", e.getMessage());
        }
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_title,
                OpenningLeaderStyleQuizscreenActivity.this,
                R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_welcome_to,
                OpenningLeaderStyleQuizscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_test_your_knowledge,
                OpenningLeaderStyleQuizscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_unlimited,
                OpenningLeaderStyleQuizscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_improve_your_score,
                OpenningLeaderStyleQuizscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_check_your_progress,
                OpenningLeaderStyleQuizscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btn_start,
                OpenningLeaderStyleQuizscreenActivity.this, R.dimen.textMedium);

    }

    /*@Click
    void btn_start() {
        if (!isInternetAvailable())
            showToast("Please check your internet connection and try again");
        else {
            showWaitingDialog(OpenningLeaderStyleQuizscreenActivity.this,
                    "Loading quiz", "Please wait...");

            MLQPlusService.getBSSServices().getFirstQuiz(
                    MLQPlusFunctions.getAccessToken(),
                    new Callback<ApiResponse<ArrayList<FirstQuizQuestion>>>() {

                        @Override
                        public void success(
                                ApiResponse<ArrayList<FirstQuizQuestion>> mQuestions,
                                Response arg1) {
                            dismissWaitingDialog();
                            if (isStartFromLeaderQuiz)
                                YourStylescreenActivity.isStartFromLeaderQuiz = true;
                            else
                                YourStylescreenActivity.isStartFromLeaderQuiz = false;

                            // If successfully load 5 questions
                            if (mQuestions.getData() != null
                                    && mQuestions.getData().size() >= 0) {
                                QuizscreenActivity.questions = mQuestions
                                        .getData();
                                startNewZoomActivity(
                                        OpenningLeaderStyleQuizscreenActivity.this,
                                        QuizscreenActivity_.class, true);
                            } else
                                // can't get enough 5 questions
                                showToast(
                                        "Cannot load quiz. Please try again!",
                                        Toast.LENGTH_LONG);

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            dismissWaitingDialog();
                            if (error.getCause() instanceof SocketTimeoutException)
                                showToast(MLQPlusConstant.TIMEOUT);
                            else
                                showToast(
                                        "Cannot load quiz. Please check your internet connection and try again!",
                                        Toast.LENGTH_LONG);
                        }
                    });

        }

    }*/

    @Click
    void ln_close() {
        OpenningLeaderStyleQuizscreenActivity.this.finish();
    }
}
