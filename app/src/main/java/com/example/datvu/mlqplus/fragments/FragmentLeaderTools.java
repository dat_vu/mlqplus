/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.activity.ViewPDFscreenActivity;
import com.mlqplus.trustedleader.activity.ViewPDFscreenActivity_;
import com.mlqplus.trustedleader.adapter.LeaderToolAdapter;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.LeaderTool;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;

@EFragment(R.layout.fragment_leader_tools)
public class FragmentLeaderTools extends FragmentBase {
	public static FragmentLeaderTools newInstance() {
		return new FragmentLeaderTools_();
	}

	@ViewById
	LinearLayout ln_child;

	@ViewById
	TextView tv_title;

	@ViewById
	ListView lv_leader_tool;

	@ViewById
	WaitingView vWaiting;

	private ArrayList<LeaderTool> mLLeaderTool = null;
	private LeaderToolAdapter mLeaderToolAdapter = null;
	private int getDataFailedCount;
	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();
		getDataFailedCount = 0;

		// Set text size for all view
		setTextSize();

		// Get Data
		getData();

		// Show Data
		showData();
	}

	private void getData() {
		if (!MLQPlusFunctions.isInternetAvailable())
			((BaseActivity) mActivity)
					.showToast(MLQPlusConstant.CONNECTION_LOST);
		else {
			// Only show at the first time
			if (getDataFailedCount == 0)
				vWaiting.showWaitingView();

			MLQPlusService.getBSSServices().getWorksSheet(
					MLQPlusFunctions.getAccessToken(),
					new Callback<ApiResponse<List<LeaderTool>>>() {

						@Override
						public void success(
								ApiResponse<List<LeaderTool>> tools,
								Response arg1) {
							// TODO Auto-generated method stub

							if (tools != null && tools.getData() != null
									&& tools.getData().size() > 0) {
								mLLeaderTool = new ArrayList<LeaderTool>();
								mLLeaderTool.addAll(tools.getData());

								// Load images before showing data
								List<String> images = new ArrayList<String>();
								for (LeaderTool leaderTool : mLLeaderTool)
									images.add(leaderTool.getIcon());

								loadImages(images, mActivity);
								showData();
							} else {
								// Retry getting data
								getDataFailedCount++;
								if (getDataFailedCount < 3)
									getData();
								else
									vWaiting.hideWaitingView();
							}
						}

						@Override
						public void failure(RetrofitError error) {
							// Retry getting data
							getDataFailedCount++;
							if (getDataFailedCount < 3)
								getData();
							else {
								vWaiting.hideWaitingView();
								try {
									if (error.getCause() instanceof SocketTimeoutException)
										showToast("Get worksheet failed: "
												+ MLQPlusConstant.TIMEOUT);
									else
										showToast("Get worksheet failed: "
												+ error.getMessage());
								} catch (Exception e) {
								}
							}
						}
					});
		}
	}

	private void showData() {
		if (mLLeaderTool != null) {
			mLeaderToolAdapter = new LeaderToolAdapter(mActivity,
					R.layout.item_leader_tool, mLLeaderTool);
			lv_leader_tool.setAdapter(mLeaderToolAdapter);
			lv_leader_tool.setSelector(R.drawable.btn_first_quiz);
		}
	}

	private void setTextSize() {
		MLQPlusFunctions
				.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}

	@ItemClick
	void lv_leader_tool(int pos) {
		if (pos < mLLeaderTool.size() && mLLeaderTool.get(pos).isUnlock())
			if (mLLeaderTool.get(pos).getPdf_link() != null) {
				Bundle bundle = new Bundle();
				bundle.putSerializable(ViewPDFscreenActivity.ASSESSMENT,
						mLLeaderTool.get(pos));
				((BaseActivity) mActivity).startNewActivityNoAnimation(
						mActivity, ViewPDFscreenActivity_.class, false, bundle);

			} else
				((BaseActivity) mActivity).showToast("Cannot get PDF link");
		else
			((BaseActivity) mActivity)
					.showToast("Please complete each module to access Leader Tools");
	}

	@Override
	public void voidAfterLoadImages() {
		super.voidAfterLoadImages();
		vWaiting.hideWaitingView();

		showData();
	}
}
*/
