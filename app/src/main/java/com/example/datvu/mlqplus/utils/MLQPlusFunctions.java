package com.example.datvu.mlqplus.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Handler;
import android.os.StrictMode;
import android.text.SpannableString;
import android.text.style.LeadingMarginSpan;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Country;
import com.example.datvu.mlqplus.models.TextSize;
import com.example.datvu.mlqplus.services.commoninfo.CommonInfoServiceInterface;
import com.example.datvu.mlqplus.views.ZoomView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.lang.reflect.Type;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.inject.Inject;

import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;
import bolts.Continuation;
import bolts.Task;


public class MLQPlusFunctions {
    @Inject
    static
    CommonInfoServiceInterface commonInfoServiceInterface;

    public static void SetViewTextSize(TextView view, Context mContext, int id) {
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources()
                .getDimensionPixelSize(id)
                + MLQPlusConstant.TEXTSIZE.getType()
                * mContext.getResources().getInteger(R.integer.mlqplus_function_text_size_coefficient));
    }

    public static void SetViewTextSize(RadioButton view, Context mContext,
                                       int id) {
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources()
                .getDimensionPixelSize(id)
                + MLQPlusConstant.TEXTSIZE.getType()
                * mContext.getResources().getInteger(R.integer.mlqplus_function_text_size_coefficient));
    }

    public static void SetViewTextSize(CheckBox view, Context mContext, int id) {
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources()
                .getDimensionPixelSize(id)
                + MLQPlusConstant.TEXTSIZE.getType()
                * mContext.getResources().getInteger(R.integer.mlqplus_function_text_size_coefficient));
    }

    public static void SetViewTextSize(EditText view, Context mContext, int id) {
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources()
                .getDimensionPixelSize(id)
                + MLQPlusConstant.TEXTSIZE.getType()
                * mContext.getResources().getInteger(R.integer.mlqplus_function_text_size_coefficient));
    }

    public static void SetViewTextSize(Button view, Context mContext, int id) {
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources()
                .getDimensionPixelSize(id)
                + MLQPlusConstant.TEXTSIZE.getType()
                * mContext.getResources().getInteger(R.integer.mlqplus_function_text_size_coefficient));
    }

    public static void makeZoomableView(Activity activity, LinearLayout parent,
                                        View child) {
        ZoomView zoomView = new ZoomView(activity);
        parent.removeView(child);
        zoomView.addView(child);
        parent.addView(zoomView);
    }

    public static void makeZoomableView(Activity activity,
                                        RelativeLayout parent, View child) {
        ZoomView zoomView = new ZoomView(activity);
        parent.removeView(child);
        zoomView.addView(child);
        parent.addView(zoomView);
    }

    public static void makeZoomableView(Activity activity, FrameLayout parent,
                                        View child) {
        ZoomView zoomView = new ZoomView(activity);
        parent.removeView(child);
        zoomView.addView(child);
        parent.addView(zoomView);
    }

    public static void setLayoutMargin(FrameLayout fl, int top, int left,
                                       int right, int bottom) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) fl
                .getLayoutParams();
        params.setMargins(left, top, right, bottom);
        fl.setLayoutParams(params);
    }

    public static void setImageViewSizeMargin(ImageView img, int width,
                                              int height, int top, int left, int right, int bottom) {
        LayoutParams lp = new LayoutParams(width,
                height);
        lp.setMargins(left, top, right, bottom);
        lp.gravity = Gravity.BOTTOM | Gravity.CENTER;
        img.setLayoutParams(lp);
    }

    public static void setImageViewSizeMargin(ImageView img, int width,
                                              int height, int top, int left, int right, int bottom, int weight) {
        LayoutParams lp = new LayoutParams(width,
                height);
        lp.setMargins(left, top, right, bottom);
        lp.weight = weight;
        lp.gravity = Gravity.CENTER;
        img.setLayoutParams(lp);
    }

    public static void marginLinearLayout(LinearLayout ln, int top, int left,
                                          int right, int bottom) {
        ViewGroup.LayoutParams layoutParams = ln.getLayoutParams();
        if (layoutParams instanceof MarginLayoutParams) {
            ((MarginLayoutParams) layoutParams).topMargin = top;
            ((MarginLayoutParams) layoutParams).leftMargin = left;
            ((MarginLayoutParams) layoutParams).rightMargin = right;
            ((MarginLayoutParams) layoutParams).bottomMargin = bottom;
        }
        ln.setLayoutParams(layoutParams);
    }

    public static void showImage(Context mContext, String url,
                                 ImageView img_view) {
        ImageLoader iLoader = ImageLoader.getInstance();
        iLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true).bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .showImageOnLoading(R.drawable.transparent).cacheOnDisc(true)
                .build();
        iLoader.displayImage(url, img_view, options);

		/*
         * Picasso.with(mContext) .load(url)
		 * .networkPolicy(NetworkPolicy.OFFLINE) .into(img_view);
		 */
    }

    public static LayoutParams fixImageSize(int width, int height) {
        int MaxWidth = 768, MaxHeight = 1028;

        LayoutParams params = new LayoutParams(width, height);

        float width_ratio = MaxWidth / width;
        float height_ratio = MaxHeight / height;
        float min_ratio = width_ratio < height_ratio ? width_ratio
                : height_ratio;
        if (min_ratio < 1) {
            params.width = (int) (width * min_ratio);
            params.height = (int) (height * min_ratio);
        }
        return params;
    }

    /*public static void loadImage(Context mContext, String url,
                                 ImageLoadingListener imageLoadingListener) {
        ImageLoader iLoader = ImageLoader.getInstance();
        iLoader.init(getImageLoaderConfiguration(mContext));
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true).bitmapConfig(Bitmap.Config.RGB_565)
                .showImageOnLoading(R.drawable.loading_icon)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT).cacheOnDisc(true)
                .build();
        iLoader.loadImage(url, options, imageLoadingListener);
    }*/

    /*public static ImageLoaderConfiguration getImageLoaderConfiguration(
            Context mContext) {
        File cacheDir = new File(mContext.getCacheDir(), "imgcachedir");
        if (!cacheDir.exists())
            cacheDir.mkdir();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                mContext).memoryCache(new WeakMemoryCache())
                .discCache(new UnlimitedDiscCache(cacheDir)).build();
        return config;
    }*/

    public static Integer getAnswerFromArray(List<Boolean> list) {
        if (list == null)
            return null;
        Integer answer = 0;
        for (int i = 0; i < list.size(); i++)
            answer += (int) ((list.get(i) ? 1 : 0) * Math.pow(2.0, i));
        return answer;
    }

    public static List<Boolean> getListFromAnswer(Integer answer,
                                                  int answerCount) {
        List<Boolean> result = new ArrayList<Boolean>();
        for (int i = 0; i < answerCount; i++)
            result.add(false);
        if (answer != null) {
            String binary = Integer.toBinaryString(answer);

            for (int i = binary.length() - 1; i >= 0; i--)
                result.set(binary.length() - 1 - i, binary.substring(i, i + 1)
                        .equals("1"));
        }

        return result;
    }

    public static List<String> getAgeList() {
        List<String> ageList = new ArrayList<String>();
        ageList.add("<18");
        ageList.add("18-24");
        ageList.add("25-34");
        ageList.add("35-44");
        ageList.add("45-54");
        ageList.add("55-64");
        ageList.add("65+");

        return ageList;
    }

    public static List<Country> getLocalCountryList() {
        List<Country> countryList = new ArrayList<Country>();
        countryList.add(new Country("AFG", "Afghanistan"));
        countryList.add(new Country("ALA", "�land Islands"));
        countryList.add(new Country("ALB", "Albania"));
        countryList.add(new Country("DZA", "Algeria"));
        countryList.add(new Country("ASM", "American Samoa"));
        countryList.add(new Country("AND", "Andorra"));
        countryList.add(new Country("AGO", "Angola"));
        countryList.add(new Country("AIA", "Anguilla"));
        countryList.add(new Country("ATA", "Antarctica"));
        countryList.add(new Country("ATG", "Antigua and Barbuda"));
        countryList.add(new Country("ARG", "Argentina"));
        countryList.add(new Country("ARM", "Armenia"));
        countryList.add(new Country("ABW", "Aruba"));
        countryList.add(new Country("AUS", "Australia"));
        countryList.add(new Country("AUT", "Austria"));
        countryList.add(new Country("AZE", "Azerbaijan"));
        countryList.add(new Country("BHS", "Bahamas"));
        countryList.add(new Country("BHR", "Bahrain"));
        countryList.add(new Country("BGD", "Bangladesh"));
        countryList.add(new Country("BRB", "Barbados"));
        countryList.add(new Country("BLR", "Belarus"));
        countryList.add(new Country("BEL", "Belgium"));
        countryList.add(new Country("BLZ", "Belize"));
        countryList.add(new Country("BEN", "Benin"));
        countryList.add(new Country("BMU", "Bermuda"));
        countryList.add(new Country("BTN", "Bhutan"));
        countryList.add(new Country("BOL", "Bolivia, Plurinational State of"));
        countryList.add(new Country("BES", "Bonaire, Sint Eustatius and Saba"));
        countryList.add(new Country("BIH", "Bosnia and Herzegovina"));
        countryList.add(new Country("BWA", "Botswana"));
        countryList.add(new Country("BVT", "Bouvet Island"));
        countryList.add(new Country("BRA", "Brazil"));
        countryList.add(new Country("IOT", "British Indian Ocean Territory"));
        countryList.add(new Country("BRN", "Brunei Darussalam"));
        countryList.add(new Country("BGR", "Bulgaria"));
        countryList.add(new Country("BFA", "Burkina Faso"));
        countryList.add(new Country("BDI", "Burundi"));
        countryList.add(new Country("CPV", "Cabo Verde"));
        countryList.add(new Country("KHM", "Cambodia"));
        countryList.add(new Country("CMR", "Cameroon"));
        countryList.add(new Country("CAN", "Canada"));
        countryList.add(new Country("CYM", "Cayman Islands"));
        countryList.add(new Country("CAF", "Central African Republic"));
        countryList.add(new Country("TCD", "Chad"));
        countryList.add(new Country("CHL", "Chile"));
        countryList.add(new Country("CHN", "China"));
        countryList.add(new Country("CXR", "Christmas Island"));
        countryList.add(new Country("CCK", "Cocos (Keeling) Islands"));
        countryList.add(new Country("COL", "Colombia"));
        countryList.add(new Country("COM", "Comoros"));
        countryList.add(new Country("COG", "Congo"));
        countryList.add(new Country("COD",
                "Congo, the Democratic Republic of the"));
        countryList.add(new Country("COK", "Cook Islands"));
        countryList.add(new Country("CRI", "Costa Rica"));
        countryList.add(new Country("CIV", "C�te d\'Ivoire"));
        countryList.add(new Country("HRV", "Croatia"));
        countryList.add(new Country("CUB", "Cuba"));
        countryList.add(new Country("CUW", "Cura�ao"));
        countryList.add(new Country("CYP", "Cyprus"));
        countryList.add(new Country("CZE", "Czech Republic"));
        countryList.add(new Country("DNK", "Denmark"));
        countryList.add(new Country("DJI", "Djibouti"));
        countryList.add(new Country("DMA", "Dominica"));
        countryList.add(new Country("DOM", "Dominican Republic"));
        countryList.add(new Country("ECU", "Ecuador"));
        countryList.add(new Country("EGY", "Egypt"));
        countryList.add(new Country("SLV", "El Salvador"));
        countryList.add(new Country("GNQ", "Equatorial Guinea"));
        countryList.add(new Country("ERI", "Eritrea"));
        countryList.add(new Country("EST", "Estonia"));
        countryList.add(new Country("ETH", "Ethiopia"));
        countryList.add(new Country("FLK", "Falkland Islands (Malvinas)"));
        countryList.add(new Country("FRO", "Faroe Islands"));
        countryList.add(new Country("FJI", "Fiji"));
        countryList.add(new Country("FIN", "Finland"));
        countryList.add(new Country("FRA", "France"));
        countryList.add(new Country("GUF", "French Guiana"));
        countryList.add(new Country("PYF", "French Polynesia"));
        countryList.add(new Country("ATF", "French Southern Territories"));
        countryList.add(new Country("GAB", "Gabon"));
        countryList.add(new Country("GMB", "Gambia"));
        countryList.add(new Country("GEO", "Georgia"));
        countryList.add(new Country("DEU", "Germany"));
        countryList.add(new Country("GHA", "Ghana"));
        countryList.add(new Country("GIB", "Gibraltar"));
        countryList.add(new Country("GRC", "Greece"));
        countryList.add(new Country("GRL", "Greenland"));
        countryList.add(new Country("GRD", "Grenada"));
        countryList.add(new Country("GLP", "Guadeloupe"));
        countryList.add(new Country("GUM", "Guam"));
        countryList.add(new Country("GTM", "Guatemala"));
        countryList.add(new Country("GGY", "Guernsey"));
        countryList.add(new Country("GIN", "Guinea"));
        countryList.add(new Country("GNB", "Guinea-Bissau"));
        countryList.add(new Country("GUY", "Guyana"));
        countryList.add(new Country("HTI", "Haiti"));
        countryList
                .add(new Country("HMD", "Heard Island and McDonald Islands"));
        countryList.add(new Country("VAT", "Holy See (Vatican City State)"));
        countryList.add(new Country("HND", "Honduras"));
        countryList.add(new Country("HKG", "Hong Kong"));
        countryList.add(new Country("HUN", "Hungary"));
        countryList.add(new Country("ISL", "Iceland"));
        countryList.add(new Country("IND", "India"));
        countryList.add(new Country("IDN", "Indonesia"));
        countryList.add(new Country("IRN", "Iran, Islamic Republic of"));
        countryList.add(new Country("IRQ", "Iraq"));
        countryList.add(new Country("IRL", "Ireland"));
        countryList.add(new Country("IMN", "Isle of Man"));
        countryList.add(new Country("ISR", "Israel"));
        countryList.add(new Country("ITA", "Italy"));
        countryList.add(new Country("JAM", "Jamaica"));
        countryList.add(new Country("JPN", "Japan"));
        countryList.add(new Country("JEY", "Jersey"));
        countryList.add(new Country("JOR", "Jordan"));
        countryList.add(new Country("KAZ", "Kazakhstan"));
        countryList.add(new Country("KEN", "Kenya"));
        countryList.add(new Country("KIR", "Kiribati"));
        countryList.add(new Country("PRK",
                "Korea, Democratic People\'s Republic of"));
        countryList.add(new Country("KOR", "Korea, Republic of"));
        countryList.add(new Country("KWT", "Kuwait"));
        countryList.add(new Country("KGZ", "Kyrgyzstan"));
        countryList
                .add(new Country("LAO", "Lao People\'s Democratic Republic"));
        countryList.add(new Country("LVA", "Latvia"));
        countryList.add(new Country("LBN", "Lebanon"));
        countryList.add(new Country("LSO", "Lesotho"));
        countryList.add(new Country("LBR", "Liberia"));
        countryList.add(new Country("LBY", "Libya"));
        countryList.add(new Country("LIE", "Liechtenstein"));
        countryList.add(new Country("LTU", "Lithuania"));
        countryList.add(new Country("LUX", "Luxembourg"));
        countryList.add(new Country("MAC", "Macao"));
        countryList.add(new Country("MKD",
                "Macedonia, the former Yugoslav Republic of"));
        countryList.add(new Country("MDG", "Madagascar"));
        countryList.add(new Country("MWI", "Malawi"));
        countryList.add(new Country("MYS", "Malaysia"));
        countryList.add(new Country("MDV", "Maldives"));
        countryList.add(new Country("MLI", "Mali"));
        countryList.add(new Country("MLT", "Malta"));
        countryList.add(new Country("MHL", "Marshall Islands"));
        countryList.add(new Country("MTQ", "Martinique"));
        countryList.add(new Country("MRT", "Mauritania"));
        countryList.add(new Country("MUS", "Mauritius"));
        countryList.add(new Country("MYT", "Mayotte"));
        countryList.add(new Country("MEX", "Mexico"));
        countryList.add(new Country("FSM", "Micronesia, Federated States of"));
        countryList.add(new Country("MDA", "Moldova, Republic of"));
        countryList.add(new Country("MCO", "Monaco"));
        countryList.add(new Country("MNG", "Mongolia"));
        countryList.add(new Country("MNE", "Montenegro"));
        countryList.add(new Country("MSR", "Montserrat"));
        countryList.add(new Country("MAR", "Morocco"));
        countryList.add(new Country("MOZ", "Mozambique"));
        countryList.add(new Country("MMR", "Myanmar"));
        countryList.add(new Country("NAM", "Namibia"));
        countryList.add(new Country("NRU", "Nauru"));
        countryList.add(new Country("NPL", "Nepal"));
        countryList.add(new Country("NLD", "Netherlands"));
        countryList.add(new Country("NCL", "New Caledonia"));
        countryList.add(new Country("NZL", "New Zealand"));
        countryList.add(new Country("NIC", "Nicaragua"));
        countryList.add(new Country("NER", "Niger"));
        countryList.add(new Country("NGA", "Nigeria"));
        countryList.add(new Country("NIU", "Niue"));
        countryList.add(new Country("NFK", "Norfolk Island"));
        countryList.add(new Country("MNP", "Northern Mariana Islands"));
        countryList.add(new Country("NOR", "Norway"));
        countryList.add(new Country("OMN", "Oman"));
        countryList.add(new Country("PAK", "Pakistan"));
        countryList.add(new Country("PLW", "Palau"));
        countryList.add(new Country("PSE", "Palestine, State of"));
        countryList.add(new Country("PAN", "Panama"));
        countryList.add(new Country("PNG", "Papua New Guinea"));
        countryList.add(new Country("PRY", "Paraguay"));
        countryList.add(new Country("PER", "Peru"));
        countryList.add(new Country("PHL", "Philippines"));
        countryList.add(new Country("PCN", "Pitcairn"));
        countryList.add(new Country("POL", "Poland"));
        countryList.add(new Country("PRT", "Portugal"));
        countryList.add(new Country("PRI", "Puerto Rico"));
        countryList.add(new Country("QAT", "Qatar"));
        countryList.add(new Country("REU", "R�union"));
        countryList.add(new Country("ROU", "Romania"));
        countryList.add(new Country("RUS", "Russian Federation"));
        countryList.add(new Country("RWA", "Rwanda"));
        countryList.add(new Country("BLM", "Saint Barth�lemy"));
        countryList.add(new Country("SHN",
                "Saint Helena, Ascension and Tristan da Cunha"));
        countryList.add(new Country("KNA", "Saint Kitts and Nevis"));
        countryList.add(new Country("LCA", "Saint Lucia"));
        countryList.add(new Country("MAF", "Saint Martin (French part)"));
        countryList.add(new Country("SPM", "Saint Pierre and Miquelon"));
        countryList.add(new Country("VCT", "Saint Vincent and the Grenadines"));
        countryList.add(new Country("WSM", "Samoa"));
        countryList.add(new Country("SMR", "San Marino"));
        countryList.add(new Country("STP", "Sao Tome and Principe"));
        countryList.add(new Country("SAU", "Saudi Arabia"));
        countryList.add(new Country("SEN", "Senegal"));
        countryList.add(new Country("SRB", "Serbia"));
        countryList.add(new Country("SYC", "Seychelles"));
        countryList.add(new Country("SLE", "Sierra Leone"));
        countryList.add(new Country("SGP", "Singapore"));
        countryList.add(new Country("SXM", "Sint Maarten (Dutch part)"));
        countryList.add(new Country("SVK", "Slovakia"));
        countryList.add(new Country("SVN", "Slovenia"));
        countryList.add(new Country("SLB", "Solomon Islands"));
        countryList.add(new Country("SOM", "Somalia"));
        countryList.add(new Country("ZAF", "South Africa"));
        countryList.add(new Country("SGS",
                "South Georgia and the South Sandwich Islands"));
        countryList.add(new Country("SSD", "South Sudan"));
        countryList.add(new Country("ESP", "Spain"));
        countryList.add(new Country("LKA", "Sri Lanka"));
        countryList.add(new Country("SDN", "Sudan"));
        countryList.add(new Country("SUR", "Suriname"));
        countryList.add(new Country("SJM", "Svalbard and Jan Mayen"));
        countryList.add(new Country("SWZ", "Swaziland"));
        countryList.add(new Country("SWE", "Sweden"));
        countryList.add(new Country("CHE", "Switzerland"));
        countryList.add(new Country("SYR", "Syrian Arab Republic"));
        countryList.add(new Country("TWN", "Taiwan, Province of China"));
        countryList.add(new Country("TJK", "Tajikistan"));
        countryList.add(new Country("TZA", "Tanzania, United Republic of"));
        countryList.add(new Country("THA", "Thailand"));
        countryList.add(new Country("TLS", "Timor-Leste"));
        countryList.add(new Country("TGO", "Togo"));
        countryList.add(new Country("TKL", "Tokelau"));
        countryList.add(new Country("TON", "Tonga"));
        countryList.add(new Country("TTO", "Trinidad and Tobago"));
        countryList.add(new Country("TUN", "Tunisia"));
        countryList.add(new Country("TUR", "Turkey"));
        countryList.add(new Country("TKM", "Turkmenistan"));
        countryList.add(new Country("TCA", "Turks and Caicos Islands"));
        countryList.add(new Country("TUV", "Tuvalu"));
        countryList.add(new Country("UGA", "Uganda"));
        countryList.add(new Country("UKR", "Ukraine"));
        countryList.add(new Country("ARE", "United Arab Emirates"));
        countryList.add(new Country("GBR", "United Kingdom"));
        countryList.add(new Country("UMI",
                "United States Minor Outlying Islands"));
        countryList.add(new Country("USA", "United States of America"));
        countryList.add(new Country("URY", "Uruguay"));
        countryList.add(new Country("UZB", "Uzbekistan"));
        countryList.add(new Country("VUT", "Vanuatu"));
        countryList
                .add(new Country("VEN", "Venezuela, Bolivarian Republic of"));
        countryList.add(new Country("VNM", "Viet Nam"));
        countryList.add(new Country("VGB", "Virgin Islands, British"));
        countryList.add(new Country("VIR", "Virgin Islands, U.S."));
        countryList.add(new Country("WLF", "Wallis and Futuna"));
        countryList.add(new Country("ESH", "Western Sahara"));
        countryList.add(new Country("YEM", "Yemen"));
        countryList.add(new Country("ZMB", "Zambia"));
        countryList.add(new Country("ZWE", "Zimbabwe"));

        return countryList;
    }

    public static List<String> getGenderList() {
        List<String> genderList = new ArrayList<String>();
        genderList.add("Male");
        genderList.add("Female");

        return genderList;
    }

    public static List<String> getLoadingTextList() {
        List<String> loadingTexts = new ArrayList<String>();
        loadingTexts.add("Be Greater Than You Think");
        loadingTexts.add("Learn More");
        loadingTexts.add("Highlight Insight");
        loadingTexts.add("Innovate, Perform, Empower");
        return loadingTexts;
    }

    /*
     * validate email
     */
    public static boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static String getAccessToken() {
        if (MLQPlusConstant.USERINFO != null
                && MLQPlusConstant.USERINFO.getId() != null)
            return MLQPlusConstant.USERINFO.getId();
        return "";
    }

    public static void showCircleProgress(int rimColor, Activity mContext,
                                          CircleProgressView mCircleView, int value, int width) {
        // value setting
        mCircleView.setMaxValue(100);
        mCircleView.setValue(0);
        mCircleView.setValueAnimated(value);

        mCircleView.setBarWidth(width);
        mCircleView.setRimWidth(width);
        mCircleView.setSpinBarColor(mContext.getResources().getColor(
                R.color.blue_circle_progress));
        mCircleView.setRimColor(rimColor);
        mCircleView.setBarColor(mContext.getResources().getColor(
                R.color.blue_circle_progress_seleted));
        mCircleView.setContourColor(Color.parseColor("#00000000"));
        mCircleView.setText("");
        mCircleView.setTextMode(TextMode.TEXT);
    }

    public static List<Integer> parseAnswerArray(String answer, int size) {
        // Init empty anwer list
        List<Integer> result = new ArrayList<Integer>();
        if (answer != null && answer.length() > 0) {
            Type listType = new TypeToken<List<Integer>>() {
            }.getType();
            result = new Gson().fromJson(answer, listType);
        }

        // Correct array
        if (result == null)
            result = new ArrayList<Integer>();
        for (int i = result.size(); i < size; i++)
            result.add(null);

        return result;
    }

    public static boolean isInternetAvailable() {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            InetAddress ipAddr = InetAddress.getByName("google.com"); // You can
            // replace
            // it
            // with
            // your
            // name

            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            return false;
        }

    }

   /* public static void submitActivityAnswer(final ModuleActivityEvent event,
                                            String moduleID, final Context mContext) {
        if (!isInternetAvailable())
            Toast.makeText(mContext,
                    "Connection's lost. Please check your internet connection",
                    Toast.LENGTH_SHORT).show();
        else {

            ModuleActivityAnswer moduleActivityAnswer = new ModuleActivityAnswer(
                    event.getActivityID(), event.getAnswer(), moduleID);
            String json_string = new Gson().toJson(moduleActivityAnswer);

            MLQPlusService.getBSSServices().submitActivityAnswer(
                    MLQPlusFunctions.getAccessToken(),
                    new TypedString(json_string),
                    new Callback<ApiResponse<Response>>() {

                        @Override
                        public void success(ApiResponse<Response> arg0,
                                            Response arg1) {
                            // TODO Auto-generated method stub
                            // Only show success message with Freetext Activity
                            // (activityType=8)
                            if (event.getActivityType().equals("8"))
                                Toast.makeText(mContext,
                                        "Response submitted. Thank you.",
                                        Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            // TODO Auto-generated method stub
                            try {
                                if (error.getCause() instanceof SocketTimeoutException)
                                    Toast.makeText(mContext, "Submit activity answers: " + MLQPlusConstant.TIMEOUT,
                                            Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, error.getMessage(),
                                            Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                            }
                        }
                    });
        }
    }*/

    /*
     * Calculate distance between two point
     */
    public static float distance(float x1, float y1, float x2, float y2) {
        return (float) Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    /*public static void replaceFragment(Activity activity, Fragment fragment,
                                       boolean isRighttoLeftTransaction) {
        if (fragment != null) {
            FragmentManager manager = activity.getFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();

            if (isRighttoLeftTransaction) {
                ft.setCustomAnimations(R.anim.slide_in_left,
                        R.anim.slide_out_left, 0, 0);
            } else
                ft.setCustomAnimations(R.anim.slide_in_right,
                        R.anim.slide_out_right, 0, 0);

            ft.replace(R.id.container, fragment);
            ft.commit();
            ((MainActivity_) activity).setCurrentFragmentIndex(-1);
        }
    }*/

    /*public static void saveTextSize(TextSize textSize, Activity mActivity) {
        if (textSize != null && mActivity != null)
            ((BaseActivity) mActivity).getSharedPreferencesService().textSize()
                    .put(textSize.getSizeInString());
    }*/

    /*public static TextSize loadTextSize(Activity mActivity) {
        String sizeInString = ((BaseActivity) mActivity)
                .getSharedPreferencesService().textSize().get();
        if (sizeInString.length() > 0)
            return getSizefromText(sizeInString);
        return TextSize.NORMAL;
    }*/

    @SuppressLint("NewApi")
    public static LayoutParams getLayoutParams(int width,
                                               int height) {
        LayoutParams params = null;
        if (android.os.Build.VERSION.SDK_INT < 19) {
            MarginLayoutParams p = new MarginLayoutParams(width, height);
            params = new LayoutParams(p);
        } else {
            params = new LayoutParams(new LayoutParams(width,
                    height, 1f));
        }
        return params;
    }

    public static void hideKeyboard(Activity mActivity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) mActivity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(mActivity
                    .getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    /*public static void loadConfigMenu(final BaseActivity activity,
                                      Callback<ApiResponse<ConfigMenu>> callback) {
        if (!isInternetAvailable())
            activity.showToast(MLQPlusConstant.CONNECTION_LOST);
        else {
            MLQPlusService.getBSSServices().getConfigMenu(
                    MLQPlusFunctions.getAccessToken(), callback);
        }
    }*/

    public static TextSize getSizefromText(String sizeInString) {
        if (sizeInString != null) {
            if (sizeInString.equals("largest"))
                return TextSize.LARGER2;
            if (sizeInString.equals("small"))
                return TextSize.SMALLER1;
            if (sizeInString.equals("smallest"))
                return TextSize.SMALLER2;
        }
        // default
        return TextSize.NORMAL;
    }

    public static SpannableString createIndentedText(String text,
                                                     int marginFirstLine, int marginNextLines) {
        SpannableString result = new SpannableString(text);
        result.setSpan(new LeadingMarginSpan.Standard(marginFirstLine,
                marginNextLines), 0, text.length(), 0);
        return result;
    }

    public static void scrollToView(final ScrollView scrollView, final View view) {

        // View needs a focus
        view.requestFocus();

        // Determine if scroll needs to happen
        final Rect scrollBounds = new Rect();
        scrollView.getHitRect(scrollBounds);
        if (!view.getLocalVisibleRect(scrollBounds)) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    scrollView.smoothScrollTo(0, view.getTop());
                }
            });
        }
    }

    /*public static int getLastSectionOpen(BaseActivity mActivity, String moduleID) {
        String info = mActivity.getSharedPreferencesService()
                .lastSectionOpenModules().get();
        if (info != null && info.length() > 0)
            try {
                // parse json to list
                Type listType = new TypeToken<List<ModuleOpenInfo>>() {
                }.getType();
                List<ModuleOpenInfo> moduleOpenInfos = new Gson().fromJson(
                        info, listType);

                if (moduleOpenInfos != null)
                    for (ModuleOpenInfo moduleOpenInfo : moduleOpenInfos)
                        if (moduleOpenInfo.getId().equals(moduleID))
                            return moduleOpenInfo.getSectionIndex();
                return -1;

            } catch (Exception e) {
                return -1;
            }

        return -1;
    }*/

    /*public static void updateLastSectionOpen(BaseActivity mActivity,
                                             String moduleID, int sectionIndex) {
        List<ModuleOpenInfo> moduleOpenInfos;
        String info = mActivity.getSharedPreferencesService()
                .lastSectionOpenModules().get();
        if (info != null && info.length() > 0) // if info has been inited
        {
            ModuleOpenInfo moduleOpenInfo = null;

            Type listType = new TypeToken<List<ModuleOpenInfo>>() {
            }.getType();
            List<ModuleOpenInfo> currentModuleOpenInfos = new Gson().fromJson(
                    info, listType);

            // clone info if it's exited
            if (currentModuleOpenInfos != null)
                for (ModuleOpenInfo mInfo : currentModuleOpenInfos)
                    if (mInfo.getId().equals(moduleID)) {
                        // update sectionIndex
                        mInfo.setSectionIndex(sectionIndex);

                        moduleOpenInfo = mInfo;
                        break;
                    }

            // Init if it's not exited
            if (moduleOpenInfo == null) {
                moduleOpenInfo = new ModuleOpenInfo();
                moduleOpenInfo.setId(moduleID);
                moduleOpenInfo.setSectionIndex(sectionIndex);

                // Add to list
                currentModuleOpenInfos.add(moduleOpenInfo);
            }

            moduleOpenInfos = currentModuleOpenInfos;
        } else // init the list
        {
            moduleOpenInfos = new ArrayList<ModuleOpenInfo>();

            // Init new moduleinfo
            ModuleOpenInfo moduleOpenInfo = new ModuleOpenInfo();
            moduleOpenInfo.setId(moduleID);
            moduleOpenInfo.setSectionIndex(sectionIndex);
            moduleOpenInfos.add(moduleOpenInfo);
        }

        // Save list to sharePreference
        mActivity.getSharedPreferencesService().lastSectionOpenModules()
                .put(new Gson().toJson(moduleOpenInfos));
    }*/

    public static void getCountryList(final Context mContext) {
        commonInfoServiceInterface.getCountryList().continueWith(new Continuation<List<Country>, Void>() {

            @Override
            public Void then(Task<List<Country>> task) throws Exception {
                if (task.isCompleted())
                {
                    List<Country> countries = task.getResult();
                    List<Country> countryList = new ArrayList<Country>();

                    if (countries != null) {
                        countryList.addAll(countries);
                        MLQPlusConstant.COUNTRIES = countryList;
                    }
                }
                else if (task.isFaulted())
                {
                    Exception e = task.getError();
                    if (!MLQPlusFunctions.isInternetAvailable()) {
                        Toast.makeText(mContext,
                                MLQPlusConstant.CONNECTION_LOST,
                                Toast.LENGTH_SHORT);
                    } else
                        Toast.makeText(mContext, e.getMessage(),
                                Toast.LENGTH_SHORT);
                }
                else if (task.isCancelled()){
                    // Do nothing
                }

                return null;
            }
        });


        /*MLQPlusService.getBSSServices().getCountryList(
                new Callback<ApiResponse<List<Country>>>() {

                    @Override
                    public void success(ApiResponse<List<Country>> countries,
                                        Response response) {
                        List<Country> countryList = new ArrayList<Country>();
                        if (countries != null) {
                            countryList.addAll(countries.getData());
                            MLQPlusConstant.COUNTRIES = countryList;
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (!MLQPlusFunctions.isInternetAvailable()) {
                            Toast.makeText(mContext,
                                    MLQPlusConstant.CONNECTION_LOST,
                                    Toast.LENGTH_SHORT);
                        } else
                            Toast.makeText(mContext, error.getMessage(),
                                    Toast.LENGTH_SHORT);
                    }
                });*/

                                                                 }

    /*public static String getModuleToken(BaseActivity activity, String id) {
        String json = activity.getSharedPreferencesService().purchasedModuleToken().get();
        if (json != null && json.length() > 0) {
            Type listType = new TypeToken<ArrayList<PurchasedModuleToken>>() {
            }.getType();
            ArrayList<PurchasedModuleToken> purchasedModuleTokens = new Gson().fromJson(json, listType);

            if (purchasedModuleTokens != null)
                for (PurchasedModuleToken purchasedModuleToken : purchasedModuleTokens)
                    if (purchasedModuleToken.getId().equals(id))
                        return purchasedModuleToken.getToken();
        }
        return null;
    }*/

    /*public static void updatePurchaseModuleToken(BaseActivity activity, PurchasedModuleToken newPurchasedModuleToken) {
        String json = activity.getSharedPreferencesService().purchasedModuleToken().get();
        ArrayList<PurchasedModuleToken> purchasedModuleTokens = null;

        if (json != null && json.length() > 0) {
            Type listType = new TypeToken<ArrayList<PurchasedModuleToken>>() {
            }.getType();
            purchasedModuleTokens = new Gson().fromJson(json, listType);

            if (purchasedModuleTokens != null)
                for (PurchasedModuleToken purchasedModuleToken : purchasedModuleTokens)
                    if (purchasedModuleToken.getId().equals(newPurchasedModuleToken.getId())) {
                        purchasedModuleTokens.remove(purchasedModuleToken);
                        break;
                    }
        }

        if (purchasedModuleTokens == null)
            purchasedModuleTokens = new ArrayList<PurchasedModuleToken>();
        purchasedModuleTokens.add(newPurchasedModuleToken);

    }*/

    }
