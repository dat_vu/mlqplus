/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.activity.QuoteActivity;
import com.mlqplus.trustedleader.activity.QuoteActivity_;
import com.mlqplus.trustedleader.adapter.LeadershipQuoteAdapter;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.LeadershipQuote;
import com.mlqplus.trustedleader.model.Quote;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;

@EFragment(R.layout.fragment_leadership_quotes)
public class FragmentLeadershipQuotes extends FragmentBase {
	public static FragmentLeadershipQuotes newInstance() {
		return new FragmentLeadershipQuotes_();
	} 
    
	@ViewById
	LinearLayout ln_child; 
 
	@ViewById   
	TextView tv_title;

	@ViewById
	ListView lv_quotes;
	
	@ViewById
	WaitingView vWaiting;

	private static ArrayList<LeadershipQuote> mLLeadershipQuote = null;
	private LeadershipQuoteAdapter mQuoteAdapter = null;
	private int getDataFailedCount;
	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();
		getDataFailedCount=0;

		// Set text size for all view
		setTextSize();

		// Get Data
		getData();
	}

	private void getData() {
		if (mLLeadershipQuote != null)
			showData();	

		if (!MLQPlusFunctions.isInternetAvailable())
			showToast(MLQPlusConstant.CONNECTION_LOST);
		else
		{
			if (mLLeadershipQuote == null && getDataFailedCount==0)
				vWaiting.showWaitingView();
			MLQPlusService.getBSSServices().getQuoteCategories(
					MLQPlusFunctions.getAccessToken(),
					new Callback<ApiResponse<List<LeadershipQuote>>>() {

						@Override
						public void success(
								ApiResponse<List<LeadershipQuote>> mQuote,
								Response arg1) {
							// TODO Auto-generated method stub
							
							if (mQuote != null && mQuote.getData() != null && mQuote.getData().size()>0) {
								vWaiting.hideWaitingView();
								mLLeadershipQuote = new ArrayList<LeadershipQuote>();
								mLLeadershipQuote.addAll(mQuote.getData());
								
								// sort data
								for (int i=0; i<mLLeadershipQuote.size()-1; i++)
									for (int j=i+1; j<mLLeadershipQuote.size(); j++)
										if (isGreaterPercent(mLLeadershipQuote.get(i).getPercent_unlock(), mLLeadershipQuote.get(j).getPercent_unlock()))
										{
											LeadershipQuote temp=mLLeadershipQuote.get(i);
											mLLeadershipQuote.set(i, mLLeadershipQuote.get(j));
											mLLeadershipQuote.set(j, temp);
										}
								
								showData();
							}
							else
							{
								// Retry getting data
								getDataFailedCount++;
								if (getDataFailedCount<3)
									getData();
								else
									vWaiting.hideWaitingView();
							}
						}

						@Override
						public void failure(RetrofitError error) {
							// Retry getting data
							getDataFailedCount++;
							if (getDataFailedCount<3)
								getData();
							else
							{
								vWaiting.hideWaitingView();
								if (error.getCause() instanceof SocketTimeoutException)
									showToast(MLQPlusConstant.TIMEOUT);
								else
									showToast("Loading failed: " + error.getMessage());
							}
						}
					});
		}
	}

	private void showData() {
		mQuoteAdapter = new LeadershipQuoteAdapter(mActivity,
				R.layout.item_quote, mLLeadershipQuote);
		lv_quotes.setAdapter(mQuoteAdapter);
		lv_quotes.setSelector(R.drawable.btn_first_quiz);
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}

	@ItemClick
	void lv_quotes(int position) {
		final LeadershipQuote leadershipQuote = (LeadershipQuote) lv_quotes
				.getItemAtPosition(position);
		if (leadershipQuote.isUnlocked()) {
			if (!MLQPlusFunctions.isInternetAvailable())
				showToast(MLQPlusConstant.CONNECTION_LOST);
			else {
				vWaiting.showWaitingView();
				MLQPlusService.getBSSServices().getQuoteDetail(
						MLQPlusFunctions.getAccessToken(),
						leadershipQuote.getId(),
						new Callback<ApiResponse<ArrayList<Quote>>>() {

							@Override
							public void success(
									ApiResponse<ArrayList<Quote>> mQuotes,
									Response arg1) {
								// TODO Auto-generated method stub
								vWaiting.hideWaitingView();
								if (mQuotes != null
										&& mQuotes.getData() != null
										&& mQuotes.getData().size() > 0) {
									Bundle bundle = new Bundle();
									bundle.putSerializable(
											QuoteActivity.LIST_QUOTE,
											mQuotes.getData());
									bundle.putString(QuoteActivity.QUOTE_NAME, leadershipQuote.getName());
									((BaseActivity) mActivity)
											.startNewActivityNoAnimation(mActivity,
													QuoteActivity_.class,
													false, bundle);
								}

							}

							@Override
							public void failure(RetrofitError error) {
								// TODO Auto-generated method stub
								vWaiting.hideWaitingView();
								if (error.getCause() instanceof SocketTimeoutException)
									showToast("Get quote detail failed: "+MLQPlusConstant.TIMEOUT);
								else
									showToast("Get quote detail failed: "+MLQPlusConstant.TIMEOUT);
							}
						});
			}
		}
		else
			((BaseActivity) mActivity).showToast("Complete modules to increase progress and unlock more quotes");
	}
	
	private boolean isGreaterPercent(String firstPercent, String secondPercent)
	{
		try
		{
			int fiPercent=Integer.parseInt(firstPercent);
			int sePercent=Integer.parseInt(secondPercent);
			
			return fiPercent>sePercent;
		}catch(Exception e)
		{
			return false;
		}
	}

}
*/
