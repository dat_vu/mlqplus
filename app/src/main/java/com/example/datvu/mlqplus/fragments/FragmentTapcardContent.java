/*
package com.example.datvu.mlqplus.fragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.generator.TapcardContentGenerator;
import com.mlqplus.trustedleader.model.ModuleActivities;
import com.mlqplus.trustedleader.fragment.FragmentTapcardContent_;

@EFragment(R.layout.fragment_tapcard_content)
public class FragmentTapcardContent extends FragmentBase {
	public static FragmentTapcardContent newInstance() {
		return new FragmentTapcardContent_();
	}
   
	@ViewById 
	LinearLayout ln_root; 
	   
	@ViewById 
	ScrollView scv;  
	 
	private ModuleActivities moduleActivities; 
		
	private Activity mActivity;
	
	@SuppressLint("NewApi") @AfterViews
	void init()
	{
		// getActivity
		mActivity=getActivity();
		
		// Show data
		showData();
	}
	
	public void setData(ModuleActivities moduleActivities)
	{
		this.moduleActivities=moduleActivities;
	}
	
	private void showData()
	{
		if (moduleActivities!=null && moduleActivities.getActivities()!=null)
			ln_root.addView(new TapcardContentGenerator().generateView(moduleActivities.getActivities(), mActivity));
		
		scv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
	        @Override
	        public void onGlobalLayout() {
	            // Ready, move up
	        	scv.pageScroll(View.FOCUS_UP);
	        	scv.pageScroll(View.FOCUS_LEFT);
	        	scv.scrollTo(0, 0);
	        }
	    });
	}
	
}
*/
