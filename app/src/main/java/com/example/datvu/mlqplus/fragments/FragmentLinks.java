/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.adapter.LinkAdapter;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.Link;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;

@EFragment(R.layout.fragment_links)
public class FragmentLinks extends FragmentBase {
	public static FragmentLinks newInstance() {
		return new FragmentLinks_();
	}  
   
	@ViewById 
	LinearLayout ln_child;

	@ViewById  
	TextView tv_title;

	@ViewById
	ListView lv_link;
	
	@ViewById
	WaitingView vWaiting;

	private ArrayList<Link> mALLink = null;
	private LinkAdapter mLinkAdapter = null;
	private int getDataFailedCount;
	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();
		getDataFailedCount=0;

		// Set text size for all view
		setTextSize();

		// Get Data
		getData();
	}

	private void getData() {
		if (!MLQPlusFunctions.isInternetAvailable())
			((BaseActivity)mActivity).showToast(MLQPlusConstant.CONNECTION_LOST);
		else
		{
			// Only show at the first time
			if (getDataFailedCount==0)
				vWaiting.showWaitingView();
			
			MLQPlusService.getBSSServices().getLinks(MLQPlusFunctions.getAccessToken(), new Callback<ApiResponse<List<Link>>>() {
				
				@Override
				public void success(ApiResponse<List<Link>> links, Response arg1) {
					// TODO Auto-generated method stub
					
					if (links!=null && links.getData()!=null && links.getData().size()>0)
					{
						mALLink=new ArrayList<Link>();
						mALLink.addAll(links.getData());
						
						// Load images before showing data
						List<String> images=new ArrayList<String>();
						for  (Link link:mALLink)
							images.add(link.getIcon());
						
						loadImages(images, mActivity);
						showData();
					}
					else
					{
						// Retry getting data
						getDataFailedCount++;
						if (getDataFailedCount<3)
							getData();
						else
							vWaiting.hideWaitingView();
					}
				}
				
				@Override
				public void failure(RetrofitError error) {
					// TODO Auto-generated method stub
					// Retry getting data
					getDataFailedCount++;
					if (getDataFailedCount<3)
						getData();
					else
					{
						vWaiting.hideWaitingView();
						try
						{
							if (error.getCause() instanceof SocketTimeoutException)
								showToast("Load link failed: "+MLQPlusConstant.TIMEOUT);
							else
								showToast("Load link failed: "+error.getMessage());
						}catch(Exception e){}
					}
				}
			});
		}
	}

	private void showData() {
		if (mALLink!=null)
		{
			mLinkAdapter = new LinkAdapter(mActivity, R.layout.item_link, mALLink);
			lv_link.setAdapter(mLinkAdapter);
			lv_link.setSelector(R.drawable.btn_first_quiz);
		}
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}
	
	@ItemClick
	void lv_link(int position)
	{
		if (position<mALLink.size() && mALLink.get(position).getLink()!=null)
			mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mALLink.get(position).getLink())));
		else
			((BaseActivity)mActivity).showToast("Cannot get link");
	}
	
	@Override
	public void voidAfterLoadImages() {
		super.voidAfterLoadImages();
		vWaiting.hideWaitingView();
		
		showData();
	}
}
*/
