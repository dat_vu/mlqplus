/*
package com.example.datvu.mlqplus.activities;

import android.content.Intent;
import android.util.Log;

import com.mlqplus.trustedleader.billing.utils.IabHelper;
import com.mlqplus.trustedleader.billing.utils.IabResult;
import com.mlqplus.trustedleader.billing.utils.Purchase;
import com.mlqplus.trustedleader.model.PurchasedModuleToken;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

public class InAppBillingBaseActivity extends BaseDialogActivity {

    private static final String TAG = "com.mlqplus.trustedleader.inappbilling";
    private String ITEM_SKU = "com.mlqplus.trustedleader.iap.";
    //private String ITEM_SKU = "android.test.purchased";
    private String moduleID;
    private IabHelper mHelper;
    private InAppPurchaseListenner listenner;
    private BaseActivity activity;

    @Override
    protected void onStart() {
        super.onStart();

        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmR4CJ70ghFAwMRSQ4U8qOuxMKErPtz3WgLzpW7sPOYXhCYS59/Auymps/kouhdvAJACn76BPdihSF2TKivTGfqKSMK8NeHXfs28oaDdaaS/txN1eojLlcbAB8SCZstOl2nmIt9iOmUypUsd3L+AfDPch16+jWqZk9kpsHKqDGwWm2+ERTkAtwtI8cQvrmUxV6A6NY8CyxHAw0KDZsc/wqy+vbZ4z565l+sMZZveybu9ikNS00W/89XOjpVeuVFNFEV2EjSdAqxDXPy07B+b+9QbFxi4pnPVsQeNMCDF/ld2gGYImP7Nfk0h2zqdLmMau/b+vr866HT5wDW/+P1s7MwIDAQAB";

        mHelper = new IabHelper(this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "In-app Billing setup failed: " + result);
                } else {
                    Log.d(TAG, "In-app Billing is set up OK");
                }
            }
        });
    }

    public void purchaseAnItem(String moduleID, BaseActivity activity, InAppPurchaseListenner listenner) {
        if (moduleID != null) {
            this.activity = activity;

            if (moduleID.equals(MLQPlusConstant.FULL_MODULES))
                ITEM_SKU = (ITEM_SKU + moduleID);
            else
                ITEM_SKU = (ITEM_SKU + "module" + moduleID);

            this.moduleID = moduleID;

            if (listenner != null)
                this.listenner = listenner;

            // Consume test item
            if (activity != null) {
                String purchaseToken = MLQPlusFunctions.getModuleToken(activity, moduleID);
                if (purchaseToken != null)
                    mHelper.consume(getPackageName(), purchaseToken);
            }

            if (mHelper != null)
                mHelper.flagEndAsync();

            mHelper.launchPurchaseFlow(this, ITEM_SKU, 10001,
                    mPurchaseFinishedListener, "mypurchasetoken");
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                return;
            } else if (purchase.getSku().equals(ITEM_SKU)) {
                onPurchasedSuccessfully(purchase, moduleID);
                if (activity != null)
                    MLQPlusFunctions.updatePurchaseModuleToken(activity, new PurchasedModuleToken(moduleID, purchase.getToken()));
            }

        }
    };

    public void onPurchasedSuccessfully(Purchase purchase, String moduleID) {
    }

    public void updateUI(String moduleID) {
        if (listenner != null)
            listenner.onPurchasedSuccessfully(moduleID);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null)
            mHelper.dispose();
        mHelper = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    public interface InAppPurchaseListenner {
        public void onPurchasedSuccessfully(String moduleID);
    }

}
*/
