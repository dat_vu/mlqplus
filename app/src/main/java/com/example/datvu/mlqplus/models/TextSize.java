package com.example.datvu.mlqplus.models;

import android.content.Context;


public enum TextSize {
	LARGER2 (0, "largest"),
	//LARGER1 (1, "largest0.5"),
	NORMAL (-1, "large"),
	SMALLER1 (-2, "small"),
	SMALLER2 (-3, "smallest"),;
	
    private final int TYPE;
    private String sizeInString;
    private TextSize(int type, String sizeInString) {
    	this.TYPE = type;
    	this.sizeInString=sizeInString;
    }
    
    public String getSizeInString()
    {
    	return sizeInString;
    }
    
    public int getType()
    {
    	return TYPE;
    }
    
	public int getSize(Context context, Size size) {
		return (int) context.getResources().getDimension(size.getSize())+ TYPE*3;
	}
}
