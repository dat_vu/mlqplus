package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.adapters.FreetextQuestionAdapter;
import com.example.datvu.mlqplus.models.ModuleActivity;
import com.example.datvu.mlqplus.models.ModuleFreeText;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@EViewGroup(R.layout.view_survey_freetext)
public class SurveyFreeTextView extends LinearLayout {

	private Context mContext;

	@ViewById
	WrapcontentListview lvFreetexts;
	
	@ViewById
	LinearLayout lnBackground;
	
	@ViewById
	View vPreventUpdateContent;
	
	private ModuleActivity surveyActivity;
	private ArrayList<ModuleFreeText> moduleFreeTexts;

	public SurveyFreeTextView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public SurveyFreeTextView(Context context, AttributeSet attrs, int defStyleAttr,
							  int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public SurveyFreeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public SurveyFreeTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}
	
	public void setQuestion(ModuleActivity moduleActivity)
	{			
		this.surveyActivity=moduleActivity;
		moduleFreeTexts=moduleActivity.getFreeText();
		
		// Parse answer string to array
		if (moduleActivity.getAnswer()!=null)
		{
			List<String> answers=getAnswersFromString(moduleActivity.getAnswer());
			if (answers!=null && answers.size()>0)
				for (int i=0; i< answers.size() && i<moduleFreeTexts.size(); i++)
					moduleFreeTexts.get(i).setAnswer(answers.get(i));
		}
		
		FreetextQuestionAdapter adapter=new FreetextQuestionAdapter((Activity)mContext, R.layout.item_survey_freetext, moduleFreeTexts, surveyActivity.getNumQuestion());
		lvFreetexts.setAdapter(adapter);
		
		/*if (SurveyGenerator.isSubmitted)
			vPreventUpdateContent.setVisibility(View.VISIBLE);*/
	}

	private List<String> getAnswersFromString(String stringAnswers)
	{
		try
		{
			Type listType = new TypeToken<List<String>>() {}.getType();
			return new Gson().fromJson(stringAnswers, listType);
		} catch(Exception e)
		{
			return null;
		}
	}
	
	public String getAnswers() {
		moduleFreeTexts=surveyActivity.getFreeText();
		if (moduleFreeTexts!=null && moduleFreeTexts.size()>0)
		{
			List<String> answers=new ArrayList<String>();
			
			/*for (int i=0; i<moduleFreeTexts.size(); i++)
			{
				View v=lvFreetexts.getChildAt(i);
				EditText edtAnswer=(EditText) v.findViewById(R.id.edtAnswer);
				if (edtAnswer!=null && edtAnswer.getText().toString().length()>0)
				{
					moduleFreeTexts.get(i).setAnswer(edtAnswer.getText().toString());
					answers.add(moduleFreeTexts.get(i).getAnswer());
				}
				return null;
			}*/
			for (ModuleFreeText freetext:moduleFreeTexts)
				if (freetext.getAnswer()!=null && freetext.getAnswer().length()>0)
				{
					answers.add(freetext.getAnswer());
				}
				else
					return null;
			return new Gson().toJson(answers);
		}
		return null;
	}
	
	@Click
	void lnBackground()
	{
		MLQPlusFunctions.hideKeyboard((Activity)mContext);
	}
}
