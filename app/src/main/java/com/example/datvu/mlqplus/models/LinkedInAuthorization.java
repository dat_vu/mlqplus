package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class LinkedInAuthorization implements Serializable{

	@SerializedName("access_token")
	private String mAccessToken;
	
	@SerializedName("expires_in")
	private long mExpiresIn;
	
	@SerializedName("expire_date")
	private long mExpireDate;
	
	@SerializedName("info")
	private LinkedInInfo mInfo;

	public long getExpireDate() {
		return mExpireDate;
	}

	public void setExpireDate(long expireDate) {
		this.mExpireDate = expireDate;
	}
	
	public String getAccessToken() {
		return mAccessToken;
	}

	public void setAccessToken(String accessToken) {
		this.mAccessToken = accessToken;
	}

	public long getExpiresIn() {
		return mExpiresIn;
	}

	public void setExpiresIn(long expiresIn) {
		this.mExpiresIn = expiresIn;
	}

	public LinkedInInfo getInfo() {
		return mInfo;
	}

	public void setInfo(LinkedInInfo info) {
		this.mInfo = info;
	}
	
	 public boolean isExpired() {
	        return  System.currentTimeMillis() > getExpireDate();
	    }
	
}
