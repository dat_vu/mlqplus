package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Module;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class TrustedLeaderProgramAdapter extends ArrayAdapter<Module> {
    private Activity mContext;
    private int layoutID;
    private ViewHolder viewHolder;

    public TrustedLeaderProgramAdapter(Activity context, int layoutID, List<Module> mALModule) {
        super(context, layoutID, mALModule);
        this.mContext = context;
        this.layoutID = layoutID;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(layoutID, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            viewHolder.tv_progress = (TextView) convertView
                    .findViewById(R.id.tv_progress);
            viewHolder.vModuleProgress = convertView
                    .findViewById(R.id.vModuleProgress);


            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.tv_name.setText(String.valueOf(getItem(position)
                .getName()));
        double process = (int) Math.round(getItem(position).getProgress());
        //if (process - (int)process == 0)
        // Show module progress
        viewHolder.tv_progress.setText(String.format("%.0f", process) + "%");
        LinearLayout.LayoutParams params = MLQPlusFunctions.getLayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
        params.weight = (int) process;
        viewHolder.vModuleProgress.setLayoutParams(params);
        /*else
			viewHolder.tv_progress.setText(String.format("%.2f", process) + "%");*/

        // Set text size
        MLQPlusFunctions.SetViewTextSize(viewHolder.tv_name, mContext, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(viewHolder.tv_progress, mContext, R.dimen.textMedium);

        return convertView;
    }

    private class ViewHolder {
        TextView tv_name;
        TextView tv_progress;
        View vModuleProgress;

    }

}
