package com.example.datvu.mlqplus.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ModuleActivity extends ObjectBase implements Serializable{
	@SerializedName("activityType")
	private String activityType;
	
	@SerializedName("isAnswered")
	private boolean isAnswered;
	
	@SerializedName("answer")
	private String answer;
	
	@SerializedName("text")
	private ModuleText text;
	
	@SerializedName("image")
	private ModuleImage image;
	
	@SerializedName("separator")
	private ModuleSeparator separator;
	
	@SerializedName("freeText")
	private ArrayList<ModuleFreeText> freeText;
	
	@SerializedName("tickBox")
	private ModuleTickBox tickBox;
	
	@SerializedName("ratingQuestion")
	private ModuleRatingQuestion ratingQuestion;
	
	@SerializedName("tapCards")
	private List<ModuleTapCard> tapCards;
	
	@SerializedName("quizQuestion")
	private ModuleQuizQuestion quizQuestion;
	
	private int numQuestion;
	
	public int getNumQuestion() {
		return numQuestion;
	}

	public void setNumQuestion(int numQuestion) {
		this.numQuestion = numQuestion;
	}

	public boolean isAnswered() {
		return isAnswered;
	}

	public void setAnswered(boolean isAnswered) {
		this.isAnswered = isAnswered;
	}

	public ModuleText getText() {
		return text;
	}

	public void setText(ModuleText text) {
		this.text = text;
	}

	public ModuleImage getImage() {
		return image;
	}

	public void setImage(ModuleImage image) {
		this.image = image;
	}

	public ModuleSeparator getSeparator() {
		return separator;
	}

	public void setSeparator(ModuleSeparator separator) {
		this.separator = separator;
	}

	public ArrayList<ModuleFreeText> getFreeText() {
		return freeText;
	}

	public void setFreeText(ArrayList<ModuleFreeText> freeText) {
		this.freeText = freeText;
	}

	public ModuleTickBox getTickBox() {
		return tickBox;
	}

	public void setTickBox(ModuleTickBox tickBox) {
		this.tickBox = tickBox;
	}

	public ModuleRatingQuestion getRatingQuestion() {
		return ratingQuestion;
	}

	public void setRatingQuestion(ModuleRatingQuestion ratingQuestion) {
		this.ratingQuestion = ratingQuestion;
	}

	public List<ModuleTapCard> getTapCards() {
		return tapCards;
	}

	public void setTapCards(List<ModuleTapCard> tapCards) {
		this.tapCards = tapCards;
	}

	public ModuleQuizQuestion getQuizQuestion() {
		return quizQuestion;
	}

	public void setQuizQuestion(ModuleQuizQuestion quizQuestion) {
		this.quizQuestion = quizQuestion;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	
}
