/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.activity.ViewPDFscreenActivity;
import com.mlqplus.trustedleader.activity.ViewPDFscreenActivity_;
import com.mlqplus.trustedleader.adapter.AssessmentAdapter;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.Assessment;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;

@EFragment(R.layout.fragment_assessments)
public class FragmentAssessment extends FragmentBase {
	public static FragmentAssessment newInstance() {
		return new FragmentAssessment_();
	}    
   
	@ViewById 
	LinearLayout ln_child; 
  
	@ViewById
	TextView tv_title;

	@ViewById
	ListView lv_assessment;
	
	@ViewById
	WaitingView vWaiting;

	private ArrayList<Assessment> mLAssessment;
	private AssessmentAdapter mAssessmentAdapter;
	private int getDataFailedCount;
	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();
		getDataFailedCount=0;

		// Set text size for all view
		setTextSize();

		// Get Data
		getData();
	}

	private void getData() {
		if (!MLQPlusFunctions.isInternetAvailable())
			((BaseActivity)mActivity).showToast(MLQPlusConstant.CONNECTION_LOST);
		else
		{
			// Only show at the first time
			if (getDataFailedCount==0)
				vWaiting.showWaitingView();
			
			MLQPlusService.getBSSServices().getAsessments(MLQPlusFunctions.getAccessToken(), new Callback<ApiResponse<List<Assessment>>>() {
				
				@Override
				public void success(ApiResponse<List<Assessment>> assessments, Response arg1) {
					// TODO Auto-generated method stub
					
					if (assessments!=null && assessments.getData()!=null && assessments.getData().size()>0)
					{
						mLAssessment=new ArrayList<Assessment>();
						mLAssessment.addAll(assessments.getData());
						
						// Load images before showing data
						List<String> images=new ArrayList<String>();
						for  (Assessment assessment:mLAssessment)
							images.add(assessment.getIcon());
						
						loadImages(images, mActivity);
						showData();
					}
					else
					{
						// Retry getting data
						getDataFailedCount++;
						if (getDataFailedCount<3)
							getData();
						else
							vWaiting.hideWaitingView();
					}
				}
				
				@Override
				public void failure(RetrofitError error) {
					// Retry getting data
					getDataFailedCount++;
					if (getDataFailedCount<3)
						getData();
					else
					{
						vWaiting.hideWaitingView();
						if (error.getCause() instanceof SocketTimeoutException)
							showToast("Load assessments failed: "+MLQPlusConstant.TIMEOUT);
						else
							showToast("Sorry! There is something wrong. Load assessments failed");
					}
				}
			});
		}
	}

	private void showData() {
		if (mLAssessment!=null)
		{
			mAssessmentAdapter = new AssessmentAdapter(mActivity, R.layout.item_assessment, mLAssessment);
			lv_assessment.setAdapter(mAssessmentAdapter);
			lv_assessment.setSelector(R.drawable.btn_first_quiz);
		}
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}
	
	@ItemClick
	void lv_assessment(int pos)
	{
		if (pos<mLAssessment.size() && mLAssessment.get(pos).getPdf_link()!=null)
		{
			
			Bundle bundle=new Bundle();
			bundle.putSerializable(ViewPDFscreenActivity.ASSESSMENT, mLAssessment.get(pos));
			((BaseActivity)mActivity).startNewActivityNoAnimation(mActivity, ViewPDFscreenActivity_.class, false, bundle);
		}
		else
			((BaseActivity)mActivity).showToast("Cannot get PDF link");
	}
	
	@Override
	public void voidAfterLoadImages() {
		super.voidAfterLoadImages();
		vWaiting.hideWaitingView();
		
		showData();
	}
	
}
*/
