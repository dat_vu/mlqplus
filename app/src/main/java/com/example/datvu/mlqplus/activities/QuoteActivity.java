/*
package com.example.datvu.mlqplus.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.fragment.FragmentQuote_;
import com.mlqplus.trustedleader.model.Quote;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_quote)
public class QuoteActivity extends BaseDialogActivity {

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    TextView tv_quote;

    @ViewById
    ViewPager view_pager;

    @ViewById
    com.mlqplus.trustedleader.view.MyTabPostionView tab_position;


    public static String LIST_QUOTE = "ListQuote";
    public static String QUOTE_NAME = "QuoteName";
    private String quoteName;
    private List<Quote> mLQuote = null;

    @AfterViews
    void init() {
        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        // Get Data
        getData();

        // Show Data
        showData();
    }

    private void getData() {
        try {
            mLQuote = (List<Quote>) getIntent().getBundleExtra(
                    MLQPlusConstant.BUNDLE).getSerializable(LIST_QUOTE);
            quoteName = getIntent().getBundleExtra(
                    MLQPlusConstant.BUNDLE).getString(QUOTE_NAME);
        } catch (Exception e) {
        }
    }

    private void showData() {
        tv_quote.setText(quoteName);

        FragmentManager fm = getFragmentManager();
        FragmentPagerAdapter quoteFragmentPagerAdapter = new FragmentPagerAdapter(
                fm) {

            @Override
            public int getCount() {
                return mLQuote.size();
            }

            @Override
            public Fragment getItem(int pos) {
                FragmentQuote_ fragmentQuote_ = new FragmentQuote_();
                fragmentQuote_.setData(mLQuote.get(pos));
                return fragmentQuote_;
            }

        };

        view_pager.setAdapter(quoteFragmentPagerAdapter);
        tab_position.setTabAlpha(1f);

        if (mLQuote != null && mLQuote.size() > 1) {
            tab_position.setSeletedTab(mLQuote.size(), 0);
            view_pager.setOnPageChangeListener(new OnPageChangeListener() {
                public void onPageScrollStateChanged(int state) {
                }

                public void onPageScrolled(int position, float positionOffset,
                                           int positionOffsetPixels) {
                }

                public void onPageSelected(int position) {
                    tab_position.setSeletedTab(mLQuote.size(), position);
                }
            });
        }
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_quote, QuoteActivity.this,
                R.dimen.textLarge);

    }

    @Click
    void lnClose() {
        this.finish();
    }
}
*/
