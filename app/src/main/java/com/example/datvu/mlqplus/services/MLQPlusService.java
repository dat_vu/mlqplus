/*
package com.example.datvu.mlqplus.services;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mlqplus.trustedleader.model.ModuleActivity;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

public class MLQPlusService {
	private static RequestInterceptor requestInterceptor;
	private static RestAdapter restAdapter;
	private static Gson mGson;

	private static String API_URL="http://leadership-api-dev.demo.beesightsoft.com/public";
	//private static String API_URL="http://leadership.api.demo.beesightsoft.com:80/public";
	
	private static RequestInterceptor getRequestInterceptor() {
		if (requestInterceptor == null) {
			requestInterceptor = new RequestInterceptor() {
	    		  @Override
	    		  public void intercept(RequestFacade request) {    			  
	    		    request.addHeader("Content-Type", "application/json");  
	    		    request.addHeader("Accept", "application/json");
	    		  }		  
	    	};
		}
		return requestInterceptor;
	}
	
	private static RestAdapter getRestAdapter () {
		if (restAdapter == null) {	
			OkHttpClient okHttpClient=new OkHttpClient();
			okHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);
			okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);

			restAdapter = new RestAdapter.Builder().setLogLevel (RestAdapter.LogLevel.FULL)
					.setEndpoint(API_URL)
					.setRequestInterceptor(getRequestInterceptor())
					.setClient(new OkClient(okHttpClient))
					.setLogLevel(LogLevel.FULL)					
					.setConverter(new GsonConverter(getGson())).build();
		}
		return restAdapter;
	}

	
	public static Gson getGson() {		
		if (mGson == null) {	
			mGson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
					.registerTypeAdapter(ModuleActivity.class, new ModuleActivityDeserializer())
					.create();
		}
		return mGson;
	}
	
    public static MLQPlusAPIs getBSSServices() {    	
        return getRestAdapter().create(MLQPlusAPIs.class);
    }
}

*/
