package com.example.datvu.mlqplus.models;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class ConfigMenu {
	@SerializedName("settings")
	private ArrayList<ConfigMenuBase> settings;

	@SerializedName("tools")
	private ArrayList<ConfigMenuBase> tools;

	@SerializedName("fun")
	private ArrayList<ConfigMenuBase> fun;
	
	@SerializedName("main")
	private ArrayList<ConfigMenuBase> main;

	@SerializedName("misc")
	private MISC misc;

	public ArrayList<ConfigMenuBase> getSettings() {
		return settings;
	}

	public void setSettings(ArrayList<ConfigMenuBase> settings) {
		this.settings = settings;
	}

	public ArrayList<ConfigMenuBase> getTools() {
		return tools;
	}

	public void setTools(ArrayList<ConfigMenuBase> tools) {
		this.tools = tools;
	}

	public ArrayList<ConfigMenuBase> getFun() {
		return fun;
	}

	public void setFun(ArrayList<ConfigMenuBase> fun) {
		this.fun = fun;
	}

	public MISC getMisc() {
		return misc;
	}

	public void setMisc(MISC misc) {
		this.misc = misc;
	}

	public ArrayList<ConfigMenuBase> getMain() {
		return main;
	}

	public void setMain(ArrayList<ConfigMenuBase> main) {
		this.main = main;
	}
}
