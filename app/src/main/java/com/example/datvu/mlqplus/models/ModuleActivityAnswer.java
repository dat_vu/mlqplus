package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class ModuleActivityAnswer {
	@SerializedName("activity_id")
	private String activity_id;
	
	@SerializedName("answer_content")
	private String answer_content;
	
	@SerializedName("module_id")
	private String module_id;

	public String getActivity_id() {
		return activity_id;
	}

	public void setActivity_id(String activity_id) {
		this.activity_id = activity_id;
	}

	public String getAnswer_content() {
		return answer_content;
	}

	public void setAnswer_content(String answer_content) {
		this.answer_content = answer_content;
	}

	public String getModule_id() {
		return module_id;
	}

	public void setModule_id(String module_id) {
		this.module_id = module_id;
	}

	public ModuleActivityAnswer(String activity_id, String answer_content,
			String module_id) {
		super();
		this.activity_id = activity_id;
		this.answer_content = answer_content;
		this.module_id = module_id;
	}
	
}
