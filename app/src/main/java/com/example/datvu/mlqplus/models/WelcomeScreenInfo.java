package com.example.datvu.mlqplus.models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WelcomeScreenInfo extends ObjectBase implements Serializable {
	@SerializedName("text")
	private String text;

	@SerializedName("background")
	private String background;

	@SerializedName("icon")
	private String icon;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public WelcomeScreenInfo(String text, String background, String icon) {
		super();
		this.text = text;
		this.background = background;
		this.icon = icon;
	}

}
