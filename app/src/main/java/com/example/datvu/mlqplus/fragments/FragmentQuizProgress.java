/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.activity.MainActivity;
import com.mlqplus.trustedleader.activity.MainActivity_;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.UserProgress;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;

@EFragment(R.layout.fragment_quiz_progress)
public class FragmentQuizProgress extends FragmentBase {
	public static FragmentQuizProgress newInstance() {
		return new FragmentQuizProgress_();
	}

	@ViewById
	LinearLayout ln_child;

	@ViewById
	WaitingView vWaiting;

	@ViewById
	TextView tv_your_quiz_rank, tv_rank, tv_best_score_result,
			tv_best_score_des, tv_average_score_result, tv_average_score_des,
			tv_attempts_result, tv_attempts_des, tv_complete_the_trusted;

	@ViewById
	FrameLayout flAttempts, flBestscore, flAvg;

	@ViewById
	ImageView imgAttemptsBg, imgBestscoreBg, imgAvgBg;

	private Activity mActivity;
	private boolean isLoading = false;
	private static UserProgress userProgress;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Set text size for all view
		setTextSize();

		// getData
		getData();
	}

	private void getData() {
		if (!isLoading) {
			if (!MLQPlusFunctions.isInternetAvailable())
				((BaseActivity) mActivity)
						.showToast(MLQPlusConstant.CONNECTION_LOST);
			else {
				isLoading = true;

				if (userProgress == null)
					vWaiting.showWaitingView();
				else
					vWaiting.hideWaitingView();

				MLQPlusService.getBSSServices().getUserProgress(
						MLQPlusFunctions.getAccessToken(),
						new Callback<ApiResponse<UserProgress>>() {

							@Override
							public void success(
									ApiResponse<UserProgress> mUserProgress,
									Response arg1) {
								// TODO Auto-generated method stub
								isLoading = false;
								vWaiting.hideWaitingView();

								if (mUserProgress != null
										&& mUserProgress.getData() != null) {
									userProgress = mUserProgress.getData();
									showData();
								}
							}

							@Override
							public void failure(RetrofitError error) {
								// TODO Auto-generated method stub
								isLoading = false;
								vWaiting.hideWaitingView();
								try {
									if (error.getCause() instanceof SocketTimeoutException)
										showToast("Get progress failed: "
												+ MLQPlusConstant.TIMEOUT);
									else
										showToast("Get progress failed: "
												+ error.getMessage());
								} catch (Exception e) {
								}
							}
						});
			}
		}
	}

	private void showData() {
		tv_best_score_result.setText(userProgress.getBest_score() + "/10");
		tv_attempts_result.setText(userProgress.getAttemp() + "");
		tv_average_score_result.setText((int) Math.round(userProgress
				.getAverage_score()) + "/10");
		tv_rank.setText(userProgress.getQuiz_rank());

		setOnTouchCircles();
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_complete_the_trusted, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_your_quiz_rank, mActivity,
				R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(tv_rank, mActivity, R.dimen.textLarge);

	}

	private void setOnTouchCircles() {
		flBestscore.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return onCircleTouch(v, imgBestscoreBg, event);
			}
		});

		flAttempts.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return onCircleTouch(v, imgAttemptsBg, event);
			}
		});

		flAvg.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return onCircleTouch(v, imgAvgBg, event);
			}
		});
	}

	private boolean onCircleTouch(View parent, View vBackGroundFocusing,
			MotionEvent event) {
		float radius = vBackGroundFocusing.getHeight() / 2;

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			// change to being clicked background
			if (MLQPlusFunctions.distance(event.getX(), event.getY(), radius,
					radius) <= radius) {
				if (parent.getParent() != null) {
					parent.getParent().requestDisallowInterceptTouchEvent(true);
				}
				vBackGroundFocusing.setVisibility(View.VISIBLE);
			}
			break;

		case MotionEvent.ACTION_MOVE:
			// Check and change to normal backgroud when mouse is out of
			// border
			if (MLQPlusFunctions.distance(event.getX(), event.getY(), radius,
					radius) > radius)
				vBackGroundFocusing.setVisibility(View.INVISIBLE);
			else
				vBackGroundFocusing.setVisibility(View.VISIBLE);
			break;

		case MotionEvent.ACTION_UP:
			if (MLQPlusFunctions.distance(event.getX(), event.getY(), radius,
					radius) <= radius) {
				if (!MLQPlusFunctions.isInternetAvailable()) {
					((BaseActivity) mActivity)
							.showToast("Please check your internet connection and try again");
					vBackGroundFocusing.setVisibility(View.INVISIBLE);
					return true;
				}

				if (mActivity instanceof MainActivity) {
					FragmentQuizzes fragment = FragmentQuizzes.newInstance();
					FragmentQuizzes.isStartFromFunScreen = false;
					((MainActivity) mActivity).replaceFragment(fragment, true);
				}

				vBackGroundFocusing.setVisibility(View.INVISIBLE);
			}
			break;
		}
		return true;
	}

}
*/
