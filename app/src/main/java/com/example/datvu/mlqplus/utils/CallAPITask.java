package com.example.datvu.mlqplus.utils;

import android.os.Handler;

import com.example.datvu.mlqplus.models.ModuleActivityEvent;


public class CallAPITask {

    private boolean isInWaiting;
    private Handler handler;
    private Runnable callAPI;
    private ModuleActivityEvent moduleActivityEvent;

    /*public CallAPITask(final ModuleActivityEvent event, final Context context, final String moduleID) {
        // TODO Auto-generated constructor stub
        isInWaiting = true;
        this.moduleActivityEvent = event;
        handler = new Handler();

        callAPI = new Runnable() {
            public void run() {
                isInWaiting = false;
                MLQPlusFunctions.submitActivityAnswer(event, moduleID, context);
            }
        };
        handler.postDelayed(callAPI, 2000);
    }*/

    public void cancelCallAPI() {
        handler.removeCallbacks(callAPI);
    }

    public boolean isWaiting() {
        return isInWaiting;
    }

    public ModuleActivityEvent getModuleActivityEvent() {
        return moduleActivityEvent;
    }
}
