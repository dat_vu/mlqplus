package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.TickboxQuestion;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.ArrayList;

public class TickboxQuestionAdapter extends BaseAdapter {
    private Activity mContext;
    private int mLayoutID;
    private ArrayList<TickboxQuestion> mALTickboxQuestion = null;
    private ViewHolder mViewHolder = null;
    private int textColor;

    public TickboxQuestionAdapter(Activity mContext, int mLayoutID,
                                  ArrayList<TickboxQuestion> mALTickboxQuestion, int textColor) {
        this.mContext = mContext;
        this.mLayoutID = mLayoutID;
        this.textColor = textColor;
        this.mALTickboxQuestion = mALTickboxQuestion;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(mLayoutID, parent, false);
            mViewHolder = new ViewHolder();
            mViewHolder.img_tickbox = (ImageView) convertView
                    .findViewById(R.id.img_tickbox);
            mViewHolder.tv_answer = (TextView) convertView
                    .findViewById(R.id.tv_answer);

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        TickboxQuestion TickboxQuestion = mALTickboxQuestion.get(position);

        if (TickboxQuestion.isSelected())
            mViewHolder.img_tickbox
                    .setImageResource(R.drawable.tickbox_selected);
        else
            mViewHolder.img_tickbox
                    .setImageResource(R.drawable.tickbox_unselected);

        mViewHolder.tv_answer.setText(TickboxQuestion.getAnswer());

        // Set size for widgets
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_answer, mContext,
                R.dimen.textMedium);

        if (textColor != 0)
            mViewHolder.tv_answer.setTextColor(textColor);

        return convertView;
    }

    @Override
    public int getCount() {
        return mALTickboxQuestion.size();
    }

    @Override
    public Object getItem(int position) {
        return mALTickboxQuestion.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        ImageView img_tickbox;
        TextView tv_answer;
    }

}
