package com.example.datvu.mlqplus.models;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class ModuleActivities implements Serializable{
	@SerializedName("activities")
	private ArrayList<ModuleActivity> activities;

	public ArrayList<ModuleActivity> getActivities() {
		return activities;
	}

	public void setActivities(ArrayList<ModuleActivity> activities) {
		this.activities = activities;
	}
}
