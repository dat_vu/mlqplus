package com.example.datvu.mlqplus.activities;


import android.annotation.SuppressLint;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.example.datvu.mlqplus.utils.MLQPlusConstant;

public class BaseDialogActivity extends BaseActivity {
    public void setDialogSize(LinearLayout ln_root, float widthPercent, float heightPercent) {
        ln_root.getLayoutParams().width = (int) (MLQPlusConstant.SCREEN_WIDTH * widthPercent / 100);
        ln_root.getLayoutParams().height = (int) (MLQPlusConstant.SCREEN_HEIGHT * heightPercent / 100);
    }

    @SuppressLint("NewApi")
    public void getScreenSize() {
        if (Build.VERSION.SDK_INT >= 11) {
            Point size = new Point();
            try {
                this.getWindowManager().getDefaultDisplay().getRealSize(size);
                MLQPlusConstant.SCREEN_WIDTH = size.x;
                MLQPlusConstant.SCREEN_HEIGHT = size.y;
            } catch (NoSuchMethodError e) {
                Log.e("error", "Can't get screen size");
            }

        } else {
            DisplayMetrics metrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            MLQPlusConstant.SCREEN_WIDTH = metrics.widthPixels;
            MLQPlusConstant.SCREEN_HEIGHT = metrics.heightPixels;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
