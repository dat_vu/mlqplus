/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.Handler;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.adapter.FunAdapter;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.ConfigMenu;
import com.mlqplus.trustedleader.model.ConfigMenuBase;
import com.mlqplus.trustedleader.model.Tool;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;
 
@EFragment(R.layout.fragment_fun)
public class FragmentFun extends FragmentBase {
	public static FragmentFun newInstance() {
		return new FragmentFun_();
	}  
  
	@ViewById  
	LinearLayout ln_child; 
	  
	@ViewById
	ListView lvFuns;
 
	@ViewById
	TextView tv_title;
	
	@ViewById
	WaitingView vWaiting;

	private Activity mActivity;
	private ArrayList<Tool> mLFuns;
	private FunAdapter mFunsAdapter = null;
	private String QUIZZES="quizzes", QUOTES="quotes", SURVEY="surveys";

	@SuppressLint("NewApi")
	@AfterViews
	void init() { 
		// getActivity
		mActivity = getActivity();
		
		// Set text size for all view
		setTextSize();

		// 	Get Data
		getData();
	}
	
	private void getData() {	
		vWaiting.showWaitingView();
		if (MLQPlusConstant.CONFIG_MENU==null)
		{
			if (!MLQPlusFunctions.isInternetAvailable())
				((BaseActivity)mActivity).showToast(MLQPlusConstant.CONNECTION_LOST);
			else {
				
				MLQPlusService.getBSSServices().getConfigMenu(MLQPlusFunctions.getAccessToken(), new Callback<ApiResponse<ConfigMenu>>() {
					
					@Override
					public void success(ApiResponse<ConfigMenu> configs, Response arg1) {
						// TODO Auto-generated method stub
						if (configs!=null && configs.getData()!=null)
						{
							MLQPlusConstant.CONFIG_MENU=configs.getData();
							getFragmentFunsData();
						}
					}
					
					@Override
					public void failure(RetrofitError error) {
						// TODO Auto-generated method stub
						vWaiting.hideWaitingView();
						if (error.getCause() instanceof SocketTimeoutException)
							showToast("Load config settings: "+MLQPlusConstant.TIMEOUT);
					}
				});
			}
		}
		else
			getFragmentFunsData();
	}
	
	private void getFragmentFunsData()
	{
		mLFuns=new ArrayList<Tool>();
		addSuitableItem(QUIZZES);	
		addSuitableItem(QUOTES);	
		addSuitableItem(SURVEY);	
		
		// Load images before showing data
		List<String> images=new ArrayList<String>();
		for  (Tool tool:mLFuns)
			images.add(tool.getIcon());
		
		showData();
		
		loadImages(images, mActivity);
	}
	
	private void addSuitableItem(String key)
	{
		for (ConfigMenuBase item:MLQPlusConstant.CONFIG_MENU.getFun())
			if (item.getConfig_name().toUpperCase().equals(key.toUpperCase()))
				mLFuns.add(new Tool(item));
	}

	private void showData() {
		mFunsAdapter = new FunAdapter(mActivity, R.layout.item_fun, mLFuns);
		lvFuns.setAdapter(mFunsAdapter);
		lvFuns.setSelector(R.drawable.btn_first_quiz);
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}
	
	@ItemClick
	void lvFuns(int pos)
	{
		Tool tool = (Tool) lvFuns.getItemAtPosition(pos);
		Fragment frag = null;
		if (tool.getConfig_name().toUpperCase().equals(QUIZZES.toUpperCase())) 
		{
			FragmentQuizzes.isStartFromFunScreen=true;
			frag = new FragmentQuizzes_();
		}
		else if (tool.getConfig_name().toUpperCase().equals(QUOTES.toUpperCase()))
			frag =  new FragmentLeadershipQuotes_();
		else if (tool.getConfig_name().toUpperCase().equals(SURVEY.toUpperCase()))
			frag = new FragmentSurveys_();
		
		try {
			MLQPlusFunctions.replaceFragment(mActivity, frag, true);
		} catch (Exception e) {
			showToast(e.getMessage());
		}
	}
	
	@Override
	public void voidAfterLoadImages() {
		super.voidAfterLoadImages();
		vWaiting.hideWaitingView();
		
		showData();
	}

}
*/
