/*
package com.example.datvu.mlqplus.activities;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.util.Base64;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.model.Module;
import com.mlqplus.trustedleader.model.Quote;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_testscreen)
public class TestscreenActivity extends BaseActivity implements ImageGetter {

    @ViewById
    LinearLayout ln_root, ln_content;

    @ViewById
    RelativeLayout rl_child;

    @ViewById
    TextView tv_test;

    @AfterViews
    void init() {
        getScreenSize();

        // Creat zoomable layout
        MLQPlusFunctions.makeZoomableView(TestscreenActivity.this, ln_root,
                rl_child);

        // Set text size for all view
        setTextSize();

        // Show Data
        showData();
    }

    private void showData() {
        String src = "<div style=\"width: 100%; float: left;background:red;\"><div style=\"background:blue;width: 10%; float: left;\"><img alt=\"\" src=\"http://local.beesightsoft.com:9696/hosting/logo.png\" style=\"height:auto; max-width:100%\" /></div><div style=\"width: 90%; float: left;\">Test Article, Test Article, Test Article, Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article,Test Article</div></div>";
        tv_test.setText(Html.fromHtml(src, new ImageGetter() {
            @Override
            public Drawable getDrawable(String source) {
                source = "R0lGODlhDwAPAKECAAAAzMzM/////wAAACwAAAAADwAPAAACIISPeQHsrZ5ModrLlN48CXF8m2iQ3YmmKqVlRtW4MLwWACH+H09wdGltaXplZCBieSBVbGVhZCBTbWFydFNhdmVyIQAAOw==";
                byte[] data = Base64.decode(source, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0,
                        data.length);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(
                        getResources(), bitmap);
                bitmapDrawable.setBounds(0, 0, 30, 30);
                return bitmapDrawable;
            }
        }, null));
        // tv_test.setText(Html.fromHtml("<p style=\"text-align:justify;color:red;\">text dfdsf dffds fd sf ds f sd f sd f sd fds fds fds g dfg fd gghgf hgf gfh gfhgfh fg hgfh gfhgfhgf gfhgf hgfh fghgfhgf hgfh fghgfhgfhg ghf hgfhgfhgf hgfh gfhgfh df gfd g dfg fd</p>"));

        String question = "Question question  question question question question question question question question question";
        // Face answers list
        List<String> answers = new ArrayList<String>();
        for (int i = 0; i < 4; i++)
            answers.add("Answer " + i);

*/
/*		QuestionStyleCheckBoxView questionStyleCheckBoxView = QuestionStyleCheckBoxView_
                .build(this);
		questionStyleCheckBoxView.setTextSize();
		questionStyleCheckBoxView.setQuestion(question, answers);
		ln_content.addView(questionStyleCheckBoxView);*//*


		*/
/*QuestionStyleFreeTextView questionStyleFreeTextView = QuestionStyleFreeTextView_
				.build(this);
		questionStyleFreeTextView.setQuestion(question, "Enter your answer...");
		ln_content.addView(questionStyleFreeTextView);*//*


*/
/*		QuestionStyleRatingView questionStyleRatingView = QuestionStyleRatingView_
				.build(this);
		questionStyleRatingView.setTextSize();
		questionStyleRatingView.setQuestion(question, answers);
		ln_content.addView(questionStyleRatingView);*//*


    }

    private void setTextSize() {
		*/
/*
		 * MLQPlusFunctions.SetViewTextSize(tv_learn_more,
		 * TestscreenActivity.this, Size.LARGE);
		 * MLQPlusFunctions.SetViewTextSize(tv_forgot_password,
		 * TestscreenActivity.this, Size.MEDIUM);
		 * MLQPlusFunctions.SetViewTextSize(tv_not_a_member,
		 * TestscreenActivity.this, Size.MEDIUM);
		 * 
		 * MLQPlusFunctions.SetViewTextSize(btn_login, TestscreenActivity.this,
		 * Size.MEDIUMLARGE);
		 * 
		 * MLQPlusFunctions.SetViewTextSize(edt_email, TestscreenActivity.this,
		 * Size.MEDIUM); MLQPlusFunctions.SetViewTextSize(edt_password,
		 * TestscreenActivity.this, Size.MEDIUM);
		 * 
		 * MLQPlusFunctions.SetViewTextSize(chb_remember_me,
		 * TestscreenActivity.this, Size.MEDIUM);
		 *//*

    }

    @SuppressWarnings("deprecation")
    @Override
    public Drawable getDrawable(String source) {
        // TODO Auto-generated method stub
        Drawable d = null;
        try {
            d = getResources().getDrawable(R.drawable.cb_selected);
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
        } catch (Resources.NotFoundException e) {
            Log.e("log_tag", "Image not found. Check the ID.", e);
        } catch (NumberFormatException e) {
            Log.e("log_tag", "Source string not a valid resource ID.", e);
        }

        return d;
    }

    @Click
    void tv_test() {
        // fake Data to show 5 module detail

        //startNewActivity(TestscreenActivity.this, QuoteActivity_.class, false,
        //getBundle10QuotesDetail(), 0, 0);
        //startNewActivity(TestscreenActivity.this, ModuleInformationscreenActivity_.class, false,
        //getBundle5ModulesDetail(), 0, 0);
        // startNewActivity(TestscreenActivity.this,
        // ModuleInformationscreenActivity_.class, false,
        // getBundleAModulesDetail(), 0, 0);

    }

    private Bundle getBundle5ModulesDetail() {
        ArrayList<Module> modules = new ArrayList<Module>();
        for (int i = 0; i < 5; i++)
            modules.add(new Module(
                    "Name " + (i + 1),
                    "",
                    i % 2 == 0,
                    i % 4 == 0,
                    0.99,
                    "description description description description description description description description description description description description description"));

        Module fullProgram = new Module("Full Program", "", true, false, 2.99,
                "");

        Bundle bundle = new Bundle();
        bundle.putSerializable(ModuleInformationscreenActivity.LIST_MODULES,
                modules);
        bundle.putSerializable(ModuleInformationscreenActivity.FULL_PROGRAM,
                fullProgram);
        return bundle;
    }

    private Bundle getBundle10QuotesDetail() {
        ArrayList<Quote> mALQuote = new ArrayList<Quote>();
        for (int i = 0; i < 10; i++)
            mALQuote.add(new Quote(
                    "\"If your actions inspire other to dream more, learn more, do more and become more, your are a leader\"",
                    "-John Wincy Adams"));

        Bundle bundle = new Bundle();
        bundle.putSerializable(QuoteActivity.LIST_QUOTE,
                mALQuote);
        return bundle;
    }

    private Bundle getBundleAModulesDetail() {
        ArrayList<Module> modules = new ArrayList<Module>();
        modules.add(new Module(
                "Name ",
                "",
                true,
                false,
                0.99,
                "description description description description description description description description description description description description description"));

        Bundle bundle = new Bundle();
        bundle.putSerializable(ModuleInformationscreenActivity.LIST_MODULES,
                modules);
        return bundle;
    }
}
*/
