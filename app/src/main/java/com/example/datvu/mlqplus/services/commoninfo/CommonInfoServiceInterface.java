package com.example.datvu.mlqplus.services.commoninfo;

import com.example.datvu.mlqplus.models.Country;

import java.util.List;

import bolts.Task;

/**
 * Created by solit_000 on 4/14/2016.
 */
public interface CommonInfoServiceInterface {
    Task<List<Country>> getCountryList();
}
