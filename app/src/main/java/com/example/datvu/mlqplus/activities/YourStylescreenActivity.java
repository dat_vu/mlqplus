package com.example.datvu.mlqplus.activities;


import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.LeadershipStyle;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.example.datvu.mlqplus.views.CircleImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_yourstylescreen)
public class YourStylescreenActivity extends BaseDialogActivity {

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    TextView tv_your_style, tv_the_results_are_in, tv_your_leadership_style,
            tv_charismatic, tv_charismatic_content;

    @ViewById
    Button btn_learn_more;

    @ViewById
    CircleImageView img_avatar;

    public static String LEADERSHIP_STYLE = "leadershipStyle";
    public static boolean isStartFromLeaderQuiz = false;

    @AfterViews
    void init() {
        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        // Reset userClickedGetStarted in the next Screen
        getSharedPreferencesService().userClickedGetStarted().put(false);

        // Show data
        getAndShowData();
    }

    private void getAndShowData() {
        LeadershipStyle leadershipStyle = (LeadershipStyle) getIntent()
                .getBundleExtra(MLQPlusConstant.BUNDLE).get(LEADERSHIP_STYLE);
        tv_charismatic.setText(leadershipStyle.getName());
        tv_charismatic_content.setText(leadershipStyle.getDescription());
        MLQPlusFunctions.showImage(YourStylescreenActivity.this,
                leadershipStyle.getIcon(), img_avatar);

        tv_charismatic_content.setMovementMethod(new ScrollingMovementMethod());
        /*if (tv_charismatic_content != null) {
			final Layout layout = tv_charismatic_content.getLayout();
			if (layout != null) {
				int scrollDelta = layout.getLineBottom(tv_charismatic_content
						.getLineCount() - 1)
						- tv_charismatic_content.getScrollY()
						- tv_charismatic_content.getHeight();
				if (scrollDelta > 0)
					tv_charismatic_content.scrollBy(0, scrollDelta);
			}
		}*/
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_your_style,
                YourStylescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_the_results_are_in,
                YourStylescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_your_leadership_style,
                YourStylescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_charismatic,
                YourStylescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_charismatic_content,
                YourStylescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btn_learn_more,
                YourStylescreenActivity.this, R.dimen.textMedium);
    }

   /* @Click
    void btn_learn_more() {
        if (!isStartFromLeaderQuiz)
            startNewZoomActivity(YourStylescreenActivity.this,
                    LearnMorescreenActivity_.class, true);
        else {
            // Reset userClickedGetStarted in the next Screen
            getSharedPreferencesService().userClickedGetStarted().put(true);
            finish();
        }
    }*/
}
