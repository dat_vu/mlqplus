package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class LeaderQuizResult implements Serializable{
	@SerializedName("quizzes_category_id")
	private String quizzes_category_id;

	@SerializedName("user_id")
	private String user_id;
	
	@SerializedName("score")
	private int score;

	public String getQuizzes_category_id() {
		return quizzes_category_id;
	}

	public void setQuizzes_category_id(String quizzes_category_id) {
		this.quizzes_category_id = quizzes_category_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
}
