/*
package com.example.datvu.mlqplus.fragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.LeaderQuizscreenActivity;
import com.mlqplus.trustedleader.activity.ModuleQuizscreenActivity;
import com.mlqplus.trustedleader.model.ModuleQuestion;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.QuizQuestionStyleView;
import com.mlqplus.trustedleader.view.QuizQuestionStyleView_;

@EFragment(R.layout.fragment_module_quiz)
public class FragmentModuleQuiz extends FragmentBase {
	public static FragmentModuleQuiz newInstance() {
		return new FragmentModuleQuiz_(); 
	}   
  
	@ViewById
	LinearLayout ln_root, lnSubmit;
	
	@ViewById
	Button btnSubmit;
	
	@ViewById 
	ScrollView scv;
	 
	private ModuleQuestion moduleQuestion;
	private int pos;
	private int questionsSize;	    
	private Activity mActivity;
	
	@SuppressLint("NewApi") @AfterViews
	void init()
	{
		// getActivity
		mActivity=getActivity();
		
		// Set text size
		setTextSize();
		
		// Show data
		showData();
	}
	
	public void setData(ModuleQuestion moduleQuestion, int pos, int questionsSize)
	{
		this.moduleQuestion=moduleQuestion;
		this.pos=pos;
		this.questionsSize=questionsSize;
	}
	
	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(btnSubmit, mActivity, R.dimen.textMedium);
	}
	
	private void showData()
	{
		LinearLayout lnContent=new LinearLayout(mActivity);
		LinearLayout.LayoutParams params=MLQPlusFunctions.getLayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		lnContent.setOrientation(LinearLayout.VERTICAL);
		lnContent.setLayoutParams(params);
		
		QuizQuestionStyleView quizQuestionStyleView=QuizQuestionStyleView_.build(mActivity);
		quizQuestionStyleView.setQuestion(moduleQuestion, pos);
		lnContent.addView(quizQuestionStyleView);
		
		// Add button submit
			if (pos==questionsSize-1)
				lnSubmit.setVisibility(View.VISIBLE);
						
		scv.addView(lnContent);
		
	}
	
	@Click
	void btnSubmit()
	{
		if (mActivity instanceof ModuleQuizscreenActivity)
		{
			((ModuleQuizscreenActivity)mActivity).btn_submit();
		}
		else
			if (mActivity instanceof LeaderQuizscreenActivity)
			{
				((LeaderQuizscreenActivity)mActivity).btn_submit();
			}
	}
	
}
*/
