package com.example.datvu.mlqplus.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import com.example.datvu.mlqplus.models.Assessment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadFileTask extends AsyncTask<Void, Integer, Void> {
	private boolean downloadSuccess;
	private Context mContext;
	private String filePath;
	private Assessment mAssessment;

	public DownloadFileTask(Context context, Assessment mAssessment) {
		this.mContext = context;
		downloadSuccess=false;
		this.mAssessment=mAssessment;
	}

	protected void onPreExecute() {
		Toast.makeText(mContext, "Dowloading file...", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected Void doInBackground(Void... voids) {
		try {
			String fileName = mAssessment.getPdf_file_name();
			String fileExtension = ".pdf";

			// download pdf file.
			URL url = new URL(mAssessment.getPdf_link());
			HttpURLConnection c = (HttpURLConnection) url.openConnection();
			c.setRequestMethod("GET");
			c.setDoOutput(true);
			c.connect();
			String PATH = Environment.getExternalStorageDirectory()
					+ "/TrustedLeader/";
			File file = new File(PATH);
			file.mkdirs();
			File outputFile = getNewFile(file, fileName, fileExtension);
			filePath=outputFile.getAbsolutePath();
			FileOutputStream fos = new FileOutputStream(outputFile);
			InputStream is = c.getInputStream();
			byte[] buffer = new byte[1024];
			int len1 = 0;
			while ((len1 = is.read(buffer)) != -1) {
				fos.write(buffer, 0, len1);
				onProgressUpdate(10);
			}
			fos.close();
			is.close();
			downloadSuccess=true;
		} catch (Exception e) {
			downloadSuccess=false;
		}

		return null;
	}

	protected void onProgressUpdate(Integer... progress) {
	}

	protected void onPostExecute(Void result) {
		if (downloadSuccess)
			Toast.makeText(mContext, "Downloaded to "+filePath, Toast.LENGTH_LONG).show();
		else
			Toast.makeText(mContext, "Download failed", Toast.LENGTH_SHORT).show();
	}

	private File getNewFile(File path, String name, String fileExtension) {
		File outputFile = new File(path, name + fileExtension);
		if (!outputFile.exists())
			return outputFile;
		return getNewFile(path, "new_" + name, fileExtension);
	}
}
