package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.ModuleImage;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.view_imagecontent)
public class ImageContentView extends LinearLayout {

	private Context mContext;

	@ViewById
	ImageView img_content;
	
	public ImageContentView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public ImageContentView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public ImageContentView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public ImageContentView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public void setContent(ModuleImage moduleImage) {
		MLQPlusFunctions.showImage(mContext, moduleImage.getImageUrl(), img_content);
	}
	
	@Click
	void lnBackground()
	{
		MLQPlusFunctions.hideKeyboard((Activity)mContext);
	}

}
