/*
package com.example.datvu.mlqplus.fragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.activity.InAppBillingBaseActivity.InAppPurchaseListenner;
import com.mlqplus.trustedleader.activity.ModuleInformationscreenActivity;
import com.mlqplus.trustedleader.billing.utils.Purchase;
import com.mlqplus.trustedleader.model.Module;
import com.mlqplus.trustedleader.model.PurchaseModuleEvent;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import de.greenrobot.event.EventBus;

@EFragment(R.layout.fragment_module_information)
public class FragmentModuleInfomation extends FragmentBase {
	public static FragmentModuleInfomation newInstance() {
		return new FragmentModuleInfomation_();
	}

	@ViewById
	TextView tv_name, tv_subname, tv_description, tv_description_content,
			tv_price;

	@ViewById
	ImageView img_icon;

	@ViewById
	LinearLayout lnPurchased, lnPrice;

	private Activity mActivity;
	private Module module;
	private EventBus eventBus;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// Init eventBus
		try {
			eventBus = EventBus.getDefault();
		} catch (Exception e) {
		}

		// getActivity
		mActivity = getActivity();

		// Set text size for all view
		setTextSize();

		showData();
	}

	public void setData(Module module) {
		this.module = module;
	}

	public void showData() {
		tv_name.setText(module.getName());
		tv_subname.setText(module.getModuleSubname());
		
		tv_subname.setSelected(true);
		tv_subname.requestFocus();
		
		if (module.isUnlocked()) {
			lnPrice.setVisibility(View.GONE);
			lnPurchased.setVisibility(View.VISIBLE);
		} else {
			lnPrice.setVisibility(View.VISIBLE);
			tv_price.setText("$" + module.getPrice());
			lnPurchased.setVisibility(View.GONE);
		}
		if (module.getDescription() != null) {
			tv_description_content.setText(Html.fromHtml(module
					.getDescription()));
		} else
			tv_description_content.setVisibility(View.GONE);

		MLQPlusFunctions.showImage(mActivity, module.getIcon(), img_icon);
		img_icon.setColorFilter(getResources().getColor(R.color.white));

	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_name, mActivity,
				R.dimen.textMediumLarge);
		MLQPlusFunctions.SetViewTextSize(tv_subname, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_description, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_description_content, mActivity,
				R.dimen.module_name);
		MLQPlusFunctions.SetViewTextSize(tv_price, mActivity,
				R.dimen.fragment_module_information_module_price);
	}

	@Click
	void img_back() {
		mActivity.onBackPressed();
	}

	@Click
	void tv_price() {
		if (module.getPublished().equals("1"))
			if (!module.isUnlocked()) {
				if (!MLQPlusFunctions.isInternetAvailable())
					((BaseActivity) mActivity)
							.showToast("Connection's lost. Please check your connection and try again");
				else {
					if (mActivity instanceof ModuleInformationscreenActivity) {
						((ModuleInformationscreenActivity) mActivity)
								.purchaseAnItem(module.getId(), (BaseActivity) mActivity,
										new InAppPurchaseListenner() {

											@Override
											public void onPurchasedSuccessfully(
													String moduleID) {
												// TODO Auto-generated method
												// stub
												module.setUnlocked(true);

												// update view
												lnPrice.setVisibility(View.GONE);
												lnPurchased
														.setVisibility(View.VISIBLE);

												eventBus.post(new PurchaseModuleEvent(
														moduleID));
											}
										});

						*/
/*
						 * try { ((ModuleInformationscreenActivity)mActivity).
						 * onPurchasedSuccessfully(new Purchase("sdfsdf",
						 * "sdfsdf", "dsfsdf"), "rwere");
						 * module.setUnlocked(true);
						 * 
						 * // update view lnPrice.setVisibility(View.GONE);
						 * lnPurchased.setVisibility(View.VISIBLE);
						 * 
						 * eventBus.post(new PurchaseModuleEvent("fdfd")); }
						 * catch (JSONException e) { // TODO Auto-generated
						 * catch block e.printStackTrace(); }
						 *//*

					}
				}
			} else
				((BaseActivity) mActivity)
						.showToast("Cannot purchase an unlocked Module");
		else
			((BaseActivity) mActivity)
					.showToast("This module hasn't been published");
	}
}
*/
