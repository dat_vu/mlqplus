package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Assessment;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.ArrayList;

public class AssessmentAdapter extends BaseAdapter {
    private Activity mContext;
    private int mLayoutID;
    private ArrayList<Assessment> mALAssessment = null;
    private ViewHolder mViewHolder = null;

    public AssessmentAdapter(Activity mContext, int mLayoutID,
                             ArrayList<Assessment> mALAssessment) {
        this.mContext = mContext;
        this.mLayoutID = mLayoutID;
        this.mALAssessment = mALAssessment;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(mLayoutID, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            mViewHolder.tv_des = (TextView) convertView
                    .findViewById(R.id.tv_des);
            mViewHolder.img_icon = (ImageView) convertView
                    .findViewById(R.id.img_icon);
            mViewHolder.vHeaderLine = convertView
                    .findViewById(R.id.vHeaderLine);

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        Assessment assessment = mALAssessment.get(position);

        // Only show the header line in the first child
        if (position == 0)
            mViewHolder.vHeaderLine.setVisibility(View.VISIBLE);
        else
            mViewHolder.vHeaderLine.setVisibility(View.GONE);

        mViewHolder.tv_name.setText(assessment.getName());
        mViewHolder.tv_des.setText(assessment.getDescription());
        MLQPlusFunctions.showImage(mContext, assessment.getIcon(),
                mViewHolder.img_icon);
        // change size of img_icon after loading from internet
/*		android.view.ViewGroup.LayoutParams layoutParams = mViewHolder.img_icon
                .getLayoutParams();
		layoutParams.width = (int) mContext.getResources().getDimension(
				R.dimen.marginBasex9);
		layoutParams.height = (int) mContext.getResources().getDimension(
				R.dimen.marginBasex9);*/

        // Set size for widgets
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_name, mContext,
                R.dimen.itemTitle);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_des, mContext,
                R.dimen.itemDescript);


        return convertView;
    }

    @Override
    public int getCount() {
        return mALAssessment.size();
    }

    @Override
    public Object getItem(int position) {
        return mALAssessment.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        TextView tv_name;
        TextView tv_des;
        View vHeaderLine;
        ImageView img_icon;
    }

}
