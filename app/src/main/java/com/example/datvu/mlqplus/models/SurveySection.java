package com.example.datvu.mlqplus.models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SurveySection extends ObjectBase implements Serializable {
	
	@SerializedName("survey_id")
	private String survey_id;
	
	@SerializedName("survey_section_name")
	private String survey_section_name;
	
	@SerializedName("activities")
	private ArrayList<ModuleActivity> activities;

	public String getSurvey_id() {
		return survey_id;
	}

	public void setSurvey_id(String survey_id) {
		this.survey_id = survey_id;
	}

	public String getSurvey_section_name() {
		return survey_section_name;
	}

	public void setSurvey_section_name(String survey_section_name) {
		this.survey_section_name = survey_section_name;
	}

	public ArrayList<ModuleActivity> getActivities() {
		return activities;
	}

	public void setActivities(ArrayList<ModuleActivity> activities) {
		this.activities = activities;
	}

	
}
