/*
package com.example.datvu.mlqplus.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.generator.ModuleGenerator;
import com.mlqplus.trustedleader.model.ModuleActivity;
import com.mlqplus.trustedleader.model.ModuleQuestion;
import com.mlqplus.trustedleader.model.Quiz;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_module_quizresultscreen)
public class ModuleQuizResultscreenActivity extends BaseDialogActivity {

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    TextView tv_quiz_title, tv_welldone, tv_result, tv_correct, tv_try;

    @ViewById
    Button btn_try_again, btn_finish;

    @ViewById
    LinearLayout lnClose;

    public static String RESULT = "result";
    private ModuleActivity moduleActivity;
    private String result;
    private Quiz quiz;
    private ArrayList<ModuleQuestion> questions;
    public static ModuleQuizResultListenner listenner;
    private boolean isFromModule;

    @AfterViews
    void init() {
        getScreenSize();

        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        // GetData
        getData();
    }

    private void getData() {
        try {
            result = getIntent().getBundleExtra(MLQPlusConstant.BUNDLE)
                    .getString(RESULT);
            Bundle bundle = getIntent().getBundleExtra(MLQPlusConstant.BUNDLE);
            if (bundle.containsKey(ModuleQuizscreenActivity.MODULE_ACTIVITY))
                moduleActivity = (ModuleActivity) bundle
                        .get(ModuleQuizscreenActivity.MODULE_ACTIVITY);
            else {
                quiz = (Quiz) bundle
                        .getSerializable(LeaderQuizscreenActivity.QUIZ);
                questions = (ArrayList<ModuleQuestion>) bundle
                        .getSerializable(LeaderQuizscreenActivity.QUESTIONS);
                lnClose.setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
        }

        // Show data
        showData();
    }

    private void showData() {
        // if module quiz
        if (moduleActivity != null) {
            if (ModuleGenerator.module != null
                    && ModuleGenerator.module.getName() != null)
                tv_quiz_title.setText(ModuleGenerator.module.getName() + " Quiz");
            else
                tv_quiz_title.setText("Your Score");
        } else // else -> leader quiz
        {
            tv_quiz_title.setText("Your Score");
            tv_try.setText(getResources().getString(
                    R.string.complete_more_modules));
        }

        tv_result.setText(result);
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_quiz_title,
                ModuleQuizResultscreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_welldone,
                ModuleQuizResultscreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_try,
                ModuleQuizResultscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btn_try_again,
                ModuleQuizResultscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btn_finish,
                ModuleQuizResultscreenActivity.this, R.dimen.textMedium);
    }

    @Click
    void lnClose() {
        // If user complete quiz and module at the first time -> show complete dialog
        if (ModuleGenerator.enableToShowCompleteDialog
                && ModuleGenerator.isCompleteModule)
            ModuleGenerator
                    .showCompleteDialog(ModuleQuizResultscreenActivity.this);

        ModuleQuizResultscreenActivity.this.finish();
    }

    @Click
    void btn_try_again() {
        if (moduleActivity != null) {
            // Update local
            moduleActivity.setAnswer(null);

            // Start quiz activity
            Bundle bundle = new Bundle();
            bundle.putSerializable(ModuleQuizscreenActivity.MODULE_ACTIVITY,
                    moduleActivity);
            startNewZoomActivity(ModuleQuizResultscreenActivity.this,
                    ModuleQuizscreenActivity_.class, true, bundle);
        } else {
            Bundle bundle = new Bundle();
            bundle.putSerializable(LeaderQuizscreenActivity.QUESTIONS,
                    questions);
            bundle.putSerializable(LeaderQuizscreenActivity.QUIZ, quiz);
            startNewZoomActivity(ModuleQuizResultscreenActivity.this,
                    LeaderQuizscreenActivity_.class, true, bundle);
        }

    }

    @Click
    void btn_finish() {
        ModuleQuizResultscreenActivity.this.finish();

        // If it's module quiz -> check to show complete module dialog
        if (moduleActivity != null) {
            // if user replay quiz after they completed module -> not show Complete
            // dialog, only close module content page
            if (listenner != null && !ModuleGenerator.enableToShowCompleteDialog)
                listenner.onFinish();

            // If user complete quiz and module at the first time -> show complete
            // dialog
            if (ModuleGenerator.enableToShowCompleteDialog
                    && ModuleGenerator.isCompleteModule)
                ModuleGenerator
                        .showCompleteDialog(ModuleQuizResultscreenActivity.this);
        }
    }

    public interface ModuleQuizResultListenner {
        public void onFinish();
    }
}
*/
