package com.example.datvu.mlqplus.activities;


import android.graphics.Paint;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.SignupRequest;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.example.datvu.mlqplus.views.CustomSpinnerView;
import com.example.datvu.mlqplus.views.MyCustomSpinnerView;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_createaccountscreen)
public class CreateAccountscreenActivity extends BaseActivity {

    @ViewById
    LinearLayout ln_child, ln_root, lnBackground;

    @ViewById
    TextView tv_learn_more, tv_privacy, tv_and, tv_terms, tv_by_signup,
            tv_cancel;

    @ViewById
    EditText txt_first_name, txt_email, txt_password;

    @ViewById
    Button btn_create_account;

    @ViewById
    MyCustomSpinnerView sp_age, sp_gender,
            sp_country;

    private String mFirstName, mEmail, mPassword, mAge, mGender, mCountry;
    private GoogleCloudMessaging mGoogleCloudMessaging;
    private int mLoadCountryFailCount = 0;

    @AfterViews
    void init() {

        // Set text size for all view
        setTextSize();

        // Init views
        initViews();
    }

    private void initViews() {
        sp_age.setData(new ArrayList<Object>(MLQPlusFunctions.getAgeList()),
                null, "Age",
                getResources().getColor(R.color.blue_create_account_text),
                R.dimen.textMedium, spinnerClickListenner);
        sp_gender.setData(
                new ArrayList<Object>(MLQPlusFunctions.getGenderList()), null,
                "Gender",
                getResources().getColor(R.color.blue_create_account_text),
                R.dimen.textMedium, spinnerClickListenner);
        /*if (MLQPlusConstant.COUNTRIES == null) {
            showWaitingDialog(this, "Loading country data", "Please wait...");

            loadCountryList();
        } else {
            sp_country.setData(
                    new ArrayList<Object>(MLQPlusConstant.COUNTRIES), null,
                    "Country",
                    getResources().getColor(R.color.blue_create_account_text),
                    R.dimen.textMedium, spinnerClickListenner);
        }*/

        tv_terms.setPaintFlags(tv_terms.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tv_privacy.setPaintFlags(tv_privacy.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_learn_more,
                CreateAccountscreenActivity.this, R.dimen.textLarge);
        MLQPlusFunctions.SetViewTextSize(tv_privacy,
                CreateAccountscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_and,
                CreateAccountscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_terms,
                CreateAccountscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_by_signup,
                CreateAccountscreenActivity.this, R.dimen.textMedium);

        MLQPlusFunctions.SetViewTextSize(btn_create_account,
                CreateAccountscreenActivity.this, R.dimen.textMedium);

        MLQPlusFunctions.SetViewTextSize(txt_first_name,
                CreateAccountscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(txt_email,
                CreateAccountscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(txt_password,
                CreateAccountscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_cancel,
                CreateAccountscreenActivity.this, R.dimen.textMedium);
    }

    @Click
    void btn_create_account() {
        if (validateInputData()) {
            if (!isInternetAvailable())
                showToast("Please check your internet connection and try again");
            else {
                showWaitingDialog(CreateAccountscreenActivity.this, "Signup",
                        "Please wait...");

                String mGenderCode = "";
                if (mGender.equalsIgnoreCase("female"))
                    mGenderCode = "1";
                else if (mGender.equalsIgnoreCase("male"))
                    mGenderCode = "0";

                SignupRequest signupRequest = new SignupRequest(mEmail,
                        mFirstName, mAge, mGenderCode, mCountry, mPassword);

                //registerGoogleCloudMessage(signupRequest);

            }
        }

    }

    /*
     * Validate input data from user
     */
    private boolean validateInputData() {
        try {
            mFirstName = txt_first_name.getText().toString().trim();
            if (mFirstName.length() == 0) {
                showToast("First name cannot be empty");
                return false;
            } else if (mFirstName.length() > 32) {
                showToast("The maximum length of first name is 32");
                return false;
            } else if (mFirstName.length() < 1) {
                showToast("The first name must be at least 1 characters");
                return false;
            }

            mEmail = txt_email.getText().toString().trim();
            if (mEmail.length() == 0) {
                showToast("Email cannot be empty");
                return false;
            } else if (!MLQPlusFunctions.isValidEmail(mEmail)) {
                showToast("Invalid email address");
                return false;
            }

            mPassword = txt_password.getText().toString().trim();
            if (mPassword.length() == 0) {
                showToast("Password cannot be empty");
                return false;
            } else if (mPassword.length() < 4 || mPassword.length() > 12) {
                showToast("Password length must be between 4 and 12 characters");
                return false;
            } else if (!mPassword.matches(".*[1-9].*")
                    && !mPassword.matches(".*[A-Za-z].*")) {
                showToast("Password must contain number or letter");
                return false;
            }

            mAge = sp_age.getSelectedValue();
            mGender = sp_gender.getSelectedValue();
            mCountry = sp_country.getSelectedValue();
            if (mAge == null) {
                mAge = "";
                /*showToast("Age cannot be empty");
				return false;*/
            }

            if (mGender == null) {
                mGender = "";
				/*showToast("Gender cannot be empty");
				return false;*/
            }

            if (mCountry == null) {
                mCountry = "";
				/*showToast("Country cannot be empty");
				return false;*/
            }
        } catch (Exception e) {
            showToast("Error " + e.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        startNewActivityNoAnimation(CreateAccountscreenActivity.this,
                WelcomescreenActivity_.class, true);
    }

    @Click
    void tv_cancel() {
        startNewActivityNoAnimation(CreateAccountscreenActivity.this,
                WelcomescreenActivity_.class, true);
    }

    /*@Click
    void tv_terms() {
        navigateToAbout();
    }

    @Click
    void tv_privacy() {
        navigateToAbout();
    }

    @Click({R.id.vTerm, R.id.vPrivacy})
    void navigateAbout() {
        navigateToAbout();
    }*/

    /*private void navigateToAbout() {
        startNewActivityNoAnimation(CreateAccountscreenActivity.this,
                AboutActivity_.class, false);
    }*/

    CustomSpinnerView.SpinnerClickListenner spinnerClickListenner = new CustomSpinnerView.SpinnerClickListenner() {

        @Override
        public void onClick() {
            // TODO Auto-generated method stub
            hideKeyboard();
        }
    };

    /*private void registerGoogleCloudMessage(final SignupRequest signupRequest) {
        new AsyncTask() {

            @Override
            protected String doInBackground(Object... params) {
                // TODO Auto-generated method stub
                // Push device ID
                String DeviceID = getSharedPreferencesService().deviceID()
                        .get();
                int count_time = 0;
                while ((DeviceID == null || DeviceID.equals(""))
                        && count_time < 10) {
                    count_time++;
                    if (mGoogleCloudMessaging == null) {
                        mGoogleCloudMessaging = GoogleCloudMessaging
                                .getInstance(CreateAccountscreenActivity.this);
                    }
                    try {
                        DeviceID = mGoogleCloudMessaging
                                .register(MLQPlusConstant.GCM_SENDER_ID);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                return DeviceID;
            }

            @Override
            protected void onPostExecute(Object result) {
                try {
                    String DeviceID = String.valueOf(result);
                    if (DeviceID == null || DeviceID.length() == 0)
                        DeviceID = "123";
                    else
                        getSharedPreferencesService().deviceID().put(DeviceID);

                    signupRequest.setDevice_token(DeviceID);
                    createAccount(signupRequest);

                } catch (Exception e) {
                }
            }

            ;

        }.execute(null, null, null);

    }*/

    /*private void createAccount(SignupRequest signupRequest) {
        MLQPlusService.getBSSServices().signup(signupRequest,
                new Callback<ApiResponse<SignupResponse>>() {

                    @Override
                    public void failure(RetrofitError error) {
                        dismissWaitingDialog();
                        if (error.getCause() instanceof SocketTimeoutException)
                            showToast(MLQPlusConstant.TIMEOUT);
                        else
                            try {
                                if (error.getResponse() != null) {
                                    ErrorRetrofit body = (ErrorRetrofit) error
                                            .getBodyAs(ErrorRetrofit.class);
                                    showToast(body.getError().getErrorMessage());
                                }
                            } catch (Exception e) {
                            }

                    }

                    @Override
                    public void success(
                            ApiResponse<SignupResponse> mSignupResponse,
                            Response response) {
                        dismissWaitingDialog();
                        MLQPlusConstant.USERINFO = mSignupResponse.getData();
                        getSharedPreferencesService().accessToken().put(
                                MLQPlusConstant.USERINFO.getId());
                        getSharedPreferencesService().isNormalAccount().put(
                                true);

                        showToast("Register successfully");
                        // Update login info
                        getSharedPreferencesService().email().put(mEmail);
                        getSharedPreferencesService().password().put(mPassword);

                        startNewActivityNoAnimation(
                                CreateAccountscreenActivity.this,
                                BackgroundActivity_.class, true);

                    }

                });
    }*/

    /*private void loadCountryList() {
        MLQPlusService.getBSSServices().getCountryList(
                new Callback<ApiResponse<List<Country>>>() {

                    @Override
                    public void success(ApiResponse<List<Country>> countries,
                                        Response response) {
                        List<Country> countryList = new ArrayList<Country>();
                        if (countries != null) {
                            countryList.addAll(countries.getData());
                            MLQPlusConstant.COUNTRIES = countryList;

                            // Set country data
                            sp_country.setData(
                                    new ArrayList<Object>(
                                            MLQPlusConstant.COUNTRIES),
                                    null,
                                    "Country",
                                    getResources().getColor(
                                            R.color.blue_create_account_text),
                                    R.dimen.textMedium, spinnerClickListenner);

                        }
                        dismissWaitingDialog();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        mLoadCountryFailCount++;
                        if (mLoadCountryFailCount == 3) {
                            dismissWaitingDialog();
                            if (error.getCause() instanceof SocketTimeoutException)
                                showToast(MLQPlusConstant.TIMEOUT);
                            else if (!MLQPlusFunctions.isInternetAvailable()) {
                                showToast(MLQPlusConstant.CONNECTION_LOST);
                            } else
                                showToast("Load country list fail");
                            startNewActivityNoAnimation(
                                    CreateAccountscreenActivity.this,
                                    WelcomescreenActivity_.class, true);
                        } else
                            loadCountryList();
                    }
                });
    }*/

    @Click
    void lnBackground() {
        MLQPlusFunctions.hideKeyboard(CreateAccountscreenActivity.this);
    }
}
