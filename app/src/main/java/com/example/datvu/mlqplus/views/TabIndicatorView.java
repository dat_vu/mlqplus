package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Module;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.view_tab_indicator)
public class TabIndicatorView extends LinearLayout {

	private Context mContext;
	private OnClickListenner onClickListenner;

	@ViewById
	View line0, line1, line2, line3, line4;

	@ViewById
	TextView tv_tab0, tv_tab1, tv_tab2, tv_tab3, tv_tab4;

	public TabIndicatorView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public TabIndicatorView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public TabIndicatorView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public TabIndicatorView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public void setOnClickListenner(OnClickListenner onClickListenner) {
		this.onClickListenner = onClickListenner;
	}
	
	private void setTextSize()
	{
		MLQPlusFunctions.SetViewTextSize(tv_tab0, mContext, R.dimen.moduleSectionTitle);
		MLQPlusFunctions.SetViewTextSize(tv_tab1, mContext, R.dimen.moduleSectionTitle);
		MLQPlusFunctions.SetViewTextSize(tv_tab2, mContext, R.dimen.moduleSectionTitle);
		MLQPlusFunctions.SetViewTextSize(tv_tab3, mContext, R.dimen.moduleSectionTitle);
		MLQPlusFunctions.SetViewTextSize(tv_tab4, mContext, R.dimen.moduleSectionTitle);
	}

	public void setTabTitle(Module module) {
		if (module != null && module.getSections()!=null && module.getSections().size() == 5) {
			tv_tab0.setText(module.getSections().get(0).getSectionName());
			tv_tab1.setText(module.getSections().get(1).getSectionName());
			tv_tab2.setText(module.getSections().get(2).getSectionName());
			tv_tab3.setText(module.getSections().get(3).getSectionName());
			tv_tab4.setText(module.getSections().get(4).getSectionName());
			selectedTab(0);
			setTextSize();
		}
	}

	public void selectedTab(int pos) {
		line0.setVisibility(View.GONE);
		line1.setVisibility(View.GONE);
		line2.setVisibility(View.GONE);
		line3.setVisibility(View.GONE);
		line4.setVisibility(View.GONE);

		switch (pos) {
		case 0:
			line0.setVisibility(View.VISIBLE);
			break;
		case 1:
			line1.setVisibility(View.VISIBLE);
			break;
		case 2:
			line2.setVisibility(View.VISIBLE);
			break;
		case 3:
			line3.setVisibility(View.VISIBLE);
			break;
		case 4:
			line4.setVisibility(View.VISIBLE);
			break;
		}
	}

	@Click
	void tv_tab0() {
		clickTab(0);
	}

	@Click
	void tv_tab1() {
		clickTab(1);
	}

	@Click
	void tv_tab2() {
		clickTab(2);
	}

	@Click
	void tv_tab3() {
		clickTab(3);
	}

	@Click
	void tv_tab4() {
		clickTab(4);
	}

	private void clickTab(int pos) {
		if (onClickListenner != null)
			onClickListenner.onClick(pos);
	}

	public interface OnClickListenner {
		public void onClick(int pos);
	}
}
