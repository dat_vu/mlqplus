package com.example.datvu.mlqplus.services;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

@SharedPref()
interface SharedPreferencesService { 
	 @DefaultString("")
	 String email(); 
	 
	 @DefaultString("")
	 String password();
	 
	 @DefaultString("")
	 String accessToken();
	 
	 @DefaultBoolean(false)
	 boolean userClickedGetStarted();
	 
	 @DefaultString("")
	 String textSize();
	 
	 @DefaultString("")
	 String deviceID();
	 
	 @DefaultBoolean(false)
	 boolean isNormalAccount();
	 
	 @DefaultString("")
	 String purchasingModule();
	 
	 @DefaultString("")
	 String lastSectionOpenModules();
	 
	 @DefaultString("")
	 String purchaseJson();
	 
	 @DefaultString("")
	 String purchaseModuleJson();
	 
	 @DefaultString("")
	 String purchasedModuleToken();
}
