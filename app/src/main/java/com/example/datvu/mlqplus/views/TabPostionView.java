package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.datvu.mlqplus.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.view_tab_position)
public class TabPostionView extends LinearLayout {

    private Context mContext;
    private float unSelectedTabAlpha = 0.4f;

    @ViewById
    LinearLayout ln_parent;

    public TabPostionView(Context context) {
        super(context);

        mContext = context;
    }

    @SuppressLint("NewApi")
    public TabPostionView(Context context, AttributeSet attrs,
                          int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
    }

    public TabPostionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public TabPostionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public void setSeletedTab(int count, int seleted) {
        ln_parent.removeAllViews();
        for (int i = 0; i < count; i++)
            if (i == seleted)
                ln_parent.addView(getView(true));
            else
                ln_parent.addView(getView(false));
        ln_parent.requestLayout();
    }

    public void setTabAlpha(float unSelectedTabAlpha) {
        this.unSelectedTabAlpha = unSelectedTabAlpha;
    }

    private View getView(boolean isSelected) {
        ImageView imageView = new ImageView(mContext);
        LayoutParams lp = new LayoutParams(
                (int) getResources().getDimension(R.dimen.marginBasex2),
                LayoutParams.WRAP_CONTENT);
        lp.setMargins((int) getResources().getDimension(R.dimen.marginBase), 0,
                (int) getResources().getDimension(R.dimen.marginBase), 0);
        imageView.setLayoutParams(lp);
        imageView.setAdjustViewBounds(true);
        if (isSelected)
            imageView.setImageResource(R.drawable.tab_selected);
        else {
            imageView.setImageResource(R.drawable.tab_unseleted);
            imageView.setAlpha(unSelectedTabAlpha);
        }
        return imageView;
    }
}
