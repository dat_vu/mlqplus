package com.example.datvu.mlqplus.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ModuleTickBox  extends ActivityBase{
	@SerializedName("title") 
	private String title;
	
	@SerializedName("answers")
	private List<ActivityAnswer> answers;
	
	@SerializedName("question")
	private String question;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ActivityAnswer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<ActivityAnswer> answers) {
		this.answers = answers;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	

}
