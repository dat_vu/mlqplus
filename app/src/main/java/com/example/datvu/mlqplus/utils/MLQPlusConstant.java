package com.example.datvu.mlqplus.utils;

import com.example.datvu.mlqplus.models.ConfigMenu;
import com.example.datvu.mlqplus.models.Country;
import com.example.datvu.mlqplus.models.Module;
import com.example.datvu.mlqplus.models.ModuleActivityError;
import com.example.datvu.mlqplus.models.SettingData;
import com.example.datvu.mlqplus.models.SignupResponse;
import com.example.datvu.mlqplus.models.TextSize;

import java.util.ArrayList;
import java.util.List;

public class MLQPlusConstant {
    public static String BUNDLE = "bundle";
    public static int SCREEN_WIDTH = 0;
    public static int SCREEN_HEIGHT = 0;
    public static SignupResponse USERINFO;
    public static String CONNECTION_LOST = "Connection's lost. Please check your internet connection";
    public static String TIMEOUT = "Request timeout!";
    public static ConfigMenu CONFIG_MENU;
    public static String GCM_SENDER_ID = "181677764765";
    public static SettingData SETTING_DATA;
    public static TextSize TEXTSIZE = TextSize.NORMAL;
    public static List<Module> CORE_MODULES;
    public static ArrayList<Module> sFullModule;
    public static List<ModuleActivityError> ACTIVITIES_ERRORS_LIST;
    public static String FULL_MODULES = "fullmodules";
    public static List<Country> COUNTRIES;
    public static final String MAIN_THREAD = "MAIN_THREAD";

}
