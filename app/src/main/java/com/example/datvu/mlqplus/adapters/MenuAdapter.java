package com.example.datvu.mlqplus.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class MenuAdapter extends ArrayAdapter<String> {
    Activity mContext;

    public MenuAdapter(Activity context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.mContext = context;
    }

    public View getView(int pos, View newview, ViewGroup viewgroup) {
        newview = mContext.getLayoutInflater()
                .inflate(R.layout.item_menu, null);

        TextView tv_name = (TextView) newview
                .findViewById(R.id.tv_setting_name);
        tv_name.setText(getItem(pos));

        ImageView img_icon = (ImageView) newview
                .findViewById(R.id.img_setting_icon);
        img_icon.setImageResource(getIcon(pos));

        LinearLayout ln_setting_bg = (LinearLayout) newview
                .findViewById(R.id.ln_setting_bg);

        if (pos % 2 == 0)
            ln_setting_bg.setBackgroundColor(mContext.getResources().getColor(
                    R.color.dark_setting_bg));
        else
            ln_setting_bg.setBackgroundColor(mContext.getResources().getColor(
                    R.color.light_setting_bg));

        // Set size for widgets
        MLQPlusFunctions.SetViewTextSize(tv_name, mContext,
                R.dimen.textLarge);

        return newview;
    }

    private int getIcon(int index) {
        switch (index) {
            case 0:
                return R.drawable.test_knoledge_icon;
            case 1:
                return R.drawable.track_icon;
            case 2:
                return R.drawable.full_access_icon;
            case 3:
                return R.drawable.test_knoledge_icon;
            case 4:
                return R.drawable.track_icon;
            default:
                return R.drawable.full_access_icon;
        }
    }
}
