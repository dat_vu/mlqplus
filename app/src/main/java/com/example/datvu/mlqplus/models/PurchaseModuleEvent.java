package com.example.datvu.mlqplus.models;

public class PurchaseModuleEvent {
	private String moduleID;

	public String getModuleID() {
		return moduleID;
	}

	public void setModuleID(String moduleID) {
		this.moduleID = moduleID;
	}

	public PurchaseModuleEvent(String moduleID) {
		super();
		this.moduleID = moduleID;
	}
}
