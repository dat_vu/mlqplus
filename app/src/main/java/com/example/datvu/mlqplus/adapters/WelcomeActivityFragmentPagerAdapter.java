package com.example.datvu.mlqplus.adapters;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;

import com.example.datvu.mlqplus.activities.WelcomescreenActivity_;
import com.example.datvu.mlqplus.fragments.FragmentWelcome;
import com.example.datvu.mlqplus.models.WelcomeScreenInfo;

import java.util.ArrayList;

public class WelcomeActivityFragmentPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<WelcomeScreenInfo> mALWelcomeScreenInfo = null;
    FragmentWelcome fragmentWelcome0, fragmentWelcome1, fragmentWelcome2;

    public WelcomeActivityFragmentPagerAdapter(FragmentManager fm, ArrayList<WelcomeScreenInfo> mALWelcomeScreenInfo) {
        super(fm);
        this.mALWelcomeScreenInfo = mALWelcomeScreenInfo;
    }

    @Override
    public Fragment getItem(int index) {
        // Sending data to fragment
        Bundle bundle = new Bundle();
        bundle.putSerializable(WelcomescreenActivity_.WELCOME_SCREEN_INFO, mALWelcomeScreenInfo.get(index));
        bundle.putInt(FragmentWelcome.INDEX, index);

        switch (index) {
            case 0:
                if (fragmentWelcome0 == null) {
                    fragmentWelcome0 = FragmentWelcome.newInstance();
                    fragmentWelcome0.setArguments(bundle);
                }
                return fragmentWelcome0;
            case 1:
                if (fragmentWelcome1 == null) {
                    fragmentWelcome1 = FragmentWelcome.newInstance();
                    fragmentWelcome1.setArguments(bundle);
                }
                return fragmentWelcome1;
            case 2:
                if (fragmentWelcome2 == null) {
                    fragmentWelcome2 = FragmentWelcome.newInstance();
                    fragmentWelcome2.setArguments(bundle);
                }

                return fragmentWelcome2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 3;
    }

}
