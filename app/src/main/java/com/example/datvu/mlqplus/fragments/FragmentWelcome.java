package com.example.datvu.mlqplus.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.activities.LoadscreenActivity;
import com.example.datvu.mlqplus.activities.WelcomescreenActivity_;
import com.example.datvu.mlqplus.models.WelcomeScreenInfo;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_welcomescreen)
public class FragmentWelcome extends Fragment {
	public static FragmentWelcome newInstance() {
		return new FragmentWelcome_();
	}

	private Activity mActivity;
	public static final String INDEX = "Index";

	@ViewById
	LinearLayout ln_root, ln_content;

	@ViewById
	ImageView img_icon, img_bg;

	@ViewById
	TextView tv_content;

	@AfterViews
	void init() {
		mActivity = getActivity();

		// Fix views in small screen
		/*
		 * if (MLQPlusConstant.SCREEN_HEIGHT < 1000) {
		 * MLQPlusFunctions.marginLinearLayout(ln_content, 0, 0, 0, (int)
		 * getResources().getDimension(R.dimen.marginBasex3)); }
		 */
		// Set text size for all view
		setTextSize();

		initViews();
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_content, mActivity,
				R.dimen.textMedium);
	}

	public void initViews() {
		WelcomeScreenInfo wsInfo = (WelcomeScreenInfo) getArguments()
				.getSerializable(WelcomescreenActivity_.WELCOME_SCREEN_INFO);
		int index = getArguments().getInt(INDEX);

		// Show Data
		if (LoadscreenActivity.mBitmaps != null
				&& LoadscreenActivity.mBitmaps.size() >= 3)
			img_bg.setImageBitmap(LoadscreenActivity.mBitmaps.get(String.valueOf(index)));
		else
			MLQPlusFunctions.showImage(mActivity, wsInfo.getBackground(),
					img_bg);
		MLQPlusFunctions.showImage(mActivity, wsInfo.getIcon(), img_icon);
		img_icon.setColorFilter(getResources().getColor(R.color.white));
		tv_content.setText(wsInfo.getText());
	}
}
