package com.example.datvu.mlqplus.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.widget.Toast;

import com.example.datvu.mlqplus.activities.BaseActivity;

import java.io.Serializable;

public class FragmentBase extends Fragment implements Serializable {
	private Toast mToast;
	private int imagesCount, loadedImagesCount;
	private Activity mActivity;
	   
	public void showToast(String msg) { 
		try
		{
			if (mToast == null) 
				mToast = Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT);
	   
			mToast.setText(msg);
			mToast.show(); 
		} catch (Exception e){}
	}
	
	
	/*public void loadImages(List<String> imgs, Activity mActivity)
	{
		if (imgs!=null && imgs.size()>0)
		{
			this.mActivity=mActivity;
			imagesCount=imgs.size();
			for (String img:imgs)
				loadImage(img);
		}
		else
			voidAfterLoadImages();
	}*/
	
	/*private void loadImage(String imageUrl)
	{
		MLQPlusFunctions.loadImage(mActivity,imageUrl, new ImageLoadingListener() {
			
			@Override
			public void onLoadingFailed(String imageUri, View view,
					FailReason failReason) {
				// TODO Auto-generated method stub
				checkDismissLoadingDialog();
			}
			
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				// TODO Auto-generated method stub
				checkDismissLoadingDialog();
			}
			
			@Override
			public void onLoadingCancelled(String imageUri, View view) {
				// TODO Auto-generated method stub
				checkDismissLoadingDialog();
			}

			@Override
			public void onLoadingStarted(String imageUri, View view) {
				// TODO Auto-generated method stub
				
			}
		});
	}*/
	
	
	private void checkDismissLoadingDialog()
	{
		loadedImagesCount++;
		
		if (loadedImagesCount==imagesCount)
			voidAfterLoadImages();
			
	}
	
	public void voidAfterLoadImages(){
		((BaseActivity)mActivity).dismissWaitingDialog();
	}
	
}
