package com.example.datvu.mlqplus.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class FixItalicLinearLayout extends LinearLayout{

	@SuppressLint("NewApi")
	public FixItalicLinearLayout(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
	}

	public FixItalicLinearLayout(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public FixItalicLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public FixItalicLinearLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);

	    final int measuredWidth = getMeasuredWidth();
	    final float tenPercentOfMeasuredWidth = measuredWidth * 0.2f;
	    final int newWidth = measuredWidth + (int) tenPercentOfMeasuredWidth;

	    setMeasuredDimension(newWidth, getMeasuredHeight());
	}

}
