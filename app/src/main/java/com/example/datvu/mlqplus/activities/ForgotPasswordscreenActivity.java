package com.example.datvu.mlqplus.activities;


import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.net.SocketTimeoutException;

@EActivity(R.layout.activity_forgotpasswordscreen)
public class ForgotPasswordscreenActivity extends BaseActivity {
    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    TextView tv_forgot_password, tv_enter_your_email, tv_cancel;

    @ViewById
    EditText edt_email;

    @ViewById
    Button btn_reset_password;

    @AfterViews
    void init() {
        // Set text size for all view
        setTextSize();
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_forgot_password, ForgotPasswordscreenActivity.this, R.dimen.textLarge);
        MLQPlusFunctions.SetViewTextSize(tv_enter_your_email, ForgotPasswordscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_cancel, ForgotPasswordscreenActivity.this, R.dimen.textMedium);

        MLQPlusFunctions.SetViewTextSize(btn_reset_password, ForgotPasswordscreenActivity.this, R.dimen.textMedium);

        MLQPlusFunctions.SetViewTextSize(edt_email, ForgotPasswordscreenActivity.this, R.dimen.textMedium);
    }

    @Click
    void tv_cancel() {
        startNewActivityNoAnimation(ForgotPasswordscreenActivity.this, LoginscreenActivity_.class, true);
    }

    @Click
    void btn_reset_password() {
        String email = edt_email.getText().toString();
        if (email.length() > 0)
            if (MLQPlusFunctions.isValidEmail(email)) {
                if (!isInternetAvailable())
                    showToast("Please check your internet connection and try again");
                else {
                    showWaitingDialog(ForgotPasswordscreenActivity.this, "", "Please wait...");
                    String json = "{\"email\": \"" + email + "\"}";
                    /*MLQPlusService.getBSSServices().forgotPassword(new TypedString(json), new Callback<ApiResponse<Response>>() {

                        @Override
                        public void success(ApiResponse<Response> arg0, Response arg1) {
                            // TODO Auto-generated method stub
                            dismissWaitingDialog();
                            showToast("Reset password successfully");
                            // Back to Login Activity
                            startNewActivityNoAnimation(ForgotPasswordscreenActivity.this, LoginscreenActivity_.class, true);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            // TODO Auto-generated method stub
                            dismissWaitingDialog();
                            if (error.getCause() instanceof SocketTimeoutException)
                                showToast(MLQPlusConstant.TIMEOUT);
                            else
                                showToast("Not found email address in database");
                        }
                    });*/
                }
            } else
                showToast("Invalid email address");
        else
            showToast("Please enter your email");
    }

    @Click
    void ln_child() {
        MLQPlusFunctions.hideKeyboard(ForgotPasswordscreenActivity.this);
    }
}
