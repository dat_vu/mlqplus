/*
package com.example.datvu.mlqplus.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.fragment.FragmentTapcardContent_;
import com.mlqplus.trustedleader.model.ModuleTapCard;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_tapcard_contentscreen)
public class TapcardContentscreenActivity extends BaseDialogActivity {

    @ViewById
    LinearLayout ln_root, ln_child, lnTabPostionView;

    @ViewById
    TextView tv_title;

    @ViewById
    ImageView img_close;

    @ViewById
    ViewPager viewPager;


    @ViewById
    com.mlqplus.trustedleader.view.MyTabPostionView tpv;

    private ModuleTapCard moduleTapCard;
    public static String TAPCARD = "tapcard";

    @AfterViews
    void init() {
        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(true);

        // Get Data
        getData();

        // Show Data
        showData();
    }

    private void getData() {
        try {
            moduleTapCard = (ModuleTapCard) getIntent().getBundleExtra(
                    MLQPlusConstant.BUNDLE).get(TAPCARD);
        } catch (Exception e) {
        }
    }

    private void showData() {
        if (moduleTapCard != null) {
            tv_title.setText(moduleTapCard.getTitle());

            // Init viewpager
            FragmentManager fragmentManager = getFragmentManager();
            FragmentPagerAdapter sessionAdapter = new FragmentPagerAdapter(
                    fragmentManager) {

                @Override
                public int getCount() {
                    // TODO Auto-generated method stub
                    return moduleTapCard.getTabs().size();
                }

                @Override
                public Fragment getItem(int pos) {
                    // TODO Auto-generated method stub
                    FragmentTapcardContent_ fragmentTapcardContent_ = new FragmentTapcardContent_();
                    fragmentTapcardContent_.setData(moduleTapCard.getTabs().get(pos));
                    return fragmentTapcardContent_;
                }
            };

            viewPager.setAdapter(sessionAdapter);
            viewPager.setOnPageChangeListener(new OnPageChangeListener() {

                @Override
                public void onPageSelected(int pos) {
                    // TODO Auto-generated method stub
                    tpv.setSeletedTab(moduleTapCard.getTabs().size(), pos);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                    // TODO Auto-generated method stub

                }
            });

            // If there is only 1 item -> hide tab position view
            if (moduleTapCard.getTabs().size() == 1)
                lnTabPostionView.setVisibility(View.GONE);
            else
                tpv.setSeletedTab(moduleTapCard.getTabs().size(), 0);
        }
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_title,
                TapcardContentscreenActivity.this, R.dimen.textMediumLarge);

    }

    @Click
    void lnClose() {
        this.finish();
    }
}
*/
