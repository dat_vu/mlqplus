package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class LeaderTool extends Assessment{

	@SerializedName("isUnlock")
	private boolean isUnlock;

	public boolean isUnlock() {
		return isUnlock;
	}

	public void setUnlock(boolean isUnlock) {
		this.isUnlock = isUnlock;
	}
}
