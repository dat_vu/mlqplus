/*
package com.example.datvu.mlqplus.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.billing.utils.Purchase;
import com.mlqplus.trustedleader.fragment.FragmentModuleInfomation_;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.Module;
import com.mlqplus.trustedleader.model.PurchaseModuleEvent;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.net.SocketTimeoutException;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

@EActivity(R.layout.activity_moduleinfomationscreen)
public class ModuleInformationscreenActivity extends InAppBillingBaseActivity {

    @ViewById
    LinearLayout ln_root, ln_accessfull_program;

    @ViewById
    LinearLayout ln_child;

    @ViewById
    TextView tv_module_information, tv_access_full_program, tv_price,
            tv_description;

    @ViewById
    ViewPager viewPager;

    @ViewById
    ImageView img_purchased;

    @ViewById
    com.mlqplus.trustedleader.view.MyTabPostionView tab_position;

    public static String LIST_MODULES = "list_module";
    public static String FULL_PROGRAM = "full_program";
    public static String PAGE_START = "page_start";

    private List<Module> modules;
    private Module fullProgram;
    private int pageStart = 0;
    private EventBus eventBus;
    private int retryCount = 0;

    @AfterViews
    void init() {
        // Init eventBus
        try {
            eventBus = EventBus.getDefault();
        } catch (Exception e) {
        }

        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Make dialog not dim
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        tab_position.setTabAlpha(1f);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        // Get Data
        getData();

        // Show Data
        showData();
    }

    private void getData() {
        try {
            modules = (List<Module>) getIntent().getBundleExtra(
                    MLQPlusConstant.BUNDLE).getSerializable(LIST_MODULES);
            pageStart = (int) getIntent()
                    .getBundleExtra(MLQPlusConstant.BUNDLE).getInt(PAGE_START);
            fullProgram = (Module) getIntent().getBundleExtra(
                    MLQPlusConstant.BUNDLE).getSerializable(FULL_PROGRAM);
        } catch (Exception e) {
        }
    }

    private void showData() {
        FragmentManager fm = getFragmentManager();
        FragmentPagerAdapter modulesInfoAdapter = new FragmentPagerAdapter(fm) {

            @Override
            public int getCount() {
                return modules.size();
            }

            @Override
            public Fragment getItem(int pos) {
                FragmentModuleInfomation_ fragmentModuleInfomation_ = new FragmentModuleInfomation_();
                fragmentModuleInfomation_.setData(modules.get(pos));
                return fragmentModuleInfomation_;
            }

        };

        viewPager.setAdapter(modulesInfoAdapter);
        viewPager.setCurrentItem(pageStart);

        tab_position.setSeletedTab(modules.size(), pageStart);
        viewPager.setOnPageChangeListener(new OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                tab_position.setSeletedTab(modules.size(), position);
            }
        });

        // show Full Program
        showFullProgram();
    }

    private void showFullProgram() {
        if (fullProgram != null) {
            tv_price.setText("$" + fullProgram.getPrice());

            if (fullProgram.isUnlocked()) {
                tv_price.setVisibility(View.GONE);
                img_purchased.setVisibility(View.VISIBLE);
            } else {
                tv_price.setVisibility(View.VISIBLE);
                img_purchased.setVisibility(View.GONE);
            }

            tv_description.setVisibility(View.VISIBLE);
            tv_description.setText(getResources().getString(
                    R.string.access_to_all_5_modules));
        } else {
            // Update size for viewPager
            LayoutParams params = MLQPlusFunctions.getLayoutParams(
                    LayoutParams.MATCH_PARENT, 0);
            params.weight = 684;
            viewPager.setLayoutParams(params);

            ln_accessfull_program.setVisibility(View.GONE);
        }
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_module_information,
                ModuleInformationscreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_access_full_program,
                ModuleInformationscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_price,
                ModuleInformationscreenActivity.this,
                R.dimen.fragment_module_information_module_price);
        MLQPlusFunctions.SetViewTextSize(tv_description,
                ModuleInformationscreenActivity.this, R.dimen.module_des);

    }

    @Click
    void ln_back() {
        this.finish();
    }

    @Click
    void tv_price() {
        if (!MLQPlusFunctions.isInternetAvailable())
            showToast("Connection's lost. Please check your connection and try again");
        else {
            purchaseAnItem(MLQPlusConstant.FULL_MODULES, ModuleInformationscreenActivity.this,
                    new InAppPurchaseListenner() {

                        @Override
                        public void onPurchasedSuccessfully(String moduleID) {
                            // TODO Auto-generated method stub
                            // Update purchase full
                            fullProgram.setUnlocked(true);
                            for (int i = 0; i < 5 && i < modules.size(); i++) {
                                modules.get(i).setUnlocked(true);
                                eventBus.post(new PurchaseModuleEvent(modules
                                        .get(i).getId()));
                            }

                            // Update UI
                            showData();
                        }
                    });

        }
    }

    @Override
    public void onPurchasedSuccessfully(Purchase purchase, final String moduleID) {
        showWaitingDialog(ModuleInformationscreenActivity.this,
                "Updating data", "Please wait...");
        retryCount = 0;

        String json = "{\"module_id\": \""
                + moduleID
                + "\",\"platform\": \"0\",\"transaction\": \"test\",\"package_name\": \""
                + purchase.getPackageName() + "\",\"product_id\": \""
                + purchase.getSku() + "\",\"purchase_token\": \""
                + purchase.getToken() + "\"}";
        // If that's FullModule
        if (moduleID.equals(MLQPlusConstant.FULL_MODULES))
            json = "{\"platform\": \"0\",\"transaction\": \"test\",\"package_name\": \""
                    + purchase.getPackageName()
                    + "\",\"product_id\": \""
                    + purchase.getSku()
                    + "\",\"purchase_token\": \""
                    + purchase.getToken() + "\"}";

        // Test json
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "JSON");
            intent.putExtra(Intent.EXTRA_TEXT, json);
            Intent mailer = Intent.createChooser(intent, null);
            startActivity(mailer);
        } catch (Exception e) {
        }

        try {
            getSharedPreferencesService().purchaseJson().put(
                    new Gson().toJson(purchase));
            getSharedPreferencesService().purchaseModuleJson().put(json);
        } catch (Exception e) {
        }

        // Backup data to sharePreferencesService
        getSharedPreferencesService().purchasingModule().put(
                json + "@@@" + moduleID);

        if (moduleID.equals(MLQPlusConstant.FULL_MODULES))
            purchaseFullModulesOnOurServer(json, moduleID);
        else
            purchaseAModuleOnOurServer(json, moduleID);
    }

    private void purchaseAModuleOnOurServer(final String json,
                                            final String moduleID) {
        MLQPlusService.getBSSServices().purchaseAModule(
                MLQPlusFunctions.getAccessToken(), new TypedString(json),
                new Callback<ApiResponse<Response>>() {

                    @Override
                    public void success(ApiResponse<Response> arg0,
                                        Response arg1) {
                        dismissWaitingDialog();

                        // Remove purchasingModule in sharePreferencesService
                        getSharedPreferencesService().purchasingModule()
                                .put("");

                        updateUI(moduleID);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        retryCount++;
                        if (retryCount > 3) {
                            dismissWaitingDialog();
                            if (error.getCause() instanceof SocketTimeoutException)
                                showToast(MLQPlusConstant.TIMEOUT);
                            else if (!MLQPlusFunctions.isInternetAvailable())
                                showToast(MLQPlusConstant.CONNECTION_LOST);
                            else
                                showToast("Purchase module error: "
                                        + error.getMessage());
                        } else
                            purchaseAModuleOnOurServer(json, moduleID);
                    }
                });

    }

    private void purchaseFullModulesOnOurServer(final String json,
                                                final String moduleID) {
        MLQPlusService.getBSSServices().purchaseFullModule(
                MLQPlusFunctions.getAccessToken(), new TypedString(json),
                new Callback<ApiResponse<Response>>() {

                    @Override
                    public void success(ApiResponse<Response> arg0,
                                        Response arg1) {
                        // TODO Auto-generated method stub
                        dismissWaitingDialog();

                        // Remove purchasingModule in sharePreferencesService
                        getSharedPreferencesService().purchasingModule()
                                .put("");

                        updateUI(moduleID);

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        // TODO Auto-generated method stub
                        retryCount++;
                        if (retryCount > 3) {
                            dismissWaitingDialog();
                            if (error.getCause() instanceof SocketTimeoutException)
                                showToast(MLQPlusConstant.TIMEOUT);
                            else if (!MLQPlusFunctions.isInternetAvailable())
                                showToast(MLQPlusConstant.CONNECTION_LOST);
                            else
                                showToast("Purchase full modules error: "
                                        + error.getMessage());
                        } else
                            purchaseFullModulesOnOurServer(json, moduleID);
                    }
                });
    }
}
*/
