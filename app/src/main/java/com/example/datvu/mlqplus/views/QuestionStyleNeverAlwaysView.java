package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.view_question_style_never_always)
public class QuestionStyleNeverAlwaysView extends LinearLayout {

	private Context mContext;

	@ViewById
	ImageView img_level_0, img_level_1, img_level_2, img_level_3, img_level_4;
	
	@ViewById
	TextView tv_never, tv_always, tv_question, tv_question_number;

	private int selectedPosition = -1;

	public QuestionStyleNeverAlwaysView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public QuestionStyleNeverAlwaysView(Context context, AttributeSet attrs, int defStyleAttr,
										int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		mContext = context;
	}

	public QuestionStyleNeverAlwaysView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mContext = context;
	}

	public QuestionStyleNeverAlwaysView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}
	
	public void setQuestion(int number, String content)
	{
		// Calculate the margin left from second lines
		/*String question=number+". "+content;
		int marginLeftFromSecondLines=10;
		Rect bounds = new Rect();
		Paint textPaint = tv_question.getPaint();
		if ((number+". ").length()+1<question.length())
		{
			textPaint.getTextBounds(question,0,(number+". ").length()+1,bounds);
			marginLeftFromSecondLines = bounds.width();
		}
		
		tv_question.setText(MLQPlusFunctions.createIndentedText(question, 0, marginLeftFromSecondLines));*/
		
		String numberText = number + ". ";
		tv_question_number.setText(numberText);
		tv_question.setText(content);
		setTextSize();
	}
	
	public void setTextColor(int questionColor, int neverColor, int alwaysColor)
	{
		tv_question.setTextColor(questionColor);
		tv_never.setTextColor(neverColor);
		tv_always.setTextColor(alwaysColor);
		tv_question_number.setTextColor(questionColor);
	}

	public void setTextSize()
	{
		// Fix views in small screen
		if (MLQPlusConstant.SCREEN_WIDTH<650)
		{
			int width=(int)getResources().getDimension(R.dimen.marginBasex5);
			
			MLQPlusFunctions.setImageViewSizeMargin(img_level_0, width, LayoutParams.WRAP_CONTENT, 0, (int)getResources().getDimension(R.dimen.marginBasex2), (int)getResources().getDimension(R.dimen.marginBasex3), 0);
			MLQPlusFunctions.setImageViewSizeMargin(img_level_1, width, LayoutParams.WRAP_CONTENT, 0, 0, 0, 0);
			MLQPlusFunctions.setImageViewSizeMargin(img_level_2, width, LayoutParams.WRAP_CONTENT, 0, (int)getResources().getDimension(R.dimen.marginBasex3), (int)getResources().getDimension(R.dimen.marginBasex3), 0);
			MLQPlusFunctions.setImageViewSizeMargin(img_level_3, width, LayoutParams.WRAP_CONTENT, 0, 0, 0, 0);
			MLQPlusFunctions.setImageViewSizeMargin(img_level_4, width, LayoutParams.WRAP_CONTENT, 0, (int)getResources().getDimension(R.dimen.marginBasex3), (int)getResources().getDimension(R.dimen.marginBasex2), 0);
						
		}
		
		MLQPlusFunctions.SetViewTextSize(tv_never, mContext, R.dimen.moduleSectionTitle);
		MLQPlusFunctions.SetViewTextSize(tv_always, mContext, R.dimen.moduleSectionTitle);
		MLQPlusFunctions.SetViewTextSize(tv_question, mContext, R.dimen.moduleSectionTitle);
		MLQPlusFunctions.SetViewTextSize(tv_question_number, mContext, R.dimen.moduleSectionTitle);
	}
	
	@Click
	void img_level_0() {
		setSelectedPosition(0);
	}

	@Click
	void img_level_1() {
		setSelectedPosition(1);
	}

	@Click
	void img_level_2() {
		setSelectedPosition(2);
	}

	@Click
	void img_level_3() {
		setSelectedPosition(3);
	}

	@Click
	void img_level_4() {
		setSelectedPosition(4);
	}

	private void setSelectedPosition(int pos) {
		selectedPosition = pos;

		// reset views
		img_level_0.setImageResource(R.drawable.answer0_unselected);
		img_level_1.setImageResource(R.drawable.answer1_unselected);
		img_level_2.setImageResource(R.drawable.answer2_unselected);
		img_level_3.setImageResource(R.drawable.answer3_unselected);
		img_level_4.setImageResource(R.drawable.answer4_unselected);

		switch (pos) {
		case 0:
			img_level_0.setImageResource(R.drawable.answer0_selected);
			break;
		case 1:
			img_level_1.setImageResource(R.drawable.answer1_selected);
			break;
		case 2:
			img_level_2.setImageResource(R.drawable.answer2_selected);
			break;
		case 3:
			img_level_3.setImageResource(R.drawable.answer3_selected);
			break;
		case 4:
			img_level_4.setImageResource(R.drawable.answer4_selected);
			break;
		}
	}

	public int getAnswer() {
		return selectedPosition;
	}
}
