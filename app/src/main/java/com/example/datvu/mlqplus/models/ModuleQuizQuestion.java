package com.example.datvu.mlqplus.models;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ModuleQuizQuestion extends ActivityBase implements Serializable{
	@SerializedName("title")
	private String title;
	
	@SerializedName("questions")
	private List<ModuleQuestion> questions;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ModuleQuestion> getQuestions() {
		return questions;
	}

	public void setQuestions(List<ModuleQuestion> questions) {
		this.questions = questions;
	}
}
