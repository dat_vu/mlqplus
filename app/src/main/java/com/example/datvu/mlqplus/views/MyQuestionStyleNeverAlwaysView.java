package com.example.datvu.mlqplus.views;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;

import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

/**
 * We use @SuppressWarning here because our java code generator doesn't know
 * that there is no need to import OnXXXListeners from View as we already are in
 * a View.
 * 
 */
@SuppressWarnings("unused")
public final class MyQuestionStyleNeverAlwaysView extends
		QuestionStyleNeverAlwaysView implements HasViews, OnViewChangedListener {

	private boolean alreadyInflated_ = false;
	private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

	public MyQuestionStyleNeverAlwaysView(Context context) {
		super(context);
		init_();
	}

	public MyQuestionStyleNeverAlwaysView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init_();
	}

	public MyQuestionStyleNeverAlwaysView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init_();
	}

	public MyQuestionStyleNeverAlwaysView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		init_();
	}

	public static QuestionStyleNeverAlwaysView build(Context context) {
		QuestionStyleNeverAlwaysView_ instance = new QuestionStyleNeverAlwaysView_(
				context);
		instance.onFinishInflate();
		return instance;
	}

	/**
	 * The mAlreadyInflated_ hack is needed because of an Android bug which
	 * leads to infinite calls of onFinishInflate() when inflating a layout with
	 * a parent and using the <merge /> tag.
	 * 
	 */
	@Override
	public void onFinishInflate() {
		if (!alreadyInflated_) {
			alreadyInflated_ = true;
			inflate(getContext(), R.layout.view_question_style_never_always, this);
			onViewChangedNotifier_.notifyViewChanged(this);
		}
		super.onFinishInflate();
	}

	private void init_() {
		OnViewChangedNotifier previousNotifier = OnViewChangedNotifier
				.replaceNotifier(onViewChangedNotifier_);
		OnViewChangedNotifier.registerOnViewChangedListener(this);
		OnViewChangedNotifier.replaceNotifier(previousNotifier);
	}

	public static QuestionStyleNeverAlwaysView build(Context context,
			AttributeSet attrs) {
		QuestionStyleNeverAlwaysView_ instance = new QuestionStyleNeverAlwaysView_(
				context, attrs);
		instance.onFinishInflate();
		return instance;
	}

	public static QuestionStyleNeverAlwaysView build(Context context,
			AttributeSet attrs, int defStyleAttr) {
		QuestionStyleNeverAlwaysView_ instance = new QuestionStyleNeverAlwaysView_(
				context, attrs, defStyleAttr);
		instance.onFinishInflate();
		return instance;
	}

	public static QuestionStyleNeverAlwaysView build(Context context,
			AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		QuestionStyleNeverAlwaysView_ instance = new QuestionStyleNeverAlwaysView_(
				context, attrs, defStyleAttr, defStyleRes);
		instance.onFinishInflate();
		return instance;
	}

	@Override
	public void onViewChanged(HasViews hasViews) {
		img_level_3 = ((ImageView) hasViews.findViewById(R.id.img_level_3));
		tv_question = ((TextView) hasViews.findViewById(R.id.tv_question));
		img_level_4 = ((ImageView) hasViews.findViewById(R.id.img_level_4));
		tv_always = ((TextView) hasViews.findViewById(R.id.tv_always));
		img_level_1 = ((ImageView) hasViews.findViewById(R.id.img_level_1));
		img_level_0 = ((ImageView) hasViews.findViewById(R.id.img_level_0));
		tv_question_number = ((TextView) hasViews
				.findViewById(R.id.tv_question_number));
		tv_never = ((TextView) hasViews.findViewById(R.id.tv_never));
		img_level_2 = ((ImageView) hasViews.findViewById(R.id.img_level_2));
		if (img_level_2 != null) {
			img_level_2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					MyQuestionStyleNeverAlwaysView.this.img_level_2();
				}

			});
		}
		if (img_level_3 != null) {
			img_level_3.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					MyQuestionStyleNeverAlwaysView.this.img_level_3();
				}

			});
		}
		if (img_level_4 != null) {
			img_level_4.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					MyQuestionStyleNeverAlwaysView.this.img_level_4();
				}

			});
		}
		if (img_level_0 != null) {
			img_level_0.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					MyQuestionStyleNeverAlwaysView.this.img_level_0();
				}

			});
		}
		if (img_level_1 != null) {
			img_level_1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					MyQuestionStyleNeverAlwaysView.this.img_level_1();
				}

			});
		}
	}

}
