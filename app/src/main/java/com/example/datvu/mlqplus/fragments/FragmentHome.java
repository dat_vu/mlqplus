package com.example.datvu.mlqplus.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.activities.BaseActivity;
import com.example.datvu.mlqplus.activities.HomescreenActivity;
import com.example.datvu.mlqplus.activities.MainActivity;
import com.example.datvu.mlqplus.activities.MainActivity_;
import com.example.datvu.mlqplus.models.ConfigMenuBase;
import com.example.datvu.mlqplus.models.Module;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.example.datvu.mlqplus.views.CustomCircleView;
import com.example.datvu.mlqplus.views.MyCustomCircleView;
import com.example.datvu.mlqplus.views.WaitingView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import at.grabner.circleprogress.CircleProgressView;

@EFragment(R.layout.fragment_homescreen)
public class FragmentHome extends Fragment {
	public static FragmentHome newInstance() {
		return new FragmentHome_();
	}

	@ViewById
	LinearLayout ln_child, ln_menu;

	@ViewById
	ImageView img_progress_bg, imgFun, imgLearn, imgTools;

	@ViewById
	WaitingView vWaiting;

	@ViewById
	MyCustomCircleView ccv_fun, ccv_tools, ccv_learn;

	@ViewById
	CircleProgressView cpv_progress;

	@ViewById
	TextView tv_title, tv_hi_user, tv_if_your_actions, tv_progress, tv_fun,
			tv_tools, tv_learn, tv_percent;

	@ViewById
	FrameLayout fl_progress, fl_learn, fl_fun, fl_tools;

	@ViewById
	ScrollView scv;

	private Activity mActivity;
	private boolean isLoading = false;
	private String randomqQuote = "";

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Init Circle view
		MLQPlusFunctions
				.showCircleProgress(
						mActivity.getResources().getColor(
								R.color.blue_circle_progress),
						mActivity,
						cpv_progress,
						0,
						mActivity.getResources().getInteger(
								R.integer.card_circle_progress_size));

		// Set text size for all view
		setTextSize();

		// Set data
		//setData();

		// Load Data
		//loadData();

	}

	/*private void loadData() {
		if (MLQPlusConstant.sFullModule == null)
			vWaiting.showWaitingView();
		else
			showData();

		// Load data
		if (!MLQPlusFunctions.isInternetAvailable())
			((BaseActivity) mActivity)
					.showToast("Connection's lost. Please check your internet connection");
		else {
			isLoading = true;
			MLQPlusService.getBSSServices().getAllModules(
					MLQPlusFunctions.getAccessToken(),
					new Callback<ApiResponse<List<Module>>>() {

						@Override
						public void success(
								ApiResponse<List<Module>> mList_modules,
								Response arg1) {
							// TODO Auto-generated method stub
							MLQPlusConstant.sFullModule = new ArrayList<Module>();
							if (mList_modules != null
									&& mList_modules.getData() != null) {
								MLQPlusConstant.sFullModule.addAll(mList_modules.getData());

								MLQPlusService
										.getBSSServices()
										.getMyModules(
												MLQPlusFunctions
														.getAccessToken(),
												new Callback<ApiResponse<List<Module>>>() {

													@Override
													public void success(
															ApiResponse<List<Module>> mListUserModules,
															Response arg1) {
														// TODO Auto-generated
														// method stub
														isLoading = false;
														vWaiting.hideWaitingView();
														if (mListUserModules != null
																&& mListUserModules
																		.getData() != null) {
															// Update info from
															// user module to
															// all module
															updateModuleInfo(mListUserModules
																	.getData());

															// Show Data
															showData();

														}
													}

													@Override
													public void failure(
															RetrofitError error) {
														isLoading = false;
														vWaiting.hideWaitingView();
														try {
															if (error
																	.getCause() instanceof SocketTimeoutException)
																((BaseActivity) mActivity)
																		.showToast(MLQPlusConstant.TIMEOUT);
															else
																((BaseActivity) mActivity)
																		.showToast("Load modules failed: "
																				+ error.getMessage());
														} catch (Exception e) {
														}
														
													}
												});
							}
						}

						@Override
						public void failure(RetrofitError error) {
							// TODO Auto-generated method stub
							isLoading = false;
							vWaiting.hideWaitingView();
							try {
								if (error
										.getCause() instanceof SocketTimeoutException)
									((BaseActivity) mActivity)
											.showToast(MLQPlusConstant.TIMEOUT);
								else
									((BaseActivity) mActivity)
											.showToast("Load modules failed: "
													+ error.getMessage());
							} catch (Exception e) {
							}
						}
					});

			TypedString jsonTimeZone = new TypedString("{\"time_zone\": \""
					+ TimeZone.getDefault().getID() + "\"}");

			MLQPlusService.getBSSServices().getRandomQuote(
					MLQPlusFunctions.getAccessToken(), jsonTimeZone,
					new Callback<ApiResponse<RandomQuote>>() {

						@Override
						public void success(
								ApiResponse<RandomQuote> mRandomQuote,
								Response arg1) {
							// TODO Auto-generated method stub
							if (mRandomQuote != null
									&& mRandomQuote.getData() != null
									&& mRandomQuote.getData().getContent() != null) {
								randomqQuote = mRandomQuote.getData()
										.getContent();
								if (randomqQuote != null
										&& randomqQuote.length() > 0)
									tv_if_your_actions.setText("\""
											+ randomqQuote + "\"");
								else
									tv_if_your_actions.setText("");
							}
						}

						@Override
						public void failure(RetrofitError arg0) {
							// TODO Auto-generated method stub

						}
					});
		}
	}*/

	private void updateModuleInfo(List<Module> user_modules) {
		if (MLQPlusConstant.sFullModule != null && user_modules != null) // find module
			for (Module user_module : user_modules)
				for (Module module : MLQPlusConstant.sFullModule)
					if (module.getId().equals(user_module.getId())) {
						// update info
						module.setTotal_activity(user_module
								.getTotal_activity());
						module.setSubmitted_activity(user_module
								.getSubmitted_activity());
						module.setUnlocked(true);
					}
	}

	/*@Override
	public void onResume() {
		super.onResume();
		if (!isLoading)
			loadData();
		setTextSize();
	};*/

	private void showData() {
		/*
		 * // Fix view in small screen if (MLQPlusConstant.SCREEN_HEIGHT < 1000)
		 * { MLQPlusFunctions.setLayoutMargin(fl_progress, (int) getResources()
		 * .getDimension(R.dimen.marginBasex3), 0, 0, 0);
		 * MLQPlusFunctions.setLayoutMargin(fl_learn, (int) getResources()
		 * .getDimension(R.dimen.marginBasex5), 0, 0, 0);
		 * MLQPlusFunctions.setLayoutMargin(fl_fun, 0, (int) getResources()
		 * .getDimension(R.dimen.marginBasex3), 0, 0);
		 * MLQPlusFunctions.setLayoutMargin(fl_tools, 0, 0, (int)
		 * getResources().getDimension(R.dimen.marginBasex5), 0); }
		 */

		if (tv_hi_user != null && MLQPlusConstant.USERINFO.getUser() != null
				&& MLQPlusConstant.USERINFO.getUser().getFirst_name() != null)
			tv_hi_user.setText("Hi, "
					+ MLQPlusConstant.USERINFO.getUser().getFirst_name());

		// Calculate the total progress
		// Get only 5 core modules
		if (MLQPlusConstant.sFullModule != null) {
			MLQPlusConstant.CORE_MODULES = new ArrayList<Module>();
			float totalProgress = 0;
			for (int i = 0; i < 5 && i < MLQPlusConstant.sFullModule.size(); i++) {
				totalProgress += MLQPlusConstant.sFullModule.get(i).getProgress();

				// Store the progress of this module
				Module module = new Module(MLQPlusConstant.sFullModule.get(i).getName());
				module.setId(MLQPlusConstant.sFullModule.get(i).getId());
				module.setUnlocked(MLQPlusConstant.sFullModule.get(i).isUnlocked());
				module.setTotal_activity(MLQPlusConstant.sFullModule.get(i).getTotal_activity());
				module.setSubmitted_activity(MLQPlusConstant.sFullModule.get(i)
						.getSubmitted_activity());
				MLQPlusConstant.CORE_MODULES.add(module);
			}

			int overallProgress = (int) Math.round(totalProgress
					/ (MLQPlusConstant.sFullModule.size() > 5 ? 5 : MLQPlusConstant.sFullModule.size()));

			tv_percent.setText(overallProgress + "%");
			cpv_progress.setValue(overallProgress);

			// Update Menu data
			((HomescreenActivity) mActivity).setMenuData(overallProgress);
		}

		// Fake quotes to show
		if (randomqQuote != null && randomqQuote.length() > 0)
			tv_if_your_actions.setText("\"" + randomqQuote + "\"");
		else
			tv_if_your_actions.setText("");
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity,
				R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(tv_hi_user, mActivity,
				R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(tv_if_your_actions, mActivity,
				R.dimen.textMedium);

	}

	private String getIconUrl(String key, ArrayList<ConfigMenuBase> mainItems) {
		if (mainItems != null && mainItems.size() > 0)
			for (ConfigMenuBase item : mainItems)
				if (item.getConfig_name().toUpperCase()
						.equals(key.toUpperCase()))
					return item.getIcon();
		return null;
	}

	/*public void setData() {
		ccv_fun.setData(onCircleClickListenner, FragmentFun_.class.getName());
		ccv_learn.setData(onCircleClickListenner,
				FragmentLearn_.class.getName());
		ccv_tools.setData(onCircleClickListenner,
				FragmentTools_.class.getName());

		// Show icons
		if (MLQPlusConstant.CONFIG_MENU != null
				&& MLQPlusConstant.CONFIG_MENU.getMain() != null) {
			showIcons();
		} else
			MLQPlusFunctions.loadConfigMenu((BaseActivity) mActivity,
					loadConfigCallback);

		cpv_progress.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				float radius = img_progress_bg.getHeight() / 2;

				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					// change to being clicked background
					if (MLQPlusFunctions.distance(event.getX(), event.getY(),
							radius, radius) <= radius) {
						if (cpv_progress.getParent() != null) {
							cpv_progress.getParent()
									.requestDisallowInterceptTouchEvent(true);
						}
						img_progress_bg.setVisibility(View.VISIBLE);
					}
					break;

				case MotionEvent.ACTION_MOVE:
					// Check and change to normal backgroud when mouse is out of
					// border
					if (MLQPlusFunctions.distance(event.getX(), event.getY(),
							radius, radius) > radius)
						img_progress_bg.setVisibility(View.INVISIBLE);
					else
						img_progress_bg.setVisibility(View.VISIBLE);
					break;

				case MotionEvent.ACTION_UP:
					if (MLQPlusFunctions.distance(event.getX(), event.getY(),
							radius, radius) <= radius) {
						if (!MLQPlusFunctions.isInternetAvailable()) {
							((BaseActivity) mActivity)
									.showToast("Please check your internet connection and try again");
							img_progress_bg.setVisibility(View.INVISIBLE);
							return true;
						}

						// Prevent fragment Progress from moving to Quiz
						// progress
						//FragmentQuizzes.isStartFromFunScreen = true;

						Bundle bundle = new Bundle();
						bundle.putSerializable(MainActivity.DATA,
								FragmentProgress_.class.getName());
						((BaseActivity) mActivity).startNewActivityNoAnimation(
								mActivity, MainActivity_.class, false, bundle);

						img_progress_bg.setVisibility(View.INVISIBLE);
					}
					break;
				}
				return true;
			}
		});
	}*/

	private void showIcons() {
		String imgUrl;

		// Learn
		imgUrl = getIconUrl("learn", MLQPlusConstant.CONFIG_MENU.getMain());
		if (imgUrl != null && imgUrl.length() > 0)
			showWhiteIcons(imgUrl, imgLearn);

		// Fun
		imgUrl = getIconUrl("fun", MLQPlusConstant.CONFIG_MENU.getMain());
		if (imgUrl != null && imgUrl.length() > 0)
			showWhiteIcons(imgUrl, imgFun);

		// Tools
		imgUrl = getIconUrl("tool", MLQPlusConstant.CONFIG_MENU.getMain());
		if (imgUrl != null && imgUrl.length() > 0)
			showWhiteIcons(imgUrl, imgTools);
	}

	private void showWhiteIcons(String imgUrl, ImageView imgView) {
		MLQPlusFunctions.showImage(mActivity, imgUrl, imgView);
		imgView.setColorFilter(getResources().getColor(R.color.white));
	}

	CustomCircleView.OnClickListenner onCircleClickListenner = new CustomCircleView.OnClickListenner() {

		@Override
		public void onClick(String fragName) {
			if (!MLQPlusFunctions.isInternetAvailable()) {
				((BaseActivity) mActivity)
						.showToast("Please check your internet connection and try again");
				return;
			}

			Bundle bundle = new Bundle();
			bundle.putSerializable(MainActivity.DATA, fragName);
			((BaseActivity) mActivity).startNewActivityNoAnimation(mActivity,
					MainActivity_.class, false, bundle);
		}
	};

	@Click
	void ln_menu() {
		((HomescreenActivity) mActivity).showMenu();
	}

	/*Callback<ApiResponse<ConfigMenu>> loadConfigCallback = new Callback<ApiResponse<ConfigMenu>>() {

		@Override
		public void success(ApiResponse<ConfigMenu> configs, Response arg1) {
			// TODO Auto-generated method stub
			if (configs != null && configs.getData() != null) {
				MLQPlusConstant.CONFIG_MENU = configs.getData();
				showIcons();
			}
		}

		@Override
		public void failure(RetrofitError error) {
			// TODO Auto-generated method stub
			if (error.getCause() instanceof SocketTimeoutException)
				((BaseActivity) mActivity).showToast("Load config settings: "+MLQPlusConstant.TIMEOUT);
		}
	};

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			try {
				// Show icons
				if (MLQPlusConstant.CONFIG_MENU != null
						&& MLQPlusConstant.CONFIG_MENU.getMain() != null) {
					showIcons();
				} else
					MLQPlusFunctions.loadConfigMenu((BaseActivity) mActivity,
							loadConfigCallback);
			} catch (Exception e) {
			}
		}
	}*/

}
