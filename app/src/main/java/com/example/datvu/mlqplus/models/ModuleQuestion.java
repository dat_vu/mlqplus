package com.example.datvu.mlqplus.models;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ModuleQuestion extends ObjectBase implements Serializable{
	@SerializedName("question")
	private String question;
	
	@SerializedName("answers")
	private List<QuizAnswer> answers;
	
	@SerializedName("correctAnswer")
	private int correctAnswer;
	
	private Integer answer;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<QuizAnswer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<QuizAnswer> answers) {
		this.answers = answers;
	}

	public int getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(int correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public Integer getAnswer() {
		return answer;
	}

	public void setAnswer(Integer answer) {
		this.answer = answer;
	}
}
