package com.example.datvu.mlqplus.activities;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.services.SharedPreferencesService_;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;

import java.net.InetAddress;

public class BaseActivity extends Activity {
    private Toast mToast;
    private ProgressDialog progressDialog;
    private static SharedPreferencesService_ sharedPreferencesService;

    public void showToast(String msg) {

        if (mToast == null)
            mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        mToast.setText(msg);
        mToast.show();

    }

    public void showToast(String msg, int time) {

        if (mToast == null)
            mToast = Toast.makeText(this, "", time);

        mToast.setText(msg);
        mToast.show();

    }

    public void showWaitingDialog(Activity activity, String title, String msg) {
        try {
            progressDialog = ProgressDialog.show(activity, title, msg);
        } catch (Exception e) {
        }
    }

    public void dismissWaitingDialog() {
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
        }
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public SharedPreferencesService_ getSharedPreferencesService() {
        if (sharedPreferencesService == null)
            sharedPreferencesService = new SharedPreferencesService_(this);
        return sharedPreferencesService;
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()
                    .getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public void startAnimation(View view, int anim, Activity activity) {
        view.setVisibility(View.VISIBLE);
        AnimationSet animation = (AnimationSet) AnimationUtils.loadAnimation(activity, anim);
        view.setAnimation(animation);
    }

    public void startAnimation(View view, int anim, Activity activity, long limitTime, Animation.AnimationListener animationListener) {
        view.setVisibility(View.VISIBLE);
        AnimationSet animation = (AnimationSet) AnimationUtils.loadAnimation(activity, anim);
        animation.setDuration(limitTime);
        animation.setAnimationListener(animationListener);
        view.setAnimation(animation);
    }

/*	public void startNewActivity(Activity activity, Class<?> cls, boolean isFinish, boolean right2leftTransaction)
	{
		Intent intent=new Intent(activity, cls);
		activity.startActivity(intent);
		
		if (isFinish)
			activity.finish();
		
		// Start Animation
		if (right2leftTransaction)
			overridePendingTransition(R.anim.activity_show_right2left, R.anim.activity_hide_right2left);
		else
			overridePendingTransition(R.anim.activity_show_left2right, R.anim.activity_hide_left2right);
	}*/

    public void startNewActivityNoAnimation(Activity activity, Class<?> cls, boolean isFinish) {
        Intent intent = new Intent(activity, cls);
        activity.startActivity(intent);

        if (isFinish)
            activity.finish();

    }
	
/*	public void startNewActivity(Activity activity, Class<?> cls, boolean isFinish, int parentAnimation, int childAnimation)
	{
		Intent intent=new Intent(activity, cls);
		activity.startActivity(intent);
		
		if (isFinish)
			activity.finish();
		
		// Start Animation
		overridePendingTransition(childAnimation, parentAnimation);
	}*/
	
/*	public void startNewActivity(Activity activity, Class<?> cls, boolean isFinish, Bundle bundle, int parentAnimation, int childAnimation)
	{
		Intent intent=new Intent(activity, cls);
		intent.putExtra(MLQPlusConstant.BUNDLE, bundle);
		activity.startActivity(intent);
		
		if (isFinish)
			activity.finish();
		
		// Start Animation
		overridePendingTransition(childAnimation, parentAnimation);
	}*/
	
/*	public void startNewActivity(Activity activity, Class<?> cls, boolean isFinish, Bundle bundle, int requestCode)
	{
		Intent intent=new Intent(activity, cls);
		intent.putExtra(MLQPlusConstant.BUNDLE, bundle);
		activity.startActivityForResult(intent, requestCode);
		
		if (isFinish)
			activity.finish();
		
		// Start Animation
		overridePendingTransition(R.anim.activity_show_right2left, R.anim.activity_hide_right2left);
	}*/

    public void startNewActivityNoAnimation(Activity activity, Class<?> cls, boolean isFinish, Bundle bundle, int requestCode) {
        Intent intent = new Intent(activity, cls);
        intent.putExtra(MLQPlusConstant.BUNDLE, bundle);
        activity.startActivityForResult(intent, requestCode);

        if (isFinish)
            activity.finish();
    }
	
/*	public void startNewActivity(Activity activity, Class<?> cls, boolean isFinish, Bundle bundle)
	{
		Intent intent=new Intent(activity, cls);
		intent.putExtra(MLQPlusConstant.BUNDLE, bundle);
		activity.startActivity(intent);
		
		if (isFinish)
			activity.finish();
		
		// Start Animation
		overridePendingTransition(R.anim.activity_show_right2left, R.anim.activity_hide_right2left);
	}*/

    public void startNewActivityNoAnimation(Activity activity, Class<?> cls, boolean isFinish, Bundle bundle) {
        Intent intent = new Intent(activity, cls);
        intent.putExtra(MLQPlusConstant.BUNDLE, bundle);
        activity.startActivity(intent);

        if (isFinish)
            activity.finish();
    }

    public void startNewZoomActivity(Activity activity, Class<?> cls, Bundle bundle, int requestCode) {
        Intent intent = new Intent(activity, cls);
        intent.putExtra(MLQPlusConstant.BUNDLE, bundle);
        activity.startActivityForResult(intent, requestCode);

        // Start Animation
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
    }

    public void startNewZoomActivity(Activity activity, Class<?> cls, boolean isFinish, Bundle bundle) {
        Intent intent = new Intent(activity, cls);
        intent.putExtra(MLQPlusConstant.BUNDLE, bundle);
        activity.startActivity(intent);

        if (isFinish)
            activity.finish();

        // Start Animation
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
    }

    public void startNewZoomActivity(Activity activity, Class<?> cls, boolean isFinish) {
        Intent intent = new Intent(activity, cls);
        activity.startActivity(intent);

        if (isFinish)
            activity.finish();

        // Start Animation
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
		
/*		// Start Animation
		overridePendingTransition(R.anim.activity_show_left2right, R.anim.activity_hide_left2right);*/
    }

    @SuppressLint("NewApi")
    public void getScreenSize() {
        if (Build.VERSION.SDK_INT >= 11) {
            Point size = new Point();
            try {
                this.getWindowManager().getDefaultDisplay().getRealSize(size);
                MLQPlusConstant.SCREEN_WIDTH = size.x;
                MLQPlusConstant.SCREEN_HEIGHT = size.y;
            } catch (NoSuchMethodError e) {
                Log.i("error", "it can't work");
            }

        } else {
            DisplayMetrics metrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            MLQPlusConstant.SCREEN_WIDTH = metrics.widthPixels;
            MLQPlusConstant.SCREEN_HEIGHT = metrics.heightPixels;
        }
    }

    public boolean isInternetAvailable() {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name

            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        boolean handleReturn = super.dispatchTouchEvent(ev);

        View view = getCurrentFocus();

        if (!(view instanceof EditText)) {
            hideKeyboard();
        }

        return handleReturn;
    }

}
