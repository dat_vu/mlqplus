package com.example.datvu.mlqplus.dagger.components;

import android.app.Application;

import com.example.datvu.mlqplus.activities.LoadscreenActivity_;
import com.example.datvu.mlqplus.dagger.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by solit_000 on 4/14/2016.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    Application getApplication();
    void inject(LoadscreenActivity_ loadScreenAactivity);
}
