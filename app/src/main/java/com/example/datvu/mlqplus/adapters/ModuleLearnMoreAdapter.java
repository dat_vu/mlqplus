package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Module;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class ModuleLearnMoreAdapter extends ArrayAdapter<Module> {
    private Activity mContext;

    public ModuleLearnMoreAdapter(Activity context, int layoutID, List<Module> modules) {
        super(context, layoutID, modules);
        this.mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_learn_more, parent,
                    false);

        // Get item
        Module module = getItem(position);

        // Get view
        ImageView img_icon = (ImageView) convertView.findViewById(R.id.img_icon);
        ImageView img_purchased = (ImageView) convertView.findViewById(R.id.img_purchased);
        TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
        TextView tv_price = (TextView) convertView.findViewById(R.id.tv_price);
        View vHeaderLine = convertView.findViewById(R.id.vHeaderLine);

        // only show Header line at the first item
        if (position == 0)
            vHeaderLine.setVisibility(View.VISIBLE);
        else
            vHeaderLine.setVisibility(View.GONE);

        tv_name.setText(module.getName());
        tv_price.setText("$" + module.getPrice());
        MLQPlusFunctions.showImage(mContext, module.getIcon(), img_icon);

        tv_name.setSelected(true);
        tv_name.requestFocus();


        // Set text size
        MLQPlusFunctions.SetViewTextSize(tv_name, mContext, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_price, mContext, R.dimen.textMedium);

        //img_icon.setColorFilter(mContext.getResources().getColor(R.color.white));

        if (module.isUnlocked()) {
            tv_price.setVisibility(View.GONE);
            img_purchased.setVisibility(View.VISIBLE);
        } else {
            tv_price.setVisibility(View.VISIBLE);
            img_purchased.setVisibility(View.GONE);
        }

        return convertView;
    }
}
