/*
package com.example.datvu.mlqplus.fragments;

import java.util.ArrayList;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.activity.DownloadModuleActivity;
import com.mlqplus.trustedleader.activity.DownloadModuleActivity_;
import com.mlqplus.trustedleader.activity.MainActivity_;
import com.mlqplus.trustedleader.activity.ModuleInformationscreenActivity;
import com.mlqplus.trustedleader.activity.ModuleInformationscreenActivity_;
import com.mlqplus.trustedleader.adapter.ModuleLearnMoreAdapter;
import com.mlqplus.trustedleader.model.Module;
import com.mlqplus.trustedleader.model.PurchaseModuleEvent;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import de.greenrobot.event.EventBus;

@EFragment(R.layout.fragment_learnmore)
public class FragmentLearnMore extends FragmentBase {
	public static FragmentLearnMore newInstance() {
		return new FragmentLearnMore_();
	}  
      
	@ViewById
	LinearLayout ln_root, ln_child; 
   
	@ViewById  
	TextView tv_title;   
  
	@ViewById 
	ImageView img_back;

	@ViewById
	ListView lv_modules;

	private ArrayList<Module> modules_more, all_modules;
	private Activity mActivity;
	private EventBus eventBus;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Register event Bus
		try {
			eventBus = EventBus.getDefault();
			eventBus.register(this);
		} catch (Exception e) {
		}

		// Set text size for all view
		setTextSize();

		// Show Data
		showData();
	}

	public void setData(ArrayList<Module> modules) {
		if (modules != null)
			this.all_modules = modules;

		// Only show modules from 6 and above
		modules_more = new ArrayList<Module>();
		if (all_modules.size() > 5)
			for (int i = 5; i < all_modules.size(); i++)
				modules_more.add(all_modules.get(i));

	}

	public void onEvent(PurchaseModuleEvent event) {
		if (modules_more != null) {
			for (Module module : modules_more)
				if (module.getId().equals(event.getModuleID()))
					module.setUnlocked(true);
			showData();
		}
	}

	private void showData() {
		if (modules_more != null) {
			ModuleLearnMoreAdapter adapter = new ModuleLearnMoreAdapter(
					mActivity, 1, modules_more);
			lv_modules.setAdapter(adapter);
			lv_modules.setSelector(R.drawable.btn_first_quiz);
			lv_modules.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					if (modules_more.get(position).getPublished().equals("1"))
						if (!modules_more.get(position).isUnlocked()) {
							ArrayList<Module> list_temp = new ArrayList<Module>();
							list_temp.addAll(modules_more);
	
							Bundle bundle = new Bundle();
							bundle.putSerializable(
									ModuleInformationscreenActivity.LIST_MODULES,
									list_temp);
							bundle.putInt(
									ModuleInformationscreenActivity.PAGE_START,
									position);
							((BaseActivity) mActivity).startNewZoomActivity(
									mActivity,
									ModuleInformationscreenActivity_.class, false,
									bundle);
						}
						else
						{
							Bundle bundle = new Bundle();
							bundle.putString(DownloadModuleActivity.MODULE_ID,
									modules_more.get(position).getId());
							((BaseActivity) mActivity).startNewActivityNoAnimation(mActivity,
									DownloadModuleActivity_.class, false, bundle);
						}
					else
						((BaseActivity)mActivity).showToast("This module hasn't been published");
						
				}
			});
		}
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
	}

	@Click
	void ln_back() {
		// Back to fragment Learn
		FragmentLearn_ fragment = new FragmentLearn_();
		fragment.setData(all_modules);
		// Replace fragment
		if (mActivity instanceof MainActivity_)
			((MainActivity_) mActivity).replaceFragment(fragment, false);
	}
}
*/
