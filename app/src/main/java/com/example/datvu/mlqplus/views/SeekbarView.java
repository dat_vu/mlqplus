package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.view_seekbar_item)
public class SeekbarView extends LinearLayout {

	private Context mContext;

	@ViewById
	TextView tv_content;

	@ViewById
	SeekBar sb;

	public SeekbarView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public SeekbarView(Context context, AttributeSet attrs, int defStyleAttr,
					   int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public SeekbarView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public SeekbarView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}
	
	public void setValue(String content, int max, int progress)
	{
		tv_content.setText(content);
		sb.setMax(max);
		sb.setProgress(progress);
	}

	public void setTextSize()
	{
		MLQPlusFunctions.SetViewTextSize(tv_content, mContext, R.dimen.textMedium);
	}

	public int getValue() {
		return sb.getProgress();
	}
}
