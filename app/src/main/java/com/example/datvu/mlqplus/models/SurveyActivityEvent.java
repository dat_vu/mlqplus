package com.example.datvu.mlqplus.models;

public class SurveyActivityEvent {
	private String activityID;
	private String answer;
	
	public String getActivityID() {
		return activityID;
	}
	public void setActivityID(String activityID) {
		this.activityID = activityID;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public SurveyActivityEvent(String activityID, String answer) {
		super();
		this.activityID = activityID;
		this.answer = answer;
	}

}
