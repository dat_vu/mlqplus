package com.example.datvu.mlqplus.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ModuleTapCard  extends ActivityBase implements Serializable{
	@SerializedName("title")
	private String title;
	
	@SerializedName("backgroundColor")
	private String backgroundColor;
	
	@SerializedName("textColor")
	private String textColor;
	
	@SerializedName("tabs")
	private ArrayList<ModuleActivities> tabs;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public String getTextColor() {
		return textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}

	public ArrayList<ModuleActivities> getTabs() {
		return tabs;
	}

	public void setTabs(ArrayList<ModuleActivities> tabs) {
		this.tabs = tabs;
	}
	
	
}
