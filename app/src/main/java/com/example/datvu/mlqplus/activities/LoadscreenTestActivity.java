/*
package com.example.datvu.mlqplus.activities;

import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.model.WelcomeScreenInfo;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@EActivity(R.layout.activity_loadscreentest)
public class LoadscreenTestActivity extends BaseActivity {

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    ImageView img_logo;

    @ViewById
    TextView tvText;

    @ViewById
    EditText edtZoomsizeFrom, edtZoomsizeTo, edtDuration;

    @ViewById
    Button btnApply;

    private ArrayList<WelcomeScreenInfo> mALWelcomeScreen = null;
    private int currentImageCount = 0;
    private int animationCount = 0;
    private Animation zoomin, zoomout;

    @AfterViews
    void init() {
        // Get SETTING
        MLQPlusConstant.TEXTSIZE = MLQPlusFunctions
                .loadTextSize(LoadscreenTestActivity.this);

        // Get screen size
        getScreenSize();

        // Set TextSize
        setTextSize();

        // Show Data
        showData();

        // Start animation
        startAnimation();

    }

    private void setTextSize() {
        // TODO Auto-generated method stub
        MLQPlusFunctions.SetViewTextSize(tvText, LoadscreenTestActivity.this,
                R.dimen.textMedium);
    }

    private void showData() {
        // Get List texts
        List<String> loadingTexts = MLQPlusFunctions.getLoadingTextList();

        // Select random text to show
        Random random = new Random();
        tvText.setText(loadingTexts.get(random.nextInt(loadingTexts.size())));
    }

    private void startAnimation() {

        zoomin = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
        zoomout = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
        zoomin.setAnimationListener(zoomInListenner);
        zoomout.setAnimationListener(zoomInListenner);

        img_logo.startAnimation(zoomin);
    }

    @Click
    void btnApply() {
        try {
            float zoomSizeFrom = Float.parseFloat(edtZoomsizeFrom.getText().toString());
            float zoomSizeTo = Float.parseFloat(edtZoomsizeTo.getText().toString());
            int duration = Integer.parseInt(edtDuration.getText().toString());

            if (zoomSizeFrom < 0 || duration < 0 || zoomSizeFrom > zoomSizeTo) {
                showToast("Invalid numbes");
            } else {

                zoomin = new ScaleAnimation(zoomSizeFrom, zoomSizeTo, zoomSizeFrom, zoomSizeTo,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f);
                zoomout = new ScaleAnimation(zoomSizeTo, zoomSizeFrom, zoomSizeTo, zoomSizeFrom,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f);

                zoomin.setAnimationListener(zoomInListenner);
                zoomout.setAnimationListener(zoomInListenner);

                zoomin.setDuration(duration);
                zoomout.setDuration(duration);
            }

        } catch (Exception e) {
        }

    }

    AnimationListener zoomInListenner = new AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            // TODO Auto-generated method stub
            img_logo.startAnimation(zoomout);
        }
    };

    AnimationListener zoomOutListenner = new AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            // TODO Auto-generated method stub
            img_logo.startAnimation(zoomin);
        }
    };

    @Click
    void ln_child() {
        MLQPlusFunctions.hideKeyboard(LoadscreenTestActivity.this);
    }

}
*/
