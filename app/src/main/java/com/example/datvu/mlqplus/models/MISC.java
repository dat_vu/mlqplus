package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class MISC {
	@SerializedName("emailBody")
	private String emailBody;

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}
	
	
}
