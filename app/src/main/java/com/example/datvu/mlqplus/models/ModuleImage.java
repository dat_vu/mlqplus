package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class ModuleImage  extends ActivityBase{
	@SerializedName("imageId")
	private String imageId;
	
	@SerializedName("imageUrl")
	private String imageUrl;

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
}
