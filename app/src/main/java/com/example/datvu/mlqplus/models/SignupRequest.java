package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class SignupRequest extends LoginRequestBase {
	@SerializedName("email")
	private String email;

	@SerializedName("first_name")
	private String first_name;

	@SerializedName("last_name")
	private String last_name;

	@SerializedName("age")
	private String age;

	@SerializedName("gender")
	private String gender;

	@SerializedName("country_id")
	private String country_id;

	@SerializedName("password")
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry_id() {
		return country_id;
	}

	public void setCountry_id(String country_id) {
		this.country_id = country_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public SignupRequest(String email, String first_name,
			String age, String gender, String country_id, String password) {
		super();
		this.email = email;
		this.first_name = first_name;
		this.last_name = "unknown";
		this.age = age;
		this.gender = gender;
		this.country_id = country_id;
		this.password = password;
	}
	
	public SignupRequest(){}

}
