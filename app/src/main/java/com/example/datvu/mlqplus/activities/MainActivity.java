package com.example.datvu.mlqplus.activities;


import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.ConfigMenuBase;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    @ViewById
    LinearLayout ln_root, ln_child, lnProgress, lnLearn, lnFun, lnTools;

    @ViewById
    FrameLayout container;

    @ViewById
    TextView tvProgress, tvLearn, tvFun, tvTools;

    @ViewById
    ImageView img_progress, img_learn, img_fun, img_tools;
    // save current fragment index to show suitable transaction
    // Tools: 3; Fun: 2; Learn: 1; Progress: 0; Others: -1
    private int currentFragmentIndex = -1;
    public static String DATA = "data";

    //private String fragName = FragmentProgress_.class.getName();

    @AfterViews
    void init() {
        getScreenSize();

        // Creat zoomable layout
        // MLQPlusFunctions.makeZoomableView(MainActivity.this, ln_root,
        // ln_child);

        // Show Data
        //showData();
    }

    /*private void showData() {
        try {
            fragName = getIntent().getBundleExtra(MLQPlusConstant.BUNDLE)
                    .getString(DATA);
            Fragment fragment = (Fragment) (Class.forName(fragName)
                    .newInstance());
            replaceFragmentNoAnimation(fragment);
        } catch (Exception e) {
        }

        // Show icons for tabs
       *//* if (MLQPlusConstant.CONFIG_MENU != null
                && MLQPlusConstant.CONFIG_MENU.getMain() != null) {
            showIcons();
        } else
            MLQPlusFunctions.loadConfigMenu(MainActivity.this,
                    loadConfigCallback);*//*
    }*/

    /*Callback<ApiResponse<ConfigMenu>> loadConfigCallback = new Callback<ApiResponse<ConfigMenu>>() {

        @Override
        public void success(ApiResponse<ConfigMenu> configs, Response arg1) {
            // TODO Auto-generated method stub
            if (configs != null && configs.getData() != null) {
                MLQPlusConstant.CONFIG_MENU = configs.getData();
                showIcons();
            }
        }

        @Override
        public void failure(RetrofitError error) {
            // TODO Auto-generated method stub
            if (error.getCause() instanceof SocketTimeoutException)
                showToast(MLQPlusConstant.TIMEOUT);
        }
    };*/

    private void showIcons() {
        String imgUrl;

        // Learn
        imgUrl = getIconUrl("learn", MLQPlusConstant.CONFIG_MENU.getMain());
        if (imgUrl != null && imgUrl.length() > 0) {
            if (currentFragmentIndex == 1)
                showWhiteIcons(imgUrl, img_learn);
            else
                showBlueIcons(imgUrl, img_learn);
        }

        // Fun
        imgUrl = getIconUrl("fun", MLQPlusConstant.CONFIG_MENU.getMain());
        if (imgUrl != null && imgUrl.length() > 0) {
            if (currentFragmentIndex == 2)
                showWhiteIcons(imgUrl, img_fun);
            else
                showBlueIcons(imgUrl, img_fun);
        }

        // Tools
        imgUrl = getIconUrl("tool", MLQPlusConstant.CONFIG_MENU.getMain());
        if (imgUrl != null && imgUrl.length() > 0) {
            if (currentFragmentIndex == 3)
                showWhiteIcons(imgUrl, img_tools);
            else
                showBlueIcons(imgUrl, img_tools);
        }

        if (currentFragmentIndex == -1) {
            // reset all
            formatATab(img_progress, tvProgress, true);
            formatATab(img_learn, tvLearn, true);
            formatATab(img_fun, tvFun, true);
            formatATab(img_tools, tvTools, true);
        }
    }

    private void showWhiteIcons(String imgUrl, ImageView imgView) {
        MLQPlusFunctions.showImage(MainActivity.this, imgUrl, imgView);
        imgView.setColorFilter(getResources().getColor(R.color.white));
    }

    private void showBlueIcons(String imgUrl, ImageView imgView) {
        MLQPlusFunctions.showImage(MainActivity.this, imgUrl, imgView);
        imgView.setColorFilter(getResources()
                .getColor(R.color.blue_locked_item));
    }

    private String getIconUrl(String key, ArrayList<ConfigMenuBase> mainItems) {
        if (mainItems != null && mainItems.size() > 0)
            for (ConfigMenuBase item : mainItems)
                if (item.getConfig_name().toUpperCase()
                        .equals(key.toUpperCase()))
                    return item.getIcon();
        return null;
    }

    /*public void replaceFragmentNoAnimation(Fragment fragment) {
        if (fragment != null) {
            int newFragmentIndex = getFragmentIndex(fragment);
            if (newFragmentIndex < 0
                    || newFragmentIndex != currentFragmentIndex) {
                FragmentManager manager = getFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();

                ft.replace(R.id.container, fragment);
                ft.commit();
                currentFragmentIndex = newFragmentIndex;
                updateTabindicator();
            }
        }
    }*/

    /*public void replaceFragment(Fragment fragment,
                                boolean isRighttoLeftTransaction) {
        if (fragment != null) {
            int newFragmentIndex = getFragmentIndex(fragment);
            if (newFragmentIndex < 0
                    || newFragmentIndex != currentFragmentIndex) {
                FragmentManager manager = getFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();

                // Show suitable transaction
                if (newFragmentIndex >= 0 && newFragmentIndex < 4
                        && currentFragmentIndex >= 0
                        && currentFragmentIndex < 4) {
                    if (newFragmentIndex > currentFragmentIndex)
                        ft.setCustomAnimations(R.anim.slide_in_left,
                                R.anim.slide_out_left, 0, 0);
                    else
                        ft.setCustomAnimations(R.anim.slide_in_right,
                                R.anim.slide_out_right, 0, 0);
                } else if (isRighttoLeftTransaction) {
                    ft.setCustomAnimations(R.anim.slide_in_left,
                            R.anim.slide_out_left, 0, 0);
                } else
                    ft.setCustomAnimations(R.anim.slide_in_right,
                            R.anim.slide_out_right, 0, 0);

                ft.replace(R.id.container, fragment);
                ft.commit();
                // manager.executePendingTransactions();
                currentFragmentIndex = newFragmentIndex;
                updateTabindicator();
            }
        }
    }*/

    /*private int getFragmentIndex(Fragment fragment) {
        if (fragment instanceof FragmentProgress_)
            return 0;
        else if (fragment instanceof FragmentLearn_)
            return 1;
        else if (fragment instanceof FragmentFun_)
            return 2;
        else if (fragment instanceof FragmentTools_)
            return 3;
        else if (fragment instanceof FragmentLearnMore_)
            return 4;
        else
            return -1;
    }*/

    /*@Override
    public void onBackPressed() {
        Fragment f = getFragmentManager().findFragmentById(R.id.container);

        if (f instanceof FragmentQuizzes_
                || f instanceof FragmentLeadershipQuotes_
                || f instanceof FragmentSurveys_) {
            if (f instanceof FragmentQuizzes_
                    && !FragmentQuizzes.isStartFromFunScreen) {
                FragmentProgress fragProgress = FragmentProgress.newInstance();
                MLQPlusFunctions.replaceFragment(this, fragProgress, false);
            } else {
                FragmentFun_ fragFun = new FragmentFun_();
                MLQPlusFunctions.replaceFragment(this, fragFun, false);
            }
        } else if (f instanceof FragmentAccount_
                || f instanceof FragmentPassword_
                || f instanceof FragmentNotifications_
                || f instanceof FragmentFont_) {
            FragmentSettings_ fragSetting = new FragmentSettings_();
            MLQPlusFunctions.replaceFragment(this, fragSetting, false);
        } else if (f instanceof FragmentLeaderTools_
                || f instanceof FragmentAssessment_
                || f instanceof FragmentLinks_) {
            FragmentTools_ fragTool = new FragmentTools_();
            MLQPlusFunctions.replaceFragment(this, fragTool, false);
        } else
            super.onBackPressed();
    }*/

    /*@Click
    void lnProgress() {
        FragmentProgress_ fragment = new FragmentProgress_();
        replaceFragment(fragment, true);
    }*/

    /*@Click
    void lnLearn() {
        FragmentLearn_ fragment = new FragmentLearn_();
        replaceFragment(fragment, true);
    }*/

    /*@Click
    void lnFun() {
        FragmentFun_ fragment = new FragmentFun_();
        replaceFragment(fragment, true);
    }*/

    /*@Click
    void lnTools() {
        FragmentTools_ fragment = new FragmentTools_();
        replaceFragment(fragment, true);
    }*/

    /*public void navigateToLearn() {
        lnLearn();
    }*/

    private void updateTabindicator() {
        // If current is Progress/Learn/Fun/Tools Fragment
        if (currentFragmentIndex >= 0 && currentFragmentIndex < 5) {
            // reset all
            formatATab(img_progress, tvProgress, false);
            formatATab(img_learn, tvLearn, false);
            formatATab(img_fun, tvFun, false);
            formatATab(img_tools, tvTools, false);

            // change icon for selected fragment
            switch (currentFragmentIndex) {
                case 0:
                    formatATab(img_progress, tvProgress, true);
                    break;
                case 1:
                    formatATab(img_learn, tvLearn, true);
                    break;
                case 2:
                    formatATab(img_fun, tvFun, true);
                    break;
                case 3:
                    formatATab(img_tools, tvTools, true);
                    break;
                case 4:
                    formatATab(img_learn, tvLearn, true);
                    break;
            }
        } else {
            // reset all
            formatATab(img_progress, tvProgress, true);
            formatATab(img_learn, tvLearn, true);
            formatATab(img_fun, tvFun, true);
            formatATab(img_tools, tvTools, true);
        }

    }

    public void formatATab(ImageView img, TextView tv, boolean isSelected) {
        if (!isSelected) {
            img.setColorFilter(getResources()
                    .getColor(R.color.blue_locked_item));
            tv.setTextColor(getResources().getColor(R.color.blue_locked_item));
        } else {
            img.setColorFilter(getResources().getColor(R.color.white));
            tv.setTextColor(getResources().getColor(R.color.white));
        }

    }

    public void setCurrentFragmentIndex(int newIndex) {
        this.currentFragmentIndex = newIndex;
    }
}
