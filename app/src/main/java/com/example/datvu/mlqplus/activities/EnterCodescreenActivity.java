/*
package com.example.datvu.mlqplus.activities;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.activity_entercodescreen)
public class EnterCodescreenActivity extends BaseDialogActivity {

    @ViewById
    LinearLayout ln_root, ln_child, ln_enter_code_view, ln_code_accepted;

    @ViewById
    TextView tv_enter_code, tv_pls_enter, tv_code, tv_incorrect_code, tv_cancel,
            tv_code_accepted, tv_you_now, tv_continue;

    @ViewById
    EditText edt_code1, edt_code2, edt_code3, edt_code4;

    @ViewById
    Button btn_submit, btn_continue;

    private int count = 0;

    @AfterViews
    void init() {
        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        // Show data
        showData();
    }

    private void showData() {
        tv_incorrect_code.setVisibility(View.INVISIBLE);
        ln_code_accepted.setVisibility(View.GONE);
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_enter_code, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_pls_enter, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_code, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_incorrect_code, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_cancel, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_code_accepted, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_you_now, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_continue, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(edt_code1, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(edt_code2, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(edt_code3, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(edt_code4, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(btn_submit, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(btn_continue, EnterCodescreenActivity.this, R.dimen.textMediumLarge);
    }

    @Click
    void btn_submit() {
        if (count == 0) {
            tv_incorrect_code.setVisibility(View.VISIBLE);
            count++;
        } else {
            ln_enter_code_view.setVisibility(View.GONE);
            ln_code_accepted.setVisibility(View.VISIBLE);
            btn_submit.setVisibility(View.GONE);
            tv_cancel.setVisibility(View.GONE);
            btn_continue.setVisibility(View.VISIBLE);
        }
    }

    @Click
    void btn_continue() {
        this.finish();
    }

    @Click
    void tv_cancel() {
        this.finish();
    }

    @Click
    void ln_child() {
        MLQPlusFunctions.hideKeyboard(EnterCodescreenActivity.this);
    }

    @Click
    void tv_enter_code() {
        MLQPlusFunctions.hideKeyboard(EnterCodescreenActivity.this);
    }
}
*/
