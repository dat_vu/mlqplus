package com.example.datvu.mlqplus.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;

public class WaitingView extends FrameLayout {
	
	private Context mContext;
	private FrameLayout flWaitingView;
	private TextView tvMsg, tvSubMsg;
	
	public WaitingView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public WaitingView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
		initView();
	}

	public WaitingView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
		initView();
	}

	public WaitingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
		initView();
	}
	
	private void initView()
	{
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null){       
            inflater.inflate(R.layout.view_waiting, this);
        }
        
        // View by ID
        flWaitingView=(FrameLayout) findViewById(R.id.flWaitingView);
        tvSubMsg=(TextView) findViewById(R.id.tvSubMsg);
        tvMsg=(TextView) findViewById(R.id.tvMsg);
	}
	
	public void showWaitingView()
	{
		flWaitingView.setVisibility(View.VISIBLE);
	}
	
	public void hideWaitingView()
	{
		flWaitingView.setVisibility(View.GONE);
	}
	
	public void setMsg(String msg, String subMsg)
	{
		if (msg!=null)
			tvSubMsg.setText(msg);
		if (subMsg!=null)
		{
			tvMsg.setVisibility(View.VISIBLE);
			tvMsg.setText(subMsg);
		}
	}

}
