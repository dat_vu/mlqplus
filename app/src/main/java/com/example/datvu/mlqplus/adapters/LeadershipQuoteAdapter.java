package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.LeadershipQuote;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.ArrayList;

public class LeadershipQuoteAdapter extends BaseAdapter {
    private Activity mContext;
    private int mLayoutID;
    private ArrayList<LeadershipQuote> mALQuote = null;
    private ViewHolder mViewHolder = null;

    public LeadershipQuoteAdapter(Activity mContext, int mLayoutID,
                                  ArrayList<LeadershipQuote> mALQuote) {
        this.mContext = mContext;
        this.mLayoutID = mLayoutID;
        this.mALQuote = mALQuote;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(mLayoutID, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            mViewHolder.tv_des = (TextView) convertView
                    .findViewById(R.id.tv_des);
            mViewHolder.img_icon = (ImageView) convertView
                    .findViewById(R.id.img_icon);
            mViewHolder.img_locked_icon = (ImageView) convertView
                    .findViewById(R.id.img_locked_icon);
            mViewHolder.firstLine = convertView.findViewById(R.id.firstLine);
            mViewHolder.img_unlocked_icon = (ImageView) convertView.findViewById(R.id.img_unlocked_icon);

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();


        LeadershipQuote quote = mALQuote.get(position);

        if (position == 0)
            mViewHolder.firstLine.setVisibility(View.VISIBLE);
        else
            mViewHolder.firstLine.setVisibility(View.GONE);


        mViewHolder.tv_name.setText(quote
                .getName());
        mViewHolder.tv_des.setText(quote
                .getDescription());
        boolean isLocked = !quote.isUnlocked();
        mViewHolder.img_icon.setImageResource(R.drawable.quotes_icon);
        if (isLocked) {
            mViewHolder.tv_des.setVisibility(View.VISIBLE);

            mViewHolder.img_locked_icon.setColorFilter(mContext.getResources().getColor(R.color.blue_locked_item));
            mViewHolder.img_icon.setColorFilter(mContext.getResources().getColor(R.color.blue_locked_item));

            mViewHolder.tv_name.setTextColor(mContext.getResources().getColor(R.color.blue_locked_item));
            mViewHolder.tv_des.setTextColor(mContext.getResources().getColor(R.color.blue_locked_item));

            mViewHolder.tv_des.setSelected(true);
            mViewHolder.tv_des.requestFocus();
        } else {
            mViewHolder.tv_des.setVisibility(View.GONE);
            mViewHolder.tv_name.setTextColor(mContext.getResources().getColor(R.color.white));
            mViewHolder.tv_des.setTextColor(mContext.getResources().getColor(R.color.white));

            mViewHolder.img_locked_icon.setColorFilter(mContext.getResources().getColor(R.color.white));
            mViewHolder.img_icon.setColorFilter(mContext.getResources().getColor(R.color.white));
            mViewHolder.img_unlocked_icon.setVisibility(View.VISIBLE);
            mViewHolder.img_locked_icon.setVisibility(View.GONE);
        }

        mViewHolder.tv_name.setSelected(true);
        mViewHolder.tv_name.requestFocus();

        // Set text size for widgets
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_name, mContext, R.dimen.itemTitle);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_des, mContext, R.dimen.itemDescript);

        return convertView;
    }

    @Override
    public int getCount() {
        return mALQuote.size();
    }

    @Override
    public Object getItem(int position) {
        return mALQuote.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        TextView tv_name;
        TextView tv_des;
        ImageView img_icon;
        ImageView img_locked_icon, img_unlocked_icon;
        View firstLine;
    }

}

