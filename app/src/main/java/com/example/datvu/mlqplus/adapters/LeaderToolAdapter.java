package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.LeaderTool;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class LeaderToolAdapter extends ArrayAdapter<LeaderTool> {
    private Activity mContext = null;
    private ViewHolder mViewHolder = null;
    private int mLayoutID;

    public LeaderToolAdapter(Activity mContext, int mLayoutID,
                             List<LeaderTool> mALLeaderTool) {
        super(mContext, mLayoutID, mALLeaderTool);
        this.mContext = mContext;
        this.mLayoutID = mLayoutID;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(mLayoutID, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            mViewHolder.tv_des = (TextView) convertView
                    .findViewById(R.id.tv_des);
            mViewHolder.img_icon = (ImageView) convertView
                    .findViewById(R.id.img_icon);
            mViewHolder.img_locked_icon = (ImageView) convertView
                    .findViewById(R.id.img_locked_icon);
            mViewHolder.img_unlocked_icon = (ImageView) convertView
                    .findViewById(R.id.img_unlocked_icon);
            mViewHolder.vHeaderLine = convertView
                    .findViewById(R.id.vHeaderLine);
            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        LeaderTool leaderTool = getItem(position);

        // Only show the header line in the first child
        if (position == 0)
            mViewHolder.vHeaderLine.setVisibility(View.VISIBLE);
        else
            mViewHolder.vHeaderLine.setVisibility(View.GONE);

        mViewHolder.tv_name.setText(leaderTool.getName());
        mViewHolder.tv_des.setText(leaderTool.getDescription());

        mViewHolder.tv_name.setSelected(true);
        mViewHolder.tv_name.requestFocus();

        mViewHolder.tv_des.setSelected(true);
        mViewHolder.tv_des.requestFocus();

        MLQPlusFunctions.showImage(mContext, leaderTool.getIcon(),
                mViewHolder.img_icon);

        boolean isUnLocked = leaderTool.isUnlock();
        if (!isUnLocked) {
            mViewHolder.img_locked_icon.setVisibility(View.VISIBLE);
            mViewHolder.img_unlocked_icon.setVisibility(View.GONE);
            mViewHolder.tv_des.setVisibility(View.VISIBLE);
            mViewHolder.img_locked_icon.setColorFilter(mContext.getResources()
                    .getColor(R.color.blue_locked_item));
            mViewHolder.img_icon.setAlpha(0.5f);

            mViewHolder.tv_name.setTextColor(mContext.getResources().getColor(
                    R.color.blue_locked_item));
            mViewHolder.tv_des.setTextColor(mContext.getResources().getColor(
                    R.color.blue_locked_item));

        } else {
            mViewHolder.img_locked_icon.clearColorFilter();
            mViewHolder.img_icon.setAlpha(1f);

            mViewHolder.tv_name.setTextColor(mContext.getResources().getColor(
                    R.color.white));
            mViewHolder.tv_des.setTextColor(mContext.getResources().getColor(
                    R.color.white));
            mViewHolder.tv_des.setVisibility(View.GONE);

            mViewHolder.img_locked_icon.setVisibility(View.GONE);
            mViewHolder.img_unlocked_icon.setVisibility(View.VISIBLE);
        }

        // Set text size for widgets
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_name, mContext,
                R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_des, mContext,
                R.dimen.textMedium);

        return convertView;
    }

    private class ViewHolder {
        TextView tv_name;
        TextView tv_des;
        View vHeaderLine;
        ImageView img_icon;
        ImageView img_locked_icon;
        ImageView img_unlocked_icon;
    }

}
