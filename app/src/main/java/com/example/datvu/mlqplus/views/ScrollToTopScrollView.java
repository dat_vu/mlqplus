package com.example.datvu.mlqplus.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;

public class ScrollToTopScrollView extends ScrollView{

	boolean isBottom = true;
	@SuppressLint("NewApi")
	public ScrollToTopScrollView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
	}

	public ScrollToTopScrollView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public ScrollToTopScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public ScrollToTopScrollView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
   
	
	public void setTop()
	{
		isBottom = false;
	}
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		if(isBottom) 
			{
				return;
			}
		super.onDraw(canvas);
	}
	
	
}
