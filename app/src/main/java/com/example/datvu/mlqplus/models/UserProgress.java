package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class UserProgress {
	@SerializedName("attemp")
	private int attemp;
	
	@SerializedName("best_score")
	private int best_score;
	
	@SerializedName("average_score")
	private double average_score;
	
	@SerializedName("quiz_rank")
	private String quiz_rank;

	public int getAttemp() {
		return attemp;
	}

	public void setAttemp(int attemp) {
		this.attemp = attemp;
	}

	public int getBest_score() {
		return best_score;
	}

	public void setBest_score(int best_score) {
		this.best_score = best_score;
	}

	public double getAverage_score() {
		return average_score;
	}

	public void setAverage_score(double average_score) {
		this.average_score = average_score;
	}

	public String getQuiz_rank() {
		return quiz_rank;
	}

	public void setQuiz_rank(String quiz_rank) {
		this.quiz_rank = quiz_rank;
	}
	
}
