package com.example.datvu.mlqplus.models;

public class TickboxQuestion {
	private boolean isSelected;
	private String answer;

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public TickboxQuestion(boolean isSelected, String answer) {
		super();
		this.isSelected = isSelected;
		this.answer = answer;
	}

}
