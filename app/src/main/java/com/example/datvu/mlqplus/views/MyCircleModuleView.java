package com.example.datvu.mlqplus.views;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;

import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

/**
* We use @SuppressWarning here because our java code
* generator doesn't know that there is no need
* to import OnXXXListeners from View as we already
* are in a View.
* 
*/
@SuppressWarnings("unused")
public final class MyCircleModuleView
 extends CircleModuleView
 implements HasViews, OnViewChangedListener
{

 private boolean alreadyInflated_ = false;
 private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

 public MyCircleModuleView(Context context) {
     super(context);
     init_();
 }

 public MyCircleModuleView(Context context, AttributeSet attrs) {
     super(context, attrs);
     init_();
 }

 public MyCircleModuleView(Context context, AttributeSet attrs, int defStyleAttr) {
     super(context, attrs, defStyleAttr);
     init_();
 }

 public MyCircleModuleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
     super(context, attrs, defStyleAttr, defStyleRes);
     init_();
 }

 public static CircleModuleView build(Context context) {
     CircleModuleView_ instance = new CircleModuleView_(context);
     instance.onFinishInflate();
     return instance;
 }

 /**
  * The mAlreadyInflated_ hack is needed because of an Android bug
  * which leads to infinite calls of onFinishInflate()
  * when inflating a layout with a parent and using
  * the <merge /> tag.
  * 
  */
 @Override
 public void onFinishInflate() {
     if (!alreadyInflated_) {
         alreadyInflated_ = true;
         inflate(getContext(), R.layout.view_circle_module, this);
         onViewChangedNotifier_.notifyViewChanged(this);
     }
     super.onFinishInflate();
 }

 private void init_() {
     OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(onViewChangedNotifier_);
     OnViewChangedNotifier.registerOnViewChangedListener(this);
     OnViewChangedNotifier.replaceNotifier(previousNotifier);
 }

 public static CircleModuleView build(Context context, AttributeSet attrs) {
     CircleModuleView_ instance = new CircleModuleView_(context, attrs);
     instance.onFinishInflate();
     return instance;
 }

 public static CircleModuleView build(Context context, AttributeSet attrs, int defStyleAttr) {
     CircleModuleView_ instance = new CircleModuleView_(context, attrs, defStyleAttr);
     instance.onFinishInflate();
     return instance;
 }

 public static CircleModuleView build(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
     CircleModuleView_ instance = new CircleModuleView_(context, attrs, defStyleAttr, defStyleRes);
     instance.onFinishInflate();
     return instance;
 }

 @Override
 public void onViewChanged(HasViews hasViews) {
     tv_start_here = ((TextView) hasViews.findViewById(R.id.tv_start_here));
     img_icon = ((ImageView) hasViews.findViewById(R.id.img_icon));
     img_bg = ((ImageView) hasViews.findViewById(R.id.img_bg));
     ln_content = ((LinearLayout) hasViews.findViewById(R.id.ln_content));
     img_border = ((ImageView) hasViews.findViewById(R.id.img_border));
     ln_content_parent = ((LinearLayout) hasViews.findViewById(R.id.ln_content_parent));
     vFirstSpace = hasViews.findViewById(R.id.vFirstSpace);
     vLastSpace = hasViews.findViewById(R.id.vLastSpace);
     lnStartHere = (LinearLayout) hasViews.findViewById(R.id.lnStartHere);
 }

}

