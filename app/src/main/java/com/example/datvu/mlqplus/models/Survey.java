package com.example.datvu.mlqplus.models;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Survey extends ObjectBase implements Serializable{
	@SerializedName("survey_name")
	private String survey_name;
	
	@SerializedName("sections")
	private List<SurveySection> sections;
	
	@SerializedName("isSubmitted")
	private boolean isSubmitted;
	
	@SerializedName("description")
	private String description;

	public String getSurvey_name() {
		return survey_name;
	}

	public void setSurvey_name(String survey_name) {
		this.survey_name = survey_name;
	}

	public List<SurveySection> getSections() {
		return sections;
	}

	public void setSections(List<SurveySection> sections) {
		this.sections = sections;
	}

	public boolean isSubmitted() {
		return isSubmitted;
	}

	public void setSubmitted(boolean isSubmitted) {
		this.isSubmitted = isSubmitted;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
