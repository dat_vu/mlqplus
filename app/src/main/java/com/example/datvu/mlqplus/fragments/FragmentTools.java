/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.adapter.LeadershipQuoteAdapter;
import com.mlqplus.trustedleader.adapter.ToolAdapter;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.Assessment;
import com.mlqplus.trustedleader.model.ConfigMenu;
import com.mlqplus.trustedleader.model.ConfigMenuBase;
import com.mlqplus.trustedleader.model.Size;
import com.mlqplus.trustedleader.model.Tool;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;
import com.mlqplus.trustedleader.fragment.FragmentAssessment_;
import com.mlqplus.trustedleader.fragment.FragmentLeaderTools_;
import com.mlqplus.trustedleader.fragment.FragmentLinks_;
import com.mlqplus.trustedleader.fragment.FragmentTools_;

@EFragment(R.layout.fragment_tools)
public class FragmentTools extends FragmentBase {
	public static FragmentTools newInstance() {
		return new FragmentTools_();
	} 
  
	@ViewById
	LinearLayout ln_child;  

	@ViewById 
	TextView tv_title; 

	@ViewById
	ListView lv_tool;
	
	@ViewById
	WaitingView vWaiting;

	private ArrayList<Tool> mLTool = null;
	private ToolAdapter mToolAdapter = null;
	private String WORKSHEET = "worksheet", ASSESSMENT = "assessment",
			LINK = "link";
	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {

		// getActivity
		mActivity = getActivity();

		// Set text size for all view
		setTextSize();

		// Get Data
		getData();
		
	}

	private void getData() {
		vWaiting.showWaitingView();
		if (MLQPlusConstant.CONFIG_MENU == null) {
			if (!MLQPlusFunctions.isInternetAvailable())
				((BaseActivity) mActivity)
						.showToast(MLQPlusConstant.CONNECTION_LOST);
			else {
				MLQPlusService.getBSSServices().getConfigMenu(
						MLQPlusFunctions.getAccessToken(),
						new Callback<ApiResponse<ConfigMenu>>() {

							@Override
							public void success(
									ApiResponse<ConfigMenu> configs,
									Response arg1) {
								// TODO Auto-generated method stub
								if (configs != null
										&& configs.getData() != null) {
									MLQPlusConstant.CONFIG_MENU = configs
											.getData();
									getFragmentToolsData();
								}
							}

							@Override
							public void failure(RetrofitError error) {
								// TODO Auto-generated method stub
								vWaiting.hideWaitingView();
								if (error.getCause() instanceof SocketTimeoutException)
									showToast("Load config settings failed: "+MLQPlusConstant.TIMEOUT);
							}
						});
			}
		} else
			getFragmentToolsData();
	}

	private void getFragmentToolsData() {
		mLTool = new ArrayList<Tool>();
		addSuitableItem(WORKSHEET);
		addSuitableItem(ASSESSMENT);
		addSuitableItem(LINK);

		// Load images before showing data
		List<String> images = new ArrayList<String>();
		for (Tool tool : mLTool)
			images.add(tool.getIcon());
		
		showData();

		loadImages(images, mActivity);
	}

	private void addSuitableItem(String key) {
		for (ConfigMenuBase item : MLQPlusConstant.CONFIG_MENU.getTools())
			if (item.getConfig_name().toUpperCase().equals(key.toUpperCase())) {
				Tool tool = new Tool(item);
				if (key.equals(ASSESSMENT))
					tool.setHaveNext(false);
				else
					tool.setHaveNext(true);

				mLTool.add(tool);
			}
	}

	private void showData() {
		mToolAdapter = new ToolAdapter(mActivity, R.layout.item_tools, mLTool);
		lv_tool.setAdapter(mToolAdapter);
		lv_tool.setSelector(R.drawable.btn_first_quiz);
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}

	@ItemClick
	void lv_tool(int position) {
		Tool tool = (Tool) lv_tool.getItemAtPosition(position);
		Fragment frag = null;
		if (tool.getConfig_name().toUpperCase().equals(WORKSHEET.toUpperCase()))
			frag = new FragmentLeaderTools_();
		else if (tool.getConfig_name().toUpperCase()
				.equals(ASSESSMENT.toUpperCase()))
			frag = new FragmentAssessment_();
		else if (tool.getConfig_name().toUpperCase().equals(LINK.toUpperCase()))
			frag = new FragmentLinks_();

		try {
			MLQPlusFunctions.replaceFragment(mActivity, frag, true);
		} catch (Exception e) {
			showToast(e.getMessage());
		}

	}
	
	@Override
	public void voidAfterLoadImages() {
		super.voidAfterLoadImages();
		vWaiting.hideWaitingView();
		
		showData();
	}

}
*/
