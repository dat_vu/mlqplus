package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class Tool extends ConfigMenuBase{

	@SerializedName("isHaveNext")
	private boolean isHaveNext;

	public boolean isHaveNext() {
		return isHaveNext;
	}

	public void setHaveNext(boolean isHaveNext) {
		this.isHaveNext = isHaveNext;
	}

	public Tool(ConfigMenuBase item) {
		super();
		setName(item.getName());
		setIcon(item.getIcon());
		setDescription(item.getDescription());
		setConfig_name(item.getConfig_name());
		isHaveNext=true;
	}

}
