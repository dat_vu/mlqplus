package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.ModuleActivity;
import com.example.datvu.mlqplus.models.ModuleActivityEvent;
import com.example.datvu.mlqplus.models.ModuleRatingQuestion;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.google.gson.Gson;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

@EViewGroup(R.layout.view_question_style_seekbar)
public class QuestionStyleRatingView extends LinearLayout {

	private Context mContext;

	@ViewById
	TextView tvRatingQuestion, tv_least_important, tv_most_important;

	@ViewById
	LinearLayout ln_answer;

	private List<RatingItemView> sbAnswers;
	private EventBus eventBus;
	private ModuleActivity moduleActivity;

	public QuestionStyleRatingView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public QuestionStyleRatingView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public QuestionStyleRatingView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public QuestionStyleRatingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public void setQuestion(ModuleActivity moduleActivity) {
		// Init eventBus
		eventBus = EventBus.getDefault();

		setTextSize();
		
		this.moduleActivity=moduleActivity;
		ModuleRatingQuestion moduleRatingQuestion=this.moduleActivity.getRatingQuestion();

		tvRatingQuestion.setText(moduleRatingQuestion.getTitle());
		
		// Set text for scale low and high
		if (moduleRatingQuestion.getScaleLow()!=null && moduleRatingQuestion.getScaleLow().length()>0)
			tv_least_important.setText(moduleRatingQuestion.getScaleLow());
		if (moduleRatingQuestion.getScaleHigh()!=null && moduleRatingQuestion.getScaleHigh().length()>0)
			tv_most_important.setText(moduleRatingQuestion.getScaleHigh());
		
		sbAnswers = new ArrayList<RatingItemView>();
		List<Integer> answerArray= MLQPlusFunctions.parseAnswerArray(moduleActivity.getAnswer(), moduleRatingQuestion.getQuestions().size());
		
		Log.d("MLQPlus", "Init Rating view id: "+moduleActivity.getId());
		if (moduleRatingQuestion.getQuestions() != null)
			for (int i = 0; i < moduleRatingQuestion.getQuestions().size(); i++) {
				sbAnswers.add(genRatingItemView(moduleRatingQuestion.getQuestions().get(i).getValue(), answerArray.get(i)));
				ln_answer.addView(sbAnswers.get(sbAnswers.size()-1));
			}
	}
	
	private RatingItemView genRatingItemView(String question, Integer answer)
	{
		RatingItemView ratingItemView = RatingItemView_.build(mContext);
		ratingItemView.setData(question, answer, onRatingListenner);
		return ratingItemView;
	}

	public void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tvRatingQuestion, mContext, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_least_important, mContext,
				R.dimen.textSmall);
		MLQPlusFunctions.SetViewTextSize(tv_most_important, mContext,
				R.dimen.textSmall);
	}

	public List<Integer> getAnswers() {
		List<Integer> answers = new ArrayList<Integer>();
		if (sbAnswers != null)
			for (RatingItemView sbAnswer : sbAnswers)
				answers.add(sbAnswer.getSeletedAnswer());
		return answers;
	}
	
	RatingItemView.OnRatingListenner onRatingListenner=new RatingItemView.OnRatingListenner() {
		
		@Override
		public void onRating() {
			// TODO Auto-generated method stub
			// Update local
			String answers=new Gson().toJson(getAnswers());
			moduleActivity.setAnswer(answers);
			
			// Call API update
			eventBus.post(new ModuleActivityEvent(moduleActivity.getId(), moduleActivity.getActivityType(), answers));
		}
	};
	
	@Click
	void lnBackground()
	{
		MLQPlusFunctions.hideKeyboard((Activity)mContext);
	}
}
