package com.example.datvu.mlqplus.activities;


import com.example.datvu.mlqplus.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_backgroundscreen)
public class BackgroundActivity extends BaseActivity {

    private boolean isTheFirstTime = true;

    @AfterViews
    void init() {
        // If user completed quiz -> move to getStarted Screen
        /*if (MLQPlusConstant.USERINFO != null && MLQPlusConstant.USERINFO.getUser() != null && MLQPlusConstant.USERINFO.getUser().getIs_has_leadership_style() != null && MLQPlusConstant.USERINFO.getUser().getIs_has_leadership_style().equals("1"))
            startNewActivityNoAnimation(BackgroundActivity.this, LearnMorescreenActivity_.class, false);
        else {
            Bundle bundle = new Bundle();
            bundle.putBoolean(OpenningQuizscreenActivity.IS_START_FROM_LEADER_QUIZ, false);
            startNewActivityNoAnimation(BackgroundActivity.this, OpenningQuizscreenActivity_.class, false, bundle);
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isTheFirstTime)
            isTheFirstTime = false;
        else
            finish();
    }
}
