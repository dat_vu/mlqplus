package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class ModuleSeparator  extends ActivityBase{
	@SerializedName("separatorColor")
	private String separatorColor;

	public String getSeparatorColor() {
		return separatorColor;
	}

	public void setSeparatorColor(String separatorColor) {
		this.separatorColor = separatorColor;
	}
	
}
