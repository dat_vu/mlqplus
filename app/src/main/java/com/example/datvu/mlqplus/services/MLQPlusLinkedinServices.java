/*
package com.example.datvu.mlqplus.services;

import java.util.Date;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.converter.GsonConverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;

public class MLQPlusLinkedinServices {

	private static RequestInterceptor requestInterceptor;
	private static RestAdapter restAdapter;
	
	private static RestAdapter restRootAdapter;
	
	private static RestAdapter restRootNoHeaderAdapter;
	private static RequestInterceptor getRequestInterceptor() {
		if (requestInterceptor == null) {
			requestInterceptor = new RequestInterceptor() {
	    		  @Override
	    		  public void intercept(RequestFacade request) {
	    		    request.addHeader("Content-Type", "application/json");  
	    		    request.addHeader("Accept", "application/json");
	    		  }		  
	    	};
		}
		return requestInterceptor;
	}
	
	private static RestAdapter getRestAdapter () {
		if (restAdapter == null) {
			restAdapter = new RestAdapter.Builder().setLogLevel (RestAdapter.LogLevel.FULL)
					.setEndpoint("")
					//.setClient(new OkClient(new OkHttpClient()))
					.setRequestInterceptor(getRequestInterceptor())
					.setLogLevel(LogLevel.FULL)
					.setConverter(new GsonConverter(getGson())).build();
		}
		return restAdapter;
	}
	

	
	
	private static RestAdapter getRestRootAdapter (String root) {
			restRootAdapter = new RestAdapter.Builder().setLogLevel (RestAdapter.LogLevel.FULL)
					.setEndpoint(root)
					//.setClient(new OkClient(new OkHttpClient()))
					.setRequestInterceptor(getRequestInterceptor())
					.setLogLevel(LogLevel.FULL)
					.setConverter(new GsonConverter(getGson())).build();
		return restRootAdapter;
	}
	
	private static RestAdapter getRestRootNoHeaderAdapter (String root) {
		restRootNoHeaderAdapter = new RestAdapter.Builder().setLogLevel (RestAdapter.LogLevel.FULL)
				.setEndpoint(root)
				//.setClient(new OkClient(new OkHttpClient()))
				.setLogLevel(LogLevel.FULL)
				.setConverter(new GsonConverter(getGson())).build();
	return restRootNoHeaderAdapter;
}
	

	private static Gson mGson;
	public static Gson getGson() {
		if (mGson == null) {
			mGson = new GsonBuilder() .registerTypeAdapter(Date.class, new DateTypeAdapter()).create();
		}
		return mGson;
	}
	
    public static MLQPlusLinkedinAPIs getwiGrubServices() {
        return getRestAdapter().create(MLQPlusLinkedinAPIs.class);
    }
    
    public static MLQPlusLinkedinAPIs getwiGrubServicesNoHeader(String root) {
        return getRestRootNoHeaderAdapter(root).create(MLQPlusLinkedinAPIs.class);
    }
    
    
    public static MLQPlusLinkedinAPIs getwiGrubServices(String root) {
        return getRestRootAdapter(root).create(MLQPlusLinkedinAPIs.class);
    }
}
*/
