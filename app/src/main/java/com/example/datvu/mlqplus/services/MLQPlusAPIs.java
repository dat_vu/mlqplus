/*
package com.example.datvu.mlqplus.services;


import com.example.datvu.mlqplus.models.About;
import com.example.datvu.mlqplus.models.ApiResponse;
import com.example.datvu.mlqplus.models.Assessment;
import com.example.datvu.mlqplus.models.ConfigMenu;
import com.example.datvu.mlqplus.models.Country;
import com.example.datvu.mlqplus.models.FirstQuizQuestion;
import com.example.datvu.mlqplus.models.LeaderQuizResult;
import com.example.datvu.mlqplus.models.LeaderTool;
import com.example.datvu.mlqplus.models.LeadershipQuote;
import com.example.datvu.mlqplus.models.LeadershipStyle;
import com.example.datvu.mlqplus.models.Link;
import com.example.datvu.mlqplus.models.LoginRequest;
import com.example.datvu.mlqplus.models.Module;
import com.example.datvu.mlqplus.models.ModuleActivityError;
import com.example.datvu.mlqplus.models.ModuleQuestion;
import com.example.datvu.mlqplus.models.Quiz;
import com.example.datvu.mlqplus.models.Quote;
import com.example.datvu.mlqplus.models.RandomQuote;
import com.example.datvu.mlqplus.models.SettingData;
import com.example.datvu.mlqplus.models.SignupRequest;
import com.example.datvu.mlqplus.models.SignupResponse;
import com.example.datvu.mlqplus.models.SocialSignupRequest;
import com.example.datvu.mlqplus.models.Survey;
import com.example.datvu.mlqplus.models.SurveyActivityAnswer;
import com.example.datvu.mlqplus.models.Tool;
import com.example.datvu.mlqplus.models.UserProgress;
import com.example.datvu.mlqplus.models.UserResponse;
import com.example.datvu.mlqplus.models.WelcomeScreenInfo;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MLQPlusAPIs {
	
	@POST("/api/auth/register")
	void signup(@Body SignupRequest signupRequest, Callback<ApiResponse<SignupResponse>> callback);
	
	*/
/*@POST("/api/auth/login")
	void login(@Body LoginRequest loginRequest, Callback<SignupResponse> callback);*//*

	
	@POST("/api/auth/login")
	void login(@Body LoginRequest loginRequest, Callback<ApiResponse<SignupResponse>> callback);
	
	@POST("/api/auth/facebookLogin")
	void loginFacebook(@Body SocialSignupRequest socialSignupRequest, Callback<ApiResponse<SignupResponse>> callback);
	
	@POST("/api/auth/linkedInLogin")
	void loginLinkedin(@Body SocialSignupRequest socialSignupRequest, Callback<ApiResponse<SignupResponse>> callback);
	
	@POST("/api/auth/forgotPassword")
	void forgotPassword(@Body TypedString json, Callback<ApiResponse<Response>> callback);
	
	@GET("/api/modules")
	void getAllModules(@Header("Authorization") String auth, Callback<ApiResponse<List<Module>>> callback);
	
	@GET("/api/modules/currentUserModule")
	void getMyModules(@Header("Authorization") String auth, Callback<ApiResponse<List<Module>>> callback);
	
	@GET("/api/modules/{moduleId}/0")
	void getModulesDetail(@Header("Authorization") String auth, @Path("moduleId") String quizid, Callback<ApiResponse<Module>> callback);
	
	@POST("/api/modules/purchase")
	void purchaseAModule(@Header("Authorization") String auth, @Body TypedString json, Callback<ApiResponse<Response>> callback);
	
	@POST("/api/modules/purchaseFull")
	void purchaseFullModule(@Header("Authorization") String auth, @Body TypedString json, Callback<ApiResponse<Response>> callback);
	
	@GET("/api/firstQuiz")
	void getFirstQuiz(@Header("Authorization") String auth, Callback<ApiResponse<ArrayList<FirstQuizQuestion>>> callback);
	
	@POST("/api/firstQuiz/submitAnswer")
	void submitFirstQuizAnswer(@Header("Authorization") String auth, @Body TypedString json, Callback<ApiResponse<LeadershipStyle>> callback);
	
	@GET("/api/commonInfo/welcomeScreen")
	void getWelcomeScreenInfo(Callback<ApiResponse<List<WelcomeScreenInfo>>> callback);
	
	@GET("/api/auth/user")
	void getUserProfile(@Header("Authorization") String auth, Callback<ApiResponse<SignupResponse>> callback);
	
	@POST("/api/modules/submitActivity")
	void submitActivityAnswer(@Header("Authorization") String auth, @Body TypedString json, Callback<ApiResponse<Response>> callback);
	
	@POST("/api/quotes/getRandomByTimezone")
	void getRandomQuote(@Header("Authorization") String auth, @Body TypedString json, Callback<ApiResponse<RandomQuote>> callback);
	
	@GET("/api/quotes/categories")
	void getQuoteCategories(@Header("Authorization") String auth, Callback<ApiResponse<List<LeadershipQuote>>> callback);
	
	@GET("/api/quotes/detail/{id}")
	void getQuoteDetail(@Header("Authorization") String auth, @Path("id") String id, Callback<ApiResponse<ArrayList<Quote>>> callback);
	
	@GET("/api/survey/currentUserSurvey")
	void getMySurvey(@Header("Authorization") String auth, Callback<ApiResponse<List<Survey>>> callback);
	
	@GET("/api/survey/detail/{surveyId}")
	void getSurveyDetail(@Header("Authorization") String auth, @Path("surveyId") String surveyId, Callback<ApiResponse<Survey>> callback);
	
	@GET("/api/quizzes/getProgress")
	void getUserProgress(@Header("Authorization") String auth, Callback<ApiResponse<UserProgress>> callback);
	
	@GET("/api/quizzes/categories")
	void getQuizzes(@Header("Authorization") String auth, Callback<ApiResponse<List<Quiz>>> callback);
	
	@POST("/api/survey/submitSurvey")
	void submitSurvey(@Header("Authorization") String auth, @Body List<SurveyActivityAnswer> surveyActivityAnswers, Callback<ApiResponse<Object>> callback);
	
	@GET("/api/assessments")
	void getAsessments(@Header("Authorization") String auth, Callback<ApiResponse<List<Assessment>>> callback);
	
	@GET("/api/links")
	void getLinks(@Header("Authorization") String auth, Callback<ApiResponse<List<Link>>> callback);
	
	@GET("/api/worksheets/0")
	void getWorksSheet(@Header("Authorization") String auth, Callback<ApiResponse<List<LeaderTool>>> callback);
	
	@GET("/api/quizzes/getQuizzes/{id}")
	void getLeaderQuizzes(@Header("Authorization") String auth, @Path("id") String id, Callback<ApiResponse<ArrayList<ModuleQuestion>>> callback);
	
	@POST("/api/quizzes/submitAnswer")
	void submitLeaderQuiz(@Header("Authorization") String auth, @Body TypedString json, Callback<ApiResponse<LeaderQuizResult>> callback);
	
	@GET("/api/commonInfo/toolScreen")
	void getTools(@Header("Authorization") String auth, Callback<ApiResponse<List<Tool>>> callback);
	
	@GET("/api/commonInfo/getConfigMenu")
	void getConfigMenu(@Header("Authorization") String auth, Callback<ApiResponse<ConfigMenu>> callback);
	
	@GET("/api/settings")
	void getSettings(@Header("Authorization") String auth, Callback<ApiResponse<SettingData>> callback);
	
	@POST("/api/settings/changeFont")
	void updateFontSize(@Header("Authorization") String auth, @Body TypedString json, Callback<ApiResponse<SettingData>> callback);
	
	@POST("/api/settings/changeNotification")
	void updateNotifications(@Header("Authorization") String auth, @Body TypedString json, Callback<ApiResponse<SettingData>> callback);
	
	@POST("/api/settings/changePassword")
	void updatePassword(@Header("Authorization") String auth, @Body TypedString json, Callback<ApiResponse<UserResponse>> callback);
	
	@POST("/api/settings/updateAccount")
	void updateAccount(@Header("Authorization") String auth, @Body SignupRequest signupRequest, Callback<ApiResponse<UserResponse>> callback);
	
	@POST("/api/auth/logout")
	void logout(@Header("Authorization") String auth, @Body TypedString json, Callback<ApiResponse<SignupResponse>> callback);
	
	@POST("/api/commonInfo/reportActivityError")
	void reportActivityError(@Header("Authorization") String auth, @Body TypedString error, Callback<ApiResponse<ModuleActivityError>> callback);
	
	@GET("/api/commonInfo/getAboutInfo")
	void getAboutInfo(@Header("Authorization") String auth, Callback<ApiResponse<List<About>>> callback);
	
	@GET("/api/commonInfo/getCountryList")
	void getCountryList(Callback<ApiResponse<List<Country>>> callback);
	
	*/
/*@FormUrlEncoded
	@POST("/api/token")
	void login(@Field("grant_type") String grant_type, @Field("username") String username, Callback<UserToken> callback);
	
	@GET("/api/quizcategories")
	void getCategories(Callback<Categories> callback);
	
	@GET("/api/quizzes/{quizid}/result")
	void getResult(@Header("Authorization") String auth, @Path("quizid") String quizid, Callback<Result> callback);
	
	@POST("/api/quizzes/play/{quizcategoryid}")
	void selectCategory(@Header("Authorization") String auth, @Path("quizcategoryid") String quizcategoryid, Callback<QuestionWrapper> callback);
	
	@POST("/api/quizzes/{quizid}/solve") 
	void answerAQuestion(@Header("Authorization") String auth, @Path("quizid") String quizid, @Body TypedString answer, Callback<QuestionWrapper> callback);*//*

}
*/
