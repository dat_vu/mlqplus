/*
package com.example.datvu.mlqplus.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.fragment.FragmentModuleQuiz_;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.LeaderQuizResult;
import com.mlqplus.trustedleader.model.ModuleQuestion;
import com.mlqplus.trustedleader.model.Quiz;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

@EActivity(R.layout.activity_module_quizscreen)
public class LeaderQuizscreenActivity extends BaseDialogActivity {

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    TextView tv_quiz_title, tv_current_question;

    @ViewById
    ViewPager viewPager;

    @ViewById
    LinearLayout ll_next, ll_back;

    private ArrayList<ModuleQuestion> questions;
    private Quiz quiz;
    public static String QUIZ = "Quiz";
    public static String QUESTIONS = "questions";
    public static final String LEADER_QUIZ_SCREEN = "Leader Quiz Screen";

    @AfterViews
    void init() {
        getScreenSize();

        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        // Get data
        getData();
    }

    public void getData() {
        try {
            questions = (ArrayList<ModuleQuestion>) getIntent().getBundleExtra(
                    MLQPlusConstant.BUNDLE).get(QUESTIONS);
            quiz = (Quiz) getIntent().getBundleExtra(MLQPlusConstant.BUNDLE).get(QUIZ);

            // Reset selected Answer
            for (int i = 0; i < questions
                    .size(); i++)
                questions.get(i)
                        .setAnswer(null);
        } catch (Exception e) {
        }

        showData();
    }

    private void showData() {
        if (questions != null && quiz != null) {


            if (quiz.getName() != null)
                tv_quiz_title.setText(quiz.getName());

            tv_current_question.setText(getCurrentQuestionPosString(0));

            ll_back.setVisibility(View.INVISIBLE);

            // Init viewpager
            FragmentManager fragmentManager = getFragmentManager();
            FragmentPagerAdapter sessionAdapter = new FragmentPagerAdapter(
                    fragmentManager) {

                @Override
                public int getCount() {
                    // TODO Auto-generated method stub
                    return questions.size();
                }

                @Override
                public Fragment getItem(int pos) {
                    // TODO Auto-generated method stub
                    FragmentModuleQuiz_ fragmentModuleQuiz_ = new FragmentModuleQuiz_();
                    fragmentModuleQuiz_.setData(questions.get(pos), pos, questions.size());
                    return fragmentModuleQuiz_;
                }
            };

            viewPager.setAdapter(sessionAdapter);
            viewPager.setOnPageChangeListener(new OnPageChangeListener() {

                @Override
                public void onPageSelected(int pos) {
                    // TODO Auto-generated method stub
                    tv_current_question
                            .setText(getCurrentQuestionPosString(pos));
                    ll_back.setVisibility(View.VISIBLE);
                    ll_next.setVisibility(View.VISIBLE);

                    if (pos == 0) {
                        ll_back.setVisibility(View.INVISIBLE);
                    }
                    if (pos == (questions.size() - 1)) {
                        ll_next.setVisibility(View.INVISIBLE);
                    }

                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                    // TODO Auto-generated method stub

                }
            });
        }
    }

    @Click
    void ll_back() {
        if (viewPager.getCurrentItem() > 0)
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
    }

    @Click
    void ll_next() {
        if (viewPager.getCurrentItem() < questions.size() - 1)
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
    }

    private String getCurrentQuestionPosString(int pos) {
        return (pos + 1) + " of "
                + questions.size();
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_quiz_title,
                LeaderQuizscreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_current_question,
                LeaderQuizscreenActivity.this, R.dimen.textMedium);
    }

    public void btn_submit() {
        String answers = getAnswers();
        if (answers == null)
            showToast("Please answer all questions");
        else {
            if (!isInternetAvailable())
                showToast("Connection's lost. Please check your internet connection");
            else {
                // Submit answers
                showWaitingDialog(LeaderQuizscreenActivity.this,
                        "Submitting anwers", "Please wait...");

                MLQPlusService.getBSSServices().submitLeaderQuiz(
                        MLQPlusFunctions.getAccessToken(),
                        new TypedString(getAnswers()), new Callback<ApiResponse<LeaderQuizResult>>() {

                            @Override
                            public void success(ApiResponse<LeaderQuizResult> result, Response arg1) {
                                // TODO Auto-generated method stub
                                dismissWaitingDialog();
                                showQuizResult(result.getData().getScore());
                                finish();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                // TODO Auto-generated method stub
                                dismissWaitingDialog();
                                showToast("Cannot submit answers. Please try again");
                            }
                        });
            }

        }
    }

    private String getAnswers() {
        String answers = "[";
        for (int i = 0; i < questions
                .size(); i++) {
            ModuleQuestion question = questions.get(i);
            if (question.getAnswer() != null)
                answers += "{\"question_id\":\"" + question.getId() + "\", \"answer\":\"" + question.getAnswer() + "\"}";
            else
                return null;

            if (i < questions.size() - 1)
                answers += ", ";
            else
                answers += "]";
        }
        return answers;
    }

    @Click
    void lnClose() {
        LeaderQuizscreenActivity.this.finish();
    }

    private void showQuizResult(int correctAnswers) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(QUESTIONS, questions);
        bundle.putSerializable(QUIZ, quiz);
        bundle.putString(ModuleQuizResultscreenActivity.RESULT, correctAnswers
                + "/" + questions.size());
        startNewZoomActivity(LeaderQuizscreenActivity.this,
                ModuleQuizResultscreenActivity_.class, true, bundle);

    }
}
*/
