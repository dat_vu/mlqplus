package com.example.datvu.mlqplus.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Module;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;


@EViewGroup(R.layout.view_circle_module)
public class CircleModuleView extends LinearLayout {

	private Context mContext;
	private OnClickListenner onClickListenner;
	private Module module;

	@ViewById
	ImageView img_bg, img_border, img_icon;

	@ViewById
	TextView tv_start_here;
	
	@ViewById
	LinearLayout ln_content, ln_content_parent, lnStartHere;
	
	@ViewById
	View vFirstSpace, vLastSpace;
	
	public CircleModuleView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public CircleModuleView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		mContext = context;
	}

	public CircleModuleView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mContext = context;
	}

	public CircleModuleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}

	public void setData(Module module, boolean isTheFirstModule,
			OnClickListenner onClickListenner) {
		this.module = module;
		this.onClickListenner = onClickListenner;
		ln_content_parent.setVisibility(View.VISIBLE);

		// Show icon
		if (!isTheFirstModule) {
			
			// Hide Views
			vFirstSpace.setVisibility(View.GONE);
			vLastSpace.setVisibility(View.GONE);
			lnStartHere.setVisibility(View.GONE);
			
			ln_content_parent.setWeightSum(521);
			
			LayoutParams lpLnContent = new LayoutParams(
					0, LayoutParams.MATCH_PARENT);
			lpLnContent.weight=270;
			ln_content.setLayoutParams(lpLnContent);
			ln_content.setWeightSum(521);
			
			LayoutParams lpImgIcon = new LayoutParams(
					LayoutParams.MATCH_PARENT, 0);
			lpImgIcon.weight = 270;
		    img_icon.setLayoutParams(lpImgIcon);
		    
		}
		/*else
		{
			// Set text size for start here text
			MLQPlusFunctions.SetViewTextSize(tv_start_here, mContext,
					R.dimen.textXSmall);
		}*/
		MLQPlusFunctions.showImage(mContext, module.getIcon(), img_icon);

		// Show background
		img_bg.setImageResource(android.R.color.transparent);
		img_bg.setImageResource(getNormalBG());

		if (module.getPublished().equals("1") && module.isUnlocked()) {
			img_border.setImageResource(R.drawable.circle_boder_unlocked);
			tv_start_here.setTextColor(getResources().getColor(
					R.color.white));
			img_icon.setColorFilter(getResources().getColor(
					R.color.white));
				
		} else {
			img_border.setImageResource(R.drawable.circle_boder_locked);
			img_icon.setColorFilter(getResources().getColor(
					R.color.module_icon_locked));
			tv_start_here.setTextColor(getResources().getColor(
					R.color.module_icon_locked));
		}

		// Click effect
		img_border.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				float radius = img_border.getWidth() / 2;

				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					// change to being clicked background
					if (MLQPlusFunctions.distance(event.getX(), event.getY(),
							img_border.getX() + radius, img_border.getY() + radius) <= radius)
						img_bg.setImageResource(R.drawable.circle_bg_pressed);
					break;

				case MotionEvent.ACTION_MOVE:
					// Check and change to normal backgroud when mouse is out of
					// border
					if (MLQPlusFunctions.distance(event.getX(), event.getY(),
							img_border.getX() + radius, img_border.getY() + radius) > radius)
						img_bg.setImageResource(getNormalBG());
					else
						img_bg.setImageResource(R.drawable.circle_bg_pressed);
					break;

				case MotionEvent.ACTION_UP:
					img_bg.setImageResource(getNormalBG());
					if (MLQPlusFunctions.distance(event.getX(), event.getY(),
							img_border.getX() + radius, img_border.getY() + radius) <= radius
							&& CircleModuleView.this.onClickListenner != null)
						CircleModuleView.this.onClickListenner
								.onClick(CircleModuleView.this.module);
					break;
				}
				return true;
			}
		});
	}

	private int getNormalBG() {
		if (module != null && module.isCompleted())
			return R.drawable.circle_bg_completed;
		return R.drawable.transparent;
	}

	public interface OnClickListenner {
		public void onClick(Module module);
	}
}
