package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class ApiResponse<T> {
	@SerializedName("data")
	private T mData;
	
	public void setData(T data) {
		this.mData = data;
	}

	public T getData() {
		return this.mData;
	}
	
}
