package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import org.androidannotations.annotations.ServiceAction;

import com.google.gson.annotations.SerializedName;

public class LinkedInInfo implements Serializable{

	@SerializedName("id")
	private String id;
	@SerializedName("firstName")
	private String firstName;
	@SerializedName("lastName")
	private String lastName;
	@SerializedName("headline")
	private String headline;
	@SerializedName("siteStandardProfileRequest")
	SiteStandardProfileRequest siteStandardProfileRequest;
	
	@SerializedName("publicProfileUrl")
	private String publicProfileUrl;
	
	@SerializedName("pictureUrl")
	private String pictureUrl;
	
	@SerializedName("emailAddress")
	private String emailAddress;
	
	public String getPublicProfileUrl() {
		return publicProfileUrl;
	}

	public void setPublicProfileUrl(String publicProfileUrl) {
		this.publicProfileUrl = publicProfileUrl;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public SiteStandardProfileRequest getSiteStandardProfileRequest() {
		return siteStandardProfileRequest;
	}

	public void setSiteStandardProfileRequest(
			SiteStandardProfileRequest siteStandardProfileRequest) {
		this.siteStandardProfileRequest = siteStandardProfileRequest;
	}
	
	public void setUrl(String url) {
		getSiteStandardProfileRequest().setUrl(url);
	}
	
	public String getUrl(){
		return getSiteStandardProfileRequest().getUrl();
	}
	public void setEmail(String emailAddress)
	{
		this.emailAddress=emailAddress;
	}
	public String getEmail()
	{
		return emailAddress;
	}

	public class SiteStandardProfileRequest implements Serializable{
		private String url;
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
	}
	
}
