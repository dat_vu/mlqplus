package com.example.datvu.mlqplus;


import android.app.Application;

import com.crittercism.app.Crittercism;
import com.example.datvu.mlqplus.dagger.components.ActivityInjectorComponent;
import com.example.datvu.mlqplus.dagger.components.ApplicationComponent;
import com.example.datvu.mlqplus.utils.TypefaceUtil;

import org.androidannotations.annotations.EApplication;

@EApplication()
public class MLQPlusApplication extends Application{

	private ApplicationComponent applicationComponent;
	private ActivityInjectorComponent activityInjectorComponent;

	public MLQPlusApplication(ApplicationComponent applicationComponent) {
		this.applicationComponent = applicationComponent;
	}

	public MLQPlusApplication(ActivityInjectorComponent activityInjectorComponent) {
		this.activityInjectorComponent = activityInjectorComponent;
	}

	@Override
	public void onCreate() {
		super.onCreate();

/*		Instabug.initialize(this, "ac04ab3b2b17a5debb321106f6cd932c");
		Instabug.getInstance().setInvocationEvent(IBGInvocationEvent.IBGInvocationEventNone);*/
		Crittercism.initialize(getApplicationContext(), "217df02a87e5403e8f7fd40d52a2bb3200555300");
		TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/arial.ttf");
	}

	private void initComponent(){
		applicationComponent = DaggerAppComponent.builder()
				.appModule(new AppModule(this))
				.build();
	}
}
