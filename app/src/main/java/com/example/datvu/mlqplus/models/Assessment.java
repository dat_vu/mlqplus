package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class Assessment extends Setting{

	@SerializedName("pdf_link")
	private String pdf_link;
	
	@SerializedName("pdf_file_name")
	private String pdf_file_name;

	public String getPdf_link() {
		return pdf_link;
	}

	public void setPdf_link(String pdf_link) {
		this.pdf_link = pdf_link;
	}

	public String getPdf_file_name() {
		return pdf_file_name;
	}

	public void setPdf_file_name(String pdf_file_name) {
		this.pdf_file_name = pdf_file_name;
	}

}
