package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class Term {
	@SerializedName("name")
	private String name;

	@SerializedName("content")
	private String content;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Term(String name, String content) {
		super();
		this.name = name;
		this.content = content;
	}

	
}
