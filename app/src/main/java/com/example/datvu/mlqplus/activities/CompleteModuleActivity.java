/*
package com.example.datvu.mlqplus.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.fragment.FragmentLearn_;
import com.mlqplus.trustedleader.generator.ModuleGenerator;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.Module;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EActivity(R.layout.activity_complete_modulescreen)
public class CompleteModuleActivity extends BaseDialogActivity {

    @ViewById
    TextView tvWellDone, tvTitle, tvCompleteMore, tvPercent, tvProgress;

    @ViewById
    LinearLayout ln_root;

    @ViewById
    Button btnHome, btnModules;

    @AfterViews
    void init() {
        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        getCurrentProgress();

    }

    private void showData() {
        tvWellDone.setText(getResources().getString(
                R.string.well_done_you_complete_the)
                + "\n" + ModuleGenerator.module.getName());

        //Update 5 core modules
        if (MLQPlusConstant.sFullModule != null) {
            MLQPlusConstant.CORE_MODULES = new ArrayList<Module>();
            for (int i = 0; i < 5 && i < MLQPlusConstant.sFullModule.size(); i++)
                MLQPlusConstant.CORE_MODULES.add(MLQPlusConstant.sFullModule.get(i));
        }

        if (ModuleGenerator.module != null
                && MLQPlusConstant.CORE_MODULES != null
                && MLQPlusConstant.CORE_MODULES.size() > 0) {

            float totalProgress = 0;
            // Update new progress
            for (int i = 0; i < MLQPlusConstant.CORE_MODULES.size(); i++)
                if (MLQPlusConstant.CORE_MODULES.get(i).getId()
                        .equals(ModuleGenerator.module.getId()))
                    totalProgress += 100;
                else
                    totalProgress += MLQPlusConstant.CORE_MODULES.get(i)
                            .getProgress();

            // Calculate current progress
            int overallProgress = (int) Math.round(totalProgress
                    / (MLQPlusConstant.CORE_MODULES.size() > 5 ? 5
                    : MLQPlusConstant.CORE_MODULES.size()));

            tvPercent.setText(overallProgress + "%");
        } else
            tvPercent.setText("20%");
    }

    private void getCurrentProgress() {
        // Load data
        if (!MLQPlusFunctions.isInternetAvailable())
            showToast("Connection's lost. Please check your internet connection");
        else {
            showWaitingDialog(this, "", "Please wait...");
            MLQPlusService.getBSSServices().getAllModules(
                    MLQPlusFunctions.getAccessToken(),
                    new Callback<ApiResponse<List<Module>>>() {

                        @Override
                        public void success(
                                ApiResponse<List<Module>> mList_modules,
                                Response arg1) {
                            // TODO Auto-generated method stub
                            MLQPlusConstant.sFullModule = new ArrayList<Module>();
                            if (mList_modules != null
                                    && mList_modules.getData() != null) {
                                MLQPlusConstant.sFullModule
                                        .addAll(mList_modules.getData());

                                MLQPlusService
                                        .getBSSServices()
                                        .getMyModules(
                                                MLQPlusFunctions
                                                        .getAccessToken(),
                                                new Callback<ApiResponse<List<Module>>>() {

                                                    @Override
                                                    public void success(
                                                            ApiResponse<List<Module>> mListUserModules,
                                                            Response arg1) {
                                                        // TODO Auto-generated
                                                        // method stub
                                                        dismissWaitingDialog();
                                                        if (mListUserModules != null
                                                                && mListUserModules
                                                                .getData() != null) {
                                                            // Update info from
                                                            // user module to
                                                            // all module
                                                            updateModuleInfo(mListUserModules
                                                                    .getData());

                                                            // Show Data
                                                            showData();

                                                        }
                                                    }

                                                    @Override
                                                    public void failure(
                                                            RetrofitError error) {
                                                        // TODO Auto-generated
                                                        // method stub
                                                        handleLoadDataError(error);

                                                    }
                                                });
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            // TODO Auto-generated method stub
                            handleLoadDataError(error);
                        }
                    });
        }
    }

    private void updateModuleInfo(List<Module> user_modules) {
        if (MLQPlusConstant.sFullModule != null && user_modules != null) // find module
            for (Module user_module : user_modules)
                for (Module module : MLQPlusConstant.sFullModule)
                    if (module.getId().equals(user_module.getId())) {
                        // update info
                        module.setTotal_activity(user_module
                                .getTotal_activity());
                        module.setSubmitted_activity(user_module
                                .getSubmitted_activity());
                        module.setUnlocked(true);
                    }
    }

    private void handleLoadDataError(RetrofitError error) {
        dismissWaitingDialog();
        try {
            if (error.getCause() instanceof SocketTimeoutException)
                showToast(MLQPlusConstant.TIMEOUT);
            else
                showToast("Load data failed: " + error.getMessage());
        } catch (Exception e) {
        }
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tvTitle, CompleteModuleActivity.this,
                R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tvWellDone,
                CompleteModuleActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tvCompleteMore,
                CompleteModuleActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btnHome, CompleteModuleActivity.this,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btnModules,
                CompleteModuleActivity.this, R.dimen.textMedium);

    }

    @Click
    void btnHome() {
        Intent iHome = new Intent(CompleteModuleActivity.this,
                HomescreenActivity_.class);
        iHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        overridePendingTransition(0, 0);
        startActivity(iHome);
    }

    @Click
    void btnModules() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(MainActivity.DATA,
                FragmentLearn_.class.getName());

        Intent iLearn = new Intent(CompleteModuleActivity.this,
                MainActivity_.class);
        iLearn.putExtra(MLQPlusConstant.BUNDLE, bundle);
        iLearn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        overridePendingTransition(0, 0);
        startActivity(iLearn);
    }

}
*/
