package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.adapters.FreetextQuestionAdapter;
import com.example.datvu.mlqplus.models.ModuleActivity;
import com.example.datvu.mlqplus.models.ModuleActivityEvent;
import com.example.datvu.mlqplus.models.ModuleFreeText;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@EViewGroup(R.layout.view_question_style_freetext)
public class QuestionStyleFreeTextView extends LinearLayout {

	private Context mContext;

	@ViewById
	WrapcontentListview lvFreetexts;
	
	@ViewById
	Button btn_submit;
	
	private EventBus eventBus;
	private ModuleActivity moduleActivity;

	public QuestionStyleFreeTextView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public QuestionStyleFreeTextView(Context context, AttributeSet attrs, int defStyleAttr,
									 int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public QuestionStyleFreeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public QuestionStyleFreeTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}
	
	public void setQuestion(ModuleActivity moduleActivity)
	{	
		// Init eventBus
		eventBus= EventBus.getDefault();
		
		setTextSize();
		
		this.moduleActivity=moduleActivity;
		ArrayList<ModuleFreeText> moduleFreeTexts=moduleActivity.getFreeText();
		
		// Parse answer string to array
		if (moduleActivity.getAnswer()!=null)
		{
			List<String> answers=getAnswersFromString(moduleActivity.getAnswer().replace("\\\"", "\""));
			if (answers!=null && answers.size()>0)
				for (int i=0; i< answers.size() && i<moduleFreeTexts.size(); i++)
					moduleFreeTexts.get(i).setAnswer(answers.get(i));
		}
		
		FreetextQuestionAdapter adapter=new FreetextQuestionAdapter((Activity)mContext, R.layout.item_freetext, moduleFreeTexts, FreetextQuestionAdapter.FREETEXT_IN_MODULE);
		lvFreetexts.setAdapter(adapter);
	}
	
	private List<String> getAnswersFromString(String stringAnswers)
	{
		try
		{
			Type listType = new TypeToken<List<String>>() {}.getType();
			return new Gson().fromJson(stringAnswers, listType);
		} catch(Exception e)
		{
			return null;
		}
	}


	public void setTextSize()
	{
		MLQPlusFunctions.SetViewTextSize(btn_submit, mContext, R.dimen.textMedium);
	}

	public String getAnswers() {
		ArrayList<ModuleFreeText> moduleFreeTexts=moduleActivity.getFreeText();
		
		if (moduleFreeTexts!=null && moduleFreeTexts.size()>0)
		{
			List<String> answers=new ArrayList<String>();
			for (ModuleFreeText freetext:moduleFreeTexts)
				if (freetext.getAnswer()!=null && freetext.getAnswer().length()>0)
				{
					answers.add(freetext.getAnswer());
				}
				else
					return null;
			return new Gson().toJson(answers).replace("\"", "\\\"");
		}
		return null;
	}
	
	@Click
	void btn_submit()
	{
		String answers=getAnswers();
		if (answers!=null && answers.length()>0)
		{
			// Update local
			moduleActivity.setAnswer(answers);
			
			MLQPlusFunctions.hideKeyboard((Activity)(mContext));
			
			// Call API update
			eventBus.post(new ModuleActivityEvent(moduleActivity.getId(), moduleActivity.getActivityType(), answers));
		}
		else
			Toast.makeText(mContext, "Please enter your answer", Toast.LENGTH_SHORT).show();
	}
	
	@Click
	void lnBackground()
	{
		MLQPlusFunctions.hideKeyboard((Activity)mContext);
	}
}
