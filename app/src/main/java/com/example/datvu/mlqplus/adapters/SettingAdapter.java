package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.ConfigMenuBase;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.ArrayList;

public class SettingAdapter extends BaseAdapter {
    private Activity mContext;
    private int mLayoutID;
    private ArrayList<ConfigMenuBase> mALSetting = null;
    private ViewHolder mViewHolder = null;

    public SettingAdapter(Activity mContext, int mLayoutID,
                          ArrayList<ConfigMenuBase> mALSetting) {
        this.mContext = mContext;
        this.mLayoutID = mLayoutID;
        this.mALSetting = mALSetting;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(mLayoutID, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            mViewHolder.tv_des = (TextView) convertView
                    .findViewById(R.id.tv_des);
            mViewHolder.img_icon = (ImageView) convertView
                    .findViewById(R.id.img_icon);
            mViewHolder.vFirstLine = convertView.findViewById(R.id.vFirstLine);

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        final ConfigMenuBase setting = mALSetting.get(position);

        // Show line at the first item
        if (position == 0)
            mViewHolder.vFirstLine.setVisibility(View.VISIBLE);

        if (setting.getName().equals("Password Settings"))
            mViewHolder.img_icon.setLayoutParams(new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.WRAP_CONTENT, 50));
        else if (setting.getName().equals("Account Settings"))
            mViewHolder.img_icon.setLayoutParams(new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.WRAP_CONTENT, 69));

        mViewHolder.tv_name.setText(setting.getName());
        mViewHolder.tv_des.setText(setting.getDescription());
        MLQPlusFunctions.showImage(mContext, setting.getIcon(),
                mViewHolder.img_icon);

        mViewHolder.tv_name.setSelected(true);
        mViewHolder.tv_name.requestFocus();

        mViewHolder.tv_des.setSelected(true);
        mViewHolder.tv_des.requestFocus();

        // Set size for widgets
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_name, mContext,
                R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_des, mContext,
                R.dimen.textToolDes);

        return convertView;
    }

    @Override
    public int getCount() {
        return mALSetting.size();
    }

    @Override
    public Object getItem(int position) {
        return mALSetting.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        TextView tv_name;
        TextView tv_des;
        ImageView img_icon;
        View vFirstLine;
    }

}
