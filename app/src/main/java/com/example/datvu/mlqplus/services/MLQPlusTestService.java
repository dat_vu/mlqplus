/*
package com.example.datvu.mlqplus.services;


import com.example.datvu.mlqplus.models.ModuleActivity;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MLQPlusTestService {
	private static RequestInterceptor requestInterceptor;
	private static RestAdapter restAdapter;
	private static Gson mGson;

	private static String API_URL="http://local.beesightsoft.com:9696/hosting/MLQPlusLeadership/Temp";
	
	private static RequestInterceptor getRequestInterceptor() {
		if (requestInterceptor == null) {
			requestInterceptor = new RequestInterceptor() {
	    		  @Override
	    		  public void intercept(RequestFacade request) {    			  
	    		    request.addHeader("Content-Type", "application/json");  
	    		    request.addHeader("Accept", "application/json");
	    		  }		  
	    	};
		}
		return requestInterceptor;
	}
	
	private static RestAdapter getRestAdapter () {
		if (restAdapter == null) {	
			restAdapter = new RestAdapter.Builder().setLogLevel (RestAdapter.LogLevel.FULL)
					.setEndpoint(API_URL)
					.setRequestInterceptor(getRequestInterceptor())
					.setLogLevel(LogLevel.FULL)				
					.setConverter(new GsonConverter(getGson())).build();
		}
		return restAdapter;
	}

	
	public static Gson getGson() {		
		if (mGson == null) {	
			mGson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
					.registerTypeAdapter(ModuleActivity.class, new ModuleActivityDeserializer())
					.create();
		}
		return mGson;
	}
	
    public static MLQPlusAPIsTest getBSSServices() {    	
        return getRestAdapter().create(MLQPlusAPIsTest.class);
    }
}
*/
