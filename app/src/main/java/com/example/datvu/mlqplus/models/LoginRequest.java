package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class LoginRequest extends LoginRequestBase implements Serializable {
	@SerializedName("email")
	private String email;

	@SerializedName("password")
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LoginRequest(String email, String password) {
		super();
		this.email = email;
		this.password = password;
		setDevice_token("");
	}

}
