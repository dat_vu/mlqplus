package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class Quiz extends ObjectBase {
	@SerializedName("quizzes_category_name")
	private String name;
	
	@SerializedName("quizzes_category_description")
	private String quizzes_category_description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Quiz(String name) {
		super();
		this.name = name;
	}

	public String getDescription() {
		return quizzes_category_description;
	}

	public void setDescription(String quizzes_category_description) {
		this.quizzes_category_description = quizzes_category_description;
	}
	
	

}
