package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class SurveyActivityAnswer {
	@SerializedName("survey_activity_id")
	private String survey_activity_id;
	
	@SerializedName("survey_answer_content")
	private String survey_answer_content;
	
	@SerializedName("survey_id")
	private String survey_id;

	public String getSurvey_activity_id() {
		return survey_activity_id;
	}

	public void setSurvey_activity_id(String survey_activity_id) {
		this.survey_activity_id = survey_activity_id;
	}

	public String getSurvey_answer_content() {
		return survey_answer_content;
	}

	public void setSurvey_answer_content(String survey_answer_content) {
		this.survey_answer_content = survey_answer_content;
	}

	public String getSurvey_id() {
		return survey_id;
	}

	public void setSurvey_id(String survey_id) {
		this.survey_id = survey_id;
	}

	public SurveyActivityAnswer(String survey_activity_id,
			String survey_answer_content, String survey_id) {
		super();
		this.survey_activity_id = survey_activity_id;
		this.survey_answer_content = survey_answer_content;
		this.survey_id = survey_id;
	}
	
	
}
