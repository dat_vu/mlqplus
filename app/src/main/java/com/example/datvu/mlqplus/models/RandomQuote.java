package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class RandomQuote {
	@SerializedName("content")
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
