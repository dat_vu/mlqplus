package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.ModuleActivity;
import com.example.datvu.mlqplus.models.SurveyActivityEvent;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

@EViewGroup(R.layout.view_survey_ratingquestion)
public class SurveyRatingQuestionView extends LinearLayout {

	private Context mContext;

	@ViewById
	ImageView img_level_0, img_level_1, img_level_2, img_level_3, img_level_4;
	
	@ViewById
	TextView tv_never, tv_always, tv_question;
	
	@ViewById
	View vMask;

	private int selectedPosition = -1;
	private ModuleActivity surveyActivity;
	private EventBus eventBus;

	public SurveyRatingQuestionView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public SurveyRatingQuestionView(Context context, AttributeSet attrs, int defStyleAttr,
									int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public SurveyRatingQuestionView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public SurveyRatingQuestionView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}
	
	public void setData(ModuleActivity surveyActivity)
	{
		// Init eventBus
		eventBus = EventBus.getDefault();
				
		this.surveyActivity=surveyActivity;
		
		setTextColor(mContext.getResources().getColor(R.color.white));
		setTextSize();
		
		showData();
	}
	
	private void showData()
	{
		if (surveyActivity!=null && surveyActivity.getRatingQuestion()!=null)
		{
			tv_question.setText(surveyActivity.getNumQuestion()+". "+ surveyActivity.getRatingQuestion().getQuestion());
			
			// Set text for scale low and high
			if (surveyActivity.getRatingQuestion().getScaleLow()!=null && surveyActivity.getRatingQuestion().getScaleLow().length()>0)
				tv_never.setText(surveyActivity.getRatingQuestion().getScaleLow());
			if (surveyActivity.getRatingQuestion().getScaleHigh()!=null && surveyActivity.getRatingQuestion().getScaleHigh().length()>0)
				tv_always.setText(surveyActivity.getRatingQuestion().getScaleHigh());
			
			if (surveyActivity.getAnswer()!=null && !surveyActivity.getAnswer().equals(""))
				try
				{
					setSelectedPosition(Integer.parseInt(surveyActivity.getAnswer()));
				}catch(Exception e){}
			
			/*if (SurveyGenerator.isSubmitted)
				vMask.setVisibility(View.VISIBLE);*/
		}
	}
	
	private void setTextColor(int textColor)
	{
		tv_question.setTextColor(textColor);
		tv_never.setTextColor(textColor);
		tv_always.setTextColor(textColor);
	}

	public void setTextSize()
	{
		// Fix views in small screen
		if (MLQPlusConstant.SCREEN_WIDTH<650)
		{
			int width=(int)getResources().getDimension(R.dimen.marginBasex5);
			
			MLQPlusFunctions.setImageViewSizeMargin(img_level_0, width, LayoutParams.WRAP_CONTENT, 0, (int)getResources().getDimension(R.dimen.marginBasex2), (int)getResources().getDimension(R.dimen.marginBasex3), 0);
			MLQPlusFunctions.setImageViewSizeMargin(img_level_1, width, LayoutParams.WRAP_CONTENT, 0, 0, 0, 0);
			MLQPlusFunctions.setImageViewSizeMargin(img_level_2, width, LayoutParams.WRAP_CONTENT, 0, (int)getResources().getDimension(R.dimen.marginBasex3), (int)getResources().getDimension(R.dimen.marginBasex3), 0);
			MLQPlusFunctions.setImageViewSizeMargin(img_level_3, width, LayoutParams.WRAP_CONTENT, 0, 0, 0, 0);
			MLQPlusFunctions.setImageViewSizeMargin(img_level_4, width, LayoutParams.WRAP_CONTENT, 0, (int)getResources().getDimension(R.dimen.marginBasex3), (int)getResources().getDimension(R.dimen.marginBasex2), 0);
						
		}
		
		MLQPlusFunctions.SetViewTextSize(tv_never, mContext, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_always, mContext, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_question, mContext, R.dimen.textMedium);
		
	}
	
	@Click
	void img_level_0() {
		setSelectedPosition(0);
	}

	@Click
	void img_level_1() {
		setSelectedPosition(1);
	}

	@Click
	void img_level_2() {
		setSelectedPosition(2);
	}

	@Click
	void img_level_3() {
		setSelectedPosition(3);
	}

	@Click
	void img_level_4() {
		setSelectedPosition(4);
	}

	private void setSelectedPosition(int pos) {
		selectedPosition = pos;
		
		// Call API update
		eventBus.post(new SurveyActivityEvent(surveyActivity.getId(), selectedPosition+""));

		// reset views
		img_level_0.setImageResource(R.drawable.answer0_unselected);
		img_level_1.setImageResource(R.drawable.answer1_unselected);
		img_level_2.setImageResource(R.drawable.answer2_unselected);
		img_level_3.setImageResource(R.drawable.answer3_unselected);
		img_level_4.setImageResource(R.drawable.answer4_unselected);

		switch (pos) {
		case 0:
			img_level_0.setImageResource(R.drawable.answer0_selected);
			break;
		case 1:
			img_level_1.setImageResource(R.drawable.answer1_selected);
			break;
		case 2:
			img_level_2.setImageResource(R.drawable.answer2_selected);
			break;
		case 3:
			img_level_3.setImageResource(R.drawable.answer3_selected);
			break;
		case 4:
			img_level_4.setImageResource(R.drawable.answer4_selected);
			break;
		}
	}

	public int getAnswer() {
		return selectedPosition;
	}
	
	@Click
	void lnBackground()
	{
		MLQPlusFunctions.hideKeyboard((Activity)mContext);
	}
}
