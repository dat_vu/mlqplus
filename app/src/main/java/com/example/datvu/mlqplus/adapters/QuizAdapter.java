package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Quiz;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class QuizAdapter extends ArrayAdapter<Quiz> {
    private Activity mContext;
    private int layoutID;
    private ViewHolder mViewHolder;

    public QuizAdapter(Activity context, int layoutID, List<Quiz> mALQuiz) {
        super(context, layoutID, mALQuiz);
        this.mContext = context;
        this.layoutID = layoutID;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(layoutID, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            mViewHolder.tvDes = (TextView) convertView
                    .findViewById(R.id.tvDes);
            mViewHolder.vFirstLine = convertView
                    .findViewById(R.id.firstLine);

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        mViewHolder.vFirstLine.setVisibility(View.GONE);

        Quiz quiz = getItem(position);

        mViewHolder.tv_name.setText(quiz.getName());
        if (quiz.getDescription() != null)
            mViewHolder.tvDes.setText(quiz.getDescription());
        else
            mViewHolder.tvDes.setVisibility(View.GONE);

        // Set size for widgets
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_name, mContext,
                R.dimen.itemTitle);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tvDes, mContext,
                R.dimen.itemDescript);

        return convertView;
    }

    private class ViewHolder {
        TextView tv_name, tvDes;
        View vFirstLine;
    }

}
