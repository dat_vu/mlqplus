/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.activity.LeaderQuizscreenActivity;
import com.mlqplus.trustedleader.activity.LeaderQuizscreenActivity_;
import com.mlqplus.trustedleader.activity.OpenningLeaderStyleQuizscreenActivity_;
import com.mlqplus.trustedleader.activity.OpenningQuizscreenActivity;
import com.mlqplus.trustedleader.activity.OpenningQuizscreenActivity_;
import com.mlqplus.trustedleader.activity.QuizscreenActivity;
import com.mlqplus.trustedleader.activity.YourStylescreenActivity;
import com.mlqplus.trustedleader.adapter.QuizAdapter;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.FirstQuizQuestion;
import com.mlqplus.trustedleader.model.ModuleActivity;
import com.mlqplus.trustedleader.model.ModuleQuestion;
import com.mlqplus.trustedleader.model.Quiz;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;

@EFragment(R.layout.fragment_quizzes)
public class FragmentQuizzes extends FragmentBase {
	public static FragmentQuizzes newInstance() {
		return new FragmentQuizzes_();
	}
	
	@ViewById
	LinearLayout ln_child;

	@ViewById
	TextView tv_title, tv_name;

	@ViewById
	ImageView img_back, img_icon;

	@ViewById
	ListView lv_quizzes;

	@ViewById
	WaitingView vWaiting;
	
	@ViewById
	View vLeaderStyleQuiz;

	private List<Quiz> mLQuizs = null;
	private QuizAdapter mQuizAdapter = null;
	private ModuleActivity mModuleActivity = null;
	private Activity mActivity;
	private int getDataFailedCount;
	public static boolean isStartFromFunScreen=true;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();
		getDataFailedCount = 0;

		// Set text size for all view
		setTextSize();

		// Get Data
		getData();
		
		// Show Leader Quiz Style 
		showLeaderQuizItem();
	}

	private void getData() {
		if (!MLQPlusFunctions.isInternetAvailable())
			showToast(MLQPlusConstant.CONNECTION_LOST);
		else {
			if (getDataFailedCount == 0)
				vWaiting.showWaitingView();
			MLQPlusService.getBSSServices().getQuizzes(
					MLQPlusFunctions.getAccessToken(),
					new Callback<ApiResponse<List<Quiz>>>() {

						@Override
						public void success(ApiResponse<List<Quiz>> mQuizzes,
								Response arg1) {
							// TODO Auto-generated method stub

							if (mQuizzes != null && mQuizzes.getData() != null
									&& mQuizzes.getData().size() > 0) {
								vWaiting.hideWaitingView();
								mLQuizs = mQuizzes.getData();
								showData();
							} else {
								// Retry getting data
								getDataFailedCount++;
								if (getDataFailedCount < 3)
									getData();
								else
									vWaiting.hideWaitingView();
							}
						}

						@Override
						public void failure(RetrofitError error) {
							// Retry getting data
							getDataFailedCount++;
							if (getDataFailedCount < 3)
								getData();
							else {
								vWaiting.hideWaitingView();
								if (error.getCause() instanceof SocketTimeoutException)
									showToast("Load Quizzes failed: "+MLQPlusConstant.TIMEOUT);
								else
									showToast("Load Quizzes failed. Error: "
											+ error.getMessage());
							}

						}
					});
		}

	}

	private void showData() {
		mQuizAdapter = new QuizAdapter(mActivity, R.layout.item_quiz, mLQuizs);
		lv_quizzes.setAdapter(mQuizAdapter);
		lv_quizzes.setSelector(R.drawable.btn_first_quiz);
	}
	
	private void showLeaderQuizItem()
	{
		tv_name.setText(mActivity.getResources().getString(
				R.string.leader_styles_quiz));
		img_icon.setImageResource(R.drawable.leader_quiz_style_icon);
	}

	private void setTextSize() {
		MLQPlusFunctions
				.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
		MLQPlusFunctions
		.SetViewTextSize(tv_name, mActivity, R.dimen.textMediumLarge);
	}

	@Override
	public void onStop() {
		YourStylescreenActivity.isStartFromLeaderQuiz = true;
		super.onStop();
	}
	
	@Override
	public void onDestroy() {
		YourStylescreenActivity.isStartFromLeaderQuiz = true;
		super.onDestroy();
	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}

	@ItemClick
	void lv_quizzes(int position) {
		final Quiz quiz = (Quiz) lv_quizzes.getItemAtPosition(position);
		if (!MLQPlusFunctions.isInternetAvailable())
			((BaseActivity) mActivity)
					.showToast(MLQPlusConstant.CONNECTION_LOST);
		else {
			vWaiting.showWaitingView();
			MLQPlusService.getBSSServices().getLeaderQuizzes(
					MLQPlusFunctions.getAccessToken(), quiz.getId(),
					new Callback<ApiResponse<ArrayList<ModuleQuestion>>>() {

						@Override
						public void success(
								ApiResponse<ArrayList<ModuleQuestion>> quizData,
								Response arg1) {
							vWaiting.hideWaitingView();
							if (quizData != null && quizData.getData() != null) {
								Intent intent = new Intent(mActivity,
										LeaderQuizscreenActivity_.class);
								Bundle bundle = new Bundle();
								bundle.putSerializable(
										LeaderQuizscreenActivity.QUESTIONS,
										quizData.getData());
								bundle.putSerializable(
										LeaderQuizscreenActivity.QUIZ, quiz);
								intent.putExtra(MLQPlusConstant.BUNDLE, bundle);
								mActivity.startActivity(intent);
							}
						}

						@Override
						public void failure(RetrofitError error) {
							// TODO Auto-generated method stub
							vWaiting.hideWaitingView();
							if (error.getCause() instanceof SocketTimeoutException)
								((BaseActivity) mActivity).showToast("Load quiz failed: "+MLQPlusConstant.TIMEOUT);
							else
								((BaseActivity) mActivity)
										.showToast("Load quiz failed");
						}
					});
		}
	}

	@Click
	void vLeaderStyleQuiz() {
		if (!MLQPlusFunctions.isInternetAvailable())
			showToast("Please check your internet connection and try again");
		else {
			((BaseActivity)mActivity).showWaitingDialog(mActivity, "Loading quiz",
					"Please wait...");
			MLQPlusService.getBSSServices().getFirstQuiz(
					MLQPlusFunctions.getAccessToken(),
					new Callback<ApiResponse<ArrayList<FirstQuizQuestion>>>() {

						@Override
						public void success(
								ApiResponse<ArrayList<FirstQuizQuestion>> mQuestions,
								Response arg1) {
							((BaseActivity)mActivity).dismissWaitingDialog();
							
							YourStylescreenActivity.isStartFromLeaderQuiz=true;

							// If successfully load 5 questions
							if (mQuestions.getData() != null
									&& mQuestions.getData().size() >= 0) {
								QuizscreenActivity.questions = mQuestions
										.getData();
								Bundle bundle = new Bundle();
								bundle.putBoolean(OpenningQuizscreenActivity.IS_START_FROM_LEADER_QUIZ, true);
								((BaseActivity)mActivity).startNewZoomActivity(
										mActivity,
										OpenningLeaderStyleQuizscreenActivity_.class, false, bundle);
							} else
								// can't get enough 5 questions
								((BaseActivity)mActivity).showToast(
										"Cannot load quiz. Please try again!",
										Toast.LENGTH_LONG);

						}

						@Override
						public void failure(RetrofitError arg0) {
							((BaseActivity)mActivity).dismissWaitingDialog();
							((BaseActivity)mActivity).showToast(
									"Cannot load quiz. Please check your internet connection and try again!",
									Toast.LENGTH_LONG);
						}
					});
		}

	}
}
*/
