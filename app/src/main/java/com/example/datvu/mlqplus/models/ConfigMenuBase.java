package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class ConfigMenuBase extends ObjectBase {
	@SerializedName("name")
	private String name;

	@SerializedName("description")
	private String description;

	@SerializedName("icon")
	private String icon;

	@SerializedName("config_name")
	private String config_name;
	
	@SerializedName("screen_group")
	private String screen_group;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getConfig_name() {
		return config_name;
	}

	public void setConfig_name(String config_name) {
		this.config_name = config_name;
	}

	public String getScreen_group() {
		return screen_group;
	}

	public void setScreen_group(String screen_group) {
		this.screen_group = screen_group;
	}

}
