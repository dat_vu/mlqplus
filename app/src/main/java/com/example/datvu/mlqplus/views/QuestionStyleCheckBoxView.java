package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.ModuleActivity;
import com.example.datvu.mlqplus.models.ModuleActivityEvent;
import com.example.datvu.mlqplus.models.ModuleTickBox;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.List;

@EViewGroup(R.layout.view_question_style_checkbox)
public class QuestionStyleCheckBoxView extends LinearLayout {

	private Context mContext;
	
	@ViewById
	ImageView img_tickbox0, img_tickbox1, img_tickbox2, img_tickbox3;

	@ViewById
	TextView tv_question, tv0, tv1, tv2, tv3;

	@ViewById
	LinearLayout ln0, ln1, ln2, ln3;
	
	private EventBus eventBus;
	private List<Boolean> answers;
	private ModuleActivity moduleActivity;

	public QuestionStyleCheckBoxView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public QuestionStyleCheckBoxView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public QuestionStyleCheckBoxView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public QuestionStyleCheckBoxView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public void setQuestion(ModuleActivity moduleActivity) {
		//Init eventBus
		eventBus=EventBus.getDefault();
		
		setTextSize();
		
		this.moduleActivity=moduleActivity;
		ModuleTickBox moduleTickBox=this.moduleActivity.getTickBox();
		
		tv_question.setText(moduleTickBox.getQuestion());

		tv0.setText(moduleTickBox.getAnswers().get(0).getValue());
		tv1.setText(moduleTickBox.getAnswers().get(1).getValue());
		tv2.setText(moduleTickBox.getAnswers().get(2).getValue());
		tv3.setText(moduleTickBox.getAnswers().get(3).getValue());

		answers= MLQPlusFunctions.getListFromAnswer(parseAnswer(moduleActivity.getAnswer()), moduleTickBox.getAnswers().size());
		showSelectedAnswer();
	}
	
	private int parseAnswer(String answer)
	{
		try
		{
			int result=Integer.parseInt(answer);
			return result;
		}catch (Exception e){}
		return 0;
	}

	private void showSelectedAnswer() {
		showSeleted(answers.get(0), img_tickbox0);
		showSeleted(answers.get(1), img_tickbox1);
		showSeleted(answers.get(2), img_tickbox2);
		showSeleted(answers.get(3), img_tickbox3);
	}
	
	private void showSeleted(boolean isSelected, ImageView img)
	{
		if (isSelected)
			img.setImageResource(R.drawable.tickbox_selected);
		else
			img.setImageResource(R.drawable.tickbox_unselected);
	}

	public void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_question, mContext, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv0, mContext, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv1, mContext, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv2, mContext, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv3, mContext, R.dimen.textMedium);
	}

	private void selectedAnswer(int index)
	{
		answers.set(index,!answers.get(index));
		showSelectedAnswer();
	}
	
	@Click
	void ln0() {
		userTickAnswer(0);
	}

	@Click
	void ln1() {
		userTickAnswer(1);
	}

	@Click
	void ln2() {
		userTickAnswer(2);
	}

	@Click
	void ln3() {
		userTickAnswer(3);
	}
	
	public void userTickAnswer(int pos)
	{
		selectedAnswer(pos);
		// Update local
		String answers=getAnswers()+"";
		moduleActivity.setAnswer(answers);
		
		// Call API update
		eventBus.post(new ModuleActivityEvent(moduleActivity.getId(), moduleActivity.getActivityType(), answers));
	}

	public int getAnswers() {
		return MLQPlusFunctions.getAnswerFromArray(answers);
	}
}
