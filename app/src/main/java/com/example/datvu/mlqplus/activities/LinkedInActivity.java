/*
package com.example.datvu.mlqplus.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.model.LinkedInAuthorization;
import com.mlqplus.trustedleader.service.MLQPlusLinkedinServices;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Calendar;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EActivity(R.layout.activity_linkedin)
public class LinkedInActivity extends BaseActivity {

    private static final String REDIRECT_URI = "http://beesightsoft.com";
    private static final String STATE = "E3ZYKC1T6H2yP4z";

    // These are constants used for build the urls
    private static final String AUTHORIZATION_URL = "https://www.linkedin.com/uas/oauth2/authorization";
    private static final String ACCESS_TOKEN_URL = "https://www.linkedin.com/uas/oauth2/accessToken";
    private static final String SECRET_KEY_PARAM = "client_secret";
    private static final String RESPONSE_TYPE_PARAM = "response_type";
    private static final String GRANT_TYPE_PARAM = "grant_type";
    private static final String GRANT_TYPE = "authorization_code";
    private static final String RESPONSE_TYPE_VALUE = "code";
    private static final String CLIENT_ID_PARAM = "client_id";
    private static final String STATE_PARAM = "state";
    private static final String REDIRECT_URI_PARAM = "redirect_uri";
    */
/*---------------------------------------*//*

    private static final String QUESTION_MARK = "?";
    private static final String AMPERSAND = "&";
    private static final String EQUALS = "=";
    private static String API_KEY;
    private static String SECRET_KEY;
    public static String LINKEDIN_ACCESSTOKEN = "LinkedinAccesstoken";
    public static int RESULTCODE = 9999;

    @ViewById
    WebView linkedinWebView;
    @ViewById
    ImageView ivCloseLinkedIn;

    private LinkedInAuthorization linkedInAuthorization;

    @AfterViews
    void init() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        linkedinWebView.requestFocus(View.FOCUS_DOWN);

        API_KEY = getString(R.string.linkedin_client_id);
        SECRET_KEY = getString(R.string.linkedin_client_secret);

        showWaitingDialog(LinkedInActivity.this, "", "Please wait...");
        linkedinWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                dismissWaitingDialog();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    String authorizationUrl) {
                if (isInternetAvailable()) {
                    if (authorizationUrl.startsWith(REDIRECT_URI)) {
                        Log.i("Authorize", "");
                        Uri uri = Uri.parse(authorizationUrl);
                        String stateToken = uri.getQueryParameter(STATE_PARAM);
                        if (stateToken == null || !stateToken.equals(STATE)) {
                            Log.e("Authorize", "State token doesn't match");
                            return true;
                        }
                        String authorizationToken = uri
                                .getQueryParameter(RESPONSE_TYPE_VALUE);
                        if (authorizationToken == null) {
                            Log.i("Authorize",
                                    "The user doesn't allow authorization.");
                            return true;
                        }
                        Log.i("Authorize", "Auth token received: "
                                + authorizationToken);
                        showWaitingDialog(LinkedInActivity.this, "Login",
                                "Please wait...");
                        MLQPlusLinkedinServices.getwiGrubServices(
                                "https://www.linkedin.com")
                                .PostRequestAccessToken("authorization_code",
                                        authorizationToken, API_KEY,
                                        REDIRECT_URI, SECRET_KEY,
                                        new Callback<LinkedInAuthorization>() {
                                            @Override
                                            public void success(
                                                    LinkedInAuthorization linkedInAuth,
                                                    Response arg1) {
                                                linkedInAuthorization = linkedInAuth;
                                                Calendar calendar = Calendar
                                                        .getInstance();
                                                calendar.add(Calendar.SECOND,
                                                        (int) linkedInAuth
                                                                .getExpiresIn());
                                                long expireDate = calendar
                                                        .getTimeInMillis();
                                                linkedInAuthorization
                                                        .setExpireDate(expireDate);

                                                // Return accesstoken
                                                Intent intent = getIntent();
                                                Bundle bundle = new Bundle();
                                                bundle.putString(
                                                        LINKEDIN_ACCESSTOKEN,
                                                        linkedInAuthorization
                                                                .getAccessToken());
                                                intent.putExtra(
                                                        MLQPlusConstant.BUNDLE,
                                                        bundle);
                                                setResult(RESULTCODE, intent);
                                                finish();
                                            }

                                            @Override
                                            public void failure(
                                                    RetrofitError arg0) {
                                                dismissWaitingDialog();
                                            }
                                        });
                    } else {
                        Log.i("Authorize", "Redirecting to: "
                                + authorizationUrl);
                        linkedinWebView.loadUrl(authorizationUrl);
                    }
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                if (errorCode == ERROR_TIMEOUT) {
                    dismissWaitingDialog();
                    view.stopLoading();
                    view.loadData("file:///android_asset/error.html",
                            "text/html", "utf-8");
                }
            }
        });
        String authUrl = getAuthorizationUrl();
        Log.i("Authorize", "Loading Auth Url: " + authUrl);
        linkedinWebView.loadUrl(authUrl);

        ivCloseLinkedIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linkedinWebView.stopLoading();
                setResult(Activity.RESULT_CANCELED);
                LinkedInActivity.this.finish();
            }
        });
    }

    private static String getAuthorizationUrl() {
        return AUTHORIZATION_URL + QUESTION_MARK + RESPONSE_TYPE_PARAM + EQUALS
                + RESPONSE_TYPE_VALUE + AMPERSAND + CLIENT_ID_PARAM + EQUALS
                + API_KEY + AMPERSAND + STATE_PARAM + EQUALS + STATE
                + AMPERSAND + REDIRECT_URI_PARAM + EQUALS + REDIRECT_URI;
    }

}
*/
