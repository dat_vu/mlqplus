package com.example.datvu.mlqplus.dagger.components;

import com.example.datvu.mlqplus.dagger.modules.UserInfoModule;
import com.example.datvu.mlqplus.dagger.scopes.ApplicationScope;

import dagger.Component;

/**
 * Created by solit_000 on 4/14/2016.
 */
// AppModule [Application] |AppComponent| <- BaseComponent needs that stuff..
@ApplicationScope
@Component(modules = {UserInfoModule.class}, dependencies = ApplicationComponent.class)
public interface BaseComponent {
}
