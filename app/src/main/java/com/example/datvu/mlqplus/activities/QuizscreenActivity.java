/*
package com.example.datvu.mlqplus.activities;

import android.app.ActionBar.LayoutParams;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.FirstQuizQuestion;
import com.mlqplus.trustedleader.model.LeadershipStyle;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

@EActivity(R.layout.activity_quizscreen)
public class QuizscreenActivity extends BaseDialogActivity {

    public static final String QUIZ_SCREEN = "Quiz Screen";

    @ViewById
    LinearLayout ln_root, ln_child, ln_question, ln_content, ln_title;

    @ViewById
    TextView tv_quiz;

    @ViewById
    Button btn_continue, btn_submit;


    @ViewById
    com.mlqplus.trustedleader.view.MyQuestionStyleNeverAlwaysView question_1,
            question_2, question_3;

    @ViewById
    ScrollView sv_question;

    private int page = 0;
    public static String PAGE = "page";
    public static String QUESTIONS = "questions";
    public static ArrayList<FirstQuizQuestion> questions;

    @AfterViews
    void init() {
        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);

        // Get Data
        try {
            page = (int) getIntent().getBundleExtra(MLQPlusConstant.BUNDLE)
                    .getInt(PAGE);
        } catch (Exception e) {
        }

        // Show data
        showData();

    }

    private void showData() {
        if (questions.size() >= 5) {
            if (page == 0) {
                btn_submit.setVisibility(View.GONE);

                question_1.setQuestion(1, questions.get(0)
                        .getFirst_question_content());
                question_2.setQuestion(2, questions.get(1)
                        .getFirst_question_content());
                question_3.setVisibility(View.GONE);
            } else {
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LayoutParams.MATCH_PARENT, 0);
                lp.weight = 520;
                sv_question.setLayoutParams(lp);
                question_1.setQuestion(3, questions.get(2)
                        .getFirst_question_content());
                question_2.setQuestion(4, questions.get(3)
                        .getFirst_question_content());
                question_3.setQuestion(5, questions.get(4)
                        .getFirst_question_content());

                btn_continue.setVisibility(View.GONE);
                LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(
                        LayoutParams.MATCH_PARENT, 0);
                lp2.weight = 37;
                ln_title.setLayoutParams(lp2);
                ln_title.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_quiz, QuizscreenActivity.this,
                R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(btn_submit, QuizscreenActivity.this,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btn_continue, QuizscreenActivity.this,
                R.dimen.textMedium);
    }

    @Click
    void btn_continue() {
        // Get user answers
        questions.get(0).setAnswer(question_1.getAnswer());
        questions.get(1).setAnswer(question_2.getAnswer());

        // check and next
        if (questions.get(0).getAnswer() != -1
                && questions.get(1).getAnswer() != -1) {
            Bundle bundle = new Bundle();
            bundle.putInt(PAGE, 1);
            startNewActivityNoAnimation(QuizscreenActivity.this,
                    QuizscreenActivity_.class, true, bundle);
        } else
            showToast("Please select your anwers");
    }

    @Click
    void btn_submit() {
        // Get user answers
        questions.get(2).setAnswer(question_1.getAnswer());
        questions.get(3).setAnswer(question_2.getAnswer());
        questions.get(4).setAnswer(question_3.getAnswer());

        // check and submit
        if (questions.get(2).getAnswer() != -1
                && questions.get(3).getAnswer() != -1
                && questions.get(4).getAnswer() != -1) {
            // Submit answer and get leadership style
            String json = "{\"answer\": [";
            for (int i = 0; i < questions.size(); i++) {
                json += questions.get(i).getAnswer();
                if (i < questions.size() - 1)
                    json += ", ";
                else
                    json += "]}";
            }
            if (!isInternetAvailable())
                showToast("Please check your internet connection and try again");
            else {
                showWaitingDialog(QuizscreenActivity.this,
                        "Submitting answers", "Please wait...");
                MLQPlusService.getBSSServices().submitFirstQuizAnswer(
                        MLQPlusFunctions.getAccessToken(), new TypedString(json),
                        new Callback<ApiResponse<LeadershipStyle>>() {

                            @Override
                            public void success(
                                    ApiResponse<LeadershipStyle> mLeadershipStyle,
                                    Response arg1) {
                                // TODO Auto-generated method stub
                                dismissWaitingDialog();
                                if (mLeadershipStyle.getData() != null) {
                                    final Bundle bundle = new Bundle();
                                    bundle.putSerializable(
                                            YourStylescreenActivity.LEADERSHIP_STYLE,
                                            mLeadershipStyle.getData());
                                    MLQPlusFunctions.loadImage(
                                            QuizscreenActivity.this,
                                            mLeadershipStyle.getData().getIcon(),
                                            new ImageLoadingListener() {

                                                @Override
                                                public void onLoadingStarted(
                                                        String imageUri,
                                                        View view) {
                                                    // TODO Auto-generated
                                                    // method stub

                                                }

                                                @Override
                                                public void onLoadingFailed(
                                                        String imageUri,
                                                        View view,
                                                        FailReason failReason) {
                                                    // TODO Auto-generated
                                                    // method stub
                                                    startNewZoomActivity(
                                                            QuizscreenActivity.this,
                                                            YourStylescreenActivity_.class,
                                                            true, bundle);
                                                }

                                                @Override
                                                public void onLoadingComplete(
                                                        String imageUri,
                                                        View view,
                                                        Bitmap loadedImage) {
                                                    // TODO Auto-generated
                                                    // method stub
                                                    startNewZoomActivity(
                                                            QuizscreenActivity.this,
                                                            YourStylescreenActivity_.class,
                                                            true, bundle);
                                                }

                                                @Override
                                                public void onLoadingCancelled(
                                                        String imageUri,
                                                        View view) {
                                                    // TODO Auto-generated
                                                    // method stub

                                                }
                                            });

                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                // TODO Auto-generated method stub
                                dismissWaitingDialog();
                                Log.d("MLQPlus", "Submit First Quiz answers failed: " + error.getMessage());

                                showToast(
                                        "Cannot submit your answers. Please try again!",
                                        Toast.LENGTH_LONG);
                            }
                        });
            }
        } else
            showToast("Please select your anwers");

    }
}
*/
