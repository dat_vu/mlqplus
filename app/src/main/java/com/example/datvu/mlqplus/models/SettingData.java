package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class SettingData {
	@SerializedName("push_notification")
	private String push_notification;
	
	@SerializedName("alert_tone")
	private String alert_tone;
	
	@SerializedName("font")
	private String font;

	public String getPush_notification() {
		return push_notification;
	}

	public void setPush_notification(String push_notification) {
		this.push_notification = push_notification;
	}

	public String getAlert_tone() {
		return alert_tone;
	}

	public void setAlert_tone(String alert_tone) {
		this.alert_tone = alert_tone;
	}

	public String getFont() {
		return font;
	}

	public void setFont(String font) {
		this.font = font;
	}
	
	
}
