package com.example.datvu.mlqplus.views;


import android.content.Context;
import android.widget.LinearLayout;

import com.example.datvu.mlqplus.R;

import org.androidannotations.annotations.EViewGroup;

@EViewGroup(R.layout.view_tickbox_item)
public class TickboxItemView extends LinearLayout {

	public TickboxItemView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
/*
	private Context mContext;

	@ViewById
	ImageView imgTickbox;
	
	@ViewById 
	LinearLayout lnContent;

	private OnRatingListenner onRatingListenner;
	private Integer selectedPosition;

	public TickboxItemView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public TickboxItemView(Context context, AttributeSet attrs, int defStyleAttr,
			int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public TickboxItemView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public TickboxItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}
	
	public void setData(String question, Integer selectedAnswer, OnRatingListenner mOnRatingListenner)
	{
		onRatingListenner=mOnRatingListenner;

		// Create tv to show title
		TextView tvTitle=new TextView(mContext);
		tvTitle.setSingleLine(false);
		tvTitle.setText(question);
		tvTitle.setTextColor(mContext.getResources().getColor(R.color.black_text));
		tvTitle.setTextIsSelectable(true);
		MLQPlusFunctions.SetViewTextSize(tvTitle, mContext, Size.MEDIUM);
		
		//lnTitle.addView(tvTitle);
		Log.d("MLQPlus", "Set text: "+question);
		setSelectedPosition(selectedAnswer);
	}

	@Click
	void img_level_0() {
		userRateClick(0);
	}

	@Click
	void img_level_1() {
		userRateClick(1);
	}

	@Click
	void img_level_2() {
		userRateClick(2);
	}

	@Click
	void img_level_3() {
		userRateClick(3);
	}

	@Click
	void img_level_4() {
		userRateClick(4);
	}
	
	private void userRateClick(int pos)
	{
		setSelectedPosition(pos);
		if (onRatingListenner!=null)
			onRatingListenner.onRating();
	}

	private void setSelectedPosition(Integer pos) {
		selectedPosition = pos;

		// reset views
		img_level_0.setImageResource(R.drawable.answer0_unselected);
		img_level_1.setImageResource(R.drawable.answer1_unselected);
		img_level_2.setImageResource(R.drawable.answer2_unselected);
		img_level_3.setImageResource(R.drawable.answer3_unselected);
		img_level_4.setImageResource(R.drawable.answer4_unselected);

		if (selectedPosition!=null)
		switch (pos) {
		case 0:
			img_level_0.setImageResource(R.drawable.answer0_selected);
			break;
		case 1:
			img_level_1.setImageResource(R.drawable.answer1_selected);
			break;
		case 2:
			img_level_2.setImageResource(R.drawable.answer2_selected);
			break;
		case 3:
			img_level_3.setImageResource(R.drawable.answer3_selected);
			break;
		case 4:
			img_level_4.setImageResource(R.drawable.answer4_selected);
			break;
		}
	}

	public Integer getSeletedAnswer() {
		return selectedPosition;
	}
	
	public interface OnRatingListenner{
		public void onRating();
	}
	*/
}
