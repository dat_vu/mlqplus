package com.example.datvu.mlqplus.adapters;

import android.app.Activity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.About;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class AboutAdapter extends ArrayAdapter<About> {
    private Activity mContext = null;
    private ViewHolder mViewHolder = null;
    private int mLayoutID;

    public AboutAdapter(Activity mContext, int mLayoutID,
                        List<About> mAbouts) {
        super(mContext, mLayoutID, mAbouts);
        this.mContext = mContext;
        this.mLayoutID = mLayoutID;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(R.layout.item_about, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tvName = (TextView) convertView
                    .findViewById(R.id.tvName);
            mViewHolder.tvContent = (TextView) convertView
                    .findViewById(R.id.tvContent);
            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        // Set text size
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tvName, mContext, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tvContent, mContext, R.dimen.textMedium);


        About about = getItem(position);
        if (about.getName() != null)
            mViewHolder.tvName.setText(about
                    .getName());

        if (about.getContent() != null) {
            String content = about.getContent();
            content = content.replace("&nbsp;", " ");
            mViewHolder.tvContent.setText(Html.fromHtml(content));
            mViewHolder.tvContent.setClickable(true);
            mViewHolder.tvContent.setMovementMethod(LinkMovementMethod.getInstance());
        }

        return convertView;
    }

    private class ViewHolder {
        TextView tvName;
        TextView tvContent;
    }

}

