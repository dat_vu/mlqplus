package com.example.datvu.mlqplus.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.activities.BaseActivity;
import com.example.datvu.mlqplus.models.Country;
import com.example.datvu.mlqplus.models.UserResponse;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.example.datvu.mlqplus.views.CustomSpinnerView;
import com.example.datvu.mlqplus.views.MyCustomSpinnerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@EFragment(R.layout.fragment_account)
public class FragmentAccount extends FragmentBase {

	private String mFirstName, mEmail, mAge, mGender, mCountry;
	private int mLoadCountryFailCount = 0;

	public static FragmentAccount newInstance() {
		return new FragmentAccount_();
	}

	@ViewById
	LinearLayout ln_root, ln_child;

	@ViewById
	TextView tv_title;

	@ViewById
	ImageView img_back;

	@ViewById
	EditText txt_first_name, txt_email;

	@ViewById
	MyCustomSpinnerView sp_age, sp_gender, sp_country;

	@ViewById
	Button btn_cancel, btn_save;

	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Set text size for all view
		setTextSize();

		// Init spinner
		initSpinners();

		// Show Data
		showData();
	}

	private void initSpinners() {
		sp_age.setData(new ArrayList<Object>(MLQPlusFunctions.getAgeList()),
				null, "Age", getResources().getColor(R.color.blue_hint_text),
				R.dimen.textMedium, spinnerClickListenner);

		sp_gender.setData(
				new ArrayList<Object>(MLQPlusFunctions.getGenderList()), null,
				"Gender", getResources().getColor(R.color.blue_hint_text),
				R.dimen.textMedium, spinnerClickListenner);

		/*if (MLQPlusConstant.COUNTRIES == null) {
			((BaseActivity) mActivity).showWaitingDialog(mActivity,
					"Loading country data", "Please wait...");
			loadCountryList();
		} else {
			sp_country.setData(
					new ArrayList<Object>(MLQPlusConstant.COUNTRIES), null,
					"Country", getResources().getColor(R.color.blue_hint_text),
					R.dimen.textMedium, spinnerClickListenner);
		}*/
	}

	/*
	 * validate user input
	 */
	private boolean validateInputData() {
		try {
			mFirstName = txt_first_name.getText().toString().trim();
			if (mFirstName.length() == 0) {
				showToast("First name cannot be empty");
				return false;
			} else if (mFirstName.length() > 32) {
				showToast("The maximum length of first name is 32");
				return false;
			}

			mEmail = txt_email.getText().toString().trim();
			if (mEmail.length() == 0) {
				showToast("Email cannot be empty");
				return false;
			} else if (!isValidEmail(mEmail)) {
				showToast("Invalid email address");
				return false;
			}

			/*
			 * if (sp_age.getSelectedValue() == null) {
			 * showToast("Age cannot be empty"); return false; }
			 * 
			 * if (sp_gender.getSelectedValue() == null) {
			 * showToast("Gender cannot be empty"); return false; }
			 * 
			 * if (sp_country.getSelectedValue() == null) {
			 * showToast("Country cannot be empty"); return false; }
			 */
		} catch (Exception e) {
			showToast("Error " + e.getMessage());
			return false;
		}

		return true;
	}

	private void showData() {
		if (MLQPlusConstant.USERINFO != null
				&& MLQPlusConstant.USERINFO.getUser() != null) {
			UserResponse userInfo = MLQPlusConstant.USERINFO.getUser();

			if (userInfo.getFirst_name() != null)
				txt_first_name.setText(userInfo.getFirst_name());

			if (userInfo.getEmail() != null)
				txt_email.setText(userInfo.getEmail());

			// Age
			if (userInfo.getAge() != null) {
				List<String> ages = MLQPlusFunctions.getAgeList();
				for (int i = 0; i < ages.size(); i++)
					if (ages.get(i).equals(userInfo.getAge())) {
						sp_age.setSelection(i);
						break;
					}
			}

			// Gender
			if (userInfo.getGender() != null) {
				List<String> genders = MLQPlusFunctions.getGenderList();
				String gender = "";
				if (userInfo.getGender().equalsIgnoreCase("0"))
					gender = "Male";
				else if(userInfo.getGender().equalsIgnoreCase("1"))
					gender = "Female";
						
				for (int i = 0; i < genders.size(); i++)
					if (genders.get(i).equals(gender)) {
						sp_gender.setSelection(i);
						break;
					}
			}

			// Country
			if (userInfo.getCountry_id() != null && !userInfo.getCountry_id().equals("NUL")) {
				List<Country> countries=new ArrayList<Country>();
				if (MLQPlusConstant.COUNTRIES!=null && MLQPlusConstant.COUNTRIES.size()>0)
					countries= MLQPlusConstant.COUNTRIES;
				for (int i = 0; i < countries.size(); i++)
					if (countries.get(i).getId()
							.equals(userInfo.getCountry_id())) {
						sp_country.setSelection(i);
						break;
					}
			}
		}
	}

	private void setTextSize() {
		MLQPlusFunctions
				.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(txt_email, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(txt_first_name, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(btn_cancel, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(btn_save, mActivity,
				R.dimen.textMedium);
	}

	/*
	 * validate email
	 */
	private boolean isValidEmail(String email) {
		Pattern pattern = Patterns.EMAIL_ADDRESS;
		return pattern.matcher(email).matches();
	}

	/*private void loadCountryList() {
		MLQPlusService.getBSSServices().getCountryList(
				new Callback<ApiResponse<List<Country>>>() {

					@Override
					public void success(ApiResponse<List<Country>> countries,
							Response response) {
						List<Country> countryList = new ArrayList<Country>();
						if (countries != null) {
							countryList.addAll(countries.getData());
							MLQPlusConstant.COUNTRIES = countryList;

							// Set country data
							sp_country.setData(
									new ArrayList<Object>(
											MLQPlusConstant.COUNTRIES),
									null,
									"Country",
									getResources().getColor(
											R.color.blue_create_account_text),
									R.dimen.textMedium, spinnerClickListenner);
						}
						((BaseActivity) mActivity).dismissWaitingDialog();
					}

					@Override
					public void failure(RetrofitError error) {
						mLoadCountryFailCount++;
						if (mLoadCountryFailCount == 3) {
							((BaseActivity) mActivity).dismissWaitingDialog();
							if (error.getCause() instanceof SocketTimeoutException)
								showToast(MLQPlusConstant.TIMEOUT);
							else if (!MLQPlusFunctions.isInternetAvailable()) {
								showToast(MLQPlusConstant.CONNECTION_LOST);
							} else
								showToast("Load country list fail");
							mActivity.onBackPressed();
						} else
							loadCountryList();
					}
				});
	}*/

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}

	@Click
	void btn_cancel() {
		mActivity.onBackPressed();
	}

	/*@Click
	void btn_save() {
		if (validateInputData()) {

			SignupRequest signupRequest = new SignupRequest();
			signupRequest.setEmail(txt_email.getText().toString());
			signupRequest.setFirst_name(txt_first_name.getText().toString());
			signupRequest.setAge(sp_age.getSelectedValue());
			signupRequest
					.setGender(sp_gender.getSelectedValue().equals("Male") ? "0"
							: "1");
			signupRequest.setCountry_id(sp_country.getSelectedValue());

			if (!MLQPlusFunctions.isInternetAvailable())
				((BaseActivity) mActivity)
						.showToast(MLQPlusConstant.CONNECTION_LOST);
			else {
				((BaseActivity) mActivity).showWaitingDialog(mActivity,
						"Updating", "Please wait...");
				MLQPlusService.getBSSServices().updateAccount(
						MLQPlusFunctions.getAccessToken(), signupRequest,
						new Callback<ApiResponse<UserResponse>>() {

							@Override
							public void success(
									ApiResponse<UserResponse> response,
									Response arg1) {
								// TODO Auto-generated method stub
								((BaseActivity) mActivity)
										.dismissWaitingDialog();
								((BaseActivity) mActivity)
										.showToast("Saved successfully");
								if (response.getData() != null)
									MLQPlusConstant.USERINFO.setUser(response
											.getData());
								mActivity.onBackPressed();
							}

							@Override
							public void failure(RetrofitError error) {
								// TODO Auto-generated method stub
								((BaseActivity) mActivity)
										.dismissWaitingDialog();
								if (error.getCause() instanceof SocketTimeoutException)
									showToast(MLQPlusConstant.TIMEOUT);
								else
									try {
										if (error.getResponse() != null) {
											ErrorRetrofit body = (ErrorRetrofit) error
													.getBodyAs(ErrorRetrofit.class);
											if (body.getError()
													.getErrorMessage()
													.contains(
															"The email is already exists"))
												((BaseActivity) mActivity)
														.showToast(body
																.getError()
																.getErrorMessage());
											else
												((BaseActivity) mActivity)
														.showToast("Update failed. Please try again!");
										}
									} catch (Exception e) {
										((BaseActivity) mActivity)
												.showToast("Update failed. Please try again!");
									}

							}
						});
			}

		}
	}*/

	CustomSpinnerView.SpinnerClickListenner spinnerClickListenner = new CustomSpinnerView.SpinnerClickListenner() {

		@Override
		public void onClick() {
			// TODO Auto-generated method stub
			((BaseActivity) mActivity).hideKeyboard();
		}
	};

	@Click
	void ln_content() {
		MLQPlusFunctions.hideKeyboard(mActivity);
	}

	@Click
	void ln_title() {
		MLQPlusFunctions.hideKeyboard(mActivity);
	}

}
