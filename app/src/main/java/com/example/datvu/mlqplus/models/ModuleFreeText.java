package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class ModuleFreeText extends ActivityBase{
	@SerializedName("question")
	private String question;
	
	@SerializedName("suggestionText")
	private String suggestionText;
	
	private String answer;
	
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getSuggestionText() {
		return suggestionText;
	}

	public void setSuggestionText(String suggestionText) {
		this.suggestionText = suggestionText;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	

}
