package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Term;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class TermAdapter extends ArrayAdapter<Term> {
    private Activity mContext;
    private int layoutID;
    private ViewHolder mViewHolder;

    public TermAdapter(Activity context, int layoutID, List<Term> mALTerm) {
        super(context, layoutID, mALTerm);
        this.mContext = context;
        this.layoutID = layoutID;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(layoutID, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            mViewHolder.tv_content = (TextView) convertView
                    .findViewById(R.id.tv_content);

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        mViewHolder.tv_name.setText(String.valueOf(getItem(position).getName()));
        mViewHolder.tv_content.setText(String.valueOf(getItem(position)
                .getContent()));
        // Set size for widgets
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_name, mContext,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_content, mContext,
                R.dimen.textMedium);

        return convertView;
    }

    private class ViewHolder {
        TextView tv_name;
        TextView tv_content;

    }

}
