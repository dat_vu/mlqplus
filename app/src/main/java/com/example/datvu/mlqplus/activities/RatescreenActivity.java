/*
package com.example.datvu.mlqplus.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_ratescreen)
public class RatescreenActivity extends BaseDialogActivity {

    @ViewById
    TextView tvTitle, tvWeHope, tvIfYouHave, tvMaybeLater;

    @ViewById
    LinearLayout ln_root;

    @ViewById
    RelativeLayout rl_child;

    @ViewById
    Button btnGiveFeedback, btnRate;

    @AfterViews
    void init() {
        // Set form size
        setDialogSize(ln_root, 87, 70);

        // Set text size for all view
        setTextSize();

        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tvTitle, RatescreenActivity.this,
                R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tvWeHope, RatescreenActivity.this,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tvIfYouHave, RatescreenActivity.this,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tvMaybeLater, RatescreenActivity.this,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btnRate, RatescreenActivity.this,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btnGiveFeedback,
                RatescreenActivity.this, R.dimen.textMedium);
    }

    @Click
    void btnRate() {
        openPlayStoreToRate();
    }

    @Click
    void tvMaybeLater() {
        this.finish();
    }

    @Click
    void btnGiveFeedback() {
        startSendFeedback();
    }

    private void openPlayStoreToRate() {
        Uri uri = Uri.parse("market://details?id="
                + RatescreenActivity.this.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to
        // intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
                | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET
                | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id="
                            + RatescreenActivity.this.getPackageName())));
        }
        RatescreenActivity.this.finish();
    }

    private void startSendFeedback() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"apps@mlqplus.com.au"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "[ MLQPlus Trusted Leader User Feedback ]");

        startActivityForResult(Intent.createChooser(intent, "Send Feedback"), 1);
        this.finish();
    }
}
*/
