package com.example.datvu.mlqplus.models;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Module extends ObjectBase implements Serializable{
	@SerializedName("module_name")
	private String name;
	
	@SerializedName("module_sub_name")
	private String moduleSubname;

	@SerializedName("module_icon")
	private String icon;
	
	@SerializedName("isAndroidUnlocked")
	private boolean unlocked;
	
	private boolean completed;
	
	@SerializedName("published")
	private String published;
	
	@SerializedName("submitted_activity")
	private int submitted_activity;
	
	@SerializedName("total_activity")
	private int total_activity;
	
	@SerializedName("price")
	private double price;
	
	@SerializedName("description")
	private String description;
	
	@SerializedName("sections")
	private List<ModuleSection> sections;
	
	public String getModuleSubname() {
		return moduleSubname;
	}

	public void setModuleSubname(String moduleSubname) {
		this.moduleSubname = moduleSubname;
	}

	public int getSubmitted_activity() {
		return submitted_activity;
	}

	public void setSubmitted_activity(int submitted_activity) {
		this.submitted_activity = submitted_activity;
	}

	public int getTotal_activity() {
		return total_activity;
	}

	public void setTotal_activity(int total_activity) {
		this.total_activity = total_activity;
	}

	public List<ModuleSection> getSections() {
		return sections;
	}

	public void setSections(List<ModuleSection> sections) {
		this.sections = sections;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {		
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public boolean isUnlocked() {
		return unlocked;
	}

	public void setUnlocked(boolean unlocked) {
		this.unlocked = unlocked;
	}

	public boolean isCompleted() {
		if (isUnlocked())
			return total_activity==submitted_activity;
		return false;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
	
	public double getProgress()
	{
		if (!isUnlocked())
			return 0;
		if (total_activity!=0)
			return 100.0 * submitted_activity/total_activity;
		return 100;
	}
	
	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	public Module(String name, String icon, boolean unlocked, boolean completed, double price, String description) {
		super();
		this.name = name;
		this.icon = icon;
		this.unlocked = unlocked;
		this.completed = completed;
		this.price=price;
		this.description=description;
	}

	public Module(String name) {
		super();
		this.name = name;
	}
	
	
}
