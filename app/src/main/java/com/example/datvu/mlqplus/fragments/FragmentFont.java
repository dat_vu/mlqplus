/*
package com.example.datvu.mlqplus.fragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.TypedValue;
import android.webkit.ConsoleMessage.MessageLevel;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.SettingData;
import com.mlqplus.trustedleader.model.TextSize;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
 
@EFragment(R.layout.fragment_font)
public class FragmentFont extends FragmentBase {

	public static FragmentFont newInstance() {
		return new FragmentFont_();
	} 
      
	@ViewById 
	LinearLayout ln_root, ln_child; 
 
	@ViewById 
	TextView tv_title;

	@ViewById
	ImageView img_tickbox_largest, img_tickbox_large,
			img_tickbox_small, img_tickbox_smallest;

	@ViewById
	TextView tv_largest, tv_large, tv_small, tv_smallest;

	@ViewById
	Button btn_cancel, btn_save;

	private Activity mActivity;
	private TextSize textSize;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Set text size for all view
		setTextSize();

		// Show current Setting
		textSize = MLQPlusConstant.TEXTSIZE;
		updateChoiceOnUI(textSize);
	}

	private void setTextSize() {
		tv_largest.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.textMedium) + 8);
		tv_large.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.textMedium));
		tv_small.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.textMedium) - 4);
		tv_smallest.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.textMedium) - 8);
		
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(btn_cancel, mActivity, R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(btn_save, mActivity, R.dimen.textLarge);
	}

	@Click
	void ln_back() {
		if (textSize==MLQPlusConstant.TEXTSIZE)
			mActivity.onBackPressed();
		else
			saveData();
	}

	void saveData() {
		if (!MLQPlusFunctions.isInternetAvailable())
			((BaseActivity)mActivity).showToast(MLQPlusConstant.CONNECTION_LOST);
		else
		{
			((BaseActivity)mActivity).showWaitingDialog(mActivity, "Updating", "Please wait...");
			MLQPlusService.getBSSServices().updateFontSize(MLQPlusFunctions.getAccessToken(), new TypedString("{\"font\": \""+textSize.getSizeInString()+"\"}"), new Callback<ApiResponse<SettingData>>() {
				
				@Override
				public void success(ApiResponse<SettingData> arg0, Response arg1) {
					// TODO Auto-generated method stub
					((BaseActivity)mActivity).dismissWaitingDialog();
					MLQPlusConstant.TEXTSIZE = textSize;
					MLQPlusFunctions.saveTextSize(textSize, mActivity);
					((BaseActivity)mActivity).showToast("Saved successfully");
					mActivity.onBackPressed();
				}
				
				@Override
				public void failure(RetrofitError arg0) {
					// TODO Auto-generated method stub
					((BaseActivity)mActivity).dismissWaitingDialog();
					((BaseActivity)mActivity).showToast("Update failed. Please try again");
				}
			});
		}
		
		

	}

	@Click
	void ln_tickbox_largest() {
		updateChoiceOnUI(TextSize.LARGER2);
	}

	@Click
	void ln_tickbox_large() {
		updateChoiceOnUI(TextSize.NORMAL);
	}

	@Click
	void ln_tickbox_small() {
		updateChoiceOnUI(TextSize.SMALLER1);
	}

	@Click
	void ln_tickbox_smallest() {
		updateChoiceOnUI(TextSize.SMALLER2);
	}
	
	private void updateChoiceOnUI(TextSize textSize)
	{
		this.textSize = textSize;
		img_tickbox_largest.setImageResource(R.drawable.unticked_circle_icon);
		img_tickbox_large.setImageResource(R.drawable.unticked_circle_icon);
		img_tickbox_small.setImageResource(R.drawable.unticked_circle_icon);
		img_tickbox_smallest.setImageResource(R.drawable.unticked_circle_icon);

		if (textSize.equals(TextSize.LARGER2))
			img_tickbox_largest.setImageResource(R.drawable.ticked_circle_icon);
		else if (textSize.equals(TextSize.NORMAL))
			img_tickbox_large.setImageResource(R.drawable.ticked_circle_icon);
		else if (textSize.equals(TextSize.SMALLER1))
			img_tickbox_small.setImageResource(R.drawable.ticked_circle_icon);
		else if (textSize.equals(TextSize.SMALLER2))
			img_tickbox_smallest
					.setImageResource(R.drawable.ticked_circle_icon);
	}
}
*/
