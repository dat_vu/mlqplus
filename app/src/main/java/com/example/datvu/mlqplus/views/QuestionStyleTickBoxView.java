package com.example.datvu.mlqplus.views;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.adapters.TickboxQuestionAdapter;
import com.example.datvu.mlqplus.models.ModuleActivity;
import com.example.datvu.mlqplus.models.ModuleActivityEvent;
import com.example.datvu.mlqplus.models.ModuleTickBox;
import com.example.datvu.mlqplus.models.TickboxQuestion;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

@EViewGroup(R.layout.view_question_style_tickbox)
public class QuestionStyleTickBoxView extends LinearLayout {

	private Context mContext;

	@ViewById
	TextView tv_question;

	@ViewById
	ListView lv_tickbox_question;

	private EventBus eventBus;
	private ArrayList<TickboxQuestion> mALTickboxQuestion = null;
	private ModuleActivity moduleActivity;
	private TickboxQuestionAdapter mTickboxQuestionAdapter = null;

	public QuestionStyleTickBoxView(Context context) {
		super(context);

		mContext = context;
	}

	@SuppressLint("NewApi")
	public QuestionStyleTickBoxView(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public QuestionStyleTickBoxView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public QuestionStyleTickBoxView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	@ItemClick
	public void lv_tickbox_question(int position) {
		if (mALTickboxQuestion.get(position).isSelected())
			mALTickboxQuestion.get(position).setSelected(false);
		else
			mALTickboxQuestion.get(position).setSelected(true);
		
		mTickboxQuestionAdapter.notifyDataSetChanged();
		moduleActivity.setAnswer(getAnswer()+"");
		
		// Call API update
		eventBus.post(new ModuleActivityEvent(moduleActivity.getId(), moduleActivity.getActivityType(), getAnswer()+""));

	}

	public void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_question, mContext, R.dimen.textMedium);
	}

	public void setQuestion(ModuleActivity activity) {
		// Init eventBus
		eventBus = EventBus.getDefault();
		
		setTextSize();
		
		if (activity != null && activity.getTickBox() != null) {

			this.moduleActivity = activity;
			ModuleTickBox moduleTickBox = activity.getTickBox();

			// Parse answer from number to an array
			List<Boolean> answers=MLQPlusFunctions.getListFromAnswer(parseAnswer(moduleActivity.getAnswer()), moduleTickBox.getAnswers().size());
			
			
			// Set Title
			if (moduleTickBox.getQuestion()!=null)
				tv_question.setText(moduleTickBox.getQuestion());		
			else
				tv_question.setVisibility(View.GONE);
			
			mALTickboxQuestion = new ArrayList<TickboxQuestion>();
			for (int i = 0; i < moduleTickBox.getAnswers().size(); i++)
				if (answers!=null && answers.size()>i && answers.get(i)!=null && moduleTickBox
						.getAnswers().get(i)!=null && moduleTickBox
						.getAnswers().get(i).getValue()!=null)
				mALTickboxQuestion.add(new TickboxQuestion(answers.get(i), moduleTickBox
						.getAnswers().get(i).getValue()));
			
			// Show list of answers
			mTickboxQuestionAdapter = new TickboxQuestionAdapter(
					(Activity) mContext,
					R.layout.item_view_question_style_tickbox,
					mALTickboxQuestion, 0);
			lv_tickbox_question.setAdapter(mTickboxQuestionAdapter);
		}
	}

	public int getAnswer() {
		List<Boolean> answer = new ArrayList<Boolean>();
		for (int i = 0; i < mALTickboxQuestion.size(); i++)
			answer.add(mALTickboxQuestion.get(i).isSelected());
		return MLQPlusFunctions.getAnswerFromArray(answer);
	}
	
	private int parseAnswer(String answer)
	{
		try
		{
			int result=Integer.parseInt(answer);
			return result;
		}catch (Exception e){}
		return 0;
	}
	
	@Click
	void lnBackground()
	{
		MLQPlusFunctions.hideKeyboard((Activity)mContext);
	}

}
