package com.example.datvu.mlqplus.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class WrapcontentListview extends ListView{
	public WrapcontentListview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WrapcontentListview(Context context) {
        super(context);
    }

    public WrapcontentListview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
