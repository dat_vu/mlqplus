package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class ActivityQuestion implements Serializable{
	@SerializedName("question")
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
