package com.example.datvu.mlqplus.adapters;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Survey;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;

import java.util.List;

public class SurveyAdapter extends ArrayAdapter<Survey> {
    private Activity mContext;
    private int layoutID;
    private ViewHolder mViewHolder;

    public SurveyAdapter(Activity context, int layoutID, List<Survey> mALQuiz) {
        super(context, layoutID, mALQuiz);
        this.mContext = context;
        this.layoutID = layoutID;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater lInflater = mContext.getLayoutInflater();
        if (convertView == null) {
            convertView = lInflater.inflate(layoutID, null);
            mViewHolder = new ViewHolder();
            mViewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            mViewHolder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            mViewHolder.tv_des = (TextView) convertView
                    .findViewById(R.id.tv_des);
            mViewHolder.firstLine = convertView.findViewById(R.id.firstLine);

            convertView.setTag(mViewHolder);
        } else
            mViewHolder = (ViewHolder) convertView.getTag();

        // Show first line at the first item
        if (position == 0)
            mViewHolder.firstLine.setVisibility(View.VISIBLE);
        else
            mViewHolder.firstLine.setVisibility(View.GONE);

        Survey survey = getItem(position);

        mViewHolder.tv_name.setText(survey.getSurvey_name());
        if (survey.getDescription() != null)
            mViewHolder.tv_des.setText(survey.getDescription());
        else
            mViewHolder.tv_des.setVisibility(View.GONE);

        mViewHolder.tv_des.setSelected(true);
        mViewHolder.tv_des.requestFocus();


        // Set size for widgets
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_name, mContext,
                R.dimen.itemTitle);
        MLQPlusFunctions.SetViewTextSize(mViewHolder.tv_des, mContext,
                R.dimen.itemDescript);

        return convertView;
    }

    private class ViewHolder {
        TextView tv_name, tv_des;
        View firstLine;
    }

}
