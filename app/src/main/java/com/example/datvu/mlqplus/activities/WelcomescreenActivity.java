package com.example.datvu.mlqplus.activities;


import android.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.adapters.WelcomeActivityFragmentPagerAdapter;
import com.example.datvu.mlqplus.models.WelcomeScreenInfo;
import com.example.datvu.mlqplus.utils.MLQPlusConstant;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.example.datvu.mlqplus.views.CustomTabPostionView;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.sromku.simple.fb.SimpleFacebook;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_welcomescreen)
public class WelcomescreenActivity extends BaseActivity {

    @ViewById
    ViewPager viewPager;

    @ViewById(R.id.tpv)
    CustomTabPostionView tpv;

    @ViewById
    LinearLayout ln_root;

    @ViewById
    FrameLayout fl_child;

    @ViewById
    Button btn_signin_email, btn_signin_fb, btn_signin_linkedin,
            btn_create_account;

    private SimpleFacebook simpleFacebook;
    private int LINKEDIN_REQUEST_CODE = 4321;
    private String facebookAccessToken = "";
    private String linkedinAccessToken = "";
    public static String WELCOME_SCREEN_INFO = "WelcomeScreenInfo";
    private static ArrayList<WelcomeScreenInfo> mALWelcomeScreen = null;
    private GoogleCloudMessaging mGoogleCloudMessaging;

    @AfterViews
    void init() {
        // Creat zoomable layout
        MLQPlusFunctions.makeZoomableView(WelcomescreenActivity.this, ln_root,
                fl_child);

        // Set text size for all view
        setTextSize();

        getData();

        simpleFacebook = SimpleFacebook.getInstance(WelcomescreenActivity.this);
        initViewParger();
        tpv.setSeletedTab(3, 0);

    }

    private void getData() {
        try {
            if (mALWelcomeScreen == null)
                mALWelcomeScreen = (ArrayList<WelcomeScreenInfo>) getIntent()
                        .getBundleExtra(MLQPlusConstant.BUNDLE).get(
                                WELCOME_SCREEN_INFO);
        } catch (Exception e) {
            finish();
        }
    }

    private void setTextSize() {
        // Fix small screen size
        MLQPlusFunctions.SetViewTextSize(btn_create_account,
                WelcomescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btn_signin_email,
                WelcomescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btn_signin_fb,
                WelcomescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btn_signin_linkedin,
                WelcomescreenActivity.this, R.dimen.textMedium);
    }

    private void initViewParger() {
        FragmentManager fm = getFragmentManager();

        if (mALWelcomeScreen == null)
            return;
        WelcomeActivityFragmentPagerAdapter tabsFragmentPagerAdapter = new WelcomeActivityFragmentPagerAdapter(
                fm, mALWelcomeScreen);
        viewPager.setAdapter(tabsFragmentPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setCurrentItem(0);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // Init tab position
                tpv.setSeletedTab(3, position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int position) {
                // TODO Auto-generated method stub

            }
        });
    }

    @Click
    void btn_create_account() {
        startNewActivityNoAnimation(WelcomescreenActivity.this,
                CreateAccountscreenActivity_.class, true);
    }

    @Click
    void btn_signin_email() {
        startNewActivityNoAnimation(WelcomescreenActivity.this,
                LoginscreenActivity_.class, true);
    }

    /*@Click
    void btn_signin_fb() {
        simpleFacebook.logout(new OnLogoutListener() {

            @Override
            public void onFail(String reason) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onException(Throwable throwable) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onThinking() {
                // TODO Auto-generated method stub

            }

            @Override
            public void onLogout() {
                // TODO Auto-generated method stub
                loginFB();
            }
        });

    }*/

    /*private void loginFB() {
        simpleFacebook.login(new OnLoginListener() {
            @Override
            public void onFail(String reason) {
                Log.w(getClass().getSimpleName(), "Failed to login");
            }

            @Override
            public void onException(Throwable throwable) {
                Log.w(getClass().getSimpleName(), "onException");
            }

            @Override
            public void onThinking() {
                Log.w(getClass().getSimpleName(), "onThinking");
            }

            @Override
            public void onNotAcceptingPermissions(Type arg0) {
                // TODO Auto-generated method stub
                Log.w(getClass().getSimpleName(), "onNotAcceptingPermissions");
            }

            @Override
            public void onLogin() {
                // change the state of the button or do whatever you want
                Log.w(getClass().getSimpleName(), "onLogin");
                if (simpleFacebook.isLogin()) {
                    facebookAccessToken = simpleFacebook.getSession()
                            .getAccessToken();

                    if (facebookAccessToken != "") {
                        if (!isInternetAvailable())
                            showToast("Please check your internet connection and try again");
                        else {
                            showWaitingDialog(WelcomescreenActivity.this,
                                    "Sign in", "Please wait...");

                            SocialSignupRequest socialSignupRequest = new SocialSignupRequest(
                                    facebookAccessToken);

                            registerGoogleCloudMessage(socialSignupRequest,
                                    true);
                        }
                    }
                }
            }
        });
    }*/

    /*Callback<ApiResponse<SignupResponse>> loginCallback = new Callback<ApiResponse<SignupResponse>>() {

        @Override
        public void success(ApiResponse<SignupResponse> mSignupResponse,
                            Response response) {
            dismissWaitingDialog();
            MLQPlusConstant.USERINFO = mSignupResponse.getData();
            getSharedPreferencesService().accessToken().put(
                    MLQPlusConstant.USERINFO.getId());

            // Load config menu
            MLQPlusFunctions.loadConfigMenu(WelcomescreenActivity.this,
                    loadConfigCallback);

            // Start homeActivity if user completed first Quiz; else start first
            // Quiz
            if (MLQPlusConstant.USERINFO.getUser().getIs_has_leadership_style()
                    .equals("0")
                    || !getSharedPreferencesService().userClickedGetStarted()
                    .get())
                startNewActivityNoAnimation(WelcomescreenActivity.this,
                        BackgroundActivity_.class, true);
            else
                startNewActivityNoAnimation(WelcomescreenActivity.this,
                        HomescreenActivity_.class, true);
        }

        @Override
        public void failure(RetrofitError error) {
            dismissWaitingDialog();
            if (error.getCause() instanceof SocketTimeoutException)
                showToast(MLQPlusConstant.TIMEOUT);
            else
                try {
                    String json = new String(((TypedByteArray) error
                            .getResponse().getBody()).getBytes());
                    showToast("Error: " + json);
                } catch (Exception e) {
                }

        }
    };

    Callback<ApiResponse<ConfigMenu>> loadConfigCallback = new Callback<ApiResponse<ConfigMenu>>() {

        @Override
        public void success(ApiResponse<ConfigMenu> configs, Response arg1) {
            // TODO Auto-generated method stub
            if (configs != null && configs.getData() != null)
                MLQPlusConstant.CONFIG_MENU = configs.getData();
        }

        @Override
        public void failure(RetrofitError error) {
            // TODO Auto-generated method stub
            if (error.getCause() instanceof SocketTimeoutException)
                showToast("Load config settings: " + MLQPlusConstant.TIMEOUT);
        }
    };*/

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LINKEDIN_REQUEST_CODE
                && resultCode == LinkedInActivity.RESULTCODE) {
            String accesstoken = data.getBundleExtra(MLQPlusConstant.BUNDLE)
                    .getString(LinkedInActivity.LINKEDIN_ACCESSTOKEN);
            if (accesstoken != "") {
                if (!isInternetAvailable())
                    showToast("Please check your internet connection and try again");
                else {
                    showWaitingDialog(WelcomescreenActivity.this, "Sign in",
                            "Please wait...");

                    SocialSignupRequest socialSignupRequest = new SocialSignupRequest(
                            accesstoken);

                    registerGoogleCloudMessage(socialSignupRequest, false);
                }
            }
        } else
            simpleFacebook
                    .onActivityResult(this, requestCode, resultCode, data);

    }*/

    /*private void registerGoogleCloudMessage(
            final SocialSignupRequest socialSignupRequest,
            final boolean isLoginFB) {
        new AsyncTask() {

            @Override
            protected String doInBackground(Object... params) {
                // TODO Auto-generated method stub
                // Push device ID
                String DeviceID = getSharedPreferencesService().deviceID()
                        .get();
                int count_time = 0;
                while ((DeviceID == null || DeviceID.equals(""))
                        && count_time < 10) {
                    count_time++;
                    if (mGoogleCloudMessaging == null) {
                        mGoogleCloudMessaging = GoogleCloudMessaging
                                .getInstance(WelcomescreenActivity.this);
                        ;
                    }
                    try {
                        DeviceID = mGoogleCloudMessaging
                                .register(MLQPlusConstant.GCM_SENDER_ID);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                return DeviceID;
            }

            @Override
            protected void onPostExecute(Object result) {
                try {
                    String DeviceID = String.valueOf(result);
                    if (DeviceID == null || DeviceID.length() == 0)
                        DeviceID = "123";
                    else
                        getSharedPreferencesService().deviceID().put(DeviceID);

                    socialSignupRequest.setDevice_token(DeviceID);
                    if (isLoginFB)
                        MLQPlusService.getBSSServices().loginFacebook(
                                socialSignupRequest, loginCallback);
                    else
                        MLQPlusService.getBSSServices().loginLinkedin(
                                socialSignupRequest, loginCallback);

                } catch (Exception e) {
                }
            }

            ;

        }.execute(null, null, null);

    }*/

    /*@Click
    void btn_signin_linkedin() {
        startNewZoomActivity(WelcomescreenActivity.this,
                LinkedInActivity_.class, null, LINKEDIN_REQUEST_CODE);
    }*/
}
