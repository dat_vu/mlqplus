/*
package com.example.datvu.mlqplus.fragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.SettingData;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

@EFragment(R.layout.fragment_notifications)
public class FragmentNotifications extends FragmentBase {
	public static FragmentNotifications newInstance() {
		return new FragmentNotifications_();
	}  
  
	@ViewById
	LinearLayout ln_root, ln_child; 
 
	@ViewById
	TextView tv_title, tv_enable_push_noti, tv_enable_alert;

	@ViewById 
	ToggleButton toggle_push_noti, toggle_alert;

	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Set text size for all view
		setTextSize();

		// Show Data
		showData();
	}

	private void showData() {
		if (MLQPlusConstant.SETTING_DATA!=null)
		{
			toggle_alert.setChecked(MLQPlusConstant.SETTING_DATA.getAlert_tone().equals("1"));
			toggle_push_noti.setChecked(MLQPlusConstant.SETTING_DATA.getPush_notification().equals("1"));
		}
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(tv_enable_push_noti, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_enable_alert, mActivity,
				R.dimen.textMedium);
	}

	@Click
	void ln_back() {
		if (MLQPlusConstant.SETTING_DATA!=null && MLQPlusConstant.SETTING_DATA.getAlert_tone()!=null &&
				MLQPlusConstant.SETTING_DATA.getPush_notification()!=null && (toggle_alert.isChecked()!=MLQPlusConstant.SETTING_DATA.getAlert_tone().equals("1")||
				toggle_push_noti.isChecked()!=MLQPlusConstant.SETTING_DATA.getPush_notification().equals("1")))
			saveData();
		else
			mActivity.onBackPressed();
	}

	void saveData()
	{
		if (!MLQPlusFunctions.isInternetAvailable())
			((BaseActivity)mActivity).showToast(MLQPlusConstant.CONNECTION_LOST);
		else
		{
			// Get toggle value
			String notification, alert;
			notification = toggle_push_noti.isChecked()?"1":"0";
			alert =toggle_alert.isChecked()?"1":"0";
			((BaseActivity)mActivity).showWaitingDialog(mActivity, "Updating", "Please wait...");
			MLQPlusService.getBSSServices().updateNotifications(MLQPlusFunctions.getAccessToken(), new TypedString("{\"push_notification\":\"" + notification +"\",\"alert_tone\":\"" + alert + "\"}"), new Callback<ApiResponse<SettingData>>() {
				
				@Override
				public void success(ApiResponse<SettingData> mSettingData, Response response) {
					((BaseActivity)mActivity).dismissWaitingDialog();
					MLQPlusConstant.SETTING_DATA = mSettingData.getData();
					((BaseActivity)mActivity).showToast("Saved successfully");
					mActivity.onBackPressed();
				}
				
				@Override
				public void failure(RetrofitError arg0) {
					// TODO Auto-generated method stub
					((BaseActivity)mActivity).dismissWaitingDialog();
					((BaseActivity)mActivity).showToast("Update failed. Please try again");
				}
			});
		}
	}
}
*/
