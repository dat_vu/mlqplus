/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.ErrorRetrofit;
import com.mlqplus.trustedleader.model.UserResponse;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

@EFragment(R.layout.fragment_password)
public class FragmentPassword extends FragmentBase {
	public static FragmentPassword newInstance() {
		return new FragmentPassword_();
	}

	@ViewById
	LinearLayout ln_root, ln_child;

	@ViewById
	TextView tv_title, tv_remember_login_details, tv_note;

	@ViewById
	EditText txt_old_password, txt_new_password, txt_confirm_new_password;

	@ViewById
	ToggleButton toggle_remember_me;

	@ViewById
	Button btnCancel, btnSave;

	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Set text size for all view
		setTextSize();
	}

	private void setTextSize() {
		MLQPlusFunctions
				.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(tv_note, mActivity, R.dimen.textSmall);
		MLQPlusFunctions.SetViewTextSize(tv_remember_login_details, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(txt_old_password, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(txt_new_password, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(txt_confirm_new_password, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(btnCancel, mActivity,
				R.dimen.textMedium);
		MLQPlusFunctions
				.SetViewTextSize(btnSave, mActivity, R.dimen.textMedium);
	}

	private boolean validateAField(String text, String name) {
		if (text.length() == 0) {
			showToast(name + " cannot be empty");
			return false;
		} else if (text.length() < 4 || text.length() > 12) {
			showToast(name + " length must be between 4 and 12 characters");
			return false;
		} else if (!(text.matches(".*[1-9].*") && text.matches(".*[A-Za-z].*"))) {
			showToast(name + " must contain number and letter");
			return false;
		}

		return true;
	}

	private boolean validateInputData() {
		if (!validateAField(txt_old_password.getText().toString(),
				"Old password"))
			return false;
		if (!validateAField(txt_new_password.getText().toString(),
				"New password"))
			return false;
		if (!validateAField(txt_confirm_new_password.getText().toString(),
				"Confirmation password"))
			return false;
		if (!txt_new_password.getText().toString()
				.equals(txt_confirm_new_password.getText().toString())) {
			showToast("New password and confirmation password are not match");
			return false;
		}

		return true;
	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}

	@Click
	void btnCancel() {
		mActivity.onBackPressed();
	}

	@Click
	void btnSave() {
		if (validateInputData()) {
			if (!MLQPlusFunctions.isInternetAvailable())
				((BaseActivity) mActivity)
						.showToast(MLQPlusConstant.CONNECTION_LOST);
			else {
				((BaseActivity) mActivity).showWaitingDialog(mActivity,
						"Updating", "Please wait...");
				String json = "{\"old_password\": \""
						+ txt_old_password.getText().toString()
						+ "\",\"new_password\": \""
						+ txt_new_password.getText().toString() + "\"}";
				MLQPlusService.getBSSServices().updatePassword(
						MLQPlusFunctions.getAccessToken(),
						new TypedString(json),
						new Callback<ApiResponse<UserResponse>>() {

							@Override
							public void success(
									ApiResponse<UserResponse> respone,
									Response arg1) {
								// TODO Auto-generated method stub
								((BaseActivity) mActivity)
										.dismissWaitingDialog();

								if (toggle_remember_me.isChecked()) {
									((BaseActivity) mActivity)
											.getSharedPreferencesService()
											.email()
											.put(MLQPlusConstant.USERINFO
													.getUser().getEmail());
									((BaseActivity) mActivity)
											.getSharedPreferencesService()
											.password()
											.put(txt_new_password.getText()
													.toString());
								} else {
									((BaseActivity) mActivity)
											.getSharedPreferencesService()
											.email().put("");
									((BaseActivity) mActivity)
											.getSharedPreferencesService()
											.password().put("");
								}

								showToast("Updated successfully!");
								mActivity.onBackPressed();
							}

							@Override
							public void failure(RetrofitError error) {
								// TODO Auto-generated method stub
								((BaseActivity) mActivity)
										.dismissWaitingDialog();
								if (error.getCause() instanceof SocketTimeoutException)
									showToast(MLQPlusConstant.TIMEOUT);
								else
									try {
										if (error.getResponse() != null) {
											ErrorRetrofit body = (ErrorRetrofit) error
													.getBodyAs(ErrorRetrofit.class);
											showToast(body.getError()
													.getErrorMessage());
										}
									} catch (Exception e) {
										showToast("Update password failed. Please try again!");
									}

							}
						});
			}
		}
	}

	@Click
	void toggle_remember_me() {
		MLQPlusFunctions.hideKeyboard(mActivity);
	}

	@Click
	void ln_child() {
		MLQPlusFunctions.hideKeyboard(mActivity);
	}
}
*/
