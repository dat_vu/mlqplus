package com.example.datvu.mlqplus.models;

import java.io.Serializable;

import org.androidannotations.annotations.ServiceAction;

import com.google.gson.annotations.SerializedName;

public class UserResponse implements Serializable {
	@SerializedName("id")
	private String id;

	@SerializedName("country_id")
	private String country_id;

	@SerializedName("email")
	private String email;

	@SerializedName("last_login")
	private String last_login;

	@SerializedName("first_name")
	private String first_name;

	@SerializedName("last_name")
	private String last_name;

	@SerializedName("age")
	private String age;

	@SerializedName("gender")
	private String gender;

	@SerializedName("social_id")
	private String social_id;
	
	@SerializedName("isNeedChangePass")
	private String isNeedChangePass;
	
	@SerializedName("is_has_leadership_style")
	private String is_has_leadership_style;

	public String getIs_has_leadership_style() {
		return is_has_leadership_style;
	}

	public void setIs_has_leadership_style(String is_has_leadership_style) {
		this.is_has_leadership_style = is_has_leadership_style;
	}

	public String getIsNeedChangePass() {
		return isNeedChangePass;
	}

	public void setIsNeedChangePass(String isNeedChangePass) {
		this.isNeedChangePass = isNeedChangePass;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCountry_id() {
		return country_id;
	}

	public void setCountry_id(String country_id) {
		this.country_id = country_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLast_login() {
		return last_login;
	}

	public void setLast_login(String last_login) {
		this.last_login = last_login;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSocial_id() {
		return social_id;
	}

	public void setSocial_id(String social_id) {
		this.social_id = social_id;
	}

	public UserResponse(String id, String country_id, String email,
			String last_login, String first_name, String last_name, String age,
			String gender, String social_id) {
		super();
		this.id = id;
		this.country_id = country_id;
		this.email = email;
		this.last_login = last_login;
		this.first_name = first_name;
		this.last_name = last_name;
		this.age = age;
		this.gender = gender;
		this.social_id = social_id;
	}

}
