package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class ModuleText extends ActivityBase{
	@SerializedName("content")
	private String content;
	
	@SerializedName("textAlignment")
	private int textAlignment;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getTextAlignment() {
		return textAlignment;
	}

	public void setTextAlignment(int textAlignment) {
		this.textAlignment = textAlignment;
	}
}
