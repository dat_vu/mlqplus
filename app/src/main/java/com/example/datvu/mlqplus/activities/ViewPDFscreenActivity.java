/*
package com.example.datvu.mlqplus.activities;

import android.content.Intent;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.model.Assessment;
import com.mlqplus.trustedleader.utils.DownloadFileTask;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_viewpdf_screen)
public class ViewPDFscreenActivity extends BaseActivity {

    @ViewById
    WebView webview;

    @ViewById
    Button btnDownload, btnShare;

    @ViewById
    TextView tvTitle;

    private Assessment assessment;
    public static String ASSESSMENT = "Assessment";

    @AfterViews
    void init() {
        // Set text size for all view
        setTextSize();

        // getData
        getData();
    }

    private void getData() {
        try {
            assessment = (Assessment) getIntent().getBundleExtra(
                    MLQPlusConstant.BUNDLE).getSerializable(ASSESSMENT);
            showData();
        } catch (Exception e) {
            showToast("Cannot get pdf file");
        }

    }

    private void showData() {
        if (assessment != null) {
            showWaitingDialog(ViewPDFscreenActivity.this, "Loading",
                    "Please wait...");
            tvTitle.setText(assessment.getName());

            // view pdf
            webview.getSettings().setJavaScriptEnabled(true);
            webview.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                public void onPageFinished(WebView view, String url) {
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            dismissWaitingDialog();
                        }
                    }, 5000);

                }
            });
            webview.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="
                    + assessment.getPdf_link());

            // show buttons
            btnDownload.setVisibility(View.VISIBLE);
            btnShare.setVisibility(View.VISIBLE);
        }
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tvTitle, ViewPDFscreenActivity.this,
                R.dimen.textLarge);
        MLQPlusFunctions.SetViewTextSize(btnDownload,
                ViewPDFscreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btnShare, ViewPDFscreenActivity.this,
                R.dimen.textMedium);
    }

    @Click
    void btnShare() {
        // Get Share message
        String shareMSG = null;
        try {
            if (MLQPlusConstant.CONFIG_MENU.getMisc().getEmailBody()
                    .contains("<toolName>")
                    && MLQPlusConstant.CONFIG_MENU.getMisc().getEmailBody()
                    .contains("<documentLink>"))
                shareMSG = MLQPlusConstant.CONFIG_MENU.getMisc().getEmailBody()
                        .replace("<toolName>", assessment.getName())
                        .replace("<documentLink>", assessment.getPdf_link());
            else
                shareMSG = "This resource was shared from the MLQplus Trusted Leader App. To view and download the "
                        + assessment.getName()
                        + ", please <a href=\""
                        + assessment.getPdf_link() + "\">click here</a>";
        } catch (Exception e) {
            shareMSG = "This resource was shared from the MLQplus Trusted Leader App. To view and download the "
                    + assessment.getName()
                    + ", please <a href=\""
                    + assessment.getPdf_link() + "\">click here</a>";
        }

        //File emailFooter=getEmailFooter();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, "");
        intent.putExtra(Intent.EXTRA_SUBJECT,
                "MLQplus Trusted Leader Resource - " + assessment.getName());
        intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(shareMSG + "<h1/><img src=\"http://s12.postimg.org/uny91xlkt/bg_email_footer.png\"/>"));
        */
/*if (emailFooter!=null)
			intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(emailFooter));*//*


        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    @Click
    void btnDownload() {
        downloadPdfContent(assessment);
    }

    public void downloadPdfContent(Assessment mAssessment) {

        DownloadFileTask downloadFileTask = new DownloadFileTask(
                ViewPDFscreenActivity.this, assessment);
        downloadFileTask.execute(null, null, null);
    }

    @Click
    void lnBack() {
        onBackPressed();
    }

	*/
/*private File getEmailFooter() {
		FileOutputStream outStream;
		File file;
		Bitmap bm = BitmapFactory.decodeResource(getResources(),
				R.drawable.bg_email_footer);
		String extStorageDirectory = Environment.getExternalStorageDirectory()
				.toString();

		file = new File(extStorageDirectory, "bg_email_footer.PNG");
		try {
			outStream = new FileOutputStream(file);
			bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
			outStream.flush();
			outStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;

	}*//*

}
*/
