package com.example.datvu.mlqplus.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ErrorRetrofit {
	@SerializedName("errors")
	private List<Error> errors;

	public Error getError() {
		if (errors!=null && errors.size()>0)
			return errors.get(0);
		return null;
	}
	
}
