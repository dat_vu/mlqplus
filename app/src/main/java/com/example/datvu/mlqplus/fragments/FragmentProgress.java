/*
package com.example.datvu.mlqplus.fragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

@EFragment(R.layout.fragment_progress)
public class FragmentProgress extends FragmentBase {

	public static FragmentProgress newInstance() {
		return new FragmentProgress_();
	}   
	
	@ViewById 
	LinearLayout ln_root, ln_child;
 
	@ViewById    
	TextView tv_title; 

	@ViewById  
	ViewPager view_pager;

	@ViewById
	com.mlqplus.trustedleader.view.MyTabPostionView tab_position;

	private FragmentManager fragmentManager;
	private Activity mActivity;
	private static int NUM_OF_PAGE = 2;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Set text size for all view
		setTextSize();

		// Show Data 
		showData();
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
	}

	@SuppressLint("NewApi")
	private void showData() {
		//view_pager.setAdapter(null);
		
		if (fragmentManager==null)
		try {
			if(android.os.Build.VERSION.SDK_INT < 19) {
				
				fragmentManager = getFragmentManager();
				FragmentStatePagerAdapter progressFragmentPagerAdapter = new FragmentStatePagerAdapter(fragmentManager) {
					
					@Override
					public int getCount() {
						return NUM_OF_PAGE;
					}
					
					@Override
					public Fragment getItem(int position) {
						switch (position) { 
						case 0:
							return FragmentLearnProgress.newInstance();
						case 1:
							return FragmentQuizProgress.newInstance();
						default:
							return null;
						}
					}
				};
				
				view_pager.setAdapter(progressFragmentPagerAdapter);
	        } else {
	        	fragmentManager = getChildFragmentManager();
	        	FragmentPagerAdapter progressFragmentPagerAdapter = progressFragmentPagerAdapter = new FragmentPagerAdapter(
						fragmentManager) {

					// Returns total number of pages
					@Override
					public int getCount() {
						return NUM_OF_PAGE;
					}

					// Returns the fragment to display for that page
					@Override
					public Fragment getItem(int position) {
						switch (position) { 
						case 0:
							return FragmentLearnProgress.newInstance();
						case 1:
							return FragmentQuizProgress.newInstance();
						default:
							return null;
						}
					}

					// Returns the page title for the top indicator
					@Override
					public CharSequence getPageTitle(int position) {
						return "Page " + position;
					}
				};
				
				view_pager.setAdapter(progressFragmentPagerAdapter);
	        }
			
		} catch (Exception e) {
			showToast(e.getMessage());
		}
		if (fragmentManager == null)
			return;
		

		tab_position.setSeletedTab(NUM_OF_PAGE, 0);
		tab_position.setTabAlpha(1f);
		view_pager.setOnPageChangeListener(new OnPageChangeListener() {
			public void onPageScrollStateChanged(int state) {
			}

			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
			}

			public void onPageSelected(int position) {
				tab_position.setSeletedTab(NUM_OF_PAGE, position);
			}
		});
		
		// Move to Quiz Progress when back from Quiz screen
		if (!FragmentQuizzes.isStartFromFunScreen)
			view_pager.setCurrentItem(1);

	}

	@Click
	void ln_back() {
		mActivity.onBackPressed();
	}

}
*/
