package com.example.datvu.mlqplus.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datvu.mlqplus.R;
import com.example.datvu.mlqplus.models.Module;
import com.example.datvu.mlqplus.models.PurchaseModuleEvent;
import com.example.datvu.mlqplus.utils.MLQPlusFunctions;
import com.example.datvu.mlqplus.views.MyCircleModuleView;
import com.example.datvu.mlqplus.views.WaitingView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

@EFragment(R.layout.fragment_learn)
public class FragmentLearn extends FragmentBase {
	public static FragmentLearn newInstance() { 
		return new FragmentLearn_();
	}  
     
	@ViewById
	LinearLayout ln_root, ln_child, ln_learn_more;
      
	@ViewById
	TextView tv_title, tv_trusted_leader_program, tv_if_you, tv_learn_more;
   
	@ViewById
	ImageView imgDot1;
	
	@ViewById
	WaitingView vWaiting;

	@ViewById
	MyCircleModuleView circle_module1, circle_module2, circle_module3,
			circle_module4, circle_module5;

	
	private static ArrayList<Module> modules;
	private boolean isLoading = false;
	private Activity mActivity;
	private EventBus eventBus;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Register event Bus
		try {
			eventBus = EventBus.getDefault();
			eventBus.register(this);
		} catch (Exception e) {
		}

		// Set text size for all view
		setTextSize();

		// Get Data
		//getData();
	}

	public void setData(ArrayList<Module> modules) {
		if (modules != null)
			this.modules = modules;
	}

	public void onEvent(PurchaseModuleEvent event) {
		if (modules != null) {
			for (Module module : modules)
				if (module.getId().equals(event.getModuleID()))
				{
					module.setUnlocked(true);
					module.setTotal_activity(1);
				}
			//showData();
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		//getData();
	};

	/*private void getData() {
		if (mActivity==null)
			mActivity=getActivity();
		
		if (!isLoading && mActivity!=null) {
			// Only show waiting dialog in the first time
			if (modules != null)
				// Show Data
				showData();

			// Download all module
			if (!((BaseActivity) mActivity).isInternetAvailable())
				showToast("Please check your internet connection and try again");
			else {
				if (modules == null)
					vWaiting.showWaitingView();
				else
					vWaiting.hideWaitingView();
				
				isLoading=true;
				MLQPlusService.getBSSServices().getAllModules(
						MLQPlusFunctions.getAccessToken(),
						new Callback<ApiResponse<List<Module>>>() {

							@Override
							public void success(ApiResponse<List<Module>> mList_modules,
									Response arg1) {

								modules = new ArrayList<Module>();
								if (mList_modules != null && mList_modules.getData()!=null) {
									modules.addAll(mList_modules.getData());
									MLQPlusService
											.getBSSServices()
											.getMyModules(
													MLQPlusFunctions
															.getAccessToken(),
													new Callback<ApiResponse<List<Module>>>() {

														@Override
														public void success(
																ApiResponse<List<Module>> mListUserModules,
																Response arg1) {
															isLoading=false;
															vWaiting.hideWaitingView();
															if (mListUserModules != null && mListUserModules.getData()!=null) {
																// Update info
																// from user
																// module to all
																// module
																updateModuleInfo(mListUserModules.getData());

																// Show Data
																showData();
															}
														}

														@Override
														public void failure(
																RetrofitError error) {
															isLoading=false;
															vWaiting.hideWaitingView();
															if (error.getCause() instanceof SocketTimeoutException)
																showToast(MLQPlusConstant.TIMEOUT);
															else
																((BaseActivity) mActivity)
																		.showToast(error
																				.getMessage());
														}
													});
								}

							}

							@Override
							public void failure(RetrofitError error) {
								isLoading=false;
								vWaiting.hideWaitingView();
								if (error.getCause() instanceof SocketTimeoutException)
									showToast(MLQPlusConstant.TIMEOUT);
								else
									((BaseActivity) mActivity).showToast(error
											.getMessage());
							}
						});
			}
		}

	}*/

	private void updateModuleInfo(List<Module> user_modules) {
		if (modules != null && user_modules != null) // find module
			for (Module user_module : user_modules)
				for (Module module : modules)
					if (module.getId().equals(user_module.getId())) {
						// update info
						module.setTotal_activity(user_module
								.getTotal_activity());
						module.setSubmitted_activity(user_module
								.getSubmitted_activity());
						module.setUnlocked(user_module.isUnlocked());
					}
	}
 
	/*private void showData() {
		if (modules != null && modules.size() >= 5) {
			circle_module1.setData(modules.get(0), true,
					onModuleClickListenner);
			circle_module2.setData(modules.get(1), false,
					onModuleClickListenner);
			circle_module3.setData(modules.get(2), false,
					onModuleClickListenner);
			circle_module4.setData(modules.get(3), false,
					onModuleClickListenner);
			circle_module5.setData(modules.get(4), false,
					onModuleClickListenner);

			ln_learn_more.setVisibility(View.VISIBLE);
		}
	}*/

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_title, mActivity, R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(tv_trusted_leader_program, mActivity,
				R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(tv_if_you, mActivity, R.dimen.textMedium);
		MLQPlusFunctions.SetViewTextSize(tv_learn_more, mActivity, R.dimen.textMedium);
	}
 
	@Click
	void ln_back() {
		mActivity.finish();
	}

	/*@Click
	void ln_learn_more() {
		// Init fragment learn more
		FragmentLearnMore_ fragment = new FragmentLearnMore_();
		fragment.setData(modules);
		// Replace fragment
		if (mActivity instanceof MainActivity_)
			((MainActivity_) mActivity).replaceFragment(fragment, true);

	}

	CircleModuleView.OnClickListenner onModuleClickListenner = new CircleModuleView.OnClickListenner() {

		@Override 
		public void onClick(Module module) {
			if (module.getPublished().equals("0"))
			{
				((BaseActivity) mActivity).showToast("This module hasn't been published");
			}
			else
				if (module.isUnlocked()) {
					Bundle bundle = new Bundle();
					bundle.putString(DownloadModuleActivity.MODULE_ID,
							module.getId());
					((BaseActivity) mActivity).startNewActivityNoAnimation(mActivity,
							DownloadModuleActivity_.class, false, bundle);
				} else {
					ArrayList<Module> first5Modules = new ArrayList<Module>();
					if (modules.size() >= 5)
						for (int i = 0; i < 5; i++)
							first5Modules.add(modules.get(i));
					Bundle bundle = new Bundle();
					bundle.putSerializable(ModuleStorescreenActivity.MODULES,
							first5Modules);
					((BaseActivity) mActivity).startNewZoomActivity(mActivity,
							ModuleStorescreenActivity_.class, false, bundle);
				}
		}
	};*/

}
