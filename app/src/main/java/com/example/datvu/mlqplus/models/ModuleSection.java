package com.example.datvu.mlqplus.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ModuleSection extends ObjectBase implements Serializable{
	
	@SerializedName("section_name")
	private String sectionName;
	
	@SerializedName("section_title")
	private String sectionTitle;
	
	@SerializedName("alignment")
	private String alignment;
	
	@SerializedName("font_size")
	private String font_size;
	
	@SerializedName("color")
	private String color;
	
	@SerializedName("top_background")
	private String topBackground;
	
	@SerializedName("activities")
	private ArrayList<ModuleActivity> activities;

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public String getSectionTitle() {
		return sectionTitle;
	}

	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	public String getTopBackground() {
		return topBackground;
	}

	public void setTopBackground(String topBackground) {
		this.topBackground = topBackground;
	}

	public ArrayList<ModuleActivity> getActivities() {
		return activities;
	}

	public void setActivities(ArrayList<ModuleActivity> activities) {
		this.activities = activities;
	}
}
