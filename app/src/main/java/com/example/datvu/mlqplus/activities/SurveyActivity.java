/*
package com.example.datvu.mlqplus.activities;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.generator.SurveyGenerator;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.ErrorRetrofit;
import com.mlqplus.trustedleader.model.ModuleActivity;
import com.mlqplus.trustedleader.model.ModuleActivityError;
import com.mlqplus.trustedleader.model.ModuleFreeText;
import com.mlqplus.trustedleader.model.Survey;
import com.mlqplus.trustedleader.model.SurveyActivityAnswer;
import com.mlqplus.trustedleader.model.SurveyActivityEvent;
import com.mlqplus.trustedleader.model.SurveySection;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedString;

@EActivity(R.layout.activity_survey)
public class SurveyActivity extends BaseActivity {
    public static String SURVEY_ID = "surveyId";
    private Survey mSurvey;
    private EventBus eventBus;
    private boolean isInitial = true;

    @ViewById
    ScrollView scv;

    @ViewById
    TextView tv_title;

    @ViewById
    LinearLayout lnSurveyContent, lnButtons, lnSurveyParent, ln_child,
            lnSurveyParentContent, ll_root;

    @ViewById
    Button btn_cancel, btn_save;

    private int mLastHeightDifferece = 0;

    @AfterViews
    void init() {
        // init EventBus
        if (isInitial) {
            eventBus = EventBus.getDefault();
            eventBus.register(this);
            isInitial = false;
        }

        // Set text size
        setTextSize();

        getData();
    }

    private void getData() {
        try {
            String surveyID = getIntent()
                    .getBundleExtra(MLQPlusConstant.BUNDLE)
                    .getString(SURVEY_ID);
            if (surveyID != null) {
                // Load survey detail
                if (!MLQPlusFunctions.isInternetAvailable()) {
                    showToast(MLQPlusConstant.CONNECTION_LOST);
                    return;
                } else {
                    // Reset activities_error_list
                    MLQPlusConstant.ACTIVITIES_ERRORS_LIST = new ArrayList<ModuleActivityError>();

                    showWaitingDialog(SurveyActivity.this, "Loading",
                            "Please wait...");
                    MLQPlusService.getBSSServices().getSurveyDetail(
                            MLQPlusFunctions.getAccessToken(), surveyID,
                            new Callback<ApiResponse<Survey>>() {

                                @Override
                                public void success(ApiResponse<Survey> survey,
                                                    Response arg1) {
                                    // Report Activities errors
                                    if (MLQPlusConstant.ACTIVITIES_ERRORS_LIST != null
                                            && MLQPlusConstant.ACTIVITIES_ERRORS_LIST
                                            .size() > 0)
                                        reportActivityError(0);

                                    if (survey != null
                                            && survey.getData() != null) {
                                        mSurvey = survey.getData();
                                        showData();
                                    }

                                    dismissWaitingDialog();
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    if (error.getCause() instanceof SocketTimeoutException)
                                        showToast(MLQPlusConstant.TIMEOUT);
                                    else if (error != null
                                            && error.getMessage() != null)
                                        showToast(error.getMessage());
                                    dismissWaitingDialog();
                                }
                            });
                }
            }

        } catch (Exception e) {
        }
    }

    private void reportActivityError(final int pos) {
        if (MLQPlusConstant.ACTIVITIES_ERRORS_LIST != null
                && MLQPlusConstant.ACTIVITIES_ERRORS_LIST.size() > pos) {
            final String errorJson = new Gson()
                    .toJson(MLQPlusConstant.ACTIVITIES_ERRORS_LIST.get(pos));

            MLQPlusService.getBSSServices().reportActivityError(
                    MLQPlusFunctions.getAccessToken(),
                    new TypedString(errorJson),
                    new Callback<ApiResponse<ModuleActivityError>>() {

                        @Override
                        public void success(
                                ApiResponse<ModuleActivityError> arg0,
                                Response arg1) {
                            // TODO Auto-generated method stub
                            reportActivityError(pos + 1);
                        }

                        @Override
                        public void failure(RetrofitError arg0) {
                            // TODO Auto-generated method stub
                            reportActivityError(pos + 1);
                        }
                    });
        }
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(btn_cancel, SurveyActivity.this,
                R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btn_save, SurveyActivity.this,
                R.dimen.textMedium);

    }

    private void showData() {
        // Check empty survey
        if (isEmptySurvey(mSurvey)) {
            lnButtons.setVisibility(View.GONE);
            showToast("There is no question in this survey");
            return;
        } else {
            if (mSurvey.isSubmitted()) {
                lnButtons.setVisibility(View.GONE);
                LayoutParams params = MLQPlusFunctions.getLayoutParams(
                        LayoutParams.MATCH_PARENT, 0);
                params.weight = 928;

                lnSurveyParentContent.setLayoutParams(params);
                showToast("You have already completed this survey. Thank you.");
            } else {
                lnButtons.setVisibility(View.VISIBLE);
            }
        }

        if (mSurvey != null) {
            if (mSurvey.getSurvey_name() != null)
                tv_title.setText(mSurvey.getSurvey_name());

            View vContent = new SurveyGenerator(mSurvey)
                    .generateView(SurveyActivity.this);
            lnSurveyContent.addView(vContent);

            // Scroll up
            MLQPlusFunctions.scrollToView(scv, vContent);
        }
    }

    @Click
    void ln_back() {
        onBackPressed();
    }

    @Click
    void btn_cancel() {
        onBackPressed();
    }

    @Click
    void btn_save() {
        if (mSurvey.isSubmitted())
            onBackPressed();
        else {
            // Check if user answers all questions
            for (SurveySection section : mSurvey.getSections())
                for (ModuleActivity activity : section.getActivities()) {
                    updateActivityAnswer(activity);

                    if (activity != null
                            && (activity.getAnswer() == null
                            || activity.getAnswer().equals("") || (activity
                            .getActivityType().equals("16") && activity
                            .getAnswer().equals("0")))) {
                        showToast("Please answer all questions");
                        return;
                    }
                }

            if (!isInternetAvailable())
                showToast(MLQPlusConstant.CONNECTION_LOST);
            else {
                // Submit answers
                List<SurveyActivityAnswer> surveyActivityAnswers = new ArrayList<SurveyActivityAnswer>();
                for (int i = 0; i < mSurvey.getSections().size(); i++)
                    for (int j = 0; j < mSurvey.getSections().get(i)
                            .getActivities().size(); j++) {
                        ModuleActivity activity = mSurvey.getSections().get(i)
                                .getActivities().get(j);
                        if (activity != null)
                            surveyActivityAnswers.add(new SurveyActivityAnswer(
                                    activity.getId(), activity.getAnswer(),
                                    mSurvey.getId()));
                    }

                showWaitingDialog(SurveyActivity.this, "Submitting",
                        "Please wait...");
                MLQPlusService.getBSSServices().submitSurvey(
                        MLQPlusFunctions.getAccessToken(),
                        surveyActivityAnswers,
                        new Callback<ApiResponse<Object>>() {

                            @Override
                            public void success(ApiResponse<Object> arg0,
                                                Response arg1) {
                                // TODO Auto-generated method stub
                                dismissWaitingDialog();
                                showToast("Your responses have been submitted. Thank you!");
                                onBackPressed();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                // TODO Auto-generated method stub
                                dismissWaitingDialog();
                                if (error.getCause() instanceof SocketTimeoutException)
                                    showToast(MLQPlusConstant.TIMEOUT);
                                else
                                    try {
                                        if (error.getResponse() != null) {
                                            ErrorRetrofit body = (ErrorRetrofit) error
                                                    .getBodyAs(ErrorRetrofit.class);
                                            showToast(body.getError()
                                                    .getErrorMessage());
                                        }
                                    } catch (Exception e) {
                                    }
                            }
                        });
            }
        }

    }

    private void updateActivityAnswer(ModuleActivity activity) {
        if (activity != null) {
            // if freetext multiquestions -> update answers
            if (activity.getActivityType().equals("8"))
                activity.setAnswer(getFreetextMultiQuestionsAnswers(activity));
        }
    }

    private String getFreetextMultiQuestionsAnswers(ModuleActivity activity) {

        ArrayList<ModuleFreeText> surveyFreeTexts = activity.getFreeText();
        if (surveyFreeTexts != null && surveyFreeTexts.size() > 0) {
            List<String> answers = new ArrayList<String>();
            for (ModuleFreeText freetext : surveyFreeTexts)
                if (freetext.getAnswer() != null
                        && freetext.getAnswer().replace(" ", "").length() > 0) {
                    answers.add(freetext.getAnswer());
                } else
                    return null;
            return new Gson().toJson(answers);
        }
        return null;

    }

    public void onEvent(SurveyActivityEvent event) {
        for (SurveySection section : mSurvey.getSections())
            for (ModuleActivity activity : section.getActivities())
                if (activity.getId().equals(event.getActivityID())) {
                    activity.setAnswer(event.getAnswer());
                    break;
                }
    }

    private boolean isEmptySurvey(Survey survey) {
        if (survey != null)
            for (SurveySection section : survey.getSections())
                if (section.getActivities().size() > 0)
                    return false;
        return true;
    }

    @Click
    void lnSurveyContent() {
        MLQPlusFunctions.hideKeyboard(SurveyActivity.this);
    }

    @Click
    void ln_child() {
        MLQPlusFunctions.hideKeyboard(SurveyActivity.this);
    }

    @Click
    void lnSurveyParent() {
        MLQPlusFunctions.hideKeyboard(SurveyActivity.this);
    }

    @Click
    void ln_title() {
        MLQPlusFunctions.hideKeyboard(SurveyActivity.this);
    }

    @Click
    void scv() {
        MLQPlusFunctions.hideKeyboard(SurveyActivity.this);
    }

    @Click
    void lnSurveyParentContent() {
        MLQPlusFunctions.hideKeyboard(SurveyActivity.this);
    }

    @Click
    void fl_survey_content() {
        MLQPlusFunctions.hideKeyboard(SurveyActivity.this);
    }
}
*/
