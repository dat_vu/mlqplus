/*
package com.example.datvu.mlqplus.fragments;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.activity.BaseActivity;
import com.mlqplus.trustedleader.activity.MainActivity;
import com.mlqplus.trustedleader.adapter.TrustedLeaderProgramAdapter;
import com.mlqplus.trustedleader.model.ApiResponse;
import com.mlqplus.trustedleader.model.Module;
import com.mlqplus.trustedleader.service.MLQPlusService;
import com.mlqplus.trustedleader.utils.MLQPlusConstant;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;
import com.mlqplus.trustedleader.view.WaitingView;

@EFragment(R.layout.fragment_learn_progress)
public class FragmentLearnProgress extends FragmentBase {
	public static FragmentLearnProgress newInstance() {
		return new FragmentLearnProgress_();
	}

	@ViewById
	LinearLayout ln_child;

	@ViewById
	TextView tv_overall_progress, tv_trusted_leader_program, tv_percent;

	@ViewById
	FrameLayout fl_progress;

	@ViewById
	WaitingView vWaiting;

	@ViewById
	at.grabner.circleprogress.CircleProgressView ccv_overall_progress;

	@ViewById
	ListView lv_trusted_leader_program;

	private boolean isLoading = false;
	private Activity mActivity;

	@SuppressLint("NewApi")
	@AfterViews
	void init() {
		// getActivity
		mActivity = getActivity();

		// Set text size for all view
		setTextSize();

		// Init progress circle
		MLQPlusFunctions.showCircleProgress(
				mActivity.getResources().getColor(R.color.blue_circle_test),
				mActivity, ccv_overall_progress, 0, getResources().getInteger(R.integer.card_circle_progress_size));

		// Get data
		getData();
	}

	private void getData() {
		if (!isLoading) {
			// Only show waiting dialog in the first time
			if (MLQPlusConstant.sFullModule != null)
				// Show Data
				showData();

			// Download all module
			if (!((BaseActivity) mActivity).isInternetAvailable())
				showToast("Please check your internet connection and try again");
			else {
				if (MLQPlusConstant.sFullModule == null)
					vWaiting.showWaitingView();
				else
					vWaiting.hideWaitingView();

				isLoading = true;
				MLQPlusService.getBSSServices().getAllModules(
						MLQPlusFunctions.getAccessToken(),
						new Callback<ApiResponse<List<Module>>>() {

							@Override
							public void success(
									ApiResponse<List<Module>> mList_modules,
									Response arg1) {
								// TODO Auto-generated method stub

								MLQPlusConstant.sFullModule = new ArrayList<Module>();
								if (mList_modules != null
										&& mList_modules.getData() != null) {
									MLQPlusConstant.sFullModule.addAll(mList_modules.getData());
									MLQPlusService
											.getBSSServices()
											.getMyModules(
													MLQPlusFunctions
															.getAccessToken(),
													new Callback<ApiResponse<List<Module>>>() {

														@Override
														public void success(
																ApiResponse<List<Module>> mListUserModules,
																Response arg1) {
															// TODO
															// Auto-generated
															// method stub
															isLoading = false;
															vWaiting.hideWaitingView();
															if (mListUserModules != null
																	&& mListUserModules
																			.getData() != null) {
																// Update info
																// from user
																// module to all
																// module
																updateModuleInfo(mListUserModules
																		.getData());

																// Show Data
																showData();

															}
														}

														@Override
														public void failure(
																RetrofitError error) {
															vWaiting.hideWaitingView();
															if (error
																	.getCause() instanceof SocketTimeoutException)
																showToast(MLQPlusConstant.TIMEOUT);
															else
																((BaseActivity) mActivity)
																		.showToast(error
																				.getMessage());
															isLoading = false;
														}
													});
								}

							}

							@Override
							public void failure(RetrofitError error) {
								isLoading = false;
								vWaiting.hideWaitingView();
								if (error.getCause() instanceof SocketTimeoutException)
									showToast(MLQPlusConstant.TIMEOUT);
								else
									((BaseActivity) mActivity).showToast(error
											.getMessage());
							}
						});
			}
		}

	}

	private void updateModuleInfo(List<Module> user_modules) {
		if (MLQPlusConstant.sFullModule != null && user_modules != null) // find module
			for (Module user_module : user_modules)
				for (Module module : MLQPlusConstant.sFullModule)
					if (module.getId().equals(user_module.getId())) {
						// update info
						module.setTotal_activity(user_module
								.getTotal_activity());
						module.setSubmitted_activity(user_module
								.getSubmitted_activity());
						module.setUnlocked(true);
					}
	}

	private void showData() {
		if (MLQPlusConstant.sFullModule != null) {
			// Fix view in small screen
			*/
/*
			 * if (MLQPlusConstant.SCREEN_HEIGHT < 1000) { MLQPlusFunctions
			 * .setLayoutMargin(fl_progress, (int) getResources()
			 * .getDimension(R.dimen.marginBasex3), 0, 0, 0); }
			 *//*

			// Show module list
			// Get only 5 core modules
			List<Module> coreModules = new ArrayList<Module>();
			for (int i = 0; i < 5 && i < MLQPlusConstant.sFullModule.size(); i++)
				coreModules.add(MLQPlusConstant.sFullModule.get(i));

			TrustedLeaderProgramAdapter adapter = new TrustedLeaderProgramAdapter(
					mActivity, R.layout.item_trusted_leader_program,
					coreModules);
			lv_trusted_leader_program.setAdapter(adapter);

			float totalProgress = 0;
			for (Module module : coreModules) {
				totalProgress += module.getProgress();
			}

			ccv_overall_progress.setValue((int) Math.round(totalProgress
					/ coreModules.size()));

			tv_percent.setText((int) Math.round(totalProgress
					/ coreModules.size())
					+ "%");
		}
	}

	private void setTextSize() {
		MLQPlusFunctions.SetViewTextSize(tv_overall_progress, mActivity,
				R.dimen.textLarge);
		MLQPlusFunctions.SetViewTextSize(tv_trusted_leader_program, mActivity,
				R.dimen.textMedium);
	}

	@ItemClick
	void lv_trusted_leader_program(int pos) {
		((MainActivity) mActivity).navigateToLearn();
	}

}
*/
