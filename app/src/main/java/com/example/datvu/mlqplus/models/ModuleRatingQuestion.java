package com.example.datvu.mlqplus.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ModuleRatingQuestion extends ActivityBase{
	@SerializedName("title")
	private String title;
	
	@SerializedName("scaleLow")
	private String scaleLow;
	
	@SerializedName("scaleHigh")
	private String scaleHigh;
	
	@SerializedName("questions")
	private List<ActivityQuestion> questions;
	
	@SerializedName("question")
	private String question;
	
	@SerializedName("answers")
	private List<Integer> answers;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ActivityQuestion> getQuestions() {
		return questions;
	}

	public void setQuestions(List<ActivityQuestion> questions) {
		this.questions = questions;
	}

	public List<Integer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Integer> answers) {
		this.answers = answers;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getScaleLow() {
		return scaleLow;
	}

	public void setScaleLow(String scaleLow) {
		this.scaleLow = scaleLow;
	}

	public String getScaleHigh() {
		return scaleHigh;
	}

	public void setScaleHigh(String scaleHigh) {
		this.scaleHigh = scaleHigh;
	}
}
