package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by solit_000 on 4/14/2016.
 */
public class RestResponse<T> {
    @SerializedName("data")
    private T data;

    private List<RestError> errors;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<RestError> getErrors() {
        return errors;
    }

    public void setErrors(List<RestError> errors) {
        this.errors = errors;
    }

    public RestResponse(T data, List<RestError> errors) {
        this.data = data;
        this.errors = errors;
    }
}
