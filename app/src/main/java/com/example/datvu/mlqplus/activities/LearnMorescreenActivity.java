/*
package com.example.datvu.mlqplus.activities;

import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.mlqplus.trustedleader.R;
import com.mlqplus.trustedleader.utils.MLQPlusFunctions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_learnmorescreen)
public class LearnMorescreenActivity extends BaseDialogActivity {

    @ViewById
    LinearLayout ln_root, ln_child;

    @ViewById
    TextView tv_learn_more, tv_trusted_leader_program, tv_complete_the_trusted_leader_program, tv_tap_the_learn, tv_fun, tv_learn, tv_tools;

    @ViewById
    Button btn_get_started;

    @AfterViews
    void init() {
        // Set form size
        setDialogSize(ln_root, 87.5f, 74.03f);

        // Make dialog not dim
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        // Set data
        setData();

        // Set text size for all view
        setTextSize();


        // Prevent closing dialog when user touchs outside
        this.setFinishOnTouchOutside(false);
    }

    private void setTextSize() {
        MLQPlusFunctions.SetViewTextSize(tv_learn_more,
                LearnMorescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_trusted_leader_program,
                LearnMorescreenActivity.this, R.dimen.textMediumLarge);
        MLQPlusFunctions.SetViewTextSize(tv_complete_the_trusted_leader_program,
                LearnMorescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_tap_the_learn,
                LearnMorescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(btn_get_started,
                LearnMorescreenActivity.this, R.dimen.textMedium);
        MLQPlusFunctions.SetViewTextSize(tv_fun,
                LearnMorescreenActivity.this, R.dimen.textSmall);
        MLQPlusFunctions.SetViewTextSize(tv_learn,
                LearnMorescreenActivity.this, R.dimen.textSmall);
        MLQPlusFunctions.SetViewTextSize(tv_tools,
                LearnMorescreenActivity.this, R.dimen.textSmall);
    }

    private void setData() {
        String text1 = "Tap the ";
        String text2 = "Learn";
        String text3 = " button on the home screen\nto get started and access the first module.";
        tv_tap_the_learn.setText(text1 + text2 + text3, BufferType.SPANNABLE);
        Spannable spannable = (Spannable) tv_tap_the_learn.getText();
        int start = text1.length();
        int end = start + text2.length();
        spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue_email)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    @Click
    void btn_get_started() {
        // Save user clicked Get Started button
        getSharedPreferencesService().userClickedGetStarted().put(true);
        startNewActivityNoAnimation(LearnMorescreenActivity.this,
                HomescreenActivity_.class, true);
    }
}
*/
