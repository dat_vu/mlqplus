package com.example.datvu.mlqplus.models;

import com.google.gson.annotations.SerializedName;

public class Link {
	@SerializedName("text")
	private String name;

	@SerializedName("icon")
	private String icon;
	
	@SerializedName("link")
	private String link;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
